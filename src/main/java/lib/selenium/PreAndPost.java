package lib.selenium;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import lib.utils.DataInputProvider;
import main.MainClass;

public class PreAndPost extends WebDriverServiceImpl{

	public String dataSheetName, sheetName;	
	public static List<String> list;
	public static String[]  systemName; 
    public static String emailId ;
    public static int rowCount, overAllCoursesCount;
    public static long randomWbtNumber;
    public static long randomIltNumber;
    public static long randomAssessmentNumber;
    public static long randomAssignmentNumber;
    /*public static String userName;
    public static String userRole;
    public static String userSkill;*/
    public static String courseTitle; 
    //User components 
    public static int assigned;
    public static int assssmentCompleted;
    public static int recommended;
    public static int recommendationCompleted;
    public static String userCurrentRole; 
    public static String userNextRole; 
    public static String aspiringRole;
    
    
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}
    
	@BeforeClass
	public void beforeClass() {
		startTestCase(testCaseName, testDescription);	
		System.out.println("Before class"); 
		testCaseStatus = "PASS";
		MainClass.testCaseNameA[MainClass.nCount] = testCaseName;
		System.out.println("BeforeClass testCaseStatus = "+testCaseStatus);
		System.out.println("MainClass.sCount = "+MainClass.sCount);
	}

	@BeforeMethod
	public void beforeMethod() throws EncryptedDocumentException, InvalidFormatException, IOException {
		//for reports		
		startTestModule(nodes);
		test.assignAuthor(authors);
		test.assignCategory(category);
		startApp("chrome");
		//backupDataSheet();
	}

	public static void backupDataSheet() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook wb1 = null;
		SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		Date date = new Date();
		String date_s = dt.format(date);
		String path = System.getProperty("user.dir");
		String fileName = path + "\\data\\DataSheet.xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);						
		}
		else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);			
		}			                                                                                                                                                                                                                                                                               
		FileOutputStream fileOut = new FileOutputStream(path + "//data//DataSheetBackup//DataSheet_"+date_s+".xlsx");
		wb1.write(fileOut);
		fileOut.close();
	}

	@AfterMethod
	public void afterMethod() {
		closeAllBrowsers();
		endResult();
	}

	@AfterClass
	public void afterClass() {
		MainClass.testCaseNameA[MainClass.nCount] = testCaseName;
		System.out.println("AfterClass testCaseStatus = "+testCaseStatus);
		System.out.println("MainClass.sCount = "+MainClass.sCount);
		MainClass.testCaseStatusA[MainClass.sCount] = testCaseStatus;
		if(testCaseStatus.equals("PASS")){
			MainClass.passCount++;
		}
		else{
			MainClass.failCount++;
		}
		MainClass.nCount++;
		MainClass.sCount++;

	}

	//@AfterTest
	public void afterTest() {

	}

	@AfterSuite
	public void afterSuite() throws Throwable {
		//endResult();
		/*new AutoMail().createNotePad();
		new AutoMail().SendAutoMail();*/
	}

	@DataProvider(name="fetchData"/*, indices = {1}*/) 
	public  Object[][] getData(){		
		return DataInputProvider.getSheet(dataSheetName, sheetName);		
	}

}


