package main;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import lib.selenium.WebDriverServiceImpl;
import lib.utils.HTMLReporter;

public class AutoMail extends HTMLReporter{	

	/*public void Change_Status() throws Throwable {
		MainClass.newNotePad=true;		
		System.out.println("Second scenario");	
	}*/

	public static String fileExtn;
	public void createNotePad() throws IOException {
		SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		Date date = new Date();
		String date_s = dt.format(date);
		FileWriter fw = null;
		try {
			fileExtn = date_s;
			File f1 = new File("output//");
			if (f1.exists()) {
				// System.err.println("folder available");
			} else {
				f1.mkdirs();
				// System.out.println("Folder Created");
			}

			fw = new FileWriter("output//output_"+fileExtn+".txt");
			System.out.println("NotePad Created");
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedWriter bw =new BufferedWriter(fw);              

		bw.newLine();
		bw.write("---------------------------------------------------------------");
		bw.newLine();           
		bw.write("Test Result");
		System.out.println("NotePad Title");
		bw.newLine();
		bw.write("---------------------------------------------------------------");
		bw.newLine();

		for(int i=1; i<=MainClass.sCount; i++)
		{
			bw.newLine();                  
			bw.write(MainClass.testCaseNameA[i-1]);
			bw.newLine();
			bw.write(MainClass.testCaseStatusA[i-1]);
			bw.newLine();
		}

		//Writing Pass n Fail count
		bw.newLine();                  
		bw.write("Total Testcases Passed: "+MainClass.passCount);
		bw.newLine();
		bw.write("Total Testcases Failed: "+MainClass.failCount);
		bw.newLine();
		bw.close();      
	}
	public void SendAutoMail() throws Throwable {

		// Create object of Property file
		Properties props = new Properties();
		// this will set host of server- you can change based on your requirement 
		props.put("mail.smtp.host", "smtp.gmail.com");
		// set the port of socket factory 
		//AWS
		props.put("mail.smtp.socketFactory.port", "25");
		//local
		//props.put("mail.smtp.socketFactory.port", "465");
		// set socket factory
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		// set the authentication to true
		props.put("mail.smtp.auth", "true");
		// set the port of SMTP server
		//AWS
		props.put("mail.smtp.port", "25");
		// local
		//props.put("mail.smtp.port", "465");
		props.put("mail.smtp.starttls.enable", "true");
		//props.put("mailAltConfig", "true");
		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("gayatri429233@gmail.com", "12345vgs9");	
			}
		});
		try {
			// Create object of MimeMessage class
			Message message = new MimeMessage(session);		
			// Set the from address
			message.setFrom(new InternetAddress("gayatri429233@gmail.com"));
			// Set the recipient address
			//message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("gayatri.kolusu@sifycorp.com"));
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("gayatri.kolusu@sifycorp.com,prashanna.rajendran@sifycorp.com,kapildev.misra@sifycorp.com"));
			message.setRecipients(Message.RecipientType.CC,InternetAddress.parse("gopalakrishnan.elumalai@sifycorp.com,arijit.dasgupta@sifycorp.com,ponradhika.murugesan@sifycorp.com"));
			//Add the subject link
			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);  
			System.out.println(strDate);  
			message.setSubject("PAL Smoke Test Report "+strDate);
			//Set content
			BodyPart messageBodyPart3 = new MimeBodyPart();
			messageBodyPart3.setText("Hi Team,"+"\n"+"\n"+"Kindly find the summary of the execution as follows"+"\n"+"Total Testcases Passed: "+MainClass.passCount +"\n"+"Total Testcases Failed: "+MainClass.failCount+"\n"+"\n"+"Regards,"+"\n"+"PAL Automation");
			//	message.setContent("<h1>This is actual message embedded in HTML tags</h1>","text/html");
			//	message.setContent("<h1>This is actual message embedded in HTML tags</h1>","text/html");
			// Create object to add multimedia type content
			BodyPart messageBodyPart1 = new MimeBodyPart();
			BodyPart messageBodyPart2 = new MimeBodyPart();
			// Set the body of email
			//	messageBodyPart1.setText("Hi Team");
			messageBodyPart1.setText("Total Scenarios Passed: "+MainClass.passCount +"\n"+"Total Scenarios Failed: "+MainClass.failCount);
			// Create another object to add another content
			MimeBodyPart messageBodyPart4 = new MimeBodyPart();
			//	messageBodyPart4.setText("Total Scenarios Passed: "+MainClass.passCount +"\n"+"Total Scenarios Failed: "+MainClass.failCount);
			// Mention the file which you want to send
			String filename = "output//output_"+fileExtn+".txt";
			// Create data source and pass the filename
			DataSource source = new FileDataSource(filename);
			// set the handler
			messageBodyPart2.setDataHandler(new DataHandler(source));
			// set the file 
			filename = filename.substring(6);
			messageBodyPart2.setFileName(filename);
			//extent report file to send
			String report = "";
			if(new WebDriverServiceImpl().environment.equals("local")) {
				report = "./src/main/java/reports/sanityReport_"+newNumber+".html";
				System.out.println(newNumber); 
				// Create data source and pass the reports file
				DataSource reportSource = new FileDataSource(report);
				messageBodyPart4.setDataHandler(new DataHandler(reportSource));
				// set the file 
				report = report.substring(23);
				messageBodyPart4.setFileName(report);
			} else {
				report = "classes//reports//sanityReport_"+newNumber+".html"; 
				// Create data source and pass the reports file
				DataSource reportSource = new FileDataSource(report);
				messageBodyPart4.setDataHandler(new DataHandler(reportSource));
				// set the file 
				report = report.substring(16);
				messageBodyPart4.setFileName(report);
			}
			
			// Create object of MimeMultipart class
			Multipart multipart = new MimeMultipart();
			// add body part 1
			multipart.addBodyPart(messageBodyPart2);
			// add body part 2
			multipart.addBodyPart(messageBodyPart3);
			// add body part 3
			multipart.addBodyPart(messageBodyPart4);
			// set the content
			message.setContent(multipart);
			// finally send the email
			Transport.send(message);
			System.out.println("=====Email Sent=====");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		System.out.println("Auto Mail Sent"); 
		//MainClass.newNotePad=false;	
	}
	@Override
	public long takeSnap() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public long cdriverTakeSnap() {
		// TODO Auto-generated method stub
		return 0;
	}


}
