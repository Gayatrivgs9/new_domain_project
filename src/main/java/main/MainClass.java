package main;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

public class MainClass {

	public static int passCount = 0, sCount = 0, nCount = 0;
	public static int failCount = 0;
	public static String[] testCaseNameA = new String[100];
	public static String[] testCaseStatusA = new String[100];
	public static void main(String[] args) {
		TestNG test = new TestNG();
		List<String> suitefiles=new ArrayList<String>();
		suitefiles.add(".//classes//main//PALSanityTestcases.xml");
		//suitefiles.add(".//src//main//java//main//PALSanityTestcases.xml");
		test.setTestSuites(suitefiles);
		test.run();

	}
}
