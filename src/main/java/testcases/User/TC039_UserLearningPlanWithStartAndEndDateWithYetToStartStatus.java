package testcases.User;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC039_UserLearningPlanWithStartAndEndDateWithYetToStartStatus extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC039_UserLearningPlanWithStartAndEndDateWithYetToStartStatus";
		testDescription = "User Learning Plan With Start And End Date With In-Prograss Status";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CourseTransactionBaselineData";
	}
	@Test(dataProvider = "fetchData")
	public void courseBaselineCreation(String data, String uName, String pwd, String schemaNew, String table, String domain,
			 String uploadURL, String schema,String system,String email,String api, String fileName, String userName, String userPwd) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		//Login to admin
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(schemaNew, table)
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testCourseTransactionUploadFiles(uploadURL, domain, schema, system, email, api, fileName); 
		//Login to user
		new LoginPage(driver,test)		
		.navigateToUser()  
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.acceptTheAlertBox()
		.clickLearningPlan()
		.clickClear() 
		.filterWithRecommendedLearning() 
		.filterWithCurrentRole()
		.clickGo()
		.userDragAndDropTheCourseWithStartAndndDateInProgress();   

	}
}







