package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC032_UserManagerCompleteReportsAndExportAsCSV extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC032_UserManagerCompleteReportsAndExportAsCSV";
		testDescription = "User Manager Results Complete Reports And Export As CSV";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserManagerViewReportsWithCSV";
	}

	@Test(dataProvider = "fetchData")	
	public void userManagerCompleteReportsAndExportAsCSV(String urlData, String uName, String pwd,
			String bu1, String bu2,String roleCount, String fileName, 
			String skillCount, String courseCount) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickManagerView() 
		.verifyUserEmail() 
		.verifyReportsCount() 
		.enterManagerBusinessUnits(bu1, bu2)
		.enterDepartment()
		.clickGoButton()
		.removeUnusedRolesFiles()
		.removeUnusedSkillsFiles()
		.removeUnusedCoursesFiles() 
		.getRoleCount(roleCount)
		.selectAllTheRoles()
		.exportAsCsvAllTheRoles() 
		.verifyExportedRolesPercentage(fileName) 
		.getSkillCount(skillCount)
		.selectAllTheSkills()
		.exportAsCsvAllTheSkills()
		.verifyExportedSkillsPercentage() 
		.getCourseCount(courseCount)
		.clickCourseSelectAll()
		.exportAsCsvAllTheCourses()
		.verifyExportedCoursesPercentage();  

	}
}



























