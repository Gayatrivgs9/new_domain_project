package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC006_AssessmentForNextRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC006_AssessmentForNextRole";
		testCaseStatus = "PASS";
		testDescription = "Next role assessment verification for user";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserRecWithNextRole";
	}

	@Test(dataProvider = "fetchData")	
	public void assessmentForNextRole(String urlData, String uName, String pwd, String helpText,
			String nextRole) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAssessment()
		.verifyFilterTextMessage(helpText)
		.unSelectDefaultCurrentRole() 
		.enterNextRole(nextRole) 
		.clickGo() 
		.verifyTotalAssessmentCoursCount()
		.getPendingAssessmentsCount()
		.getCompletedAssessmentsCount() 
		.assessmentsStatus();

	}
}




















