package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC026_PendingAssessmentsByRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC026_PendingAssessmentsByRole";
		testCaseStatus = "PASS";
		testDescription = "Pending Assessments";
		nodes = "User";
		authors = "Gayatri"; 
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "PendingAssessments";
	}

	@Test(dataProvider = "fetchData")	
	public void pendingAssessments(String data, String uName, String pwd, String pendingCourse, String course2, String course3) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName) 
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible()
		.clickAssessment()
		.verifyTotalAssessmentCoursCount() 
		.clickOnSearchButton() 
		.enterPendingAssessment()
		.clickOnSearchButton();
		/*.enterCurrentRoles()
		.verifyAssessmentCoursCount() 
		.clickOnSearchButton()
		.enterPendingAssessment()
		.getPendingAssessmentsList()
		.clickOnSearchButton()
		.enterNextRoles()
		.verifyAssessmentCoursCount()
		.clickOnSearchButton()
		.enterPendingAssessment() 
		.getPendingAssessmentsList();*/
		
				
		
	}
}



















