package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC023_UserHrReports extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC023_UserHrReports";
		testDescription = "User Hr Reports";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserHRViewReports";
	}

	@Test(dataProvider = "fetchData")	
	public void userHrReports(String urlData, String uName, String pwd, String bu1,String bu2,
			String dep1,String dep2,String roleCount, String role, String type,
			String skillCount, String category, String subCategory, 
			String skill, String courseCount, String courseType, String status ) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickHrView()  
		.verifyReportsCount() 
		.enterHRBusinessUnits(bu1, bu2)
		.enterHRDepartments(dep1, dep2)  
		.clickGoButton() 
		.getRoleCount(roleCount)
		.enterRole(role)
		.enterType(type)
		.clickSelectAndContinue(roleCount) 
		.getSkillCount(skillCount)
		.enterTheCategory(category)
		.enterSubCategory(subCategory)
		.enterSkill(skill)
		.clickSelectAndContinueOfSkill(skillCount)
		.getCourseCount(courseCount)
		.enterCourseType(courseType, status)
		.clickSelectAndContinueOfCourse(courseCount);

	}
}


