package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC019_HRSuccessionViewTrack extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC019_HRSuccessionViewTrack";
		testDescription = "HR Succession View Track Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserHrViewSuccession";
	}

	@Test(dataProvider = "fetchData")	
	public void hrSuccessionViewTrack(String urlData,String uName, String pwd,String filterCourse, String selectUser, String role, 
			String fit, String potential, String skill) throws Exception {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)  
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView() 
		.clickSuccessionView()
		.enterSelectRole(filterCourse)
		.enterSelectUser(selectUser)
		//role fit
		//.enterSkill(role)
		.enterRange(fit)
		.clickMatchButton()
		.verifyNumberOfRecordsMatch()
		.getUserDetails()
		.getUserMatches()
		//role potential 
		//.enterSkill(role)
		.enterRange(potential)
		.clickMatchButton()
		.verifyNumberOfRecordsMatch()
		.getUserDetails()
		.getUserMatches()
		//skill fit
		.enterSkill(skill)
		.enterRange(fit)
		.clickMatchButton()
		.verifyNumberOfRecordsMatch()
		.getUserDetails()
		.getUserMatches()
		//skill potential
		.enterSkill(skill)
		.enterRange(potential) 
		.clickMatchButton()
		.verifyNumberOfRecordsMatch()
		.getUserDetails()
		.getUserMatches(); 

	}
}













