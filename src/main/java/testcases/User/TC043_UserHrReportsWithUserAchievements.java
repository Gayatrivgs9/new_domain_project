package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.AchievementPage;
import pages.User.UserHomePage;

public class TC043_UserHrReportsWithUserAchievements extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC030_UserHrReportsWithUserAchievements";
		testDescription = "User Hr Reports with user achievements";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "HRBasicReports";
	}

	@Test(dataProvider = "fetchData")	
	public void userHrReportsWithAchievements(String urlData, String uName, String pwd, String businessUnit, String department, String role, String type,
			String category, String subCategory, String skill, String courseType, String status ) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickHrView()  
		.verifyReportsCount() 
		.enterBusinessUnit(businessUnit)
		.enterDepartment()  
		.clickGoButton() 
		//.getRoleCount()
		.enterRole(role)
		.enterType(type);
		//.viewUserDetails();
		
		new AchievementPage(driver, test)
		.getTheUserData()
		//.enterAspiringRole()
		.monthWiseCourseCompletionDuration()
		//.aspireTopAchievments()
		//.aspireCourseCertificates()
		.getRoleDetails()
		.getRoleOverallAnalysis()
		.getSkillDetails()
		.getSkillOverallAnalysis()
		.getCourseDetails()
		.getCourseOverallAnalysis();

	}
}











