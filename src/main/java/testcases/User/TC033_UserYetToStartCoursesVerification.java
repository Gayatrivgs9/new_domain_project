package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.LearningPlanPage;

public class TC033_UserYetToStartCoursesVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC033_UserYetToStartCoursesVerification";
		testDescription = "User YetToStart Courses Verification Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "YetToStart";
	}

	@Test(dataProvider = "fetchData")	
	public void userYetToStartCoursesVerification(String data, String uName, String pwd, String schemaNew, String table, 
			String uploadURL, String domain,String schema,String system,String email,String api,String fileName,
			String superAdminName, String superAdminPwd, String runType, String selectDomain, String successMsg, 
			String userName, String userpwd, String courseCategory, String subCategory, String skill) throws Exception {		
		//Login to admin
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(schemaNew, table)
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testCourseTransactionUploadFiles(uploadURL, domain, schema, system, email, api, fileName);
		//Login to super-admin
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(superAdminName)
		.enterPassword(superAdminPwd)
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomain(data)
		.clickTransactionBatchTrigger()
		.clickSubmitButton()
		.verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.verifySuccessMsgOfRunBatch(successMsg); 	
		//Login to user
		new LoginPage(driver,test)		
		.navigateToUser()  
		.enterUserName(userName)
		.enterPassword(userpwd)
		.clickLoginButton();
		new LearningPlanPage(driver, test)
		.acceptTheAlertBox()
		.clickClear() 
		.filterWithRecommendedLearning() 
		.filterWithCurrentRole()
		.clickGo() 
		.verifyNumberOfCoursesYetToStart()
		.verifyCumulativeCoursesDuration()
		.learningProgressGraph1()
		.enterSkillIndex(courseCategory, subCategory, skill)
		.learningProgressGraph2();

	}
}





















