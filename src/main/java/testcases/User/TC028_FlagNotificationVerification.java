package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.NotificationsPage;

public class TC028_FlagNotificationVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC028_FlagNotificationVerification";
		testDescription = "Flag Icon NotificationVerification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "FlagNotifications";
	}

	@Test(dataProvider = "fetchData")	
	public void flagNotificationVerification(String data, String uName, String pwd, String suggestedCount,
			String sugSkill, String sugSkill2, String roleCount, String sugRole) throws InterruptedException {
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName) 
		.enterPassword(pwd)
		.clickLoginButton();
		new NotificationsPage(driver, test)
		.clickFlagIcon()
		.clickSuggestions()
		.getSuggestedSkillsCount(suggestedCount)
		.verifySuggestedSkills(sugSkill, sugSkill2)
		.getSuggestedRolesCount(roleCount)
		.verifySuggestedRoles(sugRole); 
		
		
		
	}

}












