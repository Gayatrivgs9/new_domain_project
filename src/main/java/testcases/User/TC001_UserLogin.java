package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC001_UserLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "User Login and LoginOut";
		testCaseStatus = "PASS";
		testDescription = "Login testCase using Valid Credentials";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void userLoginCheck(String data, String uName, String pwd, String domain) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.verifyHomePageTitle(domain) 
		.clickUserDropdown() 
		.clickUserLogout();	 	
		
	}
}





