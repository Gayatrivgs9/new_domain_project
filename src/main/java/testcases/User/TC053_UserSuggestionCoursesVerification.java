package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.NotificationsPage;
import pages.User.UserHomePage;

public class TC053_UserSuggestionCoursesVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC053_UserSuggestionCoursesVerification";
		testDescription = "User Suggestion Courses Verification";
		testCaseStatus = "PASS";
		nodes = "User Suggestions";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserSuggestions";
	}

	@Test(dataProvider = "fetchData")	
	public void userSuggestionCoursesVerification(String urlData, String uName, String pwd, 
			String sucMsg, String notificationCount) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.verifySuggestionsCount()
		.getTheSuggestedRolesDetails()
		.getTheSuggestedSkillsDetails() 
		.clickFlagIconForSuggestions()
		.getSuggestedSkillsData()
		.enroleSuggestedRoles(sucMsg);

		new UserHomePage(driver, test)
		.clickAssessment()
		.getSuggestionsCourseCount();

		new UserHomePage(driver, test)
		.clickRecommendation()
		.getSuggestedCoursesList();

		new NotificationsPage(driver, test)
		.getNotificationCount(notificationCount)
		.clickBellIcon()
		.clickNotifications()
		.verifySuggestionsNotification();
	}
}















