package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC050_UserAchievementsWithNextRoleVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC050_UserAchievementsWithNextRoleVerification";
		testDescription = "User Achievements With Next Role Verification";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserAchNextRole";
	}

	@Test(dataProvider = "fetchData")	
	public void userAchievementsWithNextRoleVerification(String urlData, String uName, String pwd, String succMsg) throws InterruptedException, IOException {		
		//Login user
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.verifyUserCurrentRole()
		.userTopAchievements()  
		.selectNextRoleAsDefault() 
		.verifySuccessMessage(succMsg); 
		new UserHomePage(driver, test)
		.clickAssessment()
		.verifyUserNextRole();  
		new UserHomePage(driver, test)
		.clickRecommendation()
		.verifyUserNextRole();
		
		
	}
}














