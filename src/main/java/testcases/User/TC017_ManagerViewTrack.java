package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC017_ManagerViewTrack extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC017_ManagerViewTrack";
		testDescription = "Manager View Track Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserManagerView";
	}
    
	@Test(dataProvider = "fetchData")	
	public void managerViewTrack(String urlData, String uName, String pwd/*, String data1, String data2*/) throws Exception {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView()
		.clickClear() 
		.clickRecommendedLearningType()
		.clickSuggestedLearningType()
		.clickCalendar()
		.selectFromAndToDate()
		.clickGo()  
		//.enterSkillIndexManagerView(data1, data2)
		.clickSkillIndexManagerViewGo()
		.viewSkillDistributionGraph();
		
	}
}





