package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC046_DetailsOfMentors extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC046_DetailsOfMentors";
		testDescription = "Details Of Mentors";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "MentorDetails";
	}

	@Test(dataProvider = "fetchData")	
	public void detailsOfMentors(String urlData, String uName, String pwd, String user2, 
			String requestMsg) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickUserImage()
		.clickSettings()
		.selectContentType()
		.selectSkillRadioButtonsAsYes();
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(user2)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation() 
		.getMentorsDetails()
		.clickContactButton();
		//.verifyRequestSentMssage(requestMsg);  
		
		
	}
}





