package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC051_UserAspiringRoleVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC051_UserAspiringRoleVerification";
		testDescription = "Achievements of User";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void userAspiringRoleVerification(String urlData, String uName, String pwd) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.verifyAchievementsHeaderSection()
		.getTheUserData()
		.verificationOfAspiringRole()
		.monthWiseCourseCompletionDuration()
		.verifyOverAllAnalysisText()
		.verifyGraphsDrillDownText();   
		
		
	}
}





