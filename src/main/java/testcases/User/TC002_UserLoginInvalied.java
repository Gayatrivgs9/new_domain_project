package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC002_UserLoginInvalied extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_UserLoginInvalied";
		testCaseStatus = "PASS";
		testDescription = "User Login check for different Invalid combination of credentials";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLoginInvalid";
	}

	@Test(dataProvider = "fetchData")	
	public void userLoginInvalied(String urlData, String uName, String pwd, String ErrorMsg, String Scenario) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserNameWithTab(uName)
		.enterPasswordWithTab(pwd)
		.verifyInvalidLogin(ErrorMsg, Scenario);		
	}
}





