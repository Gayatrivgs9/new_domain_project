package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC035_UserCompletedCoursesVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC035_UserCompletedCoursesVerification";
		testDescription = "User Completed Courses Functionality Verification";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserCompletedCoursesAchievement";
	}

	@Test(dataProvider = "fetchData")	
	public void userCompletedCoursesVerification(String data, String uName, String pwd, String schemaNew, String table, 
			String uploadURL, String domain, String schema, String system, String email, String api, String fileName, 
			String userName, String userpwd) throws Exception {		
		//Login to admin
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(schemaNew, table)
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testCourseTransactionUploadFiles(uploadURL, domain, schema, system, email, api, fileName);
		//Run batch
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		//Login to user 
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userpwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible() 
		.clickAssessment()
		//.enterCurrentRole()
		.clickGo() 
		.verifyAssignedAndCompletedCoursesCount(); 
		new UserHomePage(driver, test)
		.clickRecommendationField() 
		.enterCurrentRole() 
		.clickGo()
		.verifyRecommendedAndCompletedCoursesCount();
		
		
	}
}





















