package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC020_HRSearchViewTrack extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC020_HRSearchViewTrack";
		testDescription = "HR View Track Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserSearchTrack";
	}

	@Test(dataProvider = "fetchData")	
	public void hrSearchViewTrack(String urlData, String uName, String pwd, String role, String skill, String roleName,String skillName,
			String fitRange, String potentialRange) throws Exception {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView()
		.clickHRViewSearch()
		//Role 
		.enterSearchBy(role)
		.enterSearchRole(roleName)
		.enterSearchRange(fitRange)
		.clickSelectSearchButton()
		.verifyNumberOfRecordsMatch()
		.searchUserDetails()
		.enterSearchRange(potentialRange)
		.clickSelectSearchButton()
		.verifyNumberOfRecordsMatch()
		.searchUserDetails()
		//Skill 
		.enterSearchBy(skill)
		.enterSearchRole(skillName)
		.enterSearchRange(fitRange)
		.clickSelectSearchButton()
		.verifyNumberOfRecordsMatch()
		.searchUserDetails()
		.enterSearchRange(potentialRange)
		.clickSelectSearchButton()
		.verifyNumberOfRecordsMatch()
		.searchUserDetails();

	}
}





























