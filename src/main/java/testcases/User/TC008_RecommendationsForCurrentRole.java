package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC008_RecommendationsForCurrentRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC008_RecommendationsForCurrentRole";
		testCaseStatus = "PASS";
		testDescription = "Recommendation courses for user current role";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserRecommWithCurrentRole";
	}

	@Test(dataProvider = "fetchData")	
	public void recommendationsForCurrentRole(String urlData, String uName, String pwd, 
			String helpText, String currentRole) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation()
		.verifyFilterTextMessage(helpText)
		.enterCurrentRole(currentRole)
		.clickGo()
		.verifyRecommendedCoursCount()
		.getYourLearningGoalsList()
		.getCompletedCoursesList()
		.clickRecommendationTopFilter()
		.enterCurrentRole(currentRole)
		.clickGo();  
		
	}
}





