package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC029_UserForgetPassword extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC029_UserForgetPassword";
		testDescription = "User Forget Password Vrification";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserForgotPwd";
	}

	@Test(dataProvider = "fetchData")	
	public void userForgetPassword(String data, String uName, String pwd, String emailId, String newPwd) throws InterruptedException {
		
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName)
		.clickForgetPassword()
		.enterUserName(uName)
		.verifyResetLink();
		/*.reLoadOfficePage(emailId, newPwd)
		.clickOutLook()
		.verifyResetMailSubject()
		.loadForgrtPasswordLink()
		.enterNewPassword(pwd); 
		new LoginPage(driver,test)			
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickUserDropdown()
		.clickUserLogout();*/
			
	}
}





