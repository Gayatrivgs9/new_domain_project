package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC016_UserViewTrack extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC016_UserViewTrack";
		testDescription = "User View Track Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserViewTrack";
	}

	@Test(dataProvider = "fetchData")	
	public void userViewTrack(String urlData, String uName, String pwd, 
			String adsMsg1, String adsMsg2, String cardMsg1, String cardMsg2, 
			String data1, String data2, String data3) throws Exception {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack() 
		.clickRecommendedLearningType() 
		.clickGo() 
		.verifyAdvancedMsg(adsMsg1, adsMsg2)
		//.verifyAtAGlanceSection(cardMsg1, cardMsg2)
		.yourRolesAchievementsDetails() 
		.getTheSuggestedRolesDetails()
		.enterSkillIndex(data1, data2, data3)
		.clickSkillIndexGo()
		.yourSkillsAchievementsDetails()
		.getTheSuggestedSkillsDetails();
		
	}
}





