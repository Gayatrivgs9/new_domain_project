package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC045_RecommendationsVerificationWithCurrentAndNextRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC045_RecommendationsVerificationWithCurrentAndNextRole";
		testCaseStatus = "PASS";
		testDescription = "Recommendation courses for user current role and next role combination";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserRecWithCurretAndNextRole";
	}

	@Test(dataProvider = "fetchData")	
	public void recommendationsVerificationWithCurrentAndNextRole(String urlData, String uName, String pwd, 
			String helpText, String currentRole, String nextRole) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation()
		.verifyFilterTextMessage(helpText)
		.enterCurrentRole(currentRole)
		.enterNextRole(nextRole)
		.clickGo()
		.verifyRecommendedCoursCount()
		.getYourLearningGoalsList()
		.getCompletedCoursesList()
		.clickRecommendationTopFilter()
		.enterCurrentRole(currentRole)
		.clickGo();  
		
	}
}





