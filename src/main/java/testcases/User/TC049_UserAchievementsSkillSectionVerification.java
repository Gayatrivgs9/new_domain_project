package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC049_UserAchievementsSkillSectionVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC049_UserAchievementsSkillSectionVerification";
		testDescription = "User Achievements Skill Section Verification";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void userAchievementsSkillSectionVerification(String urlData, String uName, String pwd, String adminUserName, String adminPwd) throws InterruptedException, IOException {		
		//Login user
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.verifyUserCurrentRole()
		.verifyUserNextRole()
		.verificationOfAspiringRole();
		//Run admin batch 
		new LoginPage(driver,test)		
		.navigateToAdmin()
		.enterUserName(adminUserName)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.verifyUserAspiringRole(); 
		
		
	}
}














