package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC005_AssessmentForCurrentRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_AssessmentForCurrentRole";
		testCaseStatus = "PASS";
		testDescription = "Current role assessment verification for user";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserAssessmentWithCurrentRole";
	}

	@Test(dataProvider = "fetchData")	
	public void assessmentForCurrentRole(String urlData, String uName, String pwd, 
			String expectedText, String currentRole) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAssessment()
		.verifyFilterTextMessage(expectedText)
		.enterCurrentRole(currentRole)
		.clickGo() 
		.verifyTotalAssessmentCoursCount()
		.getPendingAssessmentsCount()
		.getCompletedAssessmentsCount() 
		.assessmentsStatus()
		.clickAssessmentTopFilter()
		.enterCurrentRole(currentRole)
		.clickGo();

	}
}




















