package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.NotificationsPage;

public class TC027_BellNotificationVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC027_BellNotificationVerification";
		testDescription = "Bell Icon Notification Verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "BellNotifications";
	}

	@Test(dataProvider = "fetchData")	
	public void bellNotificationVerification(String data, String uName, String pwd, 
			String notificationCount, String path) throws InterruptedException, IOException {
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName) 
		.enterPassword(pwd)
		.clickLoginButton();
		new NotificationsPage(driver, test)
		.getNotificationCount(notificationCount)
		.clickBellIcon()
		.clickNotifications()
		.verifyNotifications(path)
		.clickWindowMaximize()
		.clickOkButton() 
		.clickRefresh();
		
		
	}

}












