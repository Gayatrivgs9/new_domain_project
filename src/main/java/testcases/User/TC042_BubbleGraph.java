package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC042_BubbleGraph extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC041_DefaultAsNextRole";
		testCaseStatus = "PASS";
		testDescription = "Select default as next role for user";
		nodes = "User next role";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "DefaultNextRole";
	}

	@Test(dataProvider = "fetchData")	
	public void defaultAsNextRole(String urlData, String uName, String pwd, String currentRole, 
			String nextRole) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack() 
		.clickManagerView();
		

	}
}




















