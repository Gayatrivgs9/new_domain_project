package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC018_HRViewTrackResourceStatus extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC018_HRViewTrackResourceStatus";
		testDescription = "HR View Track Resource Status Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserHrView";
	}
    
	@Test(dataProvider = "fetchData")	
	public void hrViewTrack(String urlData, String uName, String pwd, String role1, String role2, String role3, String data1, String data2, String data3) throws Exception {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickClear() 
		.clickHrView() 
		.enterTheRole(role1, role2, role3)
		.clickGo()
		.clickResourceStatus()
		.enterSkillIndex2(data1, data2, data3)
		.clickSkillIndexGoButton()
		.viewAchievmentsGraph();
		
	}
}





























