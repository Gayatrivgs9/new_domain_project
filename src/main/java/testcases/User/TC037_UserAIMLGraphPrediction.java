package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC037_UserAIMLGraphPrediction extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC037_UserAIMLGraphPrediction";
		testDescription = "User AIML Graph Prediction Verification";
		testCaseStatus = "PASS";  
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AIMLGraphGenerate";
	}

	@Test(dataProvider = "fetchData")	
	public void userAIMLGraphPrediction(String data, String uName, String pwd, String domain, String minutes,String runType, 
			String selectDomain, String successMsg, String userName, String userPwd) throws Exception {		
		
		//Login to super-admin
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.waitUpToFoldingCubeInvisible() 
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomain(domain)
		.clickAIMLPredectionBatchAsYes()
		.clickAIMLPredectionBatchSchedule() 
		.selectAIMLRuntimeConfigurationAsMinutes()
		.selectTheMinutes(minutes) 
		.clickOkButton() 
		.clickSubmitButton()
	    .verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.waitUpToFoldingCubeInvisible() 
		.verifySuccessMsgOfRunBatch(successMsg);
		//Login to user
		new LoginPage(driver,test)		
		.navigateToUser03Domain()   
		.enterUserName(userName)
		.enterPassword(userPwd) 
		.waitUpToFoldingCubeInvisible() 
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible()
		.clickTrack()
		.waitUpToFoldingCubeInvisible()
		.clickAIMLUserGraph()
		.captureAIMLGraph();
		
		
	}
}





















