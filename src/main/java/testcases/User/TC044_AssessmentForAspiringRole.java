package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC044_AssessmentForAspiringRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC044_AssessmentForAspiringRole";
		testCaseStatus = "PASS";
		testDescription = "Aspiring role assessment verification for user";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserAspiringRole";
	}

	@Test(dataProvider = "fetchData")	
	public void assessmentForAspiringRole(String urlData, String uName, String pwd, String aspiringRole,
			String sucMsg, String intAss, String createAss, String reviewRorm, String buildAssssment) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.selectAspiringRoleForUser(aspiringRole, sucMsg);
		new UserHomePage(driver, test)
		.clickAssessment()
		.enterAspiringRole() 
		.clickGo() 
		/*.verifyInternalAssessmentUserReview(intAss)
		.verifyCreateAssessment(createAss)
		.clickInternalAssessmentUserReview()
		.verifyInternalAssessmentReviewFromTitle(reviewRorm)*/;
		new UserHomePage(driver, test)
		.clickAssessment()
		.clickCreateInternalAssessment()
		.verifyAssessmentBuilderTitle(buildAssssment); 
		new UserHomePage(driver, test)
		.clickAssessment()
		.enterAspiringRole(aspiringRole); 
		new UserHomePage(driver, test)
		.clickRecommendation()
		.enterAspiringRole(aspiringRole);
		new UserHomePage(driver, test)
		.clickLearningPlan()
		.enterAspiringRole(aspiringRole);
		new UserHomePage(driver, test)
		.clickTrack()
		.enterAspiringRole(aspiringRole);

	}
}




















