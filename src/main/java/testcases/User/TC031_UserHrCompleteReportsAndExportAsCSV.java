package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC031_UserHrCompleteReportsAndExportAsCSV extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC031_UserHrCompleteReportsAndExportAsCSV";
		testDescription = "User Hr Results Complete Reports And Export As CSV";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserHRViewReportsWithCSV";
	}

	@Test(dataProvider = "fetchData")	
	public void userHrCompleteReportsAndExportAsCSV(String urlData, String uName, String pwd, 
			String businessUnit, String department1, String roleCount, String fileName, 
			String skillCount, String skillFilename) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible() 
		.clickReports()
		.clickHrView()  
		.verifyReportsCount() 
		.enterBusinessUnit(businessUnit)
		.enterDepartment()  
		.clickGoButton()
		.removeUnusedRolesFiles()
		.removeUnusedSkillsFiles()
		.removeUnusedCoursesFiles() 
		.getRoleCount(roleCount)
		.selectAllTheRoles()
		.exportAsCsvAllTheRoles() 
		.verifyExportedRolesPercentage(fileName) 
		.getSkillCount(skillCount)
		.selectAllTheSkills()
		.exportAsCsvAllTheSkills()
		.verifyExportedSkillsPercentage() 
		.getCourseCount(skillFilename)
		.clickCourseSelectAll()
		.exportAsCsvAllTheCourses()
		.verifyExportedCoursesPercentage();  

	}
}



























