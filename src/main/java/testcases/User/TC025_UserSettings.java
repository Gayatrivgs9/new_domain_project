package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserSettingsPage;

public class TC025_UserSettings extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC025_UserSettings";
		testDescription = "User Settings Update";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void profileAchievement(String urlData, String uName, String pwd, String course) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickUserDropdown()
		.clickUserSettings(); 
		new UserSettingsPage(driver, test)
		.clickSubmitButton()
		.verifySuccessMsg()
		.clickCourseTypeAsYes()
		.clickMentorsSubmitButton() ;
		//.verifyUpdatedMsg();

	}
}



























