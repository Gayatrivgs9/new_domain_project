package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC013_UserSuggestedLearningPlanWithCurrentRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC013_UserSuggestedLearningPlanWithCurrentRole";
		testCaseStatus = "PASS";
		testDescription = "User Suggested LearningPlan With Current Role Functionality Validation";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "CurrentRoleLearningPlan";
	}

	@Test(dataProvider = "fetchData")	
	public void userRecommendedLearningPlan(String urlData, String uName, String pwd) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickLearningPlan()
		.clickClear() 
		.filterWithSuggstedLearning() 
		.filterWithCurrentRole() 
		.clickGo()
		.verifyNumberOfCoursesEnrolled()
		.verifyCumulativeCoursesDuration()
		.learningProgressGraph1()
		.learningProgressGraph2(); 
		
	}
}




















