package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC009_RecommendationsForNextRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC009_RecommendationsForNextRole";
		testCaseStatus = "PASS";
		testDescription = "Recommendation courses for user next role";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserRecWithNextRole";
	}

	@Test(dataProvider = "fetchData")	
	public void recommendationsForNextRole(String urlData, String uName, String pwd, String helpText,
			String nextRole) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation()
		.unSelectDefaultCurrentRole()
		.verifyFilterTextMessage(helpText)
		.enterNextRole(nextRole) 
		.clickGo()
		.verifyRecommendedCoursCount()
		.getYourLearningGoalsList()
		.getCompletedCoursesList(); 
		
	}
}





