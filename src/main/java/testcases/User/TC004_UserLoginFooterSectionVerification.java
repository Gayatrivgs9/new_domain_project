package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC004_UserLoginFooterSectionVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC004_UserLoginFooterSectionVerification";
		testCaseStatus = "PASS";
		testDescription = "User Login Page Footer Section Verification";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserFooterSection";
	}

	@Test(dataProvider = "fetchData")	
	public void userLoginFooterSectionVerification(String data, String uName, String pwd, 
			String copyRights, String privacy, String disclaimer, String termOfUse) throws InterruptedException {		
		new LoginPage(driver,test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.verifyCopyRightOpt(copyRights, privacy, disclaimer, termOfUse);
		new LoginPage(driver,test)
		.clickPrivacyPolicy() 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new LoginPage(driver,test)
		.clickDisclaimer() 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new LoginPage(driver,test)
		.clickTermOfUse();

	}
}





