package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC048_RecommendationsForAspiringRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC048_RecommendationsForAspiringRole";
		testCaseStatus = "PASS";
		testDescription = "Recommendation courses for user aspiring role";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserRecommendation";
	}

	@Test(dataProvider = "fetchData")	
	public void recommendationsForAspiringRole(String urlData, String uName, String pwd, 
			String helpText) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation()
		.verifyFilterTextMessage(helpText)
		.enterAspiringRole() 
		.clickGo()
		.verifyRecommendedCoursCount()
		.getYourLearningGoalsList()
		.getCompletedCoursesList(); 
		
	}
}





