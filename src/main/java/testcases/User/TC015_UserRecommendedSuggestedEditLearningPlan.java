package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC015_UserRecommendedSuggestedEditLearningPlan extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC015_UserRecommendedSuggestedEditLearningPlan";
		testCaseStatus = "PASS";
		testDescription = "User Recommended Suggested Edit LearningPlan Functionality Validation";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLogin";
	}
    
	@Test(dataProvider = "fetchData")	
	public void userSuggestedLearningPlan(String urlData, String uName, String pwd, String data) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickLearningPlan()
		.filterWithRecommendedLearning()
		.filterWithCurrentRole()
		.getPlanStatusDetails()
		.clickGo()
		.clickEditPlan()
		.clickPersonalizedPlan()
		.enterDayPlan() ;
		//.clickReplan();
		//.filterByStatus();
		
		
	}
}






















