package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.NotificationsPage;
import pages.User.UserHomePage;

public class TC036_UserDragAndDropTheCourses extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC036_UserDragAndDropTheCourses";
		testDescription = "User DragAndDrop The Courses Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserDragDropCourses";
	}

	@Test(dataProvider = "fetchData")	
	public void userDragAndDropTheCourses(String data, String uName, String pwd,String userName, String userpwd) throws Exception {		
		
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test) 
		//.waitUpToFoldingCubeInvisible() 
		.acceptTheAlertBox()
		.clickLearningPlan()
		.clickCurrentPlan()
		.clickClear() 
        .clickEditPlan() 
		//.filterWithRecommendedLearning() 
		//.filterWithCurrentRole()
		//.clickGo() 
		.clickPersonalizedPlan() 
		.enterDayPlan()
		//.enterWeekPlan()
		//.enterMonthPlan() 
		//.clickReplan()
		.userDragAndDropTheCourses();
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userpwd)
		.clickLoginButton();
		new NotificationsPage(driver, test)
		//.waitUpToFoldingCubeInvisible() 
		.clickBellIcon()
		.clickNotifications()
		.userPlanApproval();
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new NotificationsPage(driver, test)
		.waitUpToFoldingCubeInvisible() 
		.clickBellIcon()
		.clickNotifications()
		.verificationOfUserCustomPlanApproval(); 
		
		
	}
}


