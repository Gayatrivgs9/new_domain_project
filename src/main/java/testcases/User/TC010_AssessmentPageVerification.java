package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC010_AssessmentPageVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC010_AssessmentPageVerification";
		testCaseStatus = "PASS";
		testDescription = "Assessment Page Verification";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void assessmentPageVerification(String urlData, String uName, String pwd, String domain) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible() 
		.clickAssessment()
		.verifyPendingAssessments() 
		.verifyCompletedAssessments()
		.verifySuggestions() 
		.verifyReviewProgress();
		
		
	}
}





