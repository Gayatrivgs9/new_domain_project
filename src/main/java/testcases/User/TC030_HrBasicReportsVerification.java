package testcases.User;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC030_HrBasicReportsVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC030_HrBasicReportsVerification";
		testDescription = "Hr Basic Reports Verification";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "HRBasicReports";
	}

	@Test(dataProvider = "fetchData")	
	public void hrBasicReportsVerification(String urlData, String uName, String pwd, String bu1, 
			String bu2, String dep1, String dep2, String roleCount, String fileName, String skillCount,
			String skillFileName, String courseCount, String courseFileName) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickHrView()  
		.verifyReportsCount() 
		.enterHRBusinessUnits(bu1, bu2) 
		.enterHRDepartments(dep1, dep2)  
		.clickGoButton() 
		//Roles verification
		.getRoleCount(roleCount)
		.verifyAllTheRoleNames(fileName)
		.verifyAllTheRoleNamesCount(fileName)
		.verifyAllTheRoleNamesPercentge(fileName)
		.clickSelectAndContinue(roleCount)
		//Skills verification
		.getSkillCount(skillCount)
		.verifyAllTheSkillNames(skillFileName)
		.verifyAllTheSkillsNamesCount(skillFileName)
		.verifyAllTheSkillsNamesPercentge(skillFileName)
		.clickSelectAndContinueOfSkill(skillCount)
		//Courses verification
		.getCourseCount(courseCount)
		.verifyAllTheCoursesNames(courseFileName)
		.verifyAllTheCoursesNamesCount(courseFileName)
		.verifyAllTheCourseNamesPercentge(courseFileName)
		.clickSelectAndContinueOfCourse(courseCount)
		;


	}
}











