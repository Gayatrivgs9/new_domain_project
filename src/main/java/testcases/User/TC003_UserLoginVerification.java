package testcases.User;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC003_UserLoginVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_UserLoginVerification";
		testCaseStatus = "PASS";
		testDescription = "User Login testCase using Valid Credentials Verification";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserVerification";
	}

	@Test(dataProvider = "fetchData")	
	public void userLoginVerification(String urlData, String uName, String pwd, String profile, String settings, String logout, 
			String copyRights, String privacy, String disclaimer, String termOfUse) throws InterruptedException {
		new LoginPage(driver, test)		
		.startUserLogin(urlData)
		.enterUserName(uName) 
		.enterPassword(pwd)
		.verifyCopyRightOpt(copyRights, privacy, disclaimer, termOfUse)
		.clickLoginButton() 
		.waitUpToFoldingCubeInvisible() 
		.clickUserDropdown()
		/*.verifySuperAdminProfileOpt(profile)
		.verifySuperAdminSettingsOpt(settings)*/
		.verifyUserLogoutOpt(logout) 
		.clickSidebarToggler()
		.verifyCopyRightOpt(copyRights, privacy, disclaimer, termOfUse) 
		.clickSuperAdminSifyCorp();	
	}

}





