package testcases.Livewire;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.livewire.LivewirePage;

public class TC002_LivewireILTCourseCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_LivewireILTCourseCreation";
		testDescription = "Livewire ILT Course Creation";
		testCaseStatus = "PASS";
		nodes = "Livewire";
		authors ="Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "LivewireDataILT";
	}

	@Test(dataProvider = "fetchData")	
	public void livewireILTCourseCreation(String uName, String pwd, String number, String courseTitle, String sessionTitle,
			String startDate, String endDate, String trainer, String location) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		new LivewirePage(driver,test)		
		.loadLivewireUrl()
		.clickLogin()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickCreateILTCourse(courseTitle) 
		.enterIltCourseDeetails(sessionTitle, startDate, endDate, trainer, location)
		.clickSaveButton()
		.clickAllCourses()
		.enterSearchCourse()
		.clickCourseDetails();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//.verifyCompletedCourses();    
		
	}

}

























