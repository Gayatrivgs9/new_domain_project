package testcases.Livewire;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.livewire.LivewirePage;

public class TC002_LivewireWBTScormCourseCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_LivewireWBTScormCourseCreation";
		testDescription = "Livewire WBT Course Creation";
		testCaseStatus = "PASS";
		nodes = "Livewire";
		authors ="Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "LivewireData";
	}

	@Test(dataProvider = "fetchData")	
	public void livewireWBTCourseCreation(String uName, String pwd, String number, String courseTitle, String courseType, String referenceType, 
			                    String referenceUrl, String startDate, String endDate,
			                    String domain, String schema, String system, String email, String api, String filename,
			                    String adminUname, String adminPwd) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException, AWTException {
		new LivewirePage(driver,test)		
		.loadLivewireUrl()
		.clickLogin()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickCreateWBTCourse(courseTitle) 
		.enterCourseDetails(courseType, referenceType, referenceUrl)
		.selectCourseStartEndDate(startDate, endDate)
		.clickOnSaveButton()
		.clickAllCourses()
		.enterSearchCourse()
		.clickCourseDetails()
		.getCourseIDAndCourseTitle();
		//Mapping skill to course
		new BaseliningPage(driver, test)
		.testLivewireCourseUploadFiles(domain, schema, system, email, api, filename); 
		
		//Run admin batch process
		new LoginPage(driver,test)		
		.navigateToAdmin() 
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		//Verify imported courses 
		new BaseliningPage(driver, test)
		.verifyCouseName();

		
		
		
		
		
		
		
		
		
		
		//.verifyCompletedCourses();    
		
	}

}

























