package testcases.Livewire;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.livewire.LivewirePage;

public class TC003_LivewireAssessmentCourseCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_LivewireAssessmentCourseCreation";
		testDescription = "Livewire Assessment Course Creation";
		testCaseStatus = "PASS";
		nodes = "Livewire";
		authors ="Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "LivewireData";
	}

	@Test(dataProvider = "fetchData")	
	public void livewireAssessmentCourseCreation(String uName, String pwd, String number, String courseTitle, String courseType, String referenceType, 
			                    String referenceUrl, String startDate, String endDate) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		new LivewirePage(driver,test)		
		.loadLivewireUrl()
		.clickLogin()
		.enterUsername(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		/*.clickCreateCourse(courseTitle) 
		.enterCourseDetails(courseType, referenceType, referenceUrl)
		.selectCourseStartEndDate(startDate, endDate)
		.clickOnSaveButton()
		.clickAllCourses()
		.enterSearchCourse()
		.clickCourseDetails()
		.getCourseIDAndCourseTitle();*/		
			
		
		
		//.verifyCompletedCourses();    
		
	}

}





