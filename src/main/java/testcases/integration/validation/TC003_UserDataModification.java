package testcases.integration.validation;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC003_UserDataModification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_UserDataModification";
		testDescription = "User Data Modification with import";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserDataMod";
	}

	@Test(dataProvider = "fetchData")	
	public void userAchievement(String data, String uName, String pwd, String aData, String source, String table,
			 String uploadURL,	String domain,String schema,String system,String email,String api, String filename) throws InterruptedException, IOException, AWTException {		
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData();
		new LoginPage(driver,test)		
		.startAdminLogin(aData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(aData, table) 
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testUserUploadFiles(uploadURL, domain, schema, system, email, api, filename);
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData();
		
	}
}














