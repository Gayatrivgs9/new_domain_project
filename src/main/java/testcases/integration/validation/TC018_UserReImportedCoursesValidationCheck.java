package testcases.integration.validation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC018_UserReImportedCoursesValidationCheck extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC018_UserReImportedCoursesValidationCheck";
		testDescription = "User Re Imported Courses Validation Check";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}

	@Test(dataProvider = "fetchData")	
	public void adminACLRole(String data, String uName, String pwd, String dataAdmin, String adminUname) throws Exception {
		/*Login to user*/
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.getCourseDetails()
		.getCourseOverallAnalysis();
		/*Login to admin*/
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(pwd)
		.clickLoginButton();
		/*import course csv file*/
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		/*.addAttribute(schema, source, attribute, type, validation);
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();*/
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsCourse()
		.selectTheSource()
		.clickUploadNewFile();
		/*new BaseliningPage(driver, test)
		.testCourseUploadFiles();*/
		/*Run batch at admin*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		/*Login to user*/
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.getCourseDetails()
		.getCourseOverallAnalysis()
		.overAllReImportedCourseCountValidation();

	}
}























