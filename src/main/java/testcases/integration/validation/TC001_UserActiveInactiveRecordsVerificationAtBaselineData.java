package testcases.integration.validation;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC001_UserActiveInactiveRecordsVerificationAtBaselineData extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC001_UserActiveInactiveRecordsVerificationAtBaselineData";
		testDescription = "User Active/Inactive Records Functionality Verification At BaselineData";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "ActiveInactiveRecords";
	}

	@Test(dataProvider = "fetchData")	
	public void userActiveInactiveRecordsVerificationAtBaselineData(String data, String uName, String pwd,String tableData,
			String schema,String uploadURL,String domain,String apiSchema,String system,String email,String api,
			String filename) throws InterruptedException, IOException, AWTException {
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickFieldMapping()
		.selectTheTableAsSchema(tableData, schema)
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testUserUploadFiles(uploadURL, domain, schema,system,email,api, filename); 


	}
}














