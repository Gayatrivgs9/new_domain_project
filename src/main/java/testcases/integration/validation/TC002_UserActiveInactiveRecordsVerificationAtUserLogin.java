package testcases.integration.validation;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC002_UserActiveInactiveRecordsVerificationAtUserLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC001_UserActiveInactiveRecordsVerificationAtUserLogin";
		testDescription = "User Active/Inactive Records Functionality Verification At User Login";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "ActiveInactiveRecords";
	}

	@Test(dataProvider = "fetchData")	
	public void userActiveInactiveRecordsVerificationAtUserLogin(String data, String uName, String pwd
			) throws InterruptedException, IOException, AWTException {
		new LoginPage(driver,test)		
		.startUserLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();

	}
}














