package testcases.integration.validation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.CommonHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC013_UsersACLPermissionToUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC013_UsersACLPermissionToUser";
		testDescription = "Users ACL Permission To User";
		testCaseStatus = "PASS";
		nodes = "Admin - User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminWithUserACL";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String adminData, String adminUName) throws InterruptedException {
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement();
		new LoginPage(driver,test)		
		.startAdminLogin(adminData) 
		.enterUserName(adminUName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserData()
		.clickNotificationOptions()
		.clickSave();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement() 
		.getTheUserData();
		new CommonHomePage(driver,test)		
		.clickUserDropdown()
		.clickUserLogout(); 
		new LoginPage(driver,test)		
		.startAdminLogin(adminData) 
		.enterUserName(adminUName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserDataNo()
		.clickNotificationOptionsNo()
		.clickSave();
				
	}

}



