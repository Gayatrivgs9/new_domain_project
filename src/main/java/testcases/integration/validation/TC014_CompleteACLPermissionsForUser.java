package testcases.integration.validation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC014_CompleteACLPermissionsForUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC014_CompleteACLPermissionsForUser";
		testDescription = "Complete ACL Permissions For User";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}

	@Test(dataProvider = "fetchData")	
	public void adminACLRole(String data, String uName, String pwd, String dataAdmin, String adminUname, String adminPwd) throws Exception {
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAssessment();
		new UserHomePage(driver, test)
		.clickRecommendation();
		new UserHomePage(driver, test)
		.clickLearningPlan();
		/*new UserHomePage(driver, test)
		.clickTrack();*/
		new UserHomePage(driver, test)
		.clickAchievement();
		new LoginPage(driver,test)		
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserOptions()
		.clickAchievementOptions()
		.clickAssessmentOptions()
		.clickNotificationOptions()
		.clickPlanOptions()
		.clickRecommendationOptions()
		.clickTrackOptions()
		.clickUserMenuOptions()	
		.clickSave();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAssessment();
		new UserHomePage(driver, test)
		.clickRecommendation();
		new UserHomePage(driver, test)
		.clickLearningPlan();
		new UserHomePage(driver, test)
		.clickTrack();
		new UserHomePage(driver, test)
		.clickAchievement();
		new UserHomePage(driver, test)
		.clickReports();
		/*new LoginPage(driver,test)		
		.startAdminLogin(dataAdmin) 
		.enterUserName(adminUname)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserOptionsNo()
		.clickAssessmentOptionsNo()
		.clickAchievementOptionsNo()
		.clickNotificationOptionsNo()
		.clickPlanOptionsNo()
		.clickRecommendationOptionsNo()
		.clickTrackOptionsNo()
		.clickUserMenuOptionsNo()
		.clickSave();*/

		
		
	}
}























