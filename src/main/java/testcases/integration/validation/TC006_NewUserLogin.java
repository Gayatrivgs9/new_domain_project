package testcases.integration.validation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.CommonHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC006_NewUserLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC006_NewUserLogin";
		testDescription = "New User Login";
		testCaseStatus = "PASS";
		nodes = "Admin - User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "NewUserLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd) throws InterruptedException {
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAssessment();
		new UserHomePage(driver, test)
		.clickRecommendation();
		new UserHomePage(driver, test)
		.clickLearningPlan();
		new UserHomePage(driver, test)
		.clickTrack();
		new UserHomePage(driver, test)
		.clickAchievement();
		new CommonHomePage(driver, test)
		.clickUserDropdown()
		.clickUserSettings()
		.clickUserDropdown()
		.clickUserLogout();
		
		
	}

}























