package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC027_TagsConfiguration extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC027_TagsConfiguration";
		testDescription = "Check the Admin Tags Configuration functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "Tags";
	}

	@Test(dataProvider = "fetchData")	
	public void adminTagsConfiguration(String data, String uName, String pwd, String sourceSystem, String selectKeys
			/*String domain, String schema, String path, String manualDomain, String aimlDomain*/) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
	    .clickTags()	
	    .enterSourceSystem(sourceSystem)
	   // .enterSelectSchema(selectSchema)
	    .enterSelectKeys(selectKeys)
	    .clickUpdateButton()
	   // .enterDomain(domain)
	   // .enterSchemaSelection(schema)
	    //.uploadNewFile(path)
	    //.clickUploadButton()
	    .enterManualTriggerDomain()
	    .enterAIMLBasedDomain();
		
		
	}
}















