package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC034_ACLRolePermissionsForLearner extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC034_ACLRolePermissionsForLearner";
		testDescription = "Check the Admin ACL Permissions For Learner functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "AdminACLPermissionsForLearner";
	}

	@Test(dataProvider = "fetchData")	
	public void aclRolePermissionsForLearner(String data, String uName, String pwd, String roleName, String roleType) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickAddRole()
		.enterAddRoleName(roleName)
		.selectStatusActive()
		.selectRoleType(roleType)
		.clickUserOptions() 
		.clickUnSelectSomeUserOptions() 
		.clickAchievementOptions()
		.clickUnSelectSomeAchievementOptions()
		.clickAssessmentOptions()
		.clickUnSelectSomeAssessmentOptions()
		.clickNotificationOptions()
		.clickPlanOptions()
		.clickUnSelectSomePlanOptions() 
		.clickRecommendationOptions()
		.clickTrackOptions()
		//.clickUserMenuOptions()		
		.clickSave();
		
	}
}






