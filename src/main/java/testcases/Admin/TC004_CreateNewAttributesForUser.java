package testcases.Admin;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC004_CreateNewAttributesForUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC004_CreateNewAttributesForUser";
		testDescription = "Create new attributes for user";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CreateUserAttributes";
	}

	@Test(dataProvider = "fetchData")	
	public void createNewAttributesForUser(String data, String uName, String pwd, String schema, String source) throws IOException, InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.addUserAttributes(schema, source)
		.clickSubmitButton()
		.verifyBaselineSuccessMsg(); 

		
	}
}





