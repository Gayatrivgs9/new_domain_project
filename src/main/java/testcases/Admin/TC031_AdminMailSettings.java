package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC031_AdminMailSettings extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC031_AdminMailSettings";
		testDescription = "Edit Admin Mail Settings";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminMailSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void mailSettings(String data, String uName, String pwd, String mailConfig, String mailSubject, String templateName,
			String mailCC, String time, String min, String successMsg) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickMailSettings() 
		.clickMailSettingsPlanApprovalEdit(mailConfig) 
		.clickSendMailAsYes()
		.enterCCType()
		.enterMailSubject(mailSubject)
		.enterTemplateName(templateName)
		.enterMailCCNames(mailCC)
		//.enterEmailFromId(emailIdFrom) 
		.clickSchedule()
		.enterScheduleEveryDay(time)
		.enterScheduleInMinutes(min)
		.clickSave()
		.verifySuccessMessage(successMsg); 
		
	}
}























