package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC001_AdminLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "Admin Login and LoginOut";
		testCaseStatus = "PASS";
		testDescription = "Login testCase using Valid Credentials";
		nodes = "Admin";
		authors = "Kamalesh";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd) throws InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickAdminDropdown()
		.clickAdminLogout();
			
	}
}





