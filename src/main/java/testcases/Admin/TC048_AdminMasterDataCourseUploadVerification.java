package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC048_AdminMasterDataCourseUploadVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC048_AdminMasterDataCourseUploadVerification";
		testDescription = "Admin Master Data Upload Course verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "MasterCourseIDVerification";
	}
	@Test(dataProvider = "fetchData")
	public void adminMasterDataCourseUploadVerification(String url, String uName, String pwd,String fileName) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaseliningData() 
		.clickCourse() 
		.enterLimit()
		.verifySchemaMasterData(fileName)
		.verifyCourseName();
		
	}
}


