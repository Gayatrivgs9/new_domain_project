package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;

public class TC013_UserBaselineDataImport extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC013_UserBaselineDataImport";
		testDescription = "Baselining for Schema as user and source as livewire";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "UserBaselineData";
	}
	@Test(dataProvider = "fetchData")
	public void userBaselineCreation(String url, String uName, String pwd,String schemaNew, String source,String attribute, String type, 
			String validation, String table,String uploadURL,String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaselining()
		/*.clickBaselinigTag() 
		.addAttribute(schema, source, attribute, type)
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();
		new BaseliningPage(driver, test)*/
		.clickFieldMapping()
		.selectTheTableAsSchema(validation, table) 
		.selectTheSource()
		.clickUploadNewFile();
		//.enterUserUploadFilePath();
		new BaseliningPage(driver, test)
		.testUserUploadFiles(uploadURL, domain, schema,system,email,api, filename);
	}
}


