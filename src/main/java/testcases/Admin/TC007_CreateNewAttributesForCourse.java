package testcases.Admin;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC007_CreateNewAttributesForCourse extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC007_CreateNewAttributesForCourse";
		testDescription = "Create new attributes for course";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CreateCourseAttribute";
	}

	@Test(dataProvider = "fetchData")	
	public void createNewAttributesForCourse(String data, String uName, String pwd, String schema, String source) throws IOException, InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.addCourseAttributes(schema, source)
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();

		
	}
}





