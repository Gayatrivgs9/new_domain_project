package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC029_Parameters extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC029_Parameters";
		testDescription = "Check the Admin Parameters functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void adminParameters(String data, String uName, String pwd) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickParameter()
		.getAllTheCourseAttributes()
		.getAllTheRoleAttributes()
		.getAllTheSkillAttributes() 
		.clickUpdate();
		
	}
}























