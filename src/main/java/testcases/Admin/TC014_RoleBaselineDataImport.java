package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;

public class TC014_RoleBaselineDataImport extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC014_RoleBaselineDataImport";
		testDescription = "Baselining for Schema as role and source as livewire";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "RoleBaselineData";
	}
	@Test(dataProvider = "fetchData")
	public void roleBaselineCreation(String data, String uName, String pwd,String schemaNew, String source,String attribute, String type, 
			String validation,String uploadURL,String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
	  /*.addAttribute(schema, source, attribute, type, validation);
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();*/
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsRole()
		.selectTheSource()
		.clickUploadNewFile();
		//.enterRoleUploadFilePath();
		new BaseliningPage(driver, test)
		.testRoleUploadFiles(uploadURL, domain, schema, system, email, api, filename);
		
				
	}
}



















