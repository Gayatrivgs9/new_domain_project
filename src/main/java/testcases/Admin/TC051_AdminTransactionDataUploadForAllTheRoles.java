package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC051_AdminTransactionDataUploadForAllTheRoles extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC045_AdminTransactionDataUploadForAllTheRoles";
		testDescription = "Transaction Data Upload for current and next roles with source system is livewire";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CourseTransMasterUpload";
	}
	@Test(dataProvider = "fetchData")
	public void adminTransactionDataUploadForAllTheRoles(String url, String uName, String pwd,
			String fileName, String successMsg) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaselining()
		.clickDataUploads()
		.clickCourseTransactionUploadButton()
		.selectSourceSystem()
		.uploadMasterDataFile(fileName, successMsg);
		
		
		
		
	}
}


