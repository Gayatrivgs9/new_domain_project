package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC030_AdminReportsSettings extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC030_AdminReportsSettings";
		testDescription = "Admin Reports Settings Verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminRep";
	}

	@Test(dataProvider = "fetchData")	
	public void adminReportsSettings(String data, String uName, String pwd, String userName, String userPwd) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickReports()
		.clickBusinessUnits()
		.clickDepartments()
		.clickUpdateButton()
		.verifySuccessMsg();
		new LoginPage(driver, test)
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickHrView()  
		.verifyReportsCount();
	}
}























