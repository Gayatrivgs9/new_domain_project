package testcases.Admin;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC005_CreateNewAttributesForRole extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_CreateNewAttributesForRole";
		testDescription = "Create new attributes for role";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CreateRoleAttribute";
	}

	@Test(dataProvider = "fetchData")	
	public void createNewAttributesForRole(String data, String uName, String pwd, String schema, String source) throws IOException, InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.addRoleAttributes(schema, source) 
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();

		
	}
}





