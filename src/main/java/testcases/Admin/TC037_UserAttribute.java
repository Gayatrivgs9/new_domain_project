package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC037_UserAttribute extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC037_UserAttribute";
		testDescription = "Check the Admin UserAttribute functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminUserAttribute";
	}

	@Test(dataProvider = "fetchData")	
	public void userAttribute(String data, String uName, String pwd) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickUserAttributeRelation()
		.clickCourses()
		.clickSuggestionData()
		.clickRecommendations()
		.clickDataTypeMatch()
		.clickSuggestedCourses()
		.clickSave();
		
		
	}
}























