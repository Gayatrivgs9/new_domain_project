package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC043_AdminMasterDataUpload extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC043_AdminMasterDataUpload";
		testDescription = "Admin Master Data Upload and source as livewire";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "MasterDataUpload";
	}
	@Test(dataProvider = "fetchData")
	public void adminMasterDataUpload(String url, String uName, String pwd,String fileName,
			String successMsg) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaselining()
		.clickMasterUpload()
		.clickUploadMasterSheets()
		.uploadMasterDataFile(fileName, successMsg); 
		
		
		
		
	}
}


