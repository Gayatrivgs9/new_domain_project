package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC039_AdminForms_ModifyAssignment extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC039_AdminForms_ModifyAssignment";
		testDescription = "Modify Assignment For Aspires with correct answers";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "AdminModifyForms";
	}

	@Test(dataProvider = "fetchData")	
	public void adminForms_ModifyAssignment(String data, String uName, String pwd, String assessmentName, String data1, String data2) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickForms()
		.clickModifyAssignment() 
		.enterSelectForm()
		.enterOriginalAnswers(data1,data2)
		.clickSubmit()
		.verifySuccessMsg();   
			
	}
}





