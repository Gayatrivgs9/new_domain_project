package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC002_AdminLoginVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_AdminLoginVerification";
		testDescription = "Admin Login testCase using Valid Credentials Verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminVerification";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String profile, String settings, String logout, String copyRights,
			                    String privacy, String disclaimer, String termOfUse) throws InterruptedException {
		new LoginPage(driver, test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton() 
		.clickAdminDropdown()
		.verifySuperAdminSettingsOpt(settings)
		.verifySuperAdminLogoutOpt(logout)
		.clickSidebarToggler()
		.verifyCopyRightOpt(copyRights, privacy, disclaimer, termOfUse) 
		.clickSuperAdminSifyCorp();	
	}

}





