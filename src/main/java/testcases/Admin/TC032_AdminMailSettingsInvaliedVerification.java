package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC032_AdminMailSettingsInvaliedVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC032_AdminMailSettingsInvaliedVerification";
		testDescription = "Admin Mail Settings Invalied data Verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "MailInvaliedVerification";
	}

	@Test(dataProvider = "fetchData")	
	public void mailSettingsInValied(String data, String uName, String pwd,String mailConfig, String mailSubject, String templateName,
			                 String mailCC, String time, String min, String infoMsg ) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickMailSettings() 
		.clickMailSettingsPlanApprovalEdit(mailConfig)
		.clickSendMailAsYes()
		//.enterCCType()
		.enterMailSubject(mailSubject)
		.enterTemplateName(templateName)
		.enterMailCCNames(mailCC)
		//.enterEmailFromId(mailId) 
		.clickSchedule()
		.enterScheduleEveryDay(time)
		.enterScheduleInMin(min)
		.clickSave()
		.verifyInfoMessage(infoMsg);
		
	}
}























