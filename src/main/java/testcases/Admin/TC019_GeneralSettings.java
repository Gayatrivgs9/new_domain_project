package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC019_GeneralSettings extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC019_GeneralSettings";
		testDescription = "Check the Admin General Settings functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "GeneralSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void generalSettings(String data, String uName, String pwd, String defaultCourseDuration) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickGeneral()
		.clickLocal()
		//.clickOAuth()
		//.clickThirdParty()
		.enterDefaultCourseDuration(defaultCourseDuration)
		.clickEnableCourseWeightage()
		.clickUpdateButton();
		
		
	}
}





