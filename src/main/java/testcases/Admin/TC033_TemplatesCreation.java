package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC033_TemplatesCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC033_TemplatesCreation";
		testDescription = "Create New Template For Course Completion";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "Templates";
	}

	@Test(dataProvider = "fetchData")	
	public void adminTemplatsCreation(String data, String uName, String pwd, String templateName, String placeHolderRole, String textArea) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickTemplates()
		.clickCreate()
		.enterTemplateName(templateName)
		.enterPlaceHoldersRole(placeHolderRole)
		.enterTemplateData(textArea)
		.clickUpdate();
		
		
	}
}







