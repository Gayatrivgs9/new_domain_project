package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC003_AdminInvaliedLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_AdminInvaliedLogin";
		testDescription = "Admin Login testCase using Invalied Credentials";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors ="Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "InvaliedAdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd , String errorMsg, String scenario) throws InterruptedException {
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserNameWithTab(uName)
		.enterPasswordWithTab(pwd)
		.verifyInvalidLogin(errorMsg, scenario);
	}

}





