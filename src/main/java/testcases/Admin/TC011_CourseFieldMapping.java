package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC011_CourseFieldMapping extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC011_CourseFieldMapping";
		testDescription = "Admin Baseline Course Field Mapping";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CourseFieldMapping";
	}

	@Test(dataProvider = "fetchData")	
	public void courseFieldMapping(String url, String uName, String pwd, String data, String path) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()		
		.clickFieldMapping()
		.selectTheTableAsSchema(data, data)
		.selectTheSource()
		.clickUploadNewFile()
		.enterUserUploadFilePath(data) 
		.clickUploadButton() 
		.clickGeneratePassword()
		.userFieldMapping(data) 
		.clickSubmitButton(); 
	}
}





