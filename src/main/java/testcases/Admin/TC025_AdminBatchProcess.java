package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC025_AdminBatchProcess extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC025_AdminBatchProcess";
		testDescription = "Check the Admin Batch Process functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminBatchProcess";
	}

	@Test(dataProvider = "fetchData")	
	public void adminBatchProcess(String data, String uName, String pwd) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		//.clickRunTransaction()
		.selectSchemaSlection()
		.clickRunBatch()
		//.waitForInvisibilityOfSpin()
		.verifyBatchProcessCompletedMsg();
	    
	    
		
		
	}
}























