package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC026_CourseWeightageConfiguration extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC026_CourseWeightageConfiguration";
		testDescription = "Check the Admin CourseWeightage Settings functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "CourseWeightage";
	}

	@Test(dataProvider = "fetchData")	
	public void adminCourseWeightageSettings(String data, String uName, String pwd, String filePath) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickCourseWeightageConfiguration()
	  //.downloadCourseAsCSV()
		.chooseFilePath(filePath)
		.clickSave(); 
		
	}
}























