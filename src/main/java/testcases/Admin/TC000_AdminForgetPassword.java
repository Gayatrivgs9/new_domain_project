package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC000_AdminForgetPassword extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC000_AdminForgetPassword";
		testDescription = "Admin Forget Password Vrification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "ForgetPwd";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String emailId, String newPwd) throws InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.clickForgetPassword()
		.enterUserName(uName)
		.verifyResetLink()
		.reLoadOfficePage(emailId, newPwd)
		.clickOutLook()
		.verifyResetMailSubject()
		.loadForgrtPasswordLink()
		.enterNewPassword(pwd); 
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickAdminDropdown()
		.clickAdminLogout();
			
	}
}





