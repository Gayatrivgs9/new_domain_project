package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC012_CourseTransactionFieldMapping extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC012_CourseTransactionFieldMapping";
		testDescription = "Admin Baseline Transaction Field Mapping";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CourseTransactionFieldMapping";
	}

	@Test(dataProvider = "fetchData")	
	public void transactionFieldMapping(String data, String uName, String pwd, String fileName, String successMsg) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()		
		.clickCourseTransactionMapping()
		.selectSourceSystem()
		.clickUploadNewFile()
		.uploadCourseTransactionFile(fileName, successMsg) 
		.clickGeneratePassword()
		.courseTransactionFieldMapping() 
		.clickSubmitButton(); 
	}
}





