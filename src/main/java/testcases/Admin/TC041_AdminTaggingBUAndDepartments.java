package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC041_AdminTaggingBUAndDepartments extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC041_AdminTaggingBUAndDepartments";
		testDescription = "Check the Admin Tagging BU/Departments functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "Tagging BUAndDep";
	}

	@Test(dataProvider = "fetchData")	
	public void adminTaggingBUAndDepartments(String data, String uName, String pwd, String LPName,
			String businessUnit) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickTaggingBUAndDepartment()
		.clickLearningPartnerUserList()
		.enterLearningPartnerName(LPName)
		.clickLinkBU()
		.verifyTaggingBUDepData()
		.enterBusinessUnit(businessUnit)
		.enterDepartment()
		.clickBUAndDepAddButton()
		.verifyBUAndDepartment()
		.clickBUAndDepSaveButton();
		
	}
}

























