package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;

public class TC040_CourseTransactionBaselineDataImport extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC040_CourseTransactionBaselineDataImport";
		testDescription = "Baselining Data Import for Schema as CourseTransaction and source as livewire";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CourseTransactionBaselineData";
	}
	@Test(dataProvider = "fetchData")
	public void courseBaselineCreation(String data, String uName, String pwd, String schemaNew, String table,String uploadURL, String domain,
			String schema,String system,String email,String api, String fileName) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(schemaNew, table)
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		//to upload the data
		.testCourseTransactionUploadFiles(uploadURL, domain, schema, system, email, api, fileName); 


	}
}



















