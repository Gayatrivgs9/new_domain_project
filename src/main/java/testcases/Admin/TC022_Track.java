package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC022_Track extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC022_Track";
		testDescription = "Admin Track functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";  
		dataSheetName = "DataSheet";
		sheetName = "Track";
	}

	@Test(dataProvider = "fetchData") 	
	public void track(String data, String uName, String pwd, String dateRange, String num1, String num2) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
	    .clickTrack()
	    .enterDefaultDateRange(dateRange)
	    .clickRoleeSectionVsOwnNotifications()
	    .clickRoleSectionVsPeersNotifications()
	    .clickSkillSectionVsOwnNotifications()
	    .clickSkillSectionVsPeersNotifications()
	    .clickRolesSectionPredictiveAnalysis()
	    .clickSkillsSectionPredictiveAnalysis()
	    .clickRolesSectionGraphs1()
	    .clickRolesSectionGraphs2()
	    .clickSkillsSectionGraphs1()
	    .clickSkillsSectionGraphs2()
	    .clickManagerView()
	    .clickManagerRolesSectionGraphs()
	    .clickSkillsManagerSectionGraphs1()
	    .clickHrView()
	    .clickHrRolesSectionGraphs()
	    .clickSkillsHrSectionGraphs1() 
	    //.clickRolesSectionGraphs()
       // .clickSkillsSectionGraphs()
	    .selectTimeRange()
	    .selectNumberRange(num1, num2)
	    .clickUpdateButton();
	    
		
		
	}
}




