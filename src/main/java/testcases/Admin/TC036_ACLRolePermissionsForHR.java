package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC036_ACLRolePermissionsForHR extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC036_ACLRolePermissionsForHR";
		testDescription = "Check the Admin ACL Permissions For HR functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "AdminACLPermissionsForHR";
	}

	@Test(dataProvider = "fetchData")	
	public void aclRolePermissionsForHR(String data, String uName, String pwd, String roleName, String roleType) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickAddRole()
		.enterAddRoleName(roleName)
		.selectStatusActive()
		.selectRoleType(roleType)
		.clickUserOptions() 
		.clickHRAssessmentOptions()
		.clickTrackOptions()
		.clickHRUserMenuOptions() 	
		.clickSave();
		
		
	}
}























