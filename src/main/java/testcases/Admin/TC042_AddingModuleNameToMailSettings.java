package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC042_AddingModuleNameToMailSettings extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC042_AddingModuleNameToMailSettings";
		testCaseStatus = "PASS";
		testDescription = "Adding Module Name To MailSettings Functionality Verification";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void addingModuleNameToMailSettings(String data, String uName, String pwd) throws InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickMailSettings()
		.enterModuleName(); 
		
			
	}
}



















