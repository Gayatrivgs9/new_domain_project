package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC000_BaselineCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_BaselineCreation";
		testDescription = "Creating Baseline from excel data";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Kamalesh";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "BaselineCreation";
	}
	@Test(dataProvider = "fetchData")
	public void baselineCreation(String uName, String pwd,String SchemaName1, String SchemaName2,
								String SchemaName3, String SchemaName4, String SchemaName6, String rowNum,
								String SchemaName1Result, String SchemaName2Result, String SchemaName3Result, String SchemaName4Result, 
								String SchemaName6Result) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		
		/*public void baselineCreation(String uName, String pwd,String inputText, String attribute, String type,
				                                       String critical,String primaryKey) throws IOException {*/
		new LoginPage(driver,test)		
		.startQcAdminLogin()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag()
		.addSchemas(SchemaName1, rowNum, 9)
		.addSchemas(SchemaName2, rowNum, 10)
		.addSchemas(SchemaName3, rowNum, 11)
		.addSchemas(SchemaName4, rowNum, 12)
		//.addSchemas(SchemaName5, rowNum, 13)
		.addSchemas(SchemaName6, rowNum, 13);
		//.selectSchema();
		/*.clickBaselining()		
		.addSchemas(SchemaName1, rowNum, 10)
		//.uploadFiles(0, DomainName)				
		.addSchemas(SchemaName2, rowNum, 11)
		//.uploadFiles(1, DomainName)		
		.addSchemas(SchemaName3, rowNum, 12)
		//.uploadFiles(2, DomainName)
		.addSchemas(SchemaName4, rowNum, 13)
		//.uploadFiles(3, DomainName)
		.addSchemas(SchemaName5, rowNum, 14)
		//.uploadFiles(4, DomainName)
		.addSchemas(SchemaName6, rowNum, 15);
		//.uploadFiles(5, DomainName);
		//.clickAdminLogout();	
*/		
	}
}





