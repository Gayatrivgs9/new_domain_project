package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC024_SuggestionThreshold extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC024_SuggestionThreshold";
		testDescription = "Check the Admin Suggested Threshold functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "ThresholdSuggestion";
	}

	@Test(dataProvider = "fetchData")	
	public void adminSuggestionThreshold(String data,String uName, String pwd, String user, String role, String skill, String course, String fit, String potential) throws Exception {

		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickThreshold()
		/*
		 * .enterUserThresholdValue(user) .enterRoleThresholdValue(role)
		 * .enterSkillThresholdValue(skill) .enterCourseThresholdValue(course)
		 * .clickSuggestionThresholdSumbitButton() .enterThresholdFitValue(fit)
		 * .enterThresholdPotentialValue(potential)
		 */
		.clickThresholdSubmit()
		.verifySuccessMsg();



	}
}























