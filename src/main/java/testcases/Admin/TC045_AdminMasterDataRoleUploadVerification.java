package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC045_AdminMasterDataRoleUploadVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC045_AdminMasterDataRoleUploadVerification";
		testDescription = "Admin Master Data Upload Role verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "MasterRoleVerification";
	}
	@Test(dataProvider = "fetchData")
	public void adminMasterDataRoleUploadVerification(String url, String uName, String pwd,String fileName) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaseliningData() 
		.clickRole() 
		.enterLimit()
		.verifySchemaMasterData(fileName)
		.verifyRoleName();
		
	}
}


