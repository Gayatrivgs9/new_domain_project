package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC049_AdminMasterDataBusinessUnitUploadVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC049_AdminMasterDataBusinessUnitUploadVerification";
		testDescription = "Admin Master Data Upload BusinessUnit verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "MasterBUVerification";
	}
	@Test(dataProvider = "fetchData")
	public void adminMasterDataBusinessUnitUploadVerification(String url, String uName, String pwd,String fileName) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaseliningData() 
		.clickBusinessUnit()
		.enterLimit()
		.verifySchemaMasterData(fileName)
		.verifyUserName();
		
	}
}


