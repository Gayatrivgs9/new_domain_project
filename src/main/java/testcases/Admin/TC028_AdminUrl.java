package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC028_AdminUrl extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC028_AdminUrl";
		testDescription = "Check the Admin AdminUrl functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminURL";
	}
    
	@Test(dataProvider = "fetchData")	
	public void adminUrl(String data, String uName, String pwd, String systemName, String baseUrl, String schema, String mappingKey, String mappingWith,
			             String username, String password) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickURL()
		.enterSystemName(systemName)
		.enterBaseUrl(baseUrl)
		.enterSelectSchema(schema)
		.enterMappingKey(mappingKey)
		.enterMappingWith(mappingWith)
		.clickAddParam()
		.enterUserame(username)
		.enterPassword(password) 
		.clickAdd();
		
		
	}
}
















