package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC023_RecommendationConfiguration extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC023_RecommendationConfiguration";
		testDescription = "RecommendationConfiguration functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "RecommendationConfiguration";
	}

	@Test(dataProvider = "fetchData")	
	public void adminRecommendationConfig(String data, String uName, String pwd, String topPeers, String basicThreshold, String mentorThreshold, 
			    String opt1, String opt2) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
	    .clickRecommendation()
	    .enterHowManyTopPeersAppear(topPeers)
	    .enterBasicThresholdToSort(basicThreshold)
	    .enterBasicThresholdForMentor(mentorThreshold)
	    .clickSkill()
	    .clickSubmitButton(); 
	    /*.enterCourseOptions()
	    .enterCourseOption1(opt1)
	    .enterCourseOption2(opt2)
	    .clickAdd()
	    .clickCourseOptionSubmit();*/
	    
		
		
	}
}























