package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC020_ThemeSettings extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC020_ThemeSettings";
		testDescription = "Check the Admin Theme Settings functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void adminThemeSettings(String data, String uName, String pwd/*, String logo, String backGround*/) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTheme()
		/*.chooseLogoImage()
		.uploadLogoImage()
		.chooseBackgroundImage()
		.uploadBackgroundImage()*/
		.enterFontSize() 
		/*.clickBold()
		.clickItalic()
		.clickCapitalize()*/
		.clickUpdate();
		 
		
	}
}























