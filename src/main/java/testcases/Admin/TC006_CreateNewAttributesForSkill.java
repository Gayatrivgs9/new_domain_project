package testcases.Admin;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC006_CreateNewAttributesForSkill extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC006_CreateNewAttributesForSkill";
		testDescription = "Create new attributes for skill";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CreateSkillAttribute";
	}

	@Test(dataProvider = "fetchData")	
	public void createNewAttributesForSkill(String data, String uName, String pwd, String schema, String source) throws IOException, InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.addSkillAttributes(schema, source) 
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();

		
	}
}





