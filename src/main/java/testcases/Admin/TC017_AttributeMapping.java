package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC017_AttributeMapping extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC017_AttributeMapping";
		testDescription = "Map the attributes based on the Schema";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AttributeMapping";
	}
    //update
	@Test(dataProvider = "fetchData")	
	public void attributeMapping(String data, String uName, String pwd, String schemaName1, String schemaName2,
								String schemaName4, String schemaName6) throws IOException, AWTException, InterruptedException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()		
		.clickAttributeMapping()
		.selectSchemas(schemaName1, schemaName2, schemaName4, schemaName6)
		.selectMultipleSchemaEnableLevels() 
		.clickSchemaConfigSaveButton()
		.uploadFileForSchemaMaping() 
		.schemaMapping()
		.clickMapNewSchemaSaveButton();
	  	
		
	}
}





















