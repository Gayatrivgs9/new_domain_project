package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC021_AdminHoursPlanning extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC021_AdminHoursPlanning";
		testDescription = "Admin Hours Planning functionality";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "HoursPlanning";
	}

	@Test(dataProvider = "fetchData")	
	public void adminHoursPlanning(String data, String uName, String pwd, String defaultDateRange, String helpText) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickPlan()
		.enterDayPlan()
		//.enterWeekPlan()
		//.enterMonthPlan()
		.clickIncludeAssessments()
		.enterDateRange(defaultDateRange)
		//.enterHelpText(helpText)
		.clickRolesSectionGraphs()
		.clickSkillsSectionGraphs()
		.clickUpdateButton();
		
		
	}
}





















