package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC009_RoleFieldMapping extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC009_RoleFieldMapping";
		testDescription = "Admin Baseline Role Field Mapping";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "RoleFieldMapping";
	}

	@Test(dataProvider = "fetchData")	
	public void roleFieldMapping(String url, String uName, String pwd, String data, String path) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {
		
		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()		
		.clickFieldMapping()
		.selectTheTableAsSchema(data, data)
		.selectTheSource()
		.clickUploadNewFile()
		.enterUserUploadFilePath(data) 
		.clickUploadButton() 
		.clickGeneratePassword()
		.userFieldMapping(data) 
		.clickSubmitButton(); 
	}
}





