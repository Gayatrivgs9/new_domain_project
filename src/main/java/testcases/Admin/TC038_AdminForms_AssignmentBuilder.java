package testcases.Admin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC038_AdminForms_AssignmentBuilder extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC038_AdminForms_AssignmentBuilder";
		testDescription = "Create New Assignment Form For Aspire";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "AdminForms";
	}

	@Test(dataProvider = "fetchData")	
	public void adminForms(String data, String uName, String pwd, String assessment, String approvalType,
			String randomNum, String name, String mapWith, String duration,String mappingKey, 
			String mappingValue, String passPercentage, String fromId, String numOfQns, String newTag, 
			String qns1, String qns2,String review1, String review2) throws Exception {
		
		new LoginPage(driver,test)		
		.startAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickForms()
		.clickBuildNewAssignment()
		.enterTypeOfAssignment(assessment)
		.enterApprovalType(approvalType)
		.enterTypeOfName(name)
		.enterMapWith(mapWith)
		.enterDurationInMin(duration)
		.enterMappingKey(mappingKey)
		.enterMappingValue(mappingValue)
		.enterPassPercentage(passPercentage) 
		.enterFormId(fromId)
		.enterNoOfQuestions(numOfQns)
		.enterNewTag(newTag)
		.dradAndDropTextArea()
		.reviewDradAndDropTextArea()
		.clickSaveForms(); 
		
		/*new AdminHomePage(driver, test)
		.clickForms()
		.clickModifyAssignment() 
		.enterSelectForm()
		.enterOriginalAnswers(ans1, ans2)
		.clickSubmit();*/
			
	}
}





