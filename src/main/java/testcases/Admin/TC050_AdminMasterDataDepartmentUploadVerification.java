package testcases.Admin;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC050_AdminMasterDataDepartmentUploadVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC050_AdminMasterDataDepartmentUploadVerification";
		testDescription = "Admin Master Data Upload Department verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "MasterDepVerification";
	}
	@Test(dataProvider = "fetchData")
	public void adminMasterDataDepartmentUploadVerification(String url, String uName, String pwd,String fileName) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(url) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton(); 
		new AdminHomePage(driver, test) 
		.clickBaseliningData() 
		.clickDepartment()
		.enterLimit()
		.verifySchemaMasterData(fileName) 
		.verifyUserName();
		
	}
}


