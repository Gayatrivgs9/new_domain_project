package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC020_TrackSkillsSectionGraphsValidationAtUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC020_TrackSkillsSectionGraphsValidationAtUser";
		testDescription = "Track Skills Section Graphs Validation At User";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}

	@Test(dataProvider = "fetchData")	
	public void trackSkillsSectionGraphsValidationAtUser(String data, String uName, String pwd, String dataAdmin, String adminUname, String adminPwd) throws Exception {
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickSkillsSectionGraphs1() 
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifySkillsGraphs1();
		new LoginPage(driver,test)	//-------	
	    .navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickSkillsSectionGraphs2()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifySkillsGraphs2();
		new LoginPage(driver,test)	//-------	
		.navigateToAdmin()
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickSkillsSectionGraphs3()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser() 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifySkillsGraphs3();
		new LoginPage(driver,test)	//-------	
		.navigateToAdmin()
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickSkillsSectionGraphs5()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser() 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifySkillsGraphs5();
		
		
	}
}























