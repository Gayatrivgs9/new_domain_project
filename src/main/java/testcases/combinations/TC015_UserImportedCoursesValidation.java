package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC015_UserImportedCoursesValidation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC015_UserImportedCoursesValidation";
		testDescription = "User Imported Courses Validation Check";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "NewUserCourseCreation";
	}

	@Test(dataProvider = "fetchData")	
	public void adminACLRole(String data, String adminUname, String adminPwd, String schema, String source,  String uploadURL,
			String domain,String schemaNew,String system,String email,String api,String filename, String userName, String userPwd) throws Exception {
		/*Login to user*/
		/*new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.clickSelectCategory();*/
		/*Login to admin*/
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		/*import course csv file*/
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsCourse()
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testCourseUploadFiles(uploadURL, domain, schemaNew, system, email, api, filename);
		/*Run batch at admin*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		/*Login to user*/
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.getCourseDetails() 
		.getCourseOverallAnalysis()
		.overAllCourseCountValidation();

	}
}























