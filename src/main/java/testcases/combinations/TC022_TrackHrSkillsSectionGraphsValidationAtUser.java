package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC022_TrackHrSkillsSectionGraphsValidationAtUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC022_TrackHrSkillsSectionGraphsValidationAtUser";
		testDescription = "Track Hr Skills Section Graphs Validation At User";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}

	@Test(dataProvider = "fetchData")	
	public void trackHrSkillsSectionGraphsValidationAtUser(String data, String uName, String pwd, String dataAdmin, String adminUname, String adminPwd) throws Exception {
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllHrRolesOptions()
		.unselctAllHrSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		/*Graph1*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickHrRolesSectionGraphs()
		.clickSkillsHrSectionGraphs1()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView()
		.verifyHrRolesGraphs()
		.verifyHrSkillsGraphs1();
		
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllHrRolesOptions()
		.unselctAllHrSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		/*Graph2*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickHrRolesSectionGraphs()
		.clickSkillsHrSectionGraphs2()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView()
		.verifyHrRolesGraphs()
		.verifyHrSkillsGraphs2();	
		
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllHrRolesOptions()
		.unselctAllHrSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		/*Graph3*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickHrRolesSectionGraphs()
		.clickSkillsHrSectionGraphs3()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickHrView()
		.verifyHrRolesGraphs()
		.verifyHrSkillsGraphs3()		
		.verifyAggregateHrGraph();
		
		
	}
}























