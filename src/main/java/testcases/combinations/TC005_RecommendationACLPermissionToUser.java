package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.CommonHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC005_RecommendationACLPermissionToUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_RecommendationACLPermissionToUser";
		testDescription = "Recommendation ACL Permission To User validation";
		testCaseStatus = "PASS";
		nodes = "Admin - User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminWithUserACL";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String adminData, String adminUName, String adminPwd) throws InterruptedException {
		/*new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation();*/
		
		//Login to admin 
		new LoginPage(driver,test)		
		.navigateToAdmin() 
		.enterUserName(adminUName)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyUserEmailId(uName)
		.clickAddRemoveRoles() 
		.clickUserRoles() 
		.clickSave()
		.clickRoles()
		.clickUserOptionEditButton()
		.clickRecommendationOptions()
		.clickSave();
		//Run batch
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		//Login to user
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickRecommendation()
		.verifyRecommendedCoursCount(); 
		new CommonHomePage(driver,test)		
		.clickUserDropdown()
		.clickUserLogout();
		
		/*new LoginPage(driver,test)		
		.startAdminLogin(adminData) 
		.enterUserName(adminUName)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickRecommendationOptionsNo()
		.clickSave();*/
		
	}

}























