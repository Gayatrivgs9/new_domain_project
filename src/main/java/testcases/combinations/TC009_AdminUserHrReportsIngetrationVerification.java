package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC009_AdminUserHrReportsIngetrationVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC009_Admin-UserHrReportsIngetrationVerification";
		testDescription = "Admin User Hr Reports Ingetration Verification";
		testCaseStatus = "PASS";
		nodes = "Admin - User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminNewUserReports";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String userName, String userPwd) throws InterruptedException {
		new LoginPage(driver, test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickReports()
		.clickBusinessUnitsNo()
		.clickDepartmentUnitsNo()
		.clickUpdateButton()
		.verifySuccessMsg();
		new LoginPage(driver, test)
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickHrView()  
		.verifyUserReports();
		new LoginPage(driver, test)		
		.navigateToAdmin()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickReports()
		.clickBusinessUnits()
		.clickDepartments()
		.clickUpdateButton();
		
	}

}


