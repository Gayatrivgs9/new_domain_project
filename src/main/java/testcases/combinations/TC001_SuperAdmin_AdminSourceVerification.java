package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;

public class TC001_SuperAdmin_AdminSourceVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC001_SuperAdmin_AdminSourceVerification";
		testDescription = "SuperAdmin - Admin SourceName Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin - Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "SuperAdminSchemaVerify";
	}

	@Test(dataProvider = "fetchData")	
	public void adminACLRole(String data, String uName, String pwd, String domain, String tableData, String adminUName, String adminPwd,
			                 String schema) throws Exception {
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.clickMain() 
		.enterMapSystemSelectDomain(domain) 
		.enterMapSystemSelectTable(tableData)
		.enterMapSystemSelectSource()
		.getSystemSelectSource()
		.clickMapSystmRegister();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.enterUserName(adminUName)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.enterSchemaName(schema)
		.verifySourceName() ;
		
	}
}























