package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC003_ACLPermissionToUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_ACLPermissionToUser";
		testDescription = "ACL Permission To User";
		testCaseStatus = "PASS";
		nodes = "Admin - User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminWithUserACL";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String adminData, String adminUName, String adminPwd) throws InterruptedException {
		//Login to admin
		new LoginPage(driver,test)		
		.navigateToAdmin() 
		.enterUserName(adminUName)
		.enterPassword(adminPwd) 
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyUserEmailId(uName)
		.clickAddRemoveRoles() 
		.clickSave() 
 		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserOptionsNo()
		.clickAchievementOptionsNo()
		.clickAssessmentOptionsNo()
		.clickNotificationOptionsNo()
		.clickPlanOptionsNo()
		.clickRecommendationOptionsNo()
		.clickTrackOptionsNo()
		.clickUserMenuOptionsNo()	
		.clickSave();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickBatchProcess()
		.selectSchemaSlection()
		.clickRunBatch()
		.verifyBatchProcessCompletedMsg();
		
		//Login to user
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAssessment();
		new UserHomePage(driver, test)
		.clickRecommendation();
		new UserHomePage(driver, test)
		.clickLearningPlan();
		new UserHomePage(driver, test)
		.clickTrack();
		new UserHomePage(driver, test)
		.clickAchievement();
		/*new UserHomePage(driver, test)
		.clickReports(); */ 
		
	}

}























