package testcases.combinations;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC014_NewUserSkillCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC014_NewUserSkillCreation";
		testDescription = "New User Skill Creation";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "NewUserSkillCreation";
	}
	@Test(dataProvider = "fetchData")
	public void newUserSkillCreation(String data, String uName, String pwd,String schema, String source,
			String attribute, String type, String validation, String uploadURL, String domain, String schemaNew,
			String system, String email, String api, String filename, String userName, String userPwd) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining();
		new BaseliningPage(driver, test)
		.testSkillUploadFiles(uploadURL, domain, schemaNew, system, email, api, filename);
		
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		//.waitUpToFoldingCubeInvisible() 
		.clickAchievement() 
		.getTheUserData(); 
		
	}
}














