package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC019_TrackRolesSectionGraphsValidationAtUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC019_TrackRolesSectionGraphsValidationAtUser";
		testDescription = "Track Graphs Validation At User";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}
 
	@Test(dataProvider = "fetchData")	
	public void trackRolesSectionGraphsValidationAtUser(String data, String uName, String pwd, String dataAdmin, String adminUname, String adminPwd) throws Exception {
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllRoles()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickRolesSectionGraphs1()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifyGraphs1();
		new LoginPage(driver,test)	//-------	
	    .navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickRolesSectionGraphs2()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifyGraphs2();
		new LoginPage(driver,test)	//-------	
		.navigateToAdmin()
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickRolesSectionGraphs3()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser() 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		//.clickUserView()
		.verifyGraphs3();
		
		
	}
}























