package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC021_TrackManagerSkillsSectionGraphsValidationAtUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC021_TrackManagerSkillsSectionGraphsValidationAtUser";
		testDescription = "Track Manager Skills Section Graphs Validation At User";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}

	@Test(dataProvider = "fetchData")	
	public void trackManagerSkillsSectionGraphsValidationAtUser(String data, String uName, String pwd, String dataAdmin, String adminUname, String adminPwd) throws Exception {
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllHrRolesOptions()
		.unselctAllHrSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		/*Graph1*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickManagerRolesSectionGraphs()
		.clickSkillsManagerSectionGraphs1()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView()
		.verifyManagerRolesGraphs()
		.verifyManagerSkillsGraphs1();
		
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllManagerRolesOptions()
		.unselctAllManagerSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		/*Graph2*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickManagerRolesSectionGraphs()
		.clickSkillsManagerSectionGraphs2()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView()
		.verifyManagerRolesGraphs()
		.verifyManagerSkillsGraphs2();	
		
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack()
		.unselctAllManagerRolesOptions()
		.unselctAllManagerSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickTrackOptions()
		.clickSave();
		/*Graph3*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickTrack() 
		.clickManagerRolesSectionGraphs()
		.clickSkillsManagerSectionGraphs3()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickTrack()
		.clickManagerView()
		.verifyManagerRolesGraphs()
		.verifyManagerSkillsGraphs3()		
		.verifyAggregateGraph();
		
		
	}
}























