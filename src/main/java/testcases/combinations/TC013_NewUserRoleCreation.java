package testcases.combinations;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC013_NewUserRoleCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC013_NewUserRoleCreation";
		testDescription = "New User Role Creation";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "NewUserRoleCreation";
	}
	@Test(dataProvider = "fetchData")
	public void userBaselineCreation(String data, String uName, String pwd,String schema, String source,
			String attribute, String type, String validation, String uploadURL, String domain, String schemaNew,
			String system, String email, String api, String filename,String userName, String userPwd) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining();
		/*.clickBaselinigTag()
		.addAttribute(schema, source, attribute, type)
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(validation, table)
		.selectTheSource()
		.clickUploadNewFile();*/
		new BaseliningPage(driver, test)
		.testRoleUploadFiles(uploadURL, domain, schemaNew, system, email, api, filename);
		
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		//.waitUpToFoldingCubeInvisible() 
		.clickAssessment()
		/*.enterCurrentRole()
		.enterNextRole()*/
		.clickGo()
		.verifyTotalAssessmentCoursCount(); 
			
	}
}














