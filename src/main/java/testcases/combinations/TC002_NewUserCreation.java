package testcases.combinations;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;

public class TC002_NewUserCreation extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_NewUserCreation";
		testDescription = "New User Creation";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "NewUserCreation";
	}
	@Test(dataProvider = "fetchData")
	public void userBaselineCreation(String data, String uName, String pwd,String schema, String source,
			String attribute, String type, String validation, String sourceData, String table, String uploadURL, String domain, String schemaNew,
			String system, String email, String api, String filename, String userPwd) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new LoginPage(driver,test)		
		.startAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickBaselining()
		.clickBaselinigTag()
		.addAttribute(schema, source, attribute, type)
		.clickSubmitButton()
		.verifyBaselineSuccessMsg();
		new BaseliningPage(driver, test)
		.clickFieldMapping()
		.selectTheTableAsSchema(validation, table)
		.selectTheSource()
		.clickUploadNewFile();
		new BaseliningPage(driver, test)
		.testUserUploadFiles(uploadURL, domain, schemaNew, system, email, api, filename);
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(emailId)
		.enterPassword(userPwd)
		.clickLoginButton()
		.clickUserDropdown() 
		.clickUserLogout();	
	}
}














