package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC023_PlanSectionGraphsValidationAtUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC023_PlanSectionGraphsValidationAtUser";
		testDescription = "Plan Section Graphs Validation At User";
		testCaseStatus = "PASS";
		nodes = "Admin - user";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "CompleteACL";
	}

	@Test(dataProvider = "fetchData")	
	public void planSectionGraphsValidationAtUser(String data, String uName, String pwd, String dataAdmin, String adminUname, String adminPwd) throws Exception {
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickPlan()
		.unselctAllRoles()
		.unselctAllSkills()
		.clickUpdateButton(); 
		new LoginPage(driver, test)		
		.navigateToUser()
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickLearningPlan();
		new LoginPage(driver,test)
		.navigateToAdmin() 
		.startAdminLogin(dataAdmin)
		.enterUserName(adminUname)
		.enterPassword(adminPwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickPlanOptions()
		.clickSave();
		/*Graph1*/
		new AdminHomePage(driver, test)
		.clickSettings()
		.clickPlan()
		.clickRoleSectionGraphs()
		.clickSkillSectionGraphs()
		.clickUpdateButton();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickLearningPlan()
		.verifyLearningProgressGraph1()
		.verifyLearningProgressGraph2();
	
	}
}























