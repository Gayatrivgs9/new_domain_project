package testcases.combinations;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.AdminHomePage;
import pages.SuperAdmin.CommonHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC011_UserMenuACLPermissionToUser extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC011_UserMenuACLPermissionToUser";
		testDescription = "UserMenu ACL Permission To User";
		testCaseStatus = "PASS";
		nodes = "Admin - User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminWithUserACL";
	}

	@Test(dataProvider = "fetchData")	
	public void adminLoginCheck(String data, String uName, String pwd, String adminData, String adminUName, String adminPwd) throws InterruptedException {
		new LoginPage(driver, test)		
		.startUserLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new LoginPage(driver,test)		
		.startAdminLogin(adminData) 
		.enterUserName(adminUName)
		.enterPassword(adminPwd) 
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyUserEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserMenuOptions()
		.clickSave();
		new LoginPage(driver, test)		
		.navigateToUser()
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickReports()
		.clickManagerView() 
		.verifyUserEmail() 
		.verifyReportsCount()
		.clickHrView()
		.verifyReportsCount();
		new CommonHomePage(driver,test)		
		.clickUserDropdown()
		.clickUserLogout();
		
		/*new LoginPage(driver,test)		
		.startAdminLogin(adminData) 
		.enterUserName(adminUName)
		.enterPassword(pwd)
		.clickLoginButton();
		new AdminHomePage(driver, test)
		.clickAcl()
		.clickUsers()
		.enterShowRecordsValue()
		.verifyEmailId(uName)
		.clickRoles()
		.clickUserOptionEditButton()
		.clickUserMenuOptionsNo()
		.clickSave();*/
		
		
		
	}

}













