package testcases.combinations;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.Admin.BaseliningPage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC021_MultipleUsersCourseTransactionBaselineDataImport extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC021_MultipleUsersCourseTransactionBaselineDataImport";
		testDescription = "Multiple Users Course Transaction Baseline DataImport Verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "usersDataUpload";
	}
	@Test(dataProvider = "fetchData")
	public void multipleUsersCourseTransactionBaselineDataImport(String uploadURL, String domain,String schema,String system,String email,
			String api, String fileName, String userName, String userpwd) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException {

		new BaseliningPage(driver, test)
		.testCourseTransactionUploadFiles(uploadURL, domain, schema, system, email, api, fileName);
		//Login to user 
		new LoginPage(driver,test)		
		.navigateToUser() 
		.enterUserName(userName)
		.enterPassword(userpwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible() 
		.clickAssessment()
		//.enterCurrentRole()
		.clickGo() 
		.verifyAssignedAndCompletedCoursesCount(); 
		new UserHomePage(driver, test)
		.clickRecommendationField() 
		//.enterCurrentRole() 
		.clickGo()
		.verifyRecommendedAndCompletedCoursesCount(); 
		/*new UserHomePage(driver, test)
		.clickAchievement()
		.getTheUserData()
		.userSkillPercentage(); */

	}
}







