package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC024_SuperAdminAddingCORSUrl extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC024_SuperAdminAddingCORSUrl";
		testDescription = "SuperAdmin CORS Url Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "CORSUrl";
	}
   
	@Test(dataProvider = "fetchData")	
	public void superAdminEmailValidation(String data, String uName, String pwd, String url, String sucMsg) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.clickCors() 
		.enterInputUrl(url)
		.clickAddButton()
		.verifyUrl(url) 
		.clickSaveButton();
		//.verifySuccessMsg(sucMsg);
	}
 }





