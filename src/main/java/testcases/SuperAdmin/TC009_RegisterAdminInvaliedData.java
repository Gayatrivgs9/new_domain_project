package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC009_RegisterAdminInvaliedData extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC009_RegisterAdminInvaliedData";
		testDescription = "Register a new Admin with invalied data";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "AdminInvaliedCreation";
	}
    
	@Test(dataProvider = "fetchData")	
	public void adminRegisterInvalied(String data, String uName, String pwd, String AdminName, String Email, String password, String domain, String errMsg,String scenario) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()	
		.clickMain()
		.enterRequiredAdminName(AdminName)
		.enterRequiredEmail(Email)
		.enterRequiredPassword(password)
		.verifyInvalidRegisterDomainCreation(domain,errMsg, scenario);
		
	}

}
