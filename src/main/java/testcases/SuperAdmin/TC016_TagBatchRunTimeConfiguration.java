package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC016_TagBatchRunTimeConfiguration extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC016_TagBatchRunTimeConfiguration";
		testDescription = "Tag Batch Run Time Configuration Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "UserBatchSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void tagBatchConfiguration(String data, String uName, String pwd, String domain, String days, String hours, String mins,
			                  String runType, String selectDomain, String successMsg) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomain(domain)
		.clickTagBatchActiveAsYes()
		.clickResetTagBatch() 
		.clickTagBatchSchedule()
		.selectUserRuntimeConfigurationAsDaily()
		.selectTheDay(days, hours, mins) 
		.clickOkButton() 
	    .tagsConfiguration() 
		.clickSubmitButton()
	 // .verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.verifySuccessMsgOfRunBatch(successMsg);  
		
	}

}


















