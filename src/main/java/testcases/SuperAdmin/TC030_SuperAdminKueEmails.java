package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC030_SuperAdminKueEmails extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC030_SuperAdminKueEmails";
		testDescription = "Super-Admin Kue Emails Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "KueEmails";
	}
   
	@Test(dataProvider = "fetchData")	
	public void superAdminEmailValidation(String data, String uName, String pwd, String domain, String source) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
	    .clickKue()
	    .clickKueEmails()
	    .enterKueEmailDomain(domain)
	    .enterKueEmailSource(source)
	    .clickKueEmailSearchButton()
	    .verifyNumberOfRecordsOfKueEmails(source);
	}
 }





