package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC003_SuperAdminInvaliedLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC003_SuperAdminInvaliedLogin";
		testDescription = "Login testCase using Invalied Credentials";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors ="Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "InvaliedSuperAdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminInvaliedLoginCheck(String data, String uName, String pwd , 
			String errorMsg, String scenario) throws InterruptedException {
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data) 
		.enterUserNameWithTab(uName)
		.enterPasswordWithTab(pwd)
		.verifyInvalidLogin(errorMsg, scenario);
	}

}





