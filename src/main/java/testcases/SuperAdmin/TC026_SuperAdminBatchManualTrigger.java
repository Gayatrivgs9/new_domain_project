package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC026_SuperAdminBatchManualTrigger extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC026_SuperAdminBatchManualTrigger";
		testDescription = "Super-Admin All Batch Manual Trigger Configuration Functionality";
		testCaseStatus = "PASS";		
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "UserBatchSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void superadminBatchManualTrigger(String data, String uName, String pwd, String domain, String days, String hours, String mins,
			                  String runType, String selectDomain, String successMsg) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomainToTrigger(domain)
		.selectUserBatchActiveAsYes()
		.clickUserBatchTrigger()
		.selectHistoricalBatchActiveAsYes()
		.clickHistoricalBatchTrigger()
		.selectNewDataBatchActiveAsYes()
		.clickNewDataBatchTrigger()
		.selectUserBatchActiveAsYes()
		.clickSuggestionBatchTrigger()
		.clickTagBatchActiveAsYes()
		.clickTagBatchTrigger()
		.clickAIMLPredectionBatchAsYes() 
		.clickAimlPredectionBatchTrigger()	
		//.clickSubmitButton()
	    //.verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.verifySuccessMsgOfRunBatch(successMsg); 
		
	}

}

















