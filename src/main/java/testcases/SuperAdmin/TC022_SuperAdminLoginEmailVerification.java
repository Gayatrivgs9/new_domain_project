package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC022_SuperAdminLoginEmailVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC022_SuperAdminLoginEmailVerification";
		testDescription = "SuperAdmin Email Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "SuperAdminEmail";
	}
   
	@Test(dataProvider = "fetchData")	
	public void superAdminEmailValidation(String data, String uName, String pwd, String email) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickSuperAdminDropdown()
		.superAdminEmailVerification(email)
		.clickSuperAdminLogout();	
	}

}





