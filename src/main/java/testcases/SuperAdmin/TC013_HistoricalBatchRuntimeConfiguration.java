package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC013_HistoricalBatchRuntimeConfiguration extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC013_HistoricalBatchRuntimeConfiguration";
		testDescription = "Historical Batch Runtime configuration Functionality";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "HistoricalBatchSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void historicalBatchRuntimeConfiguration(String data, String uName, String pwd, String domain, String hours, String mins,
			                  String runType,  String selectDomain, String successMsg) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomain(domain)
		.selectHistoricalBatchActiveAsYes()
		.clickResetHistoricalBatch()
		.clickHistoricalSchedule()
		.selectHistoricalBatchRunConfigurationAsWeekly(hours, mins)
		.clickOkButton()  
		.clickSubmitButton()
	    //.verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.verifySuccessMsgOfRunBatch(successMsg); 
		
	}

}


















