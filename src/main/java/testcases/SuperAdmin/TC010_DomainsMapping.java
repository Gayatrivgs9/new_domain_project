package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC010_DomainsMapping extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC010_DomainsMapping";
		testDescription = "SuperAdmin Domains System Mapping";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "DomainsMapping";
	}

	@Test(dataProvider = "fetchData")	
	public void domainsMapping(String urlData, String uName, String pwd, String domain, 
			String tableData, String lovLevel, String data, String result) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.clickMain()
		.enterMapSystemSelectDomain(domain) 
		.enterMapSystemSelectTable(tableData)
		.enterMapSystemSelectSource()
		.enterLogLevel(lovLevel)
		.moveLogTTLSlider() 
		.clickMapSystmRegister()
		.verifyMapSystemForDomainsCreation(data, result);
		
		
	}

}
