package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC027_SuperAdminClientsList extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC027_SuperAdminClientsList";
		testCaseStatus = "PASS";
		testDescription = "Super-Admin Clients List Varification";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "DomainCreation";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminClientsList(String data, String uName, String pwd, String elementName, String elementValue, 
			String domain, String result) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.verifyConfigurationPageTitle()
		.clickConfiguration()
		.clickClientsList()
		.clickAddElementButton()
		.enterElementName(elementName)
		.enterElementValue(elementValue)
		.selectDomain(domain)
		.clickSubmitButton(); 
		
	}

}




















