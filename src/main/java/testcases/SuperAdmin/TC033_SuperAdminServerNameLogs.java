package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.CommonHomePage;
import pages.SuperAdmin.LoginPage;

public class TC033_SuperAdminServerNameLogs extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC033_SuperAdminServerNameLogs";
		testDescription = "Check the Super-Admin Log Level Verification With ServerName";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "ServerNameLogs";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminServerNameLogs(String data, String uName, String pwd,String limit, String log,
			String logLevel, String successMsg) throws Exception {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new CommonHomePage(driver, test)
		.clickLogs()
		.enterLimit(limit)
		.enterDomainName(data) 
		.selectStartEndDate()
		.enterFilterByColumn(log, logLevel)
		.clickGoButton()
		.verifySuccessMsg(successMsg)
		.readTableData();	
		
		
	}
}





