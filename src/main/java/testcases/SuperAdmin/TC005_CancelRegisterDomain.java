package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC005_CancelRegisterDomain extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC005_CancelRegisterDomain";
		testDescription = "Cancel The Registered Domain";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "DomainCreation";
	}

	@Test(dataProvider = "fetchData")	
	public void registerDomain(String data, String uName, String pwd, String db, String domain, String rowNum, String result) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.verifyConfigurationPageTitle()
		.clickConfiguration()
		.clickMain()
		.enterDB(db)
		.enterDomain(domain)
		.clickCancelDomain();
		
	}

}




















