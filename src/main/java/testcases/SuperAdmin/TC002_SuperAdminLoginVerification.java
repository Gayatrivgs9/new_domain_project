package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC002_SuperAdminLoginVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC002_SuperAdminLoginVerification";
		testDescription = "Login testCase using Valid Credentials Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "SuperAdminVerification";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminLoginVrification(String data, String uName, String pwd, String profile, String settings, String logout, 
			String copyRights, String privacy, String disclaimer, String termOfUse) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton() 
		.clickSuperAdminDropdown()
		//.verifySuperAdminProfileOpt(profile)
		//.verifySuperAdminSettingsOpt(settings)
		.verifySuperAdminLogoutOpt(logout)
		.clickSidebarToggler() 
		.verifyCopyRightOpt(copyRights, privacy, disclaimer, termOfUse) 
		.clickSuperAdminSifyCorp();	
	}

}





