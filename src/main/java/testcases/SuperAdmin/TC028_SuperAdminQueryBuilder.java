package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC028_SuperAdminQueryBuilder extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC028_SuperAdminQueryBuilder";
		testDescription = "SuperAdmin Query Builder Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "QueryBuilder";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminQueryBuilder(String data, String uName, String pwd, String domain, String operator,
			String query) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickQueryBuilder()	
		.performQuery(domain, operator, query);
	}
}











