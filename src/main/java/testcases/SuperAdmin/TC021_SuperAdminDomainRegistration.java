package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC021_SuperAdminDomainRegistration extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC021_SuperAdminDomainRegistration";
		testDescription = "Register SuperAdmin";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "SuperAdminCreation";
	}

	@Test(dataProvider = "fetchData")	
	public void registerAdmin(String data, String uName, String pwd, String dbName, String domainName, String AdminName, String Email,
			  String password, String domain, String domainNew, String tableData) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.enterDB(dbName)
		.enterDomain(domainName)
		.clickAddDomain()	
		.enterAdminName(AdminName)
		.enterEmail(Email)
		.enterPassword(password)
		.enterSelectDomain(domain) 
		.clickRegister()
		.enterMapSystemSelectDomain(domainNew) 
		.enterMapSystemSelectTable(tableData)
		.enterMapSystemSelectSource()
		.clickMapSystmRegister();
		
	}

}




















