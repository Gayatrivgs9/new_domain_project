package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC020_OtherSettingsAddSource extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC020_OtherSettingsAddSource";
		testDescription = "Super Admin Other settings";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "OtherSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void otherSettings(String data, String uName, String pwd, String domainUrl, String sourceName, String postUrl, String getUrl,
			                  String aimlTagUrl) throws InterruptedException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickOtherSettings()
		.enterDomainURL(domainUrl)
		.clickSaveButton()
		.enterSourceName(sourceName)
		.clickRegister()
		.enterAIMLPostUrl(postUrl)
		.clickAIMLPostUrlSaveButton()
		.enterAIMLgetUrl(getUrl)
		.clickAIMLgetUrlSaveButton()
		.enterAIMLTagUrl(aimlTagUrl)
		.clickAIMLTagUrlSaveButton(); 
		
	}

}




















