package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC011_DomainsMappingInValiedData extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC011_DomainsMappingInValiedData";
		testDescription = "Domains Mapping InValied Data";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "DomainsMappingInValiedData";
	}
    
	@Test(dataProvider = "fetchData")	
	public void domainsMappingInvalied(String data, String uName, String pwd, String domain, 
			String tableData, String source, String logLevel, String errorMsg, String scenario) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName) 
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration() 
		.clickMain()
		.verifyInvalidMapSystemForDomains(domain, tableData, source, logLevel, errorMsg, scenario);

	}




}
