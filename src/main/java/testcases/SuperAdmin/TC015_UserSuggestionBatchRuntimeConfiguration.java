package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC015_UserSuggestionBatchRuntimeConfiguration extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC015_UserSuggestionBatchRuntimeConfiguration";
		testDescription = "User Suggestion Batch Runtime configuration Functionality";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "NewDataBatchSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void userSuggestionBatchRuntimeConfiguration(String data, String uName, String pwd, String domain, String advanced,
			                  String runType, String selectDomain, String successMsg) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomain(domain)
		.selectUserSuggestionBatchActiveAsYes()
		.clickResetUserSuggestionBatch() 
		.clickUserSuggestionSchedule()
		.selectUserSuggestionBatchRunConfigurationAsAdvanced(advanced)
		.clickOkButton() 
		.clickSubmitButton()
	   // .verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.verifySuccessMsgOfRunBatch(successMsg); 
		
	}

}


















