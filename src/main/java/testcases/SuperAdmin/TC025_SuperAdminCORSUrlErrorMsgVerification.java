package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC025_SuperAdminCORSUrlErrorMsgVerification extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC025_SuperAdminCORSUrlErrorMsgVerification";
		testDescription = "SuperAdmin CORS Url ErrorMsg Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "CORSErrorMsg";
	}
   
	@Test(dataProvider = "fetchData")	
	public void superAdminEmailValidation(String data, String uName, String pwd, String url, String errorMsg) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)  
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.clickCors()
		.enterInputUrl(url)
		.clickAddButton()
		.verifyErrorMsg(errorMsg); 
	}
 }





