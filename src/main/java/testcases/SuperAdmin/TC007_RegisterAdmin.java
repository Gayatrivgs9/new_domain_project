package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC007_RegisterAdmin extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC007_RegisterAdmin";
		testDescription = "Register a new Admin";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Sanity";
		dataSheetName = "DataSheet";
		sheetName = "AdminCreation";
	}

	@Test(dataProvider = "fetchData")	
	public void registerAdmin(String data, String uName, String pwd, String AdminName, String Email, String password, String domain,
			                  String rowNum, String status) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.clickMain()
		.enterAdminName(AdminName)
		.enterEmail(Email)
		.enterPassword(password)
		.enterSelectDomain(domain) 
		.clickRegister()
		.verifyAdminCreation(rowNum, status);
		
	}

}
