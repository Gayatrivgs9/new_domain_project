package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC018_MasterDataBatchRunTimeConfiguration extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC018_MasterDataBatchRunTimeConfiguration";
		testDescription = "Master Data Batch Runtime configuration Functionality";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Regression";
		dataSheetName = "DataSheet";
		sheetName = "MasterDataBatchSettings";
	}

	@Test(dataProvider = "fetchData")	
	public void userBatchRuntimeConfiguration(String data, String uName, String pwd, String domain, String days, String hours, String mins,
			                  String runType, String selectDomain, String successMsg) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickBatchSettings()
		.enterSelectDomain(domain)
		.clickMasterDataBatchReset() 
		.clickMasterDataBatchEditButton()  
		.isActiveAsYes()
		.clickEditSchedule()
		.selectUserRuntimeConfigurationAsDaily()
		.selectTheDay(days, hours, mins)
		.clickEditOkButton()
		.clickOkButton() 
		.clickSubmitButton()
	    .verifyBatchConfigurationSuccessMsg()
		.enterRunType(runType)
		.selectTheDomain(selectDomain)
		.clickRunBatch()
		.verifySuccessMsgOfRunBatch(successMsg)
		.clickTransactionDataBatchEdit()
		.isActiveAsNo()
		.clickOkButton()
		.scheduleStatusVerification(); 
		
	}

}


















