package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC029_SuperAdminKueJobs extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC029_SuperAdminKue";
		testDescription = "Super-Admin Kue Verification";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "KueJobs";
	}
   
	@Test(dataProvider = "fetchData")	
	public void superAdminKueJobs(String data, String uName, String pwd, String domain, String status, String totalRecords) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
	    .clickKue()
	    .clickKueJobs()
	    .enterKueJobsDomain(domain)
	    .enterKueJobsStatus(status)
	    .enterKueJobsTotalRecords(totalRecords);
	      
	}
 }





