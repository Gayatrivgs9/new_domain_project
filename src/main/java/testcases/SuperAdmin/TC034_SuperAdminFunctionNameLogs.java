package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.CommonHomePage;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC034_SuperAdminFunctionNameLogs extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC034_SuperAdminFunctionNameLogs";
		testDescription = "Check the Super-Admin Function Name Log Level Verification";
		testCaseStatus = "PASS";
		nodes = "Admin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "FunctionNameLogs";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminFunctionNameLogs(String data, String userEmail, String userPwd, 
			String uName, String pwd,String limit, String log, String logLevel,
			String successMsg) throws Exception {
		//User action
		new LoginPage(driver,test)		
		.startUserLogin(data)
		.enterUserName(userEmail)
		.enterPassword(userPwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.clickAchievement()
		.threadSleep(); 
		//Verification at super-admin
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data) 
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new CommonHomePage(driver, test)
		.clickLogs()
		.enterLimit(limit)
		.enterDomainName(data) 
		.selectStartEndDate()
		.enterFilterByColumn(log, logLevel)
		.clickGoButton()
		.verifySuccessMsg(successMsg)
		.readTableData();	
		
		
	}
}





