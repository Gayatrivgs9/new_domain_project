package testcases.SuperAdmin;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC001_SuperAdminLogin extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC001_SuperAdminLogin";
		testCaseStatus = "PASS";
		testDescription = "Login testCase using Valid Credentials";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "SuperAdminLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void superAdminLogin(String data, String uName, String pwd) throws InterruptedException {
		new LoginPage(driver, test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName) 
		.enterPassword(pwd)
		.clickLoginButton()
		.verifySuperAdminHeaderTitle() 
		.clickSuperAdminDropdown()
		.clickSuperAdminLogout();	
	}

}





