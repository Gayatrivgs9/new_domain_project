package testcases.SuperAdmin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;

public class TC006_RegisterDomainInvaliedData extends PreAndPost{

	@BeforeTest
	public void setValues() {
		testCaseName = "TC006_RegisterDomainInvaliedData";
		testDescription = "Register a new Domain with invalied data";
		testCaseStatus = "PASS";
		nodes = "SuperAdmin";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "DomainCreationInvalied";
	}

	@Test(dataProvider = "fetchData")	
	public void registerDomainInvalied(String data, String uName, String pwd, String DB, String Domain,
			 String errorMsg, String scenario) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		new LoginPage(driver,test)		
		.startSuperAdminLogin(data)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton()
		.clickConfiguration()
		.clickMain()
		.enterDB(DB)
		.enterDomain(Domain)
		.clickAddDomain()	
		.verifyInvalidDomainCreation(errorMsg, scenario); 
		
	}

}
