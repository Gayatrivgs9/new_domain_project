package pages.SuperAdmin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LogsPage extends PreAndPost {

	public LogsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	
	public LogsPage enterLimit(String data) {	
		type(locateElement("xpath", "LogsPage.xpath.limit"), data);
		return this;
	}	
	public LogsPage enterDomainName(String data) {
		click(locateElement("xpath", "LogsPage.xpath.clearDomain"));
		click(locateElement("xpath", "Logs.xpath.domainNameFilter"));
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		return this;
	}
	public LogsPage selectStartEndDate() throws Exception {
		click(locateElement("xpath", "LogsPage.xpath.calender"));  
		Thread.sleep(2000);
		DateFormat dateFormat = new SimpleDateFormat("dd");
		Date date = new Date();

		String todaysDate = dateFormat.format(date);
		int sData = Integer.parseInt(todaysDate);
		int startDate = (sData-2);  
		System.out.println("Start Date : "+startDate); 
		click(locateElementWithoutProp("xpath", "//td[text()='"+startDate+"']"));
		click(locateElementWithoutProp("xpath", "//td[text()='"+sData+"']"));
		click(locateElement("xpath", "LogsPage.xpath.apply"));
		return this;
	}
	public LogsPage clickGoButton() {
		click(locateElement("xpath", "LogsPage.xpath.Go"));
		return this;
	}
	public LogsPage enterFilterWithLevel() {
		click(locateElement("xpath", "LogsPage.xpath.filterWithLevel"));
		click(locateElement("xpath", "LogsPage.xpath.info"));  
		return this;
	}
	public LogsPage enterFilterWithMessage() {
		click(locateElement("xpath", "LogsPage.xpath.filterWithMessage"));
		click(locateElement("xpath", "LogsPage.xpath.result"));  
		return this;
	}
	public LogsPage clickFilter() {
		click(locateElement("xpath", "LogsPage.xpath.filter"));
		return this;
	}
	public LogsPage enterFilterByColumn(String data, String logLevel) throws InterruptedException {
		click(locateElement("xpath", "LogsPage.xpath.filterByColumn"));
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		Thread.sleep(1000); 
		type(locateElement("name", "LogsPage.name.colValue"), logLevel);
		return this;
	}
	public LogsPage verifySuccessMsg(String data) {
		waitUntilVisibilityOfWebElement("LogsPage.xpath.logsSuccessMsg");
		verifyExactText(locateElement("xpath", "LogsPage.xpath.logsSuccessMsg"), data);
		return this;
	}
	public LogsPage readTableData() {
		List<WebElement> allEles = locateElements("xpath", "LogsPage.xpath.logData");
		System.out.println(allEles.size()); 
		if(allEles.isEmpty() != true) {
		for (int i = 0; i < allEles.size(); i++) {
			String text = allEles.get(i).getText();
			reportStep("Filter record data is : "+text, "PASS"); 
		}
		} else {
			reportStep("Logs not generated", "FAIL"); 
		}
		return this;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
