package pages.SuperAdmin;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ConfigPage extends PreAndPost {

	public ConfigPage(RemoteWebDriver driver, ExtentTest test) {		
		this.driver = driver;
		this.test = test;	
	}

	public ConfigPage clickMain() {
		click(locateElement("xpath", "SuperAdminPage.xpath.main"));
		return this;
	}
	public CORSPage clickCors() {
		click(locateElement("xpath", "SuperAdminPage.xpath.cors"));
		return new CORSPage(driver, test); 
	}
	public ClientsPage clickClientsList() {
		click(locateElement("xpath", "SuperAdminPage.xpath.clientsList"));
		return new ClientsPage(driver, test);  
	}
	public ConfigPage enterDB(String data) {
		type(locateElement("id", "ConfigurtionPage.id.dbName"), data);
		return this;
	}
	public ConfigPage enterHostIP(String data) {
		type(locateElement("id", "ConfigurtionPage.id.hostIP"), data);
		return this;
	}
	public ConfigPage enterDomain(String data) {
		type(locateElement("id", "ConfigurtionPage.id.domain"), data);
		
		return this;
	}
	public ConfigPage clickAddDomain() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.addDomain"));
		//verifyDomain(data, result); 
		return this;
	}
	public ConfigPage clickCancelDomain() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.cancelBtnOfAddDomain"));
		Thread.sleep(2000);
		return this;
	}
	
	//***************************************************************************************
	public WebElement eleAdminNameReq;
	public WebElement eleEmailReq;
	public WebElement elePasswordReq;
	public WebElement eleDomainNameReq;
	public ConfigPage verifyInvalidRegisterDomainCreation(String domain, String errorMsg, String scenario) throws InterruptedException {		

		switch (scenario){
		case("1"): { 
			Thread.sleep(1000);
			enterRequiredSelectDomain(domain);
			clickRegister();
			eleAdminNameReq = locateElement("xpath", "ConfigurtionPage.xpath.adminReq");
			String text = eleAdminNameReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break; 
		}
		case("2"): {
			Thread.sleep(1000);
			enterRequiredSelectDomain(domain);
			clickRegister();
			eleEmailReq = locateElement("xpath", "ConfigurtionPage.xpath.emailReq");
			String text = eleEmailReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;

		}
		case("3"): 	{
			Thread.sleep(1000);
			enterRequiredSelectDomain(domain);
			clickRegister();
			elePasswordReq = locateElement("xpath", "ConfigurtionPage.xpath.passwordReq");
			String text = elePasswordReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Blank Username Login Check Verified", "PASS");
			}
			else{
				System.out.println("FAIL");
				reportStep("Issue with Blank Username Login Check", "FAIL");
			}
			break;
		}
		case("4"): {
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon1"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon1"));
			Thread.sleep(2000);
			clickRegister();
			Thread.sleep(2000);
			eleDomainNameReq = locateElement("xpath", "ConfigurtionPage.xpath.domainNameReq");
			String text = eleDomainNameReq.getText();
			System.out.println(text+" PASS");
			/*if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}*/
			break;
		}	
		case("5"): {
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon1"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon1"));
			clickRegister();
			eleAdminNameReq = locateElement("xpath", "ConfigurtionPage.xpath.adminReq");
			String text = eleAdminNameReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}	
		}
		return this;		
	}

	public WebElement eleDBNameReq;
	public WebElement eleHostIPReq;
	public WebElement eleDomainReq;
	public ConfigPage verifyInvalidDomainCreation(String errorMsg, String scenario) throws InterruptedException {		

		switch (scenario){
		case("1"): { 
			Thread.sleep(1000);
			eleDBNameReq = locateElement("xpath", "ConfigurtionPage.xpath.DBNameReq");
			String text = eleDBNameReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break; 
		}
		case("2"): {
			Thread.sleep(1000);
			eleHostIPReq = locateElement("xpath", "ConfigurtionPage.xpath.hostIPReq");
			String text = eleHostIPReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;

		}
		case("3"): 	{
			Thread.sleep(1000);
			eleDomainReq = locateElement("xpath", "ConfigurtionPage.xpath.domainReq");
			String text = eleDomainReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Blank Username Login Check Verified", "PASS");
			}
			else{
				System.out.println("FAIL");
				reportStep("Issue with Blank Username Login Check", "FAIL");
			}
			break;
		}
		case("4"): {
			click(locateElement("xpath", "ConfigurtionPage.xpath.addDomain")); 
			Thread.sleep(1000);
			eleHostIPReq = locateElement("xpath", "ConfigurtionPage.xpath.hostIPReq");
			String text = eleHostIPReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}	
		}
		return this;		
	}
	
	WebElement eleSelectDomainReq;
	WebElement eleTableReq;
	WebElement eleSourceReq;
	WebElement eleLogLevelReq;
	WebElement eleAll;
	public ConfigPage verifyInvalidMapSystemForDomains(String domain, String tableData, String source, String logLevel, String errorMsg, String scenario) throws InterruptedException {		

		switch (scenario){
		case("1"): { 
			Thread.sleep(2000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon2"));
			Thread.sleep(4000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon2"));
			enterMapSystemSelectTable(tableData);
			enterMapSelectSource(source);
			eleSelectDomainReq = locateElement("xpath", "ConfigurtionPage.xpath.selectDomainName");
			String text = eleSelectDomainReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break; 
		}
		case("2"): {
			Thread.sleep(1000);
			enterMapSystemDomain(domain);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon3"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon3"));
			enterMapSelectSource(source);
			eleTableReq = locateElement("xpath", "ConfigurtionPage.xpath.tableReq");
			String text = eleTableReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;

		}
		case("3"): 	{
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon2"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon2"));
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon3"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon3"));
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon4"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon4"));
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon5"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon5"));
			eleAll = locateElement("xpath", "ConfigurtionPage.xpath.selectDomainName");
			String text = eleAll.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Blank Username Login Check Verified", "PASS");
			}
			else{
				System.out.println("FAIL");
				reportStep("Issue with Blank Username Login Check", "FAIL");
			}
			break;
		}
		case("4"): {
			Thread.sleep(1000);
			enterMapSystemDomain(domain);
			//enterMapSystemSelectTable(tableData);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon4"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon4"));
			eleSourceReq = locateElement("xpath", "ConfigurtionPage.xpath.sourceAlsoReq");
			String text = eleSourceReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}case("5"): {
			Thread.sleep(1000);
			enterMapSystemDomain(domain);
			//enterMapSystemSelectTable(tableData);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon5"));
			Thread.sleep(1000);
			click(locateElement("xpath", "ConfigurtionPage.xpath.icon5"));
			eleLogLevelReq = locateElement("xpath", "ConfigurtionPage.xpath.logLevelReq");
			String text = eleLogLevelReq.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}
		}
		return this;		
	}


	//Register Domain Admin ***************************************************************************************
	public ConfigPage enterAdminName(String data) {
		type(locateElement("id", "ConfigurtionPage.id.adminName"), data);
		return this;
	}
	public ConfigPage enterRequiredAdminName(String data) {
		type(locateElement("id", "ConfigurtionPage.id.adminName"), data);
		return this;
	}
	public ConfigPage enterEmail(String data) {
		type(locateElement("name", "ConfigurtionPage.name.email"), data);
		return this;
	}
	public ConfigPage enterRequiredEmail(String data) {
		type(locateElement("name", "ConfigurtionPage.name.email"), data);
		return this;
	}
	public ConfigPage enterPassword(String data) {
		type(locateElement("name", "ConfigurtionPage.name.password"), data);
		return this;
	}
	public ConfigPage enterRequiredPassword(String data) {
		type(locateElement("name", "ConfigurtionPage.name.password"), data);
		return this;
	}
	public ConfigPage enterSelectDomain(String data) {
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.selectDomain"), data);
		return this;
	}
	public ConfigPage enterRequiredSelectDomain(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.selectDomain"), data);
		Thread.sleep(2000); 
		return this;
	}
	public ConfigPage clickRegister() throws InterruptedException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.register"));
		//Thread.sleep(5000);
		return this;
	} 
	public ConfigPage clickCancelRegisterAdmin() throws InterruptedException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.cancelBtnAdmin"));
		Thread.sleep(1000);
		return this;
	} 
	public ConfigPage enterMapSystemSelectDomain(String data) throws InterruptedException {
		Thread.sleep(2000);
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemSelectDomain"), data);
		return this;
	}
	public ConfigPage enterMapSystemSelectTable(String data) {
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemSelectTable"), data);
		return this;
	}
	public ConfigPage enterMapSystemSelectSource() {
		//click(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemSelectSource"));
		return this;
	}
	public ConfigPage enterLogLevel(String data) {
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.logLevel"), data);
		return this;
	}
	public ConfigPage moveLogTTLSlider() throws InterruptedException {
		moveSlider(locateElement("xpath","ConfigurtionPage.xpath.logTTL"));
		return this;
	}
	/*
	List<String[]> allData = csvReader.readAll(); 

	// print Data 
	for (String[] row : allData) { 
		int i =0; String[] c = new String[4];
		for (String cell : row) {
			c[i] = cell; 
			i++;
		} */
	public ConfigPage getSystemSelectSource() {
		int i = 1; 
		systemName = new  String[6];  
		List<WebElement> allElements = locateElements("xpath", "BaseliningPage.xpath.multipleSources"); 
		int size = allElements.size();
		System.out.println(size); 
		for (i = 0; i <allElements.size() ; i++) {
			String system = allElements.get(i).getText();
			systemName[i] = system; 
		}
		return this;
	}
	
	public ConfigPage clickMapSystmRegister() throws InterruptedException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemRegister"));
		return this;
	}
	public ConfigPage enterMapSystemDomain(String data) {
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemSelectDomain"), data);
		return this;
	}
	public ConfigPage enterMapSelectSource(String data) {
		typeValue(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemSelectSource"), data);
		return this;
	}
	//Domain Logs Settings**************************************************************************************
	public void enterRegisteredAdmin() {
		
	}
	//********************************************************************************************

	/*public ConfigPage selectDomain(String data) {
		type(locateElement("xpath", "ConfigurtionPage.xpath.selectDomain"), data);		
		String text=""; 
		try {
			text = eleDBList.getText();
		} catch (Exception e) {
			text = eleDBNotFound.getText();
		}		
		if((text).equals(data)){
			click(eleDBList);
		}
		else if((text).equals("No items found")){
			reportStep("The DB with name "+data+" is not available.","FAIL");
		}
		else	{
			List<WebElement> ddList = driver.findElements(By.xpath("//div[@class='ng-option']"));
			for(WebElement dd : ddList){
				text = dd.getText();
				if((text).equals(data))	{
					click(dd);
				}				
			}			
		}
		return this;
	}*/


	public ConfigPage verifyAdminCreation(String data, String result) throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {

		int rowNum = Integer.parseInt(data); 
		WebElement errorMsg = locateElement("xpath", "ConfigurtionPage.xpath.errorMsg");
		if(errorMsg.getText() != null) {
			String alertText = errorMsg.getText();
			reportStep(alertText, "PASS");
			result = "FAIL";
			writeData(8, rowNum, result, "AdminCreation", "DataSheet", "Red");
		} else {
			WebElement eleSuccess = locateElement("xpath", "ConfigurtionPage.xpath.successMsg");
			String alertText = eleSuccess.getText();
			reportStep(alertText, "PASS");
			result = "PASS";
			writeData(8, rowNum, "PASS", "AdminCreation", "DataSheet", "Green");
		}
		/*try {
			WebElement eleSuccess = locateElement("xpath", "ConfigurtionPage.xpath.successMsg");
			String alertText = eleSuccess.getText();
			reportStep(alertText, "PASS");				
			writeData(7, rowNum, "PASS", "AdminCreation", "Green");
		} catch (NoSuchElementException e) {
			try {
				WebElement eleError = locateElement("xpath", "ConfigurtionPage.xpath.errorMsg");
				String alertText = eleError.getText();
				reportStep(alertText, "FAIL");
				writeData(7, rowNum, "FAIL", "AdminCreation", "Red");
			} catch (NoSuchElementException e2) {
				writeData(7, rowNum, "FAIL", "AdminCreation", "Red");
				reportStep("Admin Creation Failed", "FAIL");					
			}				
		}*/
		return this;
	}

	public ConfigPage verifyDomain(String data, String result) throws EncryptedDocumentException, InvalidFormatException, IOException { 
		int rowNum = Integer.parseInt(data);
		WebElement eleError = locateElement("xpath", "ConfigurtionPage.xpath.errorMsg");
		if (eleError.getText() != null) {
			String alertText = eleError.getText();
			reportStep(alertText, "PASS");
			result = "FAIL";
			writeData(6, rowNum, result, "DomainCreation", "DataSheet", "Red"); 
		} else {
			WebElement eleSuccess = locateElement("xpath", "ConfigurtionPage.xpath.successMsg");
			String alertText = eleSuccess.getText();
			reportStep(alertText, "PASS");
			result = "PASS";
			writeData(6, rowNum, "PASS", "DomainCreation", "DataSheet", "Green");
		}
		return this;
	} 
	public ConfigPage verifyDomainNameAtDomainOption(String domainName) throws InterruptedException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.selectDomain"));
		Thread.sleep(1000);
		try {
			List<WebElement> elements = locateElements("xpath", "ConfigurtionPage.xpath.domainOptions");
			for (WebElement each : elements) {  
				String text = each.getText();
				if (domainName.equals(text)) {
					break;
				}
			}
			reportStep("Domain Name verified successfully. Domain Name is: "+domainName, "PASS");
		} catch (WebDriverException e) {
			reportStep("Probleam with Domain Name: "+domainName, "FAIL");
		}
		return this;
	}
	public ConfigPage verifyDomainNameAtMapSystem(String domainName) throws InterruptedException {
		click(locateElement("xpath", "ConfigurtionPage.xpath.mapSystemSelectDomain"));
		Thread.sleep(1000); 
		try {
		List<WebElement> elements = locateElements("xpath", "ConfigurtionPage.xpath.mappingDomainOptions");
		for (WebElement each : elements) {  
			String text = each.getText().trim();
			if (domainName.equals(text)) { 
				break;
			}
		}
			 reportStep("Domain Name verified successfully. Domain Name is: "+domainName, "PASS");
		} catch (WebDriverException e) {
			reportStep("Probleam with Domain Name: "+domainName, "FAIL");
		
		}
		return this;
	}
	public ConfigPage verifyDomainCreation(String data) throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {

		WebElement eleSuccess = locateElement("xpath", "ConfigurtionPage.xpath.successMsg");
		WebElement eleError = locateElement("xpath", "ConfigurtionPage.xpath.errorMsg");
		int rowNum = Integer.parseInt(data);
		if(eleSuccess.getText() != null) { 
			String alertText = eleSuccess.getText();
			System.out.println(alertText); 
			reportStep(alertText, "PASS");
			writeData(6, rowNum, "PASS", "DomainCreation", "DataSheet", "Green");	
		} else {
			try {
				String alertText = eleError.getText();
				System.out.println(alertText); 
				reportStep(alertText, "FAIL");
				writeData(6, rowNum, "FAIL", "DomainCreation", "DataSheet", "Red");					
			} catch (NoSuchElementException e2) {
				writeData(6, rowNum, "FAIL", "DomainCreation", "DataSheet", "Red");
				reportStep("Domain Creation Failed", "FAIL");					
			}
		}
		return this;
		/*try {
			WebElement eleSuccess = locateElement("xpath", "ConfigurtionPage.xpath.successMsg");
			String alertText = eleSuccess.getText();
			System.out.println(alertText); 
			reportStep(alertText, "PASS");
			writeData(6, rowNum, "PASS", "DomainCreation", "Green");				
		} catch (NoSuchElementException e) {
			try {
				WebElement eleError = locateElement("xpath", "ConfigurtionPage.xpath.errorMsg");
				String alertText = eleError.getText();
				System.out.println(alertText); 
				reportStep(alertText, "FAIL");
				writeData(6, rowNum, "FAIL", "DomainCreation", "Red");					
			} catch (NoSuchElementException e2) {
				writeData(6, rowNum, "FAIL", "DomainCreation", "Red");
				reportStep("Domain Creation Failed", "FAIL");					
			}
		}
		return this;*/
	}

	public ConfigPage verifyMapSystemForDomainsCreation(String data, String result) throws InterruptedException, IOException, EncryptedDocumentException, InvalidFormatException {
		WebElement eleSuccess = locateElement("xpath", "ConfigurtionPage.xpath.domainSuccessMsg");
		String text = eleSuccess.getText();
		int rowNum = Integer.parseInt(data); 
		if(text != null) {
			//String alertText = eleSuccess.getText();
			reportStep(text, "PASS");
			result = "PASS";
			writeData(6, rowNum, result, "DomainsMapping", "DataSheet", "Green");
		} else {
			WebElement errorMsg = locateElement("xpath", "ConfigurtionPage.xpath.errorMsg");
			String alertText = errorMsg.getText();
			reportStep(alertText, "PASS");
			result = "FAIL";
			writeData(6, rowNum, result, "DomainsMapping", "DataSheet", "Red");
		}
		return this;
	}

	/*public ConfigPage validateDBNameField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{
		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleDB, eleAddDomain, eleDBError);		
		}
		return this;
	}

	public ConfigPage validateHostIPField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{
		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleHost, eleAddDomain, eleHostError);		
		}
		return this;
	}

	public ConfigPage validateDomainField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{
		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleDomain, eleAddDomain, eleDomainError);		
		}
		return this;
	}

	public ConfigPage validateAdminNameField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{

		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleAdminName, eleRegister, eleAdminNameError);
		}

		return this;
	}




	public ConfigPage validateEmailField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{
		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleEmail, eleRegister, eleEmailError);		
		}
		return this;
	}

	public ConfigPage validatePwdField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{
		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleAdminPwd, eleRegister, elePwdError);		
		}
		return this;
	}

	public ConfigPage validateSelectDomainField(String Field) throws EncryptedDocumentException, InvalidFormatException, IOException{
		if(!Field.equalsIgnoreCase("NA"))
		{
			validateFieldCC(Field, "FieldLevel", eleSelectDomain, eleRegister, eleSelectDomainError);		
		}
		return this;
	}
	 */



}
