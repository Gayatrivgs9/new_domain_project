package pages.SuperAdmin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class KuePage extends PreAndPost{

	public KuePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public KuePage clickKueJobs() {
		click(locateElement("xpath", "Kue.xpath.kueJobs"));   
		return this;
	}	
	public KuePage clickKueEmails() {
		click(locateElement("xpath", "Kue.xpath.kueEmails")); 
		return this;
	}	
	public KuePage enterKueJobsDomain(String domain) {
		click(locateElement("xpath", "Kue.xpath.domainKue"));
		typeValue(locateElement("xpath", "Kue.xpath.domainKue"), domain);
		return this;
	}
	public KuePage enterKueJobsStatus(String status) {
		click(locateElement("xpath", "Kue.xpath.statusKue"));
		typeValue(locateElement("xpath", "Kue.xpath.statusKue"), status);
		return this;
	}
	public KuePage enterKueJobsTotalRecords(String totalRecords) {
		click(locateElement("xpath", "Kue.xpath.totalRecordsKue"));  
		typeValue(locateElement("xpath", "Kue.xpath.totalRecordsKue"), totalRecords);
		return this; 
	}
	public KuePage enterKueEmailDomain(String domain) {
		typeValue(locateElement("xpath", "Kue.xpath.emailDomainKue"), domain);
		return this;
	}
	public KuePage enterKueEmailSource(String data) {
		typeValue(locateElement("xpath", "Kue.xpath.sourceKue"), data);
		return this;  
	}
	public KuePage clickKueEmailSearchButton() {
		click(locateElement("xpath", "Kue.xpath.searchButtonKue"));
		return this; 
	}
	public KuePage verifyNumberOfRecordsOfKueEmails(String source) throws InterruptedException {
		Thread.sleep(2000); 
		List<WebElement> allElements = locateElements("xpath", "Kue.xpath.numOfRecords");
		if(allElements.isEmpty() != true) {
			click(locateElement("xpath", "Kue.xpath.step-forward"));
			Thread.sleep(2000);
			String text = getText(locateElement("xpath", "Kue.xpath.courseCompRecord"));
			reportStep(source+" number of total records is: "+text, "PASS");
		} else {
			reportStep(getText(locateElement("xpath", "Kue.xpath.emailNoRecordsFoundKue")), "PASS");
		}
		return this;  
	}
	
	/*public KuePage verifyTheEmailsRecords() {
		if(locateElement("xpath", "Kue.xpath.emailNoRecordsFoundKue").getTagName().equals("")) {

		} else {
			String text = getText(locateElement("xpath", "Kue.xpath.emailNoRecordsFoundKue"));
			reportStep(text, "PASS"); 
		}
		return this; 
	}*/
	/*public KuePage enterEmailDomain(String domain) {
		click(locateElement("xpath", "Kue.xpath.domainKue"));
		typeValue(locateElement("xpath", "Kue.xpath.domainKue"), domain);
		return this;
	}*/
}

























