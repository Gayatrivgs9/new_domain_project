package pages.SuperAdmin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class QueryBuilderPage extends PreAndPost{

	public QueryBuilderPage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public QueryBuilderPage enterDomainName(String data) {
		WebElement eleDomain= locateElement("xpath", "QueryBuilderPage.xpath.selectDomainNew");
		type(eleDomain, data);
		return this;
	}	
	public QueryBuilderPage enterAdminSettings(String data) throws InterruptedException {
		Thread.sleep(1000);
		WebElement eleInput2 = locateElement("xpath", "QueryBuilderPage.xpath.input2");
		type(eleInput2, data); 
		return this;
	}	
	public QueryBuilderPage enterOperator(String data) {
		WebElement eleOperator = locateElement("xpath", "QueryBuilderPage.xpath.input3");
		type(eleOperator, data); 
		return this;
	}	
	public QueryBuilderPage enterQuery(String data) {
		type(locateElement("xpath", "QueryBuilderPage.xpath.query"), data); 
		return this; 
	}	
	public QueryBuilderPage enterData(String data) {
		type(locateElement("xpath", "QueryBuilderPage.xpath.dataNew"), data);
		return this; 
	}
	public QueryBuilderPage enterOption(String data) {
		type(locateElement("xpath", "QueryBuilderPage.xpath.optionsNew"), data);
		return this; 
	}
	public QueryBuilderPage clickRunButton() {
		click(locateElement("xpath", "QueryBuilderPage.xpath.runButton"));
		return this;  
	}

	public QueryBuilderPage performQuery(String domain, String operator, String query) throws InterruptedException {
		List<String> listData = new ArrayList<String>(Arrays.asList("users", "roledatas", "skilldatas", "coursedatas")); 

		for (int i = 0; i < listData.size(); i++) {
			WebElement eleDomain= locateElement("xpath", "QueryBuilderPage.xpath.selectDomainNew");
			typeValue(eleDomain, domain); 
			//Admin value
			typeValue(locateElement("xpath", "QueryBuilderPage.xpath.input2"), listData.get(i)); 
			//remove
			WebElement eleOperator = locateElement("xpath", "QueryBuilderPage.xpath.input3");
			typeValue(eleOperator, operator);
			//query
			typeValueWithTab(locateElement("xpath", "QueryBuilderPage.xpath.query"), query);
			Thread.sleep(1000);
			click(locateElement("xpath", "QueryBuilderPage.xpath.runButton"));
			Thread.sleep(2000);
		}
		return this;
	}
	public QueryBuilderPage performQueryForAll(String domain, String operator, String query) throws InterruptedException {
		List<String> listData = new ArrayList<String>(Arrays.asList("users","roledatas","skilldatas", "coursedatas","userbaselines",
				"rolebaselines","skillbaselines","coursebaselines",	"domainsettings", "templates", "formdatas","programbaselines",
				"emails","acls","programdatas","notifications","admins","foreignfields","competencydatas","logs","formmasters","mailsettings")); 

		for (int i = 0; i < listData.size(); i++) {
			WebElement eleDomain= locateElement("xpath", "QueryBuilderPage.xpath.selectDomainNew");
			typeValue(eleDomain, domain); 
			//Admin value
			typeValue(locateElement("xpath", "QueryBuilderPage.xpath.input2"), listData.get(i)); 
			//remove
			WebElement eleOperator = locateElement("xpath", "QueryBuilderPage.xpath.input3");
			typeValue(eleOperator, operator);
			//query
			typeValueWithTab(locateElement("xpath", "QueryBuilderPage.xpath.query"), query);
			Thread.sleep(1000);
			click(locateElement("xpath", "QueryBuilderPage.xpath.runButton"));
			Thread.sleep(2000);
		}
		return this;
	}
	public QueryBuilderPage performQuery(String domain, String operation, String query, String data, String option) throws InterruptedException {
		List<String> listData = new ArrayList<String>(Arrays.asList("users", "roledatas", "skilldatas", "coursedatas")); 

		for (int i = 0; i < listData.size(); i++) {
			WebElement eleDomain= locateElement("xpath", "QueryBuilderPage.xpath.selectDomainNew");
			typeValue(eleDomain, domain); 
			//Admin value
			typeValue(locateElement("xpath", "QueryBuilderPage.xpath.input2"), listData.get(i)); 
			Thread.sleep(1000);
			//remove
			WebElement eleOperator = locateElement("xpath", "QueryBuilderPage.xpath.input3");
			typeValue(eleOperator, operation);
			Thread.sleep(1000);
			//query
			typeValueWithTab(locateElement("xpath", "QueryBuilderPage.xpath.query"), query);
			Thread.sleep(2000);
			//data
			typeValueWithTab(locateElement("xpath", "QueryBuilderPage.xpath.dataNew"), data);
			//option
			typeValueWithTab(locateElement("xpath", "QueryBuilderPage.xpath.optionsNew"), option);
			//run
			click(locateElement("xpath", "QueryBuilderPage.xpath.runButton")); 
			Thread.sleep(2000);
		}
		return this;
	}



























}
