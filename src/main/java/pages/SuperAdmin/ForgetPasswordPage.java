package pages.SuperAdmin;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ForgetPasswordPage extends PreAndPost{

	public ForgetPasswordPage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public ForgetPasswordPage reLoadOfficePage(String email, String pwd) throws InterruptedException {  
		driver.navigate().to("https://www.office.com/?omkt=en-ca&auth=2");
		type(locateElement("name", "LoginPage.name.loginfmt"), email);
		click(locateElement("xpath", "LoginPage.xpath.butn"));
		type(locateElement("name", "LoginPage.name.passwd"), pwd); 
		click(locateElement("xpath", "LoginPage.xpath.butn"));
		click(locateElement("id", "LoginPage.id.btnNo"));
		Thread.sleep(7000); 
		return this;
	}
	public ForgetPasswordPage clickOutLook() throws InterruptedException {
		click(locateElement("id", "LoginPage.id.linkOutlook"));
		switchToWindow(1); 
		jsClick(locateElement("xpath", "LoginPage.xpath.inbox"));
		Thread.sleep(1000); 
		jsClick(locateElement("xpath", "LoginPage.xpath.others"));
		return this;  
	}
	public ForgetPasswordPage verifyResetMailSubject() throws InterruptedException {
		Thread.sleep(2000);
		List<WebElement> elements = locateElements("xpath", "LoginPage.xpath.Unread");
		for (WebElement eachElement : elements) {	
			String ele1 = eachElement.findElement(By.xpath(prop.getProperty("LoginPage.xpath.Unread"))).getAttribute("aria-label");
			String ele2 = eachElement.findElement(By.xpath(prop.getProperty("LoginPage.xpath.liveSify"))).getAttribute("aria-label");
			String ele3 = eachElement.findElement(By.xpath(prop.getProperty("LoginPage.xpath.linkResetForPassword"))).getAttribute("aria-label");
			/*String ele4 = eachElement.findElement(By.xpath(prop.getProperty("LoginPage.xpath.useBelowLink"))).getAttribute("aria-label");		*/
			if (ele1.contains("Unread") &&  ele2.contains("PAL Reset password for PAL login") && 
					ele3.contains("Please click on Reset Password to reset your login password") /*&& ele4.contains("Use the below link to reset your PAL Admin Password reset password"*/) {
				jsClick(locateElement("xpath", "LoginPage.xpath.Unread"));
				break;  
			} 	}
		// - PAL - qctestpal
		Thread.sleep(4000);
		return this; 
	}
	public ForgetPasswordPage loadForgrtPasswordLink() throws InterruptedException {
		/*String mailLink = locateElement("xpath", "LoginPage.xpath.allowText").getText();
		String replaceAll = mailLink.replace("[", "").replace("]", "").replace("reset password", ""); 
		String newLink ="https://"+replaceAll;
		System.out.println(newLink); 
		driver.navigate().to(newLink);*/ 
		//jsClick(locateElement("xpath", "LoginPage.xpath.allowText"));
		jsClick(locateElement("xpath", "LoginPage.xpath.allowText"));
		Thread.sleep(3000);
		if(allWindowHandles.size() == 3) {
			switchToWindow(2); 
		} else {
			jsClick(locateElement("xpath", "LoginPage.xpath.allowText"));
			Thread.sleep(3000);
			switchToWindow(2);
		}

		return this;		
	}
	public ForgetPasswordPage enterNewPassword(String data) throws InterruptedException {
		type(locateElement("id", "LoginPage.id.password"), data);
		type(locateElement("name", "LoginPage.name.confirmPassword"), data);
		Thread.sleep(2000);  
		jsClick(locateElement("xpath", "LoginPage.xpath.pwdResetButton"));  
		return this;
	}


}

















