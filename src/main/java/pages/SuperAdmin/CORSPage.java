package pages.SuperAdmin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CORSPage extends PreAndPost {

	public CORSPage(RemoteWebDriver driver, ExtentTest test) {		
		this.driver = driver;
		this.test = test;	
	}

	public CORSPage enterInputUrl(String data) {
		type(locateElement("xpath", "CORSPage.xpath.input"), data);
		return this;
	}
	public CORSPage clickAddButton() {
		click(locateElement("xpath", "CORSPage.xpath.addButton"));
		return this;
	}
	public CORSPage verifyErrorMsg(String data) {
		verifyExactText(locateElement("xpath", "CORSPage.xpath.dupUrl"), data);
		return this;
	}
	public CORSPage clickSaveButton() {
		click(locateElement("xpath", "CORSPage.xpath.saveButton"));
		return this;
	}
	public CORSPage verifyUrl(String data) {
		List<WebElement> urls = locateElements("xpath", "CORSPage.xpath.inputUrls");
		String text = "";
		String newText = "";
		for (int i = 0; i < urls.size(); i=i+2) {
			text = urls.get(i).getText();
		    newText =	verifyTwoStringValues(text, data);
		}	
		if(!newText.equals(text)) {
			click(locateElement("xpath", "CORSPage.xpath.iconForward"));  
		} 
		List<WebElement> urls2 = locateElements("xpath", "CORSPage.xpath.inputUrls");
		String text2 = "";
		for (int i = 0; i < urls2.size(); i=i+2) {
			text2 = urls2.get(i).getText();
			verifyTwoStringValues(text2, data);
		}
		return this;
	}
	public CORSPage verifySuccessMsg(String data) {
		verifyExactText(locateElement("xpath", "CORSPage.xpath.successMsg"), data);
		return this; 
	}
	public CORSPage verifyDuplicateUrlErrorMsg(String data) {
		verifyExactText(locateElement("xpath", "CORSPage.xpath.dupUrl"), data);
		return this; 
	}
	public CORSPage enterAccessKeyId(String data) {
		type(locateElement("xpath", "CORSPage.xpath.accessKey"), data);
		return this; 
	}
	public CORSPage enterSecretAccessKey(String data) {
		type(locateElement("xpath", "CORSPage.xpath.secretKey"), data);
		return this; 
	}	
	public CORSPage enterRegion(String data) {
		type(locateElement("xpath", "CORSPage.xpath.region"), data);  
		return this;
	}
	public CORSPage enterLoadBalancerTargetGroupArn(String data) {
		type(locateElement("xpath", "CORSPage.xpath.loadBalancerTarget"), data);
		return this;
	}
	public CORSPage enterProtocal(String data) {
		type(locateElement("xpath", "CORSPage.xpath.protocol"), data);
		return this;
	}
	public CORSPage enterAPIPart(String data) {
		type(locateElement("xpath", "CORSPage.xpath.protocol"), data);
		return this;
	}
	public CORSPage clickEnableAsYes() {
		WebElement enableYes = locateElement("xpath", "CORSPage.xpath.enableYes");
		if(enableYes.isSelected()!=true) {
			click(enableYes);
		}
		return this;
	}
	public CORSPage clickEnableAsNo() {
		WebElement enableNo = locateElement("xpath", "CORSPage.xpath.enableNo");
		if(enableNo.isSelected()!=true) {
			click(enableNo);
		}
		return this;
	}




}






















