package pages.SuperAdmin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ClientsPage extends PreAndPost {

	public ClientsPage(RemoteWebDriver driver, ExtentTest test) {		
		this.driver = driver;
		this.test = test;	
	}

	public ClientsPage clickAddElementButton() {
		click(locateElement("xpath", "SuperAdminPage.xpath.addElement"));
		return this;
	}
	public ClientsPage clickAddButton() {
		click(locateElement("xpath", "CORSPage.xpath.addElementButton"));
		return this;
	}
	public ClientsPage verifyErrorMsg(String data) {
		verifyExactText(locateElement("xpath", "CORSPage.xpath.dupUrl"), data);
		return this;
	}
	public ClientsPage clickSaveButton() {
		click(locateElement("xpath", "CORSPage.xpath.saveButton"));
		return this;
	}
	public ClientsPage enterElementName(String data) {
		type(locateElement("id", "ClientPage.id.elemname"), data);
		return this;
	}
	public ClientsPage enterElementValue(String data) {
		type(locateElement("id", "ClientPage.id.elemvalue"), data);  
		return this;
	}
	public ClientsPage selectDomain(String value) {
		selectDropDownUsingVisibleText(locateElement("id", "ClientPage.id.domainsname"), value);
		return this;
	}
	public ClientsPage clickSubmitButton() {
		click(locateElement("xpath", "ClientPage.xpath.submitBtn"));
		return this;
	}
	public ClientsPage clickCancelButton() {
		click(locateElement("xpath", "ClientPage.xpath.cancelBtn"));
		return this;
	}
}



























