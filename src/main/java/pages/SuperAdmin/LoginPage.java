package pages.SuperAdmin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LoginPage extends PreAndPost{

	public LoginPage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;
	}

	public WebElement eleSuperAdminLogin;
	public WebElement eleInvaliedUsername;
	public WebElement eleInvaliedPassword;
	public WebElement eleBlankUsername;
	public WebElement eleBlankPwd;
	public LoginPage startSuperAdminLogin() {	
		loadUrl("superAdmin.url");  
		return this;
	}
	
	public LoginPage startQcSuperAdminLogin() {	
		loadUrl("qcSuperAdmin.url");  
		System.out.println("URL loaded");
		return this;
	}
	
	public LoginPage startSuperAdminLogin(String data) {
		/*if (data.equals("qcSuperAdmin")) {  
			loadUrl("qcSuperAdmin.url");
		} else*/ if (data.equals("div1pal")) {  
			loadUrl("dev1palSA.url");
		} else if(data.equals("newdomain01")){
			loadUrl("newdomain01.sa.url");  
		}else if(data.equals("newdomain02")){
			loadUrl("newdomain02.sa.url");  
		}else if(data.equals("newdomain03")){
			loadUrl("newdomain03.sa.url");  
		}else if(data.equals("newdomain04")){
			loadUrl("newdomain04.sa.url");  
		}else if(data.equals("newdomain05")){
			loadUrl("newdomain05.sa.url");  
		}else if(data.equals("newdomain06")){
			loadUrl("newdomain06.sa.url");  
		}else if(data.equals("newdomain07")){
			loadUrl("newdomain07.sa.url");  
		}else if(data.equals("newdomain08")){
			loadUrl("newdomain08.sa.url");  
		}else if(data.equals("stgpal01")){
			loadUrl("stg.sa.url");   
		}else if(data.equals("palpoc1")){
			loadUrl("palpoc1.sa.url");   
		}
		
		return this;
	}
	public LoginPage startAdminLogin() {
		loadUrl("admin.url");
		return this;
	}
	public LoginPage startAdminLogin(String data) {
		if (data.contains("div1pal")) {  
			loadUrl("dev1palAdmin.url");
		} else if(data.contains("newdomain01")){
			loadUrl("newdomain01.admin.url");
		}else if(data.contains("newdomain02")){
			loadUrl("newdomain02.admin.url");
		}else if(data.contains("newdomain03")){
			loadUrl("newdomain03.admin.url");
		}else if(data.contains("newdomain04")){
			loadUrl("newdomain04.admin.url");
		}else if(data.contains("newdomain05")){
			loadUrl("newdomain05.admin.url");
		}else if(data.contains("newdomain06")){
			loadUrl("newdomain06.admin.url");
		}else if(data.contains("newdomain07")){
			loadUrl("newdomain07.admin.url");
		}else if(data.contains("newdomain08")){
			loadUrl("newdomain08.admin.url");
		}else if(data.contains("stgpal01")){
			loadUrl("stg.admin.url");
		}else if(data.equals("palpoc1")){
			loadUrl("palpoc1.admin.url");   
		}else {
			loadUrl("adminqc.url");
		}
		return this; 
	}
	public LoginPage startQcAdminLogin() {
		loadUrl("adminqc.url");
		System.out.println("URL loaded");
		return this;
	}
	
	public LoginPage startUserLogin() {	
		loadUrl("user.url");
		return this;
	}
	
	public LoginPage startQcUserLogin() {	
		loadUrl("qcUser.url");
		System.out.println("User URL launched");
		return this;
	}
	
	public LoginPage startUserLogin(String data) {

		if (data.contains("div1pal")) {  
			loadUrl("dev1palUser.url");
		} else if(data.contains("newdomain01")){
			loadUrl("newdomain01.user.url");
		}else if(data.contains("newdomain02")){
			loadUrl("newdomain02.user.url");
		}else if(data.contains("newdomain03")){
			loadUrl("newdomain03.user.url");
		}else if(data.contains("newdomain04")){ 
			loadUrl("newdomain04.user.url");
		}else if(data.contains("newdomain05")){ 
			loadUrl("newdomain05.user.url");
		}else if(data.contains("newdomain06")){
			loadUrl("newdomain06.user.url");
		}else if(data.contains("newdomain07")){
			loadUrl("newdomain07.user.url");
		}else if(data.contains("newdomain08")){
			loadUrl("newdomain08.user.url");
		}else if(data.contains("stgpal01")){ 
			loadUrl("stg.user.url");
		}else if(data.equals("palpoc1")){
			loadUrl("palpoc1.user.url");   
		} else {
			loadUrl("qcUser.url");
		}
		return this;
	}  
	public LoginPage verifyLogoImage() throws InterruptedException {
		Thread.sleep(3000);  
		verifyPartialAttribute(locateElement("xpath", "ThemePage.xpath.logoValidationPath"), "src", "https://testpalapi.sifylivewire.com/uploads/");
		return this;
	}
	public LoginPage verifyBackgroundImage() throws InterruptedException {
		Thread.sleep(1000);  
		verifyPartialAttribute(locateElement("xpath", "ThemePage.xpath.imgPath"), "style", "background-image: url(\"https://testpalapi.sifylivewire.com/uploads");
		return this;
	}
	/*public LoginPage selectDB(String data) {
		type(eleDBbox, data);		
		String text="";
		try {
			text = eleDB.getText();
		} catch (Exception e) {
			try {
				text = eleDBNotFound.getText();
			} catch (NoSuchElementException e2) {
			}
		}
		if((text).equals(data)){
			click(eleDB);
		}
		else if((text).equals("No items found")){
			reportStep("The DB with name "+data+" is not available.","FAIL");
		}	
		else{
			List<WebElement> ddList = driver.findElements(By.xpath("//div[@class='ng-option']"));
			for(WebElement dd : ddList)	{
				text = dd.getText();
				if((text).equals(data))	{
					click(dd);
					break;
				}				
			}			
		}
		return this;
	}*/

	public LoginPage enterUserName(String data) {
		WebElement eleUsername = locateElement("LoginPage.id.username");
		type(eleUsername, data);
		return this; 
	}	
	public LoginPage enterUserNameWithTab(String data) {
		WebElement eleUsername = locateElement("LoginPage.id.username");
		typeValueWithTab(eleUsername, data);
		return this; 
	}	
	public LoginPage enterPassword(String data) {
		WebElement elePassword = locateElement("LoginPage.id.password");
		type(elePassword, data);
		return this;
	}
	public LoginPage enterPasswordWithTab(String data) {
		WebElement elePassword = locateElement("LoginPage.id.password");
		typeValueWithTab(elePassword, data);
		return this;
	}
	public LoginPage clickForgetPassword() {
		click(locateElement("link", "LoginPage.link.forgetPwd"));
		return this;  
	}
	public CommonHomePage clickLoginButton() throws InterruptedException { 
		//Thread.sleep(2000); 
		click(locateElement("xpath", "LoginPage.xpath.signin"));
		return new CommonHomePage(driver,test);		
	}
	public ForgetPasswordPage verifyResetLink() throws InterruptedException {
		WebElement ele = locateElement("xpath", "LoginPage.xpath.signin");
		click(ele); 
		verifyPartialText(locateElement("xpath", "LoginPage.xpath.resetLink"), "Reset Link Sent to your mail");
		return new ForgetPasswordPage(driver, test); 
	}
	public LoginPage navigateToUser() throws InterruptedException {
		Thread.sleep(1000);
		driver.navigate().to("https://newdomain08.sifylivewire.com/user/login"); 
		return this;
	}
	public LoginPage navigateToUser03Domain() throws InterruptedException {
		Thread.sleep(5000); 
		driver.navigate().to("https://newdomain03.sifylivewire.com/user/login"); 
		return this;
	}
	public LoginPage navigateToAdmin() throws InterruptedException {
		Thread.sleep(1000);
		driver.navigate().to("https://newdomain04.sifylivewire.com/admin/login"); 
		return this;
	}
	public LoginPage waitUpToFoldingCubeInvisible() {
		if(locateElement("xpath", "LoginPage.xpath.foldingCube")!= null) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfAllElements((locateElement("xpath", "LoginPage.xpath.foldingCube"))));
		} else {
			System.out.println("FoldingCube is invisible");  
		}  
		return this;  
	}
	public LoginPage verifyCopyRightOpt(String data, String data2, String data3, String data4) throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleCopyright = locateElement("xpath", "SuperAdminPage.xpath.newCopyright");
		verifyPartialText(eleCopyright, data);
		//verifyPartialText(eleCopyright, data2);
		//verifyPartialText(eleCopyright, data3);
		//verifyPartialText(eleCopyright, data4);
		return this;
	}
	public LoginPage clickPrivacyPolicy() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("link", "SuperAdminPage.link.privacyPolicy"));
		return this;
	}
	public LoginPage clickDisclaimer() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("link", "SuperAdminPage.link.disclaimer"));
		return this;
	} 
	public LoginPage clickTermOfUse() throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("link", "SuperAdminPage.link.termOfUse"));
		return this;
	}
	public LoginPage verifyInvalidLogin(String errorMsg, String scenario) throws InterruptedException {		

		switch (scenario){
		case("1"): {
			click(locateElement("xpath", "LoginPage.xpath.signin"));
			eleInvaliedUsername = locateElement("xpath", "LoginPage.xpath.invaliedUsername");
			String text = eleInvaliedUsername.getText();
			System.out.println(text);
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}
		case("2"): {
			click(locateElement("xpath", "LoginPage.xpath.signin")); 
			eleInvaliedPassword = locateElement("xpath", "LoginPage.xpath.invaliedPassword");
			String text = eleInvaliedPassword.getText();
			System.out.println(text);
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;

		}
		case("3"): 	{
			Thread.sleep(1000);
			eleBlankUsername = locateElement("xpath", "LoginPage.xpath.blankUsername");
			String text = eleBlankUsername.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Blank Username Login Check Verified", "PASS");
			}
			else{
				System.out.println("FAIL");
				reportStep("Issue with Blank Username Login Check", "FAIL");
			}
			break;
		}
		case("4"): 	{
			/*Thread.sleep(1000);
			driver.getKeyboard().sendKeys(Keys.TAB);*/
			//Thread.sleep(1000);
			eleBlankPwd = locateElement("xpath", "LoginPage.xpath.blankPassword");
			String text = eleBlankPwd.getText();
			if(text.contains(errorMsg)){
				System.out.println("PASS");
				reportStep("Blank Password Login Check Verified", "PASS");
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Blank Password Invalid Login Check", "FAIL");
			}
			break;
		}
		case("5"): 	{	
			/*Thread.sleep(1000);
			driver.getKeyboard().sendKeys(Keys.TAB);
			Thread.sleep(1000);*/
			String error1 = locateElement("xpath", "LoginPage.xpath.blankUsername").getText();
			System.out.println(error1);
			String error2 = locateElement("xpath", "LoginPage.xpath.blankPassword").getText();
			System.out.println(error2);
			if((error1.contains(errorMsg))&&(error2.contains(errorMsg)))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}
		case("6"): { 
			//click(locateElement("xpath", "LoginPage.xpath.signin")); 
			WebElement eleInvaliedUsername = locateElement("xpath", "LoginPage.xpath.propermail");
			String text = eleInvaliedUsername.getText();
			if(text.contains(errorMsg))	{
				System.out.println("PASS");
				reportStep("Invalid Login Check Verified", "PASS");				
			}
			else	{
				System.out.println("FAIL");
				reportStep("Issue with Invalid Login Check", "FAIL");
			}
			break;
		}
		}
		return this;		
	}





}
