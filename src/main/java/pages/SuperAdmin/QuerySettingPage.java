package pages.SuperAdmin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class QuerySettingPage extends PreAndPost{

	public QuerySettingPage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public QuerySettingPage enterOperator(String data) {
		WebElement eleDomainURL = locateElement("xpath", "QuerySetting.xpath.addOperator");
		type(eleDomainURL, data); 
		return this;
	}	
	public QuerySettingPage clickAddButton() {
		WebElement eleSave = locateElement("xpath", "QuerySetting.xpath.addButton");
		click(eleSave); 
		return this;
	}
	public QuerySettingPage clickSubmit() {
		click(locateElement("xpath", "QuerySetting.xpath.submitButton")); 
		return this;
	}	
	
}










