package pages.SuperAdmin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CommonHomePage extends PreAndPost{

	public CommonHomePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}	

	public CommonHomePage waitUpToFoldingCubeInvisible() {
		if(locateElement("xpath", "LoginPage.xpath.foldingCube")!= null) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfAllElements((locateElement("xpath", "LoginPage.xpath.foldingCube"))));
		} else {
			System.out.println("FoldingCube is invisible");  
		}
		return this;  
	}
	public CommonHomePage verifySuperAdminHeaderTitle() {
		WebElement eleHead = locateElement("xpath", "SuperAdminPage.xpath.header");
		verifyExactText(eleHead, "Configurations");
		WebElement eleHeader = locateElement("xpath", "SuperAdminPage.xpath.headerTitle");
		verifyExactText(eleHeader, "Domain, Org-Admin configuration page");
		return this;
	}
	public CommonHomePage clickSuperAdminDropdown() {
		WebElement eleLogOutDropdown = locateElement("xpath", "UserPage.xpath.profiledropdown");
		click(eleLogOutDropdown); 
		return this;
	}
	public CommonHomePage clickAdminDropDown() throws InterruptedException {
		Thread.sleep(1000);
		WebElement eleLogOutDropdown = locateElement("xpath", "LoginPage.xpath.adminSignoutdropdown");
		click(eleLogOutDropdown);
		return this;
	}
	public CommonHomePage superAdminEmailVerification(String data) {
		verifyPartialText(locateElement("xpath", "ConfigurtionPage.xpath.emailVerify"), data); 
		return this;
	}
	public CommonHomePage verifySuperAdminProfileOpt(String data) {
		WebElement eleProfile = locateElement("link", "SuperAdminPage.link.profile");
		verifyExactText(eleProfile, data);
		return this;
	}
	public CommonHomePage clickUserProfile() {
		WebElement eleProfile = locateElement("link", "SuperAdminPage.link.profile");
		click(eleProfile);
		return this;
	}
	public CommonHomePage clickUserSettings() throws InterruptedException {
		click(locateElement("link", "SuperAdminPage.link.settings"));
		Thread.sleep(5000);  
		return this;
	}
	public CommonHomePage verifySuperAdminSettingsOpt(String data) {
		WebElement eleSettings = locateElement("link", "SuperAdminPage.link.settings");
		verifyExactText(eleSettings, data);
		return this;
	}
	public CommonHomePage verifySuperAdminLogoutOpt(String data) {
		WebElement eleLogout = locateElement("link", "SuperAdminPage.link.signoutOpt");
		verifyExactText(eleLogout, data);
		return this;
	}
	public CommonHomePage clickSidebarToggler() {
		click(locateElement("xpath", "SuperAdminPage.xpath.sidebar"));
		return this;
	}
	public CommonHomePage verifyUserLogoutOpt(String data) {
		WebElement eleLogout = locateElement("link", "SuperAdminPage.link.signoutOpt");
		verifyExactText(eleLogout, data);
		return this;
	}
	public CommonHomePage verifyCopyRightOpt(String data, String data2, String data3, String data4) throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleCopyright = locateElement("xpath", "SuperAdminPage.xpath.copyright");
		verifyPartialText(eleCopyright, data);
		verifyPartialText(eleCopyright, data2);
		verifyPartialText(eleCopyright, data3);
		verifyPartialText(eleCopyright, data4);
		return this;
	}
	
	public LoginPage clickSuperAdminSifyCorp() throws InterruptedException {
		WebElement eleSifyCorp = locateElement("link", "SuperAdminPage.link.Sifycorp");
		click(eleSifyCorp); 
		switchToWindow(1); 
		Thread.sleep(2000);
		return new LoginPage(driver, test);
	}

	public LoginPage clickSuperAdminLogout() {
		WebElement eleLogOut = locateElement("xpath", "LoginPage.xpath.signout");
		click(eleLogOut); 
		return new LoginPage(driver, test);
	}

	public CommonHomePage clickAdminDropdown() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "UserPage.xpath.profiledropdown"));
		return this; 
	}
	public LoginPage clickAdminLogout() {
		click(locateElement("xpath", "LoginPage.xpath.signout")); 
		return new LoginPage(driver, test);
	}
	public CommonHomePage verifyHomePageTitle(String data) throws InterruptedException {
		Thread.sleep(2000);
		waitUntilVisibilityOfWebElement("LoginPage.xpath.titleOfThePage"); 
		WebElement element = locateElement("xpath", "LoginPage.xpath.titleOfThePage");
		String text = element.getText().trim();
		System.out.println("User homepage title : "+text); 
		if(text.contains(data)) {
			reportStep("User homepage title : "+text,"PASS"); 
		} else {
			reportStep("User homepage title does not contains "+text,"FAIL");
		}
		return this;
	}
	public CommonHomePage clickUserDropdown() throws InterruptedException {
		Thread.sleep(4000); 
		click(locateElement("xpath", "UserPage.xpath.profiledropdown"));
		return this; 
	} 
	public CommonHomePage clickUserDiv1Dropdown() throws InterruptedException {
		Thread.sleep(4000); 
		click(locateElementWithXpath("UserPage.xpath.div1profiledropdown"));
		return this; 
	}

	public LoginPage clickUserLogout() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("link", "UserPage.link.logout")); 
		return new LoginPage(driver, test);
	}
	public CommonHomePage verifyConfigurationPageTitle() {
		WebElement eleTitle = locateElement("xpath", "SuperAdminPage.xpath.headerTitle");
		verifyExactText(eleTitle, "Domain, Org-Admin configuration page");
		verifyExactText(locateElement("xpath", "SuperAdminPage.xpath.RegisterDomainText"), "Register Domain");
		verifyExactText(locateElement("xpath", "SuperAdminPage.xpath.RegisterDomainAdminText"), "Register Domain Admin");
		return this;
	}	
	public ConfigPage clickConfiguration() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "SuperAdminPage.xpath.configuration"));
		return new ConfigPage(driver, test);
	}
	public BatchSettingsPage clickBatchSettings() throws InterruptedException {
		Thread.sleep(3000);
		click(locateElement("xpath", "SuperAdminPage.xpath.batchSettings"));
		return new BatchSettingsPage(driver, test);
	}
	public OtherSettingsPage clickOtherSettings() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "SuperAdminPage.xpath.otherSettings"));
		return new OtherSettingsPage(driver, test);
	}
	public QueryBuilderPage clickQueryBuilder() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "SuperAdminPage.xpath.queryBuilder"));
		return new QueryBuilderPage(driver, test); 
	}
	public QuerySettingPage clickQuerySetting() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "SuperAdminPage.xpath.querySetting"));
		return new QuerySettingPage(driver, test);
	}
	public LogsPage clickLogs() throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("xpath", "AdminHomePage.xpath.logs"));
		return new LogsPage(driver, test);
	}
	public KuePage clickKue() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "SuperAdminPage.xpath.kue"));
		return new KuePage(driver, test); 
	}



}


