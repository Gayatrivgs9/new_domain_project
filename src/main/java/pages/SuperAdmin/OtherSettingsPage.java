package pages.SuperAdmin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class OtherSettingsPage extends PreAndPost{

	public OtherSettingsPage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public OtherSettingsPage enterDomainURL(String data) {
		WebElement eleDomainURL = locateElement("xpath", "OtherSettingsPage.xpath.domainURL");
		type(eleDomainURL, data); 
		return this;
	}	
	public OtherSettingsPage clickSaveButton() {
		WebElement eleSave = locateElement("xpath", "OtherSettingsPage.xpath.saveButton");
		click(eleSave); 
		verifyExactText(locateElement("xpath", "OtherSettingsPage.xpath.successMsg"), "Successfully Updated");
		return this;
	}	
	public OtherSettingsPage enterSourceName(String data) {
		WebElement eleSourceName = locateElement("xpath", "OtherSettingsPage.xpath.sourceName");
		type(eleSourceName, data); 
		return this;
	}	
	public OtherSettingsPage clickRegister() {
		click(locateElement("xpath", "OtherSettingsPage.xpath.register")); 
		click(locateElement("xpath", "OtherSettingsPage.xpath.sourceAddedSucMsg"));
		return this;  
	}	
	public OtherSettingsPage clickCancel() {
		click(locateElement("xpath", "OtherSettingsPage.xpath.cancel"));
		return this; 
	}
	public OtherSettingsPage enterAIMLPostUrl(String data) throws InterruptedException {
		Thread.sleep(2000);
		type(locateElement("name", "OtherSettingsPage.name.postUrl"), data);
		return this;
	}
	public OtherSettingsPage clickAIMLPostUrlSaveButton() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "OtherSettingsPage.xpath.postUrlSave"));  
		//verifyExactText(locateElement("xpath", "OtherSettingsPage.xpath.successMsg"), "Successfully Updated");
		return this;
	}
	public OtherSettingsPage enterAIMLgetUrl(String data) throws InterruptedException {
		Thread.sleep(2000);
		type(locateElement("name", "OtherSettingsPage.name.getUrl"), data);
		return this;
	}
	public OtherSettingsPage clickAIMLgetUrlSaveButton() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "OtherSettingsPage.xpath.getUrlSave")); 
		verifyExactText(locateElement("xpath", "OtherSettingsPage.xpath.successMsg"), "Successfully Updated");
		return this;
	}
	public OtherSettingsPage enterAIMLTagUrl(String data) throws InterruptedException {
		Thread.sleep(2000); 
		type(locateElement("name", "OtherSettingPage.name.tagUrl"), data);
		return this;
	}
	public OtherSettingsPage clickAIMLTagUrlSaveButton() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "OtherSettingsPage.xpath.tagUrlSave"));  
		verifyExactText(locateElement("xpath", "OtherSettingsPage.xpath.successMsg"), "Successfully Updated");
		return this;
	}
	
}


