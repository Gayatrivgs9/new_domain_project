package pages.SuperAdmin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class BatchSettingsPage extends PreAndPost{

	public BatchSettingsPage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public BatchSettingsPage enterSelectDomain(String data) throws InterruptedException {
		Thread.sleep(3000); 
		click(locateElement("xpath", "BatchSettingsPage.xpath.1icon"));  
		WebElement domain = locateElement("xpath", "BatchSettingsPage.xpath.domain");
		typeValue(domain, data);
		typeValue(locateElement("xpath", "BatchSettingsPage.xpath.batchType"), "Time Based");
		Thread.sleep(2000);
		return this;
	}
	public BatchSettingsPage enterSelectDomainToTrigger(String data) throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("xpath", "BatchSettingsPage.xpath.1icon"));  
		WebElement domain = locateElement("xpath", "BatchSettingsPage.xpath.domain");
		typeValue(domain, data);
		typeValue(locateElement("xpath", "BatchSettingsPage.xpath.batchType"), "Time Based");
		Thread.sleep(2000);
		return this;
	}
	public BatchSettingsPage selectUserBatchActiveAsYes() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.userBatchYes");
		if(element.isSelected()!= true) {
			click(element);
		}
		return this;
	}
	public BatchSettingsPage selectUserRuntimeConfigurationAsDaily() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.userDaily"));
		return this;
	}
	public BatchSettingsPage selectAIMLRuntimeConfigurationAsMinutes() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.AIMLMinutes"));
		return this;
	}
	public BatchSettingsPage selectTheMinutes(String data1) throws InterruptedException {
		selectDropDownUsingValue(locateElement("xpath", "BatchSettingsPage.xpath.everyMinuteRun"), data1);
		return this;
	}
	public BatchSettingsPage selectTheDay(String data1, String data2, String data3) throws InterruptedException {
		WebElement daily = locateElement("xpath", "BatchSettingsPage.xpath.dailyRadio");
		if (daily.isSelected()!=true) {
			click(daily);  
		}
		selectDropDownUsingValue(locateElement("xpath", "BatchSettingsPage.xpath.everyDayRun"), data1);
		Thread.sleep(2000); 
		WebElement eleTime = locateElement("xpath", "BatchSettingsPage.xpath.time");
		selectDropDownUsingValue(eleTime, data2); 
		selectDropDownUsingValue(locateElement("xpath", "BatchSettingsPage.xpath.timeMin"), data3);
		return this;
	}
	public BatchSettingsPage selectHistoricalBatchActiveAsYes() {
		WebElement eleHistoricalBatchTime = locateElement("xpath", "BatchSettingsPage.xpath.historicalBatchYes");
		if(eleHistoricalBatchTime.isSelected()!=true) {
			click(eleHistoricalBatchTime); 
		}
		return this;
	}
	public BatchSettingsPage selectHistoricalBatchRunConfigurationAsWeekly(String hrs, String mins) {
		click(locateElement("xpath", "BatchSettingsPage.xpath.weekly"));
		WebElement monday = locateElement("xpath", "BatchSettingsPage.xpath.monday");
		if (monday.isSelected()!=true) {
			click(monday);
		}
		selectDropDownUsingValue(locateElement("xpath", "BatchSettingsPage.xpath.weeklyHrs"), hrs);
		selectDropDownUsingValue(locateElement("xpath", "BatchSettingsPage.xpath.weeklyMins"), mins);
		return this;  
	}
	public BatchSettingsPage selectNewDataBatchActiveAsYes() {
		WebElement newData = locateElement("xpath", "BatchSettingsPage.xpath.newDataBatch");
		if (newData.isSelected()!=true) {
			click(newData); 
		}
		return this;
	}
	public BatchSettingsPage selectNewDataBatchRunConfigurationAsAdvanced(String data) {
		click(locateElement("xpath", "BatchSettingsPage.xpath.newDataAdvanced"));
		type(locateElement("xpath", "BatchSettingsPage.xpath.newDataAdvancedInput"), data); 
		return this;
	}
	public BatchSettingsPage selectUserSuggestionBatchActiveAsYes() {
		WebElement userSuggestion = locateElement("xpath", "BatchSettingsPage.xpath.UserSuggestionBatch");
		if (userSuggestion.isSelected()!=true) {
			click(userSuggestion); 
		}
		return this;
	}
	public BatchSettingsPage selectUserSuggestionBatchRunConfigurationAsAdvanced(String data) {
		click(locateElement("xpath", "BatchSettingsPage.xpath.UserSuggestionBatchAdvanced"));
		type(locateElement("xpath", "BatchSettingsPage.xpath.UserSuggestionBatchAdvancedInput"), data);
		return this;
	}
	public BatchSettingsPage enterMasterDataBatchTime(String data) {
		WebElement eleMasterDataBatchTime = locateElement("xpath", "BatchSettingsPage.xpath.masterDataBatch");
		typeValue(eleMasterDataBatchTime ,data);  
		return this;
	}
	public BatchSettingsPage enterTransactionDataBatchTime(String data) {
		WebElement eleTransactionDataBatchTime = locateElement("xpath", "BatchSettingsPage.xpath.transactionDataBatch");
		typeValue(eleTransactionDataBatchTime, data); 
		return this;
	}
	public BatchSettingsPage clickUserBatchTrigger() throws InterruptedException {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.userBatchTrigger");
		click(eleSubmit); 
		Thread.sleep(3000);
		return this;
	}
	public BatchSettingsPage clickHistoricalBatchTrigger() throws InterruptedException {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.HistoricalBatchTrigger");
		click(eleSubmit);
		Thread.sleep(3000);
		return this;
	}
	public BatchSettingsPage clickNewDataBatchTrigger() throws InterruptedException {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.NewDataBatchTrigger");
		click(eleSubmit); 
		Thread.sleep(2000);
		return this;
	}
	public BatchSettingsPage clickSuggestionBatchTrigger() throws InterruptedException {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.UserSuggestionBatchTrigger");
		click(eleSubmit); 
		Thread.sleep(2000);
		return this;
	}
	public BatchSettingsPage clickTagBatchTrigger() throws InterruptedException {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.TagBatchTrigger");
		click(eleSubmit); 
		Thread.sleep(2000);
		return this;
	}
	public BatchSettingsPage clickAimlPredectionBatchTrigger() throws InterruptedException {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.AIMLPredictionBatchTrigger");
		click(eleSubmit); 
		Thread.sleep(2000);  
		return this;
	}
	public BatchSettingsPage clickSubmitButton() {
		WebElement eleSubmit = locateElement("xpath", "BatchSettingsPage.xpath.submitButton");
		click(eleSubmit); 
		return this;
	}
	public BatchSettingsPage verifyBatchConfigurationSuccessMsg() {
		WebElement batchMsg = locateElement("xpath", "BatchSettingsPage.xpath.batchSucMsg");
		String text = batchMsg.getText();
		System.out.println(text);
		verifyPartialText(batchMsg, "Batch Configuration Updated Successfully");
		return this; 
	}
	public BatchSettingsPage enterRunType(String data) throws InterruptedException {
		click(locateElement("xpath", "BatchSettingsPage.xpath.2icon"));
		typeValue(locateElement("xpath", "BatchSettingsPage.xpath.RunType"), data);
		Thread.sleep(2000);
		return this;
	} 
	public BatchSettingsPage selectTheDomain(String data) {
		click(locateElement("xpath", "BatchSettingsPage.xpath.3icon"));
		typeValue(locateElement("xpath", "BatchSettingsPage.xpath.selectDomain"), data); 
		return this;
	}
	public BatchSettingsPage clickRunBatch() throws InterruptedException {
		click(locateElement("xpath", "BatchSettingsPage.xpath.RunBatch"));
		//invisibilityOfWebElement("BatchSettingsPage.xpath.spin"); 
		return this; 
	}
	public BatchSettingsPage clickRunTypeClear() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.clear"));
		return this;
	}
	public BatchSettingsPage waitUpToFoldingCubeInvisible() {
		if(locateElement("xpath", "LoginPage.xpath.foldingCube")!= null) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfAllElements((locateElement("xpath", "LoginPage.xpath.foldingCube"))));
		} else {
			System.out.println("FoldingCube is invisible");  
		}
		return this; 
	} 
	public BatchSettingsPage verifySuccessMsgOfRunBatch(String data) {
		String txt = locateElement("xpath", "BatchSettingsPage.xpath.successMsg").getText();
		if (txt.contains(data)) {
			System.out.println("Success message: "+txt);
			reportStep("Success message: ", "PASS");  
		} else {
			System.out.println("message: fetched Successfully"); 
		} 
		return this;
	}
	public BatchSettingsPage clickUserSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.userSchedule"));
		return this;
	}
	public BatchSettingsPage clickHistoricalSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.historicalSchedule"));
		return this;
	}
	public BatchSettingsPage clickNewDataSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.newDataSchedule"));
		return this;
	}
	public BatchSettingsPage clickUserSuggestionSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.userSuggestionSchedule"));
		return this;
	}
	public BatchSettingsPage clickTagBatchSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.TagBatchSchedule"));  
		return this;
	}
	public BatchSettingsPage clickAIMLPredectionBatchSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.AimlBatchTimeSchedule"));  
		return this;
	}
	public BatchSettingsPage clickTransBatchSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.AimlBatchTimeSchedule"));  
		return this;
	}
	public BatchSettingsPage clickTagBatchActiveAsYes() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.selectYes");
		if(element.isSelected() != true) {
			click(element);
		}
		return this;
	}
	public BatchSettingsPage clickAIMLPredectionBatchAsYes() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.AimlBatchSchedule");
		if(element.isSelected() != true) {
			click(element);
		}  
		return this;
	}
	public BatchSettingsPage clickResetUserBatch() {

		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.userBatchStatusProgress");
		if(element.getText().contains("Available") == true) {
			reportStep("User Batch Execution Status : Available", "PASS");
		} else {
			List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.userBatchResetNew");
			for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
				allElements.get(0).click();
			}
		} 
		return this; 
	}
	public BatchSettingsPage clickResetHistoricalBatch() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.historicalBatchStatusProgress");
		if(element.getText().contains("Available") == true) {
			reportStep("Historical Batch Execution Status : Available", "PASS");
		} else {
			List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.historicalBatchResetNew");
			for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
				allElements.get(0).click();
			}
		} 
		return this; 
	}
	public BatchSettingsPage clickResetNewDataBatch() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.newDataBatchStatusProgress");
		if(element.getText().contains("Available") == true) {
			reportStep("NewData Batch Execution Status : Available", "PASS");
		} else {
			List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.nweDataBatchResetNew");
			for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
				allElements.get(0).click();
			}
		} 
		return this;
	}
	public BatchSettingsPage clickResetUserSuggestionBatch() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.userSuggestionBatchStatusProgress");
		if(element.getText().contains("Available") == true) {
			reportStep("User Suggestion Batch Execution Status : Available", "PASS");
		} else {
			List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.userSuggestionBatchResetNew");
			for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
				allElements.get(0).click();
			}
		} 
		return this;
	}
	public BatchSettingsPage clickResetTagBatch() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.tagBatchStatusProgress");
		if(element.getText().contains("Available") == true) {
			reportStep("Tag Batch Execution Status : Available", "PASS");
		} else {
			List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.tagBatchResetNew");
			for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
				allElements.get(0).click();
			}
		} 
		return this;
	}
	public BatchSettingsPage clickResetAIMLBatch() {
		WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.aimlPredctionBatchStatusProgress");
		if(element.getText().contains("Available") == true) {
			reportStep("AIML Predection Batch Execution Status : Available", "PASS");
		} else {
			List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.aimlPrdectionBatchResetNew");
			for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
				allElements.get(0).click();
			}
		} 
		return this;
	}
	public BatchSettingsPage clickOkButton() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.okButton"));  
		return this;
	} 
	public BatchSettingsPage clickEditOkButton() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.okButton2"));  
		return this; 
	}
	public BatchSettingsPage clickTransactionBatchTrigger() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.transactionDataBatchManualTrigger"));
		return this;  
	}
	public BatchSettingsPage clickTransactionDataBatchEdit() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.transactionDataBatchEdit")); 
		return this;
	} 
	public BatchSettingsPage isActiveAsYes() {
		WebElement inputYes = locateElement("xpath", "BatchSettingsPage.xpath.transactionEditInputYes");
		if (inputYes.isSelected()!=true) {
			click(inputYes);  
		}
		return this;
	}
	public BatchSettingsPage clickEditSchedule() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.editSchedule"));
		return this; 
	}
	public BatchSettingsPage clickTransactionDataEditOkButton() {
		click(locateElement("xpath", "BatchSettingsPage.xpath.transactionOkButton"));
		return this; 
	}
	public BatchSettingsPage isActiveAsNo() {
		WebElement inputNo = locateElement("xpath", "BatchSettingsPage.xpath.transactionEditInputNo");
		if (inputNo.isSelected()!=true) {
			click(inputNo);
		}
		return this; 
	}
	public BatchSettingsPage scheduleStatusVerification() { 
		String text = locateElement("xpath", "BatchSettingsPage.xpath.transactionDataAsFalse").getText();
		if(text!=null) {
			System.out.println("After Schedule Runtime Configuration Active Status As"+text);
			verifyPartialText(locateElement("xpath", "BatchSettingsPage.xpath.transactionDataAsFalse"), "false"); 
			reportStep("After Schedule Runtime Configuration Active Status As "+text, "PASS"); 
		} else {
			reportStep("After Schedule Runtime Configuration Active Status As "+text, "FAIL");
		}
		return this;
	}
	public BatchSettingsPage tagsConfiguration() {
		typeValue(locateElement("xpath", "BatchSettingsPage.xpath.sourceInput"), "livewire"); 
		typeValue(locateElement("xpath", "BatchSettingsPage.xpath.selectKeyInput"), "description");
		click(locateElement("xpath", "BatchSettingsPage.xpath.tagbatchSaveButton")); 
		return this;
	}

	public BatchSettingsPage clickMasterDataBatchReset() {
		String text = "";
		List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.livewireMasterDataNew");
		for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
			text = allElements.get(0).getText(); 
		}
		if(text.contains("livewire") == true) {
			WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.livewireMasterResetNew");
			if(element.getText().contains("Available") == true) {
				reportStep("Master Data Batch Execution Status : Available", "PASS");
			} else {
				List<WebElement> allResetElements = locateElements("xpath", "BatchSettingsPage.xpath.livewireMasterResetTagANew");
				for (int i = 0; i < (allResetElements.size()+1)-(allResetElements.size()); i++) {
					allResetElements.get(0).click();
				}
			} 
		}
		return this;
	}

	public BatchSettingsPage clickMasterDataBatchEditButton() throws InterruptedException {
		Thread.sleep(1000); 
		List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.masterDataEditButtonNew");
		for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
			allElements.get(0).click();
		}
		return this;
	}
	
	public BatchSettingsPage clickTransactionDataBatchReset() {
		String text = "";
		List<WebElement> allElements = locateElements("xpath", "BatchSettingsPage.xpath.livewireTransactionDataNew");
		for (int i = 0; i < (allElements.size()+1)-(allElements.size()); i++) {
			text = allElements.get(0).getText(); 
		}
		if(text.contains("livewire") == true) {
			WebElement element = locateElement("xpath", "BatchSettingsPage.xpath.livewireTrnsResetNew");
			if(element.getText().contains("Available") == true) {
				reportStep("Transaction Data Batch Execution Status : Available", "PASS");
			} else {
				List<WebElement> allResetElements = locateElements("xpath", "BatchSettingsPage.xpath.livewireTrnsResetTagANew");
				for (int i = 0; i < (allResetElements.size()+1)-(allResetElements.size()); i++) {
					allResetElements.get(0).click();
				}
			} 
		}
		return this;
	}












}







