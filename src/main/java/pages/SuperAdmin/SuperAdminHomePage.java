package pages.SuperAdmin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class SuperAdminHomePage extends PreAndPost{

	public SuperAdminHomePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		
	
	public SuperAdminHomePage clickConfiguration() {
		click(locateElement("xpath", "SuperAdminPage.xpath.configuration")); 
		return this;
	}
	
	public LoginPage clickBatchSettings() {
		click(locateElement("xpath", "SuperAdminPage.xpath.batchSettings")); 
		return new LoginPage(driver, test);
	}
	
	public SuperAdminHomePage clickOtherSettings() {
		click(locateElement("xpath", "SuperAdminPage.xpath.otherSettings"));
		return this; 
	}	
	
	

}










