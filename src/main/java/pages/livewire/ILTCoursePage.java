package pages.livewire;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ILTCoursePage extends PreAndPost{

	public ILTCoursePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public ILTCoursePage clickAssessmentPanelTitle() {
		click(locateElement("xpath", "livewirePage.xpath.courseTitle"));  
		return this;
	}
	public ILTCoursePage enterIltCourseDeetails(String data1, String data2, String data3, String data4, String data5) {
		type(locateElement("id", "livewirePage.id.iltTitle"), data1); 
		type(locateElement("id", "livewirePage.id.ilt_start"), data2);
		type(locateElement("id", "livewirePage.id.ilt_end"), data3);
		type(locateElement("id", "livewirePage.id.ilt_instructor"), data4);
		type(locateElement("id", "livewirePage.id.ilt_location"), data5);
		return this;
	}
	public ILTCoursePage clickSaveButton() throws InterruptedException {
		click(locateElement("id", "livewirePage.id.create_ilt_course_saveButton"));  
		Thread.sleep(3000);
		click(locateElement("id", "livewirePage.id.New_close_course"));
		Thread.sleep(1000); 
		click(locateElement("xpath", "livewirePage.xpath.courseCloseYesBtn"));
		return this;
	}
	public ILTCoursePage clickAllCourses() throws InterruptedException {
		//waitUntilVisibilityOfWebElement("livewirePage.xpath.allCourses"); 
		Thread.sleep(12000);
		click(locateElement("xpath", "livewirePage.xpath.allCourses"));
		return this;  
	}
	public ILTCoursePage enterSearchCourse() throws InterruptedException {
		Thread.sleep(12000); 
		type(locateElement("xpath", "livewirePage.xpath.allCoursesSearchBtn"), "JAVA COURSE_"+randomWbtNumber);
		click(locateElement("xpath", "livewirePage.xpath.allCoursesNewSearchBtn"));
		Thread.sleep(4000); 
		return this; 
	}
	public ILTCoursePage clickCourseDetails() {
		jsClick(locateElement("xpath", "livewirePage.xpath.courseDetails"));
		return this; 
	}

}


























