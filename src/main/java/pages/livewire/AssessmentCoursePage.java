package pages.livewire;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AssessmentCoursePage extends PreAndPost{

	public AssessmentCoursePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public AssessmentCoursePage enterTotalQuestions(String data, String data2) {
		type(locateElement("id", "livewirePage.id.assessmntNewCourse"), data);  
		type(locateElement("id", "livewirePage.id.assessmntQuestionsPerpage"), data2);
		return this;
	}
	public AssessmentCoursePage enterNumberOfQnsInPool(String data) {
		type(locateElement("xpath", "livewirePage.xpath.numberQuestionsInPool"), data);
		return this;
	} 
	public AssessmentCoursePage clickOnPanelTopicTitle() {
		click(locateElement("xpath", "livewirePage.xpath.panel_topic_title")); 
		return this;
	}   
	public AssessmentCoursePage enterNumberOfQnsInTopic(String data) {
		type(locateElement("xpath", "livewirePage.xpath.topic_no_questions"), data);
		return this;    
	} 
	public AssessmentCoursePage clickAddQuestionEvent() {
		click(locateElement("xpath", "livewirePage.xpath.add_question_event")); 
		return this;      
	} 
	public AssessmentCoursePage enterSelectQuestionBy() {
		selectDropDownUsingVisibleText(locateElement("xpath", "livewirePage.id.search_type"), "Question");
		return this;  
	}
	public AssessmentCoursePage clickQnsSearchButton() throws InterruptedException {
		click(locateElement("xpath", "livewirePage.xpath.newSearchButton")); 
		Thread.sleep(10000); 
		return this;      
	} 
}
































