package pages.livewire;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.opencsv.CSVWriter;

import lib.selenium.PreAndPost;

public class AssignmentCoursePage extends PreAndPost{

	public AssignmentCoursePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public AssignmentCoursePage loadLivewireUrl() throws InterruptedException {
		loadUrl("livewire.test2"); 
		return this;
	}
	public AssignmentCoursePage clickLogin() {
		WebElement element = locateElement("xpath", "livewirePage.xpath.login");
		click(element);
		return this;
	}
	public AssignmentCoursePage enterUsername(String data) {
		type(locateElement("id", "livewirePage.id.email"), data);
		return this;
	}
	public AssignmentCoursePage enterPassword(String data) {
		type(locateElement("id", "livewirePage.id.pwd"), data);
		return this;
	}
	public AssignmentCoursePage clickLoginButton() {
		WebElement element = locateElement("xpath", "livewirePage.xpath.loginButton");
		click(element);
		return this;
	} 
	static long randomNumber;
	public AssignmentCoursePage clickCreateCourse(String data) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(7000);
		click(locateElement("xpath", "livewirePage.xpath.createCourse"));
		click(locateElement("xpath", "livewirePage.xpath.wbtCourse"));
		randomNumber = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		System.out.println("random number is : "+randomNumber); 
		type(locateElement("xpath", "livewirePage.xpath.wbtCourseTitle"), "JAVA COURSE_"+randomNumber);
		writeData(0, 1, "JAVA COURSE_"+randomNumber, "WriteCourseName", "DataSheet", "GREEN");  
		click(locateElement("xpath", "livewirePage.xpath.SaveButtonwbtCourse"));
		return this;
	}
	public AssignmentCoursePage enterCourseDetails(String data1, String data2, String data3) {
		//click(locateElement("xpath", "livewirePage.xpath.1showMore"));
		selectDropDownUsingVisibleText(locateElement("xpath", "livewirePage.xpath.course_type_id"), data1);
		selectDropDownUsingVisibleText(locateElement("xpath", "livewirePage.xpath.reference_type"), data2);
		type(locateElement("xpath", "livewirePage.xpath.reference_url"), data3);
		return this; 
	}
	public AssignmentCoursePage selectCourseStartEndDate(String data1, String data2) throws InterruptedException {
		click(locateElement("xpath", "livewirePage.xpath.add_optional_btn"));
		System.out.println("from date : "+data1+" to date : "+data2); 
	    dateselectionAssessmentTopMultiplePool("livewirePage.xpath.delv_start", data1);
		//type(locateElement("xpath", "livewirePage.xpath.delv_start"), data1);
		Thread.sleep(2000);
		dateselectionAssessmentTopMultiplePool("livewirePage.xpath.delv_end", data2);
		//type(locateElement("xpath", "livewirePage.xpath.delv_end"), data2);
		return this;
	}

	public AssignmentCoursePage clickOnSaveButton() throws InterruptedException {
		click(locateElement("xpath", "livewirePage.xpath.courseDetailsSaveButton"));
		Thread.sleep(2000); 
		click(locateElement("xpath", "livewirePage.xpath.courseCloseBtn"));
		Thread.sleep(1000); 
		click(locateElement("xpath", "livewirePage.xpath.courseCloseYesBtn"));
		return this;  
	}

	public AssignmentCoursePage clickAllCourses() throws InterruptedException {
		Thread.sleep(7000); 
		click(locateElement("xpath", "livewirePage.xpath.allCourses"));
		return this;  
	}
	public AssignmentCoursePage clickEnroleCourse() {
		click(locateElement("xpath", "livewirePage.xpath.enrolledCourse")); 
		click(locateElement("xpath", "livewirePage.xpath.EnrollAndStart"));
		return this; 
	}
	public AssignmentCoursePage verifyCompletedCourses() throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("xpath", "livewirePage.xpath.completedCourses"));
		return this; 
	} 
	public AssignmentCoursePage enterSearchCourse() throws InterruptedException {
		Thread.sleep(7000); 
		type(locateElement("xpath", "livewirePage.xpath.allCoursesSearchBtn"), "JAVA COURSE_"+randomNumber);
		click(locateElement("xpath", "livewirePage.xpath.allCoursesNewSearchBtn"));
		Thread.sleep(4000); 
		return this; 
	}
	public AssignmentCoursePage clickCourseDetails() {
		click(locateElement("xpath", "livewirePage.xpath.courseDetails"));
		return this; 
	}

	//Write into CSV file
	public AssignmentCoursePage getCourseIDAndCourseTitle() throws IOException {
		String courseID = getText(locateElement("xpath", "livewirePage.xpath.courseIDDtails")).trim();
		String courseSkill = "java";
		String courseTitle = getText(locateElement("xpath", "livewirePage.xpath.courseTitle")).trim().replace("-WBT-1 - Completed", ""); 
		try {
			FileWriter fileWriter = new FileWriter(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\Livewire Files\\course_masterdata_java_livewire.csv");
			CSVWriter csvWriter = new CSVWriter(fileWriter);
			String[] header = { "course_id", "skill", "title" }; 
			csvWriter.writeNext(header);
			String[] data1 = {courseID,courseSkill,courseTitle}; 
			csvWriter.writeNext(data1); 
			//close file 
			csvWriter.close(); 
		} catch (Exception e) { 
			System.err.println("File not found");  
		}  
		return this;
	}

	public void dateselectionBottomAssessmentMutiplePool(String xpath, String datetxt) {
		String month_word = null;
		String spltdate[] = datetxt.split("-");
		System.out.println("SPLIT DATE LENGTH  ==> " + spltdate.length);
		String datespt = spltdate[0];
		String monthspt = spltdate[1];
		String yearspt = spltdate[2];
		System.err.println("entering bottom date selection");
		System.err.println("month split_0 is ==> " + monthspt);
		driver.findElement(By.xpath(prop.getProperty(xpath))).click(); 
		try {

			List<WebElement> header = driver.findElements(
					By.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']"));

			for (WebElement webElement : header) {
				String attribute = webElement.getAttribute("style");
				System.err.println("Attribute is ==> " + attribute);
				if (attribute.contains("block")) {

					/* SELECTION OF HEADER */
					Thread.sleep(5000);
					int size = driver
							.findElements(By
									.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
							.size();
					System.err.println("Size :: " + size);

					try {
						driver.findElements(By
								.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
						.get(size - 4).click();

						driver.findElements(By
								.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[4]//th[2]"))
						.get(size - 4).click();
						System.err.println("Try Executed");

					} catch (Exception e) {
						driver.findElements(By
								.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
						.get(size - 8).click();

						driver.findElements(By
								.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[4]//th[2]"))
						.get(size - 8).click();
						System.err.println("Catch Executed");
					}

					/*
					 * try { driver.findElements(By
					 * .xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"
					 * )) .get(size - (size-1)).click(); int value =
					 * size-(size-1);
					 * System.err.println("Try Size is :: "+value); } catch
					 * (Exception e) { driver.findElements(By
					 * .xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"
					 * )) .get(size - (size-1)).click(); int value =
					 * size-(size-1);
					 * System.err.println("Catch Size is :: "+value); }
					 */

					// Thread.sleep(2000);
					// driver.findElement(By.xpath("//*[@class = 'datetimepicker
					// datetimepicker-dropdown-bottom-right
					// dropdown-menu']/div[4]//th[2]")).click();

					/* SELECTION OF YEAR */
					List<WebElement> years = driver.findElements(By.xpath("//*[@class = 'datetimepicker-years']"));
					for (WebElement webEle : years) {
						if (webEle.isDisplayed()) {
							List<WebElement> yrs = driver.findElements(By.tagName("span"));
							for (WebElement webElement2 : yrs) {
								String classname = webElement2.getAttribute("class");
								if (!classname.contains("disabled") || !classname.contains("active disabled")) {
									if (webElement2.getText().equals(yearspt)) {
										System.err.println("clicked year");
										webElement2.click();
										break;
									} else {
										continue;
									}
								}
							}
						}
					}

					/* SELECTION OF MONTH */

					System.err.println("month split is ==> " + monthspt);
					if (monthspt.trim().equals("01".trim())) {
						month_word = "Jan".trim();
					} else if (monthspt.trim().equals("02".trim())) {
						month_word = "Feb".trim();
					} else if (monthspt.trim().equals("03".trim())) {
						month_word = "Mar".trim();
					} else if (monthspt.trim().equals("04".trim())) {
						month_word = "Apr".trim();
					} else if (monthspt.trim().equals("05".trim())) {
						month_word = "May".trim();
					} else if (monthspt.trim().equals("06".trim())) {
						month_word = "Jun".trim();
					} else if (monthspt.trim().equals("07".trim())) {
						month_word = "Jul".trim();
					} else if (monthspt.trim().equals("08".trim())) {
						month_word = "Aug".trim();
					} else if (monthspt.trim().equals("09".trim())) {
						month_word = "Sep".trim();
					} else if (monthspt.trim().equals("10".trim())) {
						month_word = "Oct".trim();
					} else if (monthspt.trim().equals("11".trim())) {
						month_word = "Nov".trim();
					} else if (monthspt.trim().equals("12".trim())) {
						month_word = "Dec".trim();
					}

					List<WebElement> month = driver.findElements(By.xpath("//*[@class = 'datetimepicker-months']"));

					for (WebElement webEle : month) {
						if (webEle.isDisplayed()) {
							List<WebElement> mon = driver.findElements(By.tagName("span"));
							for (WebElement webElement2 : mon) {
								String classname = webElement2.getAttribute("class");
								if (!classname.contains("disabled") || !classname.contains("active disabled")) {
									if (webElement2.getText().equals(month_word)) {
										System.err.println("clicked month");
										webElement2.click();
										break;
									} else {
										continue;
									}
								}
							}
						}
					}

					/* SELECTION OF DAY */

					List<WebElement> dats = driver.findElements(By.xpath("//*[@class='datetimepicker-days']"));
					for (WebElement datElement : dats) {
						if (datElement.isDisplayed()) {
							List<WebElement> days = driver.findElements(By.tagName("td"));
							for (WebElement webElement2 : days) {
								String clasname = webElement2.getAttribute("class");
								if (!clasname.contains("old") || !clasname.contains("disabled")) {
									String date = webElement2.getText();
									if (date.trim().equalsIgnoreCase("1") || date.trim().equalsIgnoreCase("2")
											|| date.trim().equalsIgnoreCase("3") || date.trim().equalsIgnoreCase("4")
											|| date.trim().equalsIgnoreCase("5") || date.trim().equalsIgnoreCase("6")
											|| date.trim().equalsIgnoreCase("7") || date.trim().equalsIgnoreCase("8")
											|| date.trim().equalsIgnoreCase("9")) {
										date = "0" + date.trim();
										System.out.println("CONCATENATE DATE ==> " + date);
									}
									if (date.trim().equals(datespt)) {
										webElement2.click();
										System.err.println("Date is clicked");
										break;
									} else {
										continue;
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			System.err.println("error message ==> " + e);
		}

	}

	public void dateselectionAssessmentTopMultiplePool(String xpath, String datetxt) {

		// Auto-generated method stub
		String month_word = null;
		String spltdate[] = datetxt.split("-");
		System.out.println("SPLIT DATE LENGTH  ==> " + spltdate.length);
		String datespt = spltdate[0];
		String monthspt = spltdate[1];
		String yearspt = spltdate[2];
		System.err.println("entering to enroll date selection");
		System.err.println("month split_0 is ==> " + monthspt);
		driver.findElement(By.xpath(prop.getProperty(xpath))).click(); 
		try { 

			List<WebElement> header = driver.findElements(
					By.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']"));

			for (WebElement webElement : header) {
				String attribute = webElement.getAttribute("style");
				System.err.println("Attribute is ==> " + attribute);
				if (attribute.contains("block")) {

					/* SELECTION OF HEADER */
					// Thread.sleep(5000);
					int size = driver
							.findElements(By
									.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
							.size();
					System.err.println("Top Size :: " + size);

					try {
						driver.findElements(By
								.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
						.get(size - 4).click();
						Thread.sleep(2000);

						driver.findElements(By.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[4]//th[2]")).get(size-4).click();
					} catch (Exception e) {
						driver.findElements(By
								.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
						.get(size - 8).click();

						Thread.sleep(2000);
						driver.findElements(By.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[4]//th[2]")).get(size-8).click();
					}

					/* try {
		driver.findElements(By
		.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
		.get(size - size).click();
		} catch (Exception e) {
		driver.findElements(By
		.xpath("//*[@class = 'datetimepicker datetimepicker-dropdown-top-left dropdown-menu']/div[3]/table/thead/tr[1]/th[2]"))
		.get(size - size).click();
		}*/

					/* SELECTION OF YEAR */
					
					List<WebElement> years = driver.findElements(By.xpath("//*[@class = 'datetimepicker-years']"));
					for (WebElement webEle : years) {
						if (webEle.isDisplayed()) {
							List<WebElement> yrs = driver.findElements(By.tagName("span"));
							for (WebElement webElement2 : yrs) {
								String classname = webElement2.getAttribute("class");
								if (!classname.contains("disabled") || !classname.contains("active disabled")) {
									if (webElement2.getText().equals(yearspt)) {
										System.err.println("clicked year");
										webElement2.click();
										break;
									} else {
										continue;
									}
								}
							}
						}
					}

					/* SELECTION OF MONTH */

					System.err.println("month split is ==> " + monthspt);
					if (monthspt.trim().equals("01".trim())) {
						month_word = "Jan".trim();
					} else if (monthspt.trim().equals("02".trim())) {
						month_word = "Feb".trim();
					} else if (monthspt.trim().equals("03".trim())) {
						month_word = "Mar".trim();
					} else if (monthspt.trim().equals("04".trim())) {
						month_word = "Apr".trim();
					} else if (monthspt.trim().equals("05".trim())) {
						month_word = "May".trim();
					} else if (monthspt.trim().equals("06".trim())) {
						month_word = "Jun".trim();
					} else if (monthspt.trim().equals("07".trim())) {
						month_word = "Jul".trim();
					} else if (monthspt.trim().equals("08".trim())) {
						month_word = "Aug".trim();
					} else if (monthspt.trim().equals("09".trim())) {
						month_word = "Sep".trim();
					} else if (monthspt.trim().equals("10".trim())) {
						month_word = "Oct".trim();
					} else if (monthspt.trim().equals("11".trim())) {
						month_word = "Nov".trim();
					} else if (monthspt.trim().equals("12".trim())) {
						month_word = "Dec".trim();
					}

					List<WebElement> month = driver.findElements(By.xpath("//*[@class = 'datetimepicker-months']"));

					for (WebElement webEle : month) {
						if (webEle.isDisplayed()) {
							List<WebElement> mon = driver.findElements(By.tagName("span"));
							for (WebElement webElement2 : mon) {
								String classname = webElement2.getAttribute("class");
								if (!classname.contains("disabled") || !classname.contains("active disabled")) {
									if (webElement2.getText().equals(month_word)) {
										System.err.println("clicked month");
										webElement2.click();
										break;
									} else {
										continue;
									}
								}
							}
						}
					}

					/* SELECTION OF DAY */

					List<WebElement> dats = driver.findElements(By.xpath("//*[@class='datetimepicker-days']"));
					for (WebElement datElement : dats) {
						if (datElement.isDisplayed()) {
							List<WebElement> days = driver.findElements(By.tagName("td"));
							for (WebElement webElement2 : days) {
								String clasname = webElement2.getAttribute("class");
								if (!clasname.contains("old") || !clasname.contains("disabled")) {
									String date = webElement2.getText();
									if (date.trim().equalsIgnoreCase("1") || date.trim().equalsIgnoreCase("2")
											|| date.trim().equalsIgnoreCase("3") || date.trim().equalsIgnoreCase("4")
											|| date.trim().equalsIgnoreCase("5") || date.trim().equalsIgnoreCase("6")
											|| date.trim().equalsIgnoreCase("7") || date.trim().equalsIgnoreCase("8")
											|| date.trim().equalsIgnoreCase("9")) {
										date = "0" + date.trim();
										System.out.println("CONCATENATE DATE ==> " + date);
									}
									if (date.trim().equals(datespt)) {
										webElement2.click();
										System.err.println("Date is clicked");
										break;
									} else {
										continue;
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			System.err.println("error message ==> " + e);
		}

	}




}









