package pages.livewire;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LivewirePage extends PreAndPost{

	public LivewirePage(RemoteWebDriver driver, ExtentTest test) {	
		this.driver = driver;
		this.test = test;	
	}		

	public LivewirePage loadLivewireUrl() throws InterruptedException {
		loadUrl("livewire.test2"); 
		return this;
	}
	public LivewirePage clickLogin() {
		WebElement element = locateElement("xpath", "livewirePage.xpath.login");
		click(element);
		return this;
	}
	public LivewirePage enterUsername(String data) {
		type(locateElement("id", "livewirePage.id.email"), data);
		return this;
	}
	public LivewirePage enterPassword(String data) {
		type(locateElement("id", "livewirePage.id.pwd"), data);
		return this;
	}
	public LivewirePage clickLoginButton() {
		WebElement element = locateElement("xpath", "livewirePage.xpath.loginButton");
		click(element);
		return this;
	} 
	
	
	public WBTCoursePage clickCreateWBTCourse(String data) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(15000);
		click(locateElement("xpath", "livewirePage.xpath.createCourse"));
		click(locateElement("xpath", "livewirePage.xpath.wbtCourse"));
		randomWbtNumber = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		type(locateElement("xpath", "livewirePage.xpath.wbtCourseTitle"), "JAVA COURSE_"+randomWbtNumber);
		writeData(0, 1, "JAVA COURSE_"+randomWbtNumber, "WriteCourseName", "DataSheet", "GREEN");  
		click(locateElement("xpath", "livewirePage.xpath.SaveButtonwbtCourse"));
		return new WBTCoursePage(driver, test); 
	}
	
	
	public ILTCoursePage clickCreateILTCourse(String data) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(7000);
		click(locateElement("xpath", "livewirePage.xpath.createCourse"));
		click(locateElement("xpath", "livewirePage.xpath.iltCourse"));
		randomIltNumber = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		type(locateElement("xpath", "livewirePage.xpath.wbtCourseTitle"), "JAVA ILT_"+randomIltNumber);
		writeData(0, 1, "JAVA ILT_"+randomIltNumber, "WriteCourseName", "DataSheet", "GREEN");  
		click(locateElement("xpath", "livewirePage.xpath.SaveButtonwbtCourse"));
		return new ILTCoursePage(driver, test);  
	}
	
	public AssessmentCoursePage clickCreateAssessmentCourse(String data) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(7000);
		click(locateElement("xpath", "livewirePage.xpath.createCourse"));
		click(locateElement("xpath", "livewirePage.xpath.assessmntNewCourse"));
		randomAssessmentNumber = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		type(locateElement("xpath", "livewirePage.xpath.wbtCourseTitle"), "JAVA ASSESSMENT_"+randomAssessmentNumber);
		writeData(0, 1, "JAVA ASSESSMENT"+randomAssessmentNumber, "WriteCourseName", "DataSheet", "GREEN");  
		click(locateElement("xpath", "livewirePage.xpath.SaveButtonwbtCourse"));
		return new AssessmentCoursePage(driver, test);     
	}
	
	public AssignmentCoursePage clickCreateAssignmentCourse(String data) throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(7000);
		click(locateElement("xpath", "livewirePage.xpath.createCourse"));
		click(locateElement("xpath", "livewirePage.xpath.assignmentNewCourse"));
		randomAssignmentNumber = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		type(locateElement("xpath", "livewirePage.xpath.wbtCourseTitle"), "JAVA ASSIGNMENT_"+randomAssignmentNumber);
		writeData(0, 1, "JAVA ASSIGNMENT"+randomAssignmentNumber, "WriteCourseName", "DataSheet", "GREEN");  
		click(locateElement("xpath", "livewirePage.xpath.SaveButtonwbtCourse"));
		return new AssignmentCoursePage(driver, test);   
	}

}
























