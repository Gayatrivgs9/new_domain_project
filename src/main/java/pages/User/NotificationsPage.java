package pages.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class NotificationsPage extends PreAndPost {

	public NotificationsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public NotificationsPage waitUpToFoldingCubeInvisible() {
		if(locateElement("xpath", "LoginPage.xpath.foldingCube")!= null) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfAllElements((locateElement("xpath", "LoginPage.xpath.foldingCube"))));
		} else {
			System.out.println("FoldingCube is invisible");  
		}
		return this; 
	}
	public NotificationsPage clickBellIcon() throws InterruptedException {
		Thread.sleep(500);
		waitUntilVisibilityOfWebElement("NotificationsPage.xpath.bellIcon");
		click(locateElement("xpath", "NotificationsPage.xpath.bellIcon"));  
		return this;  
	}
	public NotificationsPage clickNotifications() throws InterruptedException {
		click(locateElement("xpath","NotificationsPage.xpath.bellInserted"));
		return this;
	}
	public NotificationsPage verifySuggestionsNotification() {
		if (locateElement("xpath", "NotificationsPage.xpath.tableTrNotifications") != null) {
			String text = locateElement("xpath", "NotificationsPage.xpath.tableTrNotifications").getText();
			if(text.contains("User Suggestions")) {
				reportStep("User suggested notifications generated successfully", "PASS");
			}
		} else {
			reportStep("User suggested notifications not generated", "FAIL");
		}
		return this;
	}
	public NotificationsPage verifyNotifications(String fileName) throws InterruptedException, IOException {
		List<String> lst = new ArrayList<>();
		String userName = "";
		List<WebElement> allEle = locateElements("xpath", "NotificationsPage.xpath.pageNavigation");
		for (int i = 0; i < allEle.size(); i++) {
			click(allEle.get(i)); 
			Thread.sleep(1000);
			List<WebElement> allNotifications = locateElements("xpath", "NotificationsPage.xpath.notificationCount");
			for (int j = 0; j < allNotifications.size(); j++) {
				String text = allNotifications.get(j).getText();
				lst.add(text);
			}
		}
		XSSFWorkbook wb = null;
		if(environment.equals("local")) {
			wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
			XSSFSheet sheetAt = wb.getSheetAt(0);
			rowCount = sheetAt.getLastRowNum(); 
			System.out.println(rowCount); 
			for (int i = 0; i <=rowCount; i++) { 
				XSSFRow row = sheetAt.getRow(i);
				XSSFCell cell = row.getCell(0); 
				userName = cell.getStringCellValue().trim(); 
				System.out.println(userName);
				verifyTwoStringPartialValues(lst.get(i), userName);
			}
		}else {
			wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
			XSSFSheet sheetAt = wb.getSheetAt(0); 
			rowCount = sheetAt.getLastRowNum();
			System.out.println(rowCount); 
			list = new ArrayList<>(); 
			for (int i = 1; i <=rowCount; i++) { 
				XSSFRow row = sheetAt.getRow(i);
				XSSFCell cell = row.getCell(0); 
				userName = cell.getStringCellValue().trim(); 
				verifyTwoStringPartialValues(userName, lst.get(i));
			}
		}
		wb.close();
		return this;
	}

	public NotificationsPage getNotificationCount(String data) throws InterruptedException {
		String result = "";
		if (locateElements("xpath", "NotificationsPage.xpath.bellCountNew").isEmpty() != true) {
			List<WebElement> elements = locateElements("xpath", "NotificationsPage.xpath.bellCountNew");
			int size = elements.size();
			result = ""+size;
			System.out.println(elements.size());
			reportStep("Notification Count: " + elements.size(), "PASS");
		} 
		if(result.contains(data)) {
			reportStep("Notification Count: " +result+" is matched successfully", "PASS");
		} else {
			reportStep("Notification Count not matched", "FAIL");
		}

		return this; 
	}
	public NotificationsPage clickWindowMaximize() throws InterruptedException {
		Thread.sleep(2000);
		if(locateElements("xpath", "NotificationsPage.xpath.notificationCount").isEmpty() !=true) {
			click(locateElement("xpath", "NotificationsPage.xpath.notificationCount"));
		}else {
			reportStep("No Notifications", "FAIL");
		}
		return this;
	}
	public NotificationsPage clickOkButton() {
		click(locateElement("xpath", "NotificationsPage.xpath.window-maximize"));
		click(locateElement("xpath", "NotificationsPage.xpath.okButton"));
		return this; 
	}
	public NotificationsPage mentorApproval() throws InterruptedException {
		if(locateElements("xpath", "NotificationsPage.xpath.mentorApproval").isEmpty() !=true) {
			List<WebElement> allElements = locateElements("xpath", "NotificationsPage.xpath.mentorApproval");
			for (int i = 0; i < allElements.size(); i++) {
				Thread.sleep(1000);   
				jsClick(allElements.get(0));
				reportStep("Mentor Approval As Ok", "PASS");
			} 
		}else { 
			reportStep("No Mentor Approval Notifications", "PASS");
		}
		return this;
	}
	public NotificationsPage userPlanApproval() throws InterruptedException {
		Thread.sleep(1000);
		if(locateElements("xpath", "NotificationsPage.xpath.boldNotifications").isEmpty()!= true && 
				locateElement("xpath", "NotificationsPage.xpath.boldNotifications").getText().contains("User plan approval")) {
			WebElement allElements = locateElement("xpath", "NotificationsPage.xpath.managerUserPlanApproval");
			Thread.sleep(1000);   
			jsClick(allElements);
			reportStep("User Plan Approval As Ok", "PASS");
			//plan approval user details
			if (locateElement("xpath", "NotificationsPage.xpath.nameUser").getText()!=null) {
				String userName = locateElement("xpath", "NotificationsPage.xpath.nameUser").getText();
				String userRole = locateElement("xpath", "NotificationsPage.xpath.roleUser").getText();
				String userDepartment = locateElement("xpath", "NotificationsPage.xpath.departmentUser").getText();
				reportStep("Plan approval username : "+userName+" Role : "+userRole+" department : "+userDepartment, "PASS");
			} else {
				reportStep("No data available related to user", "PASS");
			}
			//actual plan and requested plan details
			if (locateElement("xpath", "NotificationsPage.xpath.startNend").getText()!=null) {	
				String actualPlan = locateElement("xpath", "NotificationsPage.xpath.startNend").getText();
				String requestedPlan = locateElement("xpath", "NotificationsPage.xpath.futureReq").getText();
				reportStep("actual plan details : "+actualPlan+"requested plan details : "+requestedPlan, "PASS");
				click(locateElement("xpath", "NotificationsPage.xpath.approveAll"));
				verifyPartialText(locateElement("xpath", "NotificationsPage.xpath.PlanaprSucMsg"), "Plan approved");
			} else {
				reportStep("No details available for actual and requestd plan", "PASS");
			}
		} else {
			reportStep("No Pending plan approvals", "PASS"); 
		}
		return this;
	}

	public NotificationsPage verificationOfUserCustomPlanApproval() {
		if(locateElements("xpath", "//td[contains(text(), 'Custom Plan You Had Requested')]").isEmpty()!= true) {
			String text = locateElement("xpath", "NotificationsPage.xpath.planReqApp").getText();
			click(locateElement("xpath", "NotificationsPage.xpath.planReqApp"));
			click(locateElement("xpath", "NotificationsPage.xpath.okButton"));  
			reportStep(text, "PASS"); 
		} else {
			reportStep("There is No Custom Plan Approval Notifications", "PASS");
		}
		return this;

	}

	public NotificationsPage clickCrossButton() throws InterruptedException {
		click(locateElement("xpath", "NotificationsPage.xpath.crossButton"));
		Thread.sleep(1000);
		return this;  
	} 
	public NotificationsPage clickRefresh() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "NotificationsPage.xpath.reFreshBtn"));
		Thread.sleep(1000);
		return this;  
	}
	public NotificationsPage clickFlagIcon() throws InterruptedException { 
		Thread.sleep(2000); 
		click(locateElement("xpath", "NotificationsPage.xpath.flag"));
		return this;
	}
	public NotificationsPage clickSuggestions() {
		click(locateElement("xpath", "NotificationsPage.xpath.suggestions"));
		return this;
	}
	public NotificationsPage getSuggestedSkillsCount(String data) {
		String count = locateElement("xpath", "NotificationsPage.xpath.suggestionsCount").getText().replaceAll("\\D", "");
		reportStep("Suggested Skills Count: "+count, "PASS");
		if(count.contains(data)) {
			reportStep("Suggested Skills Count: "+count+" matched successfully", "PASS");
		} else {
			reportStep("Suggested Skills Count: "+count+" not matched", "FAIL");
		}
		return this;
	} 
	public NotificationsPage verifySuggestedSkills(String data1, String data2) {
		String skill1 = locateElement("xpath", "NotificationsPage.xpath.suggestionsSkill1").getText();
		String skill2 = locateElement("xpath", "NotificationsPage.xpath.suggestionsSkill2").getText();
		reportStep("Suggested Skill Names: "+skill1+", "+skill2, "PASS");
		if(skill1.contains(data1) && skill2.contains(data2)) {
			reportStep("Suggested skills: "+skill1+", "+skill2+" matched successfully", "PASS");
		} else {
			reportStep("Suggested skills not matched", "FAIL");
		}
		return this;
	} 
	public NotificationsPage getSuggestedRolesCount(String data) {
		String count = locateElement("xpath", "NotificationsPage.xpath.suggestionsRoleCount").getText().replaceAll("\\D", "");
		reportStep("Suggested Roles Count: "+count, "PASS");
		if(count.contains(data)) {
			reportStep("Suggested Skills Count: "+count+" matched successfully", "PASS");
		} else {
			reportStep("Suggested Skills Count: "+count+" not matched", "FAIL");
		}
		return this;
	}
	public NotificationsPage verifySuggestedRoles(String data) {
		String role = locateElement("xpath", "NotificationsPage.xpath.suggestionsRoles").getText();
		reportStep("Suggested Role Names: "+role, "PASS");
		if(role.contains(data)) {
			reportStep("Suggested role: "+role+" matched successfully", "PASS");
		} else {
			reportStep("Suggested role not matched", "FAIL");
		}
		return this;
	}
	public NotificationsPage enterSkillIndex(String data1) {
		typeValue(locateElement("xpath", "TrackPage.xpath.skillIndex"), data1);
		//typeValue(locateElement("xpath", "TrackPage.xpath.skillIndex"), data2);  
		return this;
	}
	public NotificationsPage clickSkillIndexGo() {
		click(locateElement("xpath", "TrackPage.xpath.skillIndexGo"));
		return this;
	}
	public NotificationsPage clickManagerView() {
		click(locateElement("xpath", "TrackPage.xpath.managerView"));
		return this;
	}
	public NotificationsPage enterSkillIndexManagerView(String data1, String data2) {
		typeValue(locateElement("xpath", "TrackPage.xpath.skillIndexManagerView"), data1);
		typeValue(locateElement("xpath", "TrackPage.xpath.skillIndexManagerView"), data2);  
		return this;
	}
	public NotificationsPage clickSkillIndexManagerViewGo() {
		click(locateElement("xpath", "TrackPage.xpath.skillIndexManagerViewGo"));
		return this;
	}
	public NotificationsPage viewSkillDistributionGraph() {
		scrolltoview("TrackPage.xpath.managerViewDownload");
		reportStep("Skill Distribution Graph", "PASS");
		return this;
	}
	public NotificationsPage clickHrView() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "TrackPage.xpath.hrView"));
		return this;
	}
	public NotificationsPage enterTheRole(String data1, String data2, String data3) {
		typeValue(locateElement("xpath", "TrackPage.xpath.rolefilter"), data1);
		typeValue(locateElement("xpath", "TrackPage.xpath.rolefilter"), data2);
		typeValue(locateElement("xpath", "TrackPage.xpath.rolefilter"), data3);
		return this;
	}
	public NotificationsPage clickResourceStatus() {
		click(locateElement("xpath", "TrackPage.xpath.resourceStatus"));
		return this;
	}
	public NotificationsPage clickSuccessionView() {
		click(locateElement("xpath", "TrackPage.xpath.successionView"));
		return this;
	}
	public NotificationsPage clickHRViewSearch() {
		click(locateElement("xpath", "TrackPage.xpath.hrViewSearch"));
		return this;
	}
	public NotificationsPage enterSkill(String data1, String data2) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterWithSkills"), data1);
		typeValue(locateElement("xpath", "TrackPage.xpath.filterWithSkills"), data2);
		return this;
	}
	public NotificationsPage clickSkillIndexGoButton() {
		click(locateElement("xpath", "TrackPage.xpath.resourceGo"));
		return this;
	}
	public NotificationsPage viewAchievmentsGraph() {
		scrolltoview("TrackPage.xpath.buttonDownload");
		reportStep("Achievements Graph", "PASS");
		return this;
	}
	public NotificationsPage enterSelectRole(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterSelectRole"), data);
		return this;
	}
	public NotificationsPage enterLevel(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterLevel"), data);
		return this;
	}
	public NotificationsPage enterSelectUser(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterSelectUser"), data);
		return this;
	}
	public NotificationsPage enterRange(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterRange"), data);
		return this;
	}
	public NotificationsPage enterSkill(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterSkill"), data);
		return this;
	}
	public NotificationsPage clickMatchButton() {
		click(locateElement("xpath", "TrackPage.xpath.filterMatchButton"));
		return this;
	}
	public NotificationsPage enterSearchBy(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "TrackPage.xpath.searchBy"), data);
		Thread.sleep(2000);
		return this;
	} 
	public NotificationsPage enterSearchRole(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.selectRole"), data);
		return this;
	}
	public NotificationsPage enterSearchRange(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.selectRange"), data);
		return this;
	}
	public NotificationsPage clickSelectSearchButton() {
		click(locateElement("xpath", "TrackPage.xpath.selectSearchButton"));
		return this;
	}


}





























