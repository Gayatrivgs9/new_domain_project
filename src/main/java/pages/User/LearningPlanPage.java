package pages.User;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LearningPlanPage extends PreAndPost {

	public LearningPlanPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	public LearningPlanPage waitUpToFoldingCubeInvisible() {
		if(locateElement("xpath", "LoginPage.xpath.foldingCube")!= null) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfAllElements((locateElement("xpath", "LoginPage.xpath.foldingCube"))));
		} else {
			System.out.println("FoldingCube is invisible");  
		}
		return this; 
	} 
	public LearningPlanPage acceptTheAlertBox() throws InterruptedException {
		Thread.sleep(2000);
		if(locateElements("xpath", "NotificationsPage.xpath.planGuideYes").isEmpty()!= true) {
			click(locateElement("xpath", "NotificationsPage.xpath.planGuideYes")); 
		} else {
			reportStep("Alert box already accepted", "PASS");
			click(locateElement("xpath", "UserHomePage.xpath.learningPlan"));
		}
		return this;
	}
	public LearningPlanPage enterAspiringRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"), data);
		return this;
	}
	public LearningPlanPage filterWithRecommendedLearning() throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("xpath", "LearningPlan.xpath.filter"));
		click(locateElement("xpath", "LearningPlan.xpath.Recommended"));
		return this;
	}
	public LearningPlanPage verifyNextRole() {
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.nextRole"), "Next Role - "+userNextRole);
		return this;
	}
	public LearningPlanPage filterWithSuggstedLearning() throws InterruptedException {
		click(locateElement("xpath", "LearningPlan.xpath.filter"));
		click(locateElement("xpath", "LearningPlan.xpath.Suggested"));
		return this;
	}
	public LearningPlanPage filterWithCurrentRole() {
		click(locateElement("xpath", "LearningPlan.xpath.filterWithRole"));
		click(locateElement("xpath", "LearningPlan.xpath.currentRole"));
		if(locateElements("xpath", "LearningPlan.xpath.aspiringRole").isEmpty() != true) {
			click(locateElement("xpath", "LearningPlan.xpath.aspiringRole"));
		} else {
			System.out.println("Aspiring role is not available");
		}
		return this;
	}
	public LearningPlanPage filterWithNextRole() {
		click(locateElement("xpath", "LearningPlan.xpath.filterWithRole"));
		click(locateElement("xpath", "LearningPlan.xpath.nextRole"));
		if(locateElements("xpath", "LearningPlan.xpath.aspiringRole").isEmpty() != true) {
			click(locateElement("xpath", "LearningPlan.xpath.aspiringRole"));
		} else {
			System.out.println("Aspiring role is not available");
		}
		return this;
	}
	public LearningPlanPage clickCalendar() {
		click(locateElement("xpath", "LearningPlan.xpath.calendar"));
		String text = locateElement("xpath", "LearningPlan.xpath.calendarDateValue").getText();
		reportStep("Calendar filters the data in between : "+text, "PASS"); 
		return this;
	}
	public LearningPlanPage selectFromAndToDate() {
		click(locateElement("xpath", "LearningPlan.xpath.fromDate"));
		click(locateElement("xpath", "LearningPlan.xpath.toDate"));
		click(locateElement("xpath", "LearningPlan.xpath.apply"));
		return this;
	}
	public LearningPlanPage getPlanStatusDetails(){
		String text = getText(locateElement("xpath", "LearningPlan.xpath.daterangeInput"));
		System.out.println(text); 
		return this;
	}
	public LearningPlanPage clickGo() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "LearningPlan.xpath.go"));
		return this;
	}  
	public LearningPlanPage clickClear() throws InterruptedException {
		Thread.sleep(5000);
		click(locateElement("xpath", "LearningPlan.xpath.clear"));
		return this; 
	} 
	public LearningPlanPage clickCurrentPlan() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "LearningPlan.xpath.currentPlan"));   
		return this;
	}
	public LearningPlanPage clickEditPlan() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "LearningPlan.xpath.editPlan"));   
		return this;
	}
	public LearningPlanPage verifyNumberOfCoursesEnrolled() {
		try {
			WebElement enrolled = locateElement("xpath", "LearningPlan.xpath.noOfCoursesEnrolled");
			String text = enrolled.getText();
			System.out.println(text); 
			WebElement enrolledPer = locateElement("xpath", "LearningPlan.xpath.noOfCoursesEnrolledPer");
			String textPer = enrolledPer.getText();
			System.out.println(textPer);
			WebElement completed = locateElement("xpath", "LearningPlan.xpath.noOfCoursesCompleted");
			String text2 = completed.getText();
			System.out.println(text2);
			WebElement completedPer = locateElement("xpath", "LearningPlan.xpath.noOfCoursesCompletedPer");
			String text2Per = completedPer.getText();
			System.out.println(text2Per);
			reportStep("Learning Plan Status Number Of Courses Enrolled "+text+" and Percentage "+textPer+" Completed "+text2+" Percentage "+text2Per, "PASS"); 
		} catch (WebDriverException e) {
			reportStep("No Data Available For LearningPlan", "FAIL");
		}
		return this;
	}
	public LearningPlanPage verifyNumberOfCoursesYetToStart() {
		try {
			WebElement enrolled = locateElement("xpath", "LearningPlan.xpath.noOfCoursesEnrolled");
			String text = enrolled.getText();
			System.out.println(text); 
			WebElement enrolledPer = locateElement("xpath", "LearningPlan.xpath.noOfCoursesEnrolledPer");
			String textPer = enrolledPer.getText();
			System.out.println(textPer);
			WebElement completed = locateElement("xpath", "LearningPlan.xpath.noOfCoursesYetToStart");
			String text2 = completed.getText();
			System.out.println(text2);
			WebElement completedPer = locateElement("xpath", "LearningPlan.xpath.noOfCoursesYetToStartPer");
			String text2Per = completedPer.getText();
			System.out.println(text2Per);
			reportStep("Learning Plan Status Number Of Courses Enrolled "+text+" and Percentage "+textPer+" YetToStart "+text2+" Percentage "+text2Per, "PASS"); 
		} catch (WebDriverException e) {
			reportStep("No Data Available For LearningPlan", "FAIL");
		}
		return this;
	}
	public LearningPlanPage verifyNumberOfCoursesInProgress() {
		try {
			WebElement enrolled = locateElement("xpath", "LearningPlan.xpath.noOfCoursesEnrolled");
			String text = enrolled.getText();
			System.out.println(text); 
			WebElement enrolledPer = locateElement("xpath", "LearningPlan.xpath.noOfCoursesEnrolledPer");
			String textPer = enrolledPer.getText();
			System.out.println(textPer);
			WebElement completed = locateElement("xpath", "LearningPlan.xpath.noOfCoursesInProgress");
			String text2 = completed.getText();
			System.out.println(text2);
			WebElement completedPer = locateElement("xpath", "LearningPlan.xpath.noOfCoursesInProgressPer");
			String text2Per = completedPer.getText();
			System.out.println(text2Per);
			reportStep("Learning Plan Status Number Of Courses Enrolled "+text+" and Percentage "+textPer+" InProgress "+text2+" Percentage "+text2Per, "PASS"); 
		} catch (WebDriverException e) {
			reportStep("No Data Available For LearningPlan", "FAIL");
		}
		return this;
	}
	public LearningPlanPage verifyCumulativeCoursesDuration() {
		try {
			WebElement enrolled = locateElement("xpath", "LearningPlan.xpath.CumulativeCoursesDurationEnrolled");
			String text = enrolled.getText();
			System.out.println(text); 
			WebElement enrolledPer = locateElement("xpath", "LearningPlan.xpath.CumulativeCoursesDurationEnrolledPer");
			String textPer = enrolledPer.getText();
			System.out.println(textPer); 
			WebElement completed = locateElement("xpath", "LearningPlan.xpath.CumulativeCoursesDurationCompleted");
			String text2 = completed.getText();
			System.out.println(text2);
			WebElement completedPer = locateElement("xpath", "LearningPlan.xpath.CumulativeCoursesDurationCompletedPer");
			String text2Per = completedPer.getText();
			System.out.println(text2Per);
			reportStep("Learning Plan Status CumulativeCoursesDuration in Hrs Enrolled "+text+" Perecentage "+textPer+" Completed "+text2+" Percentage "+text2Per, "PASS");
		} catch (WebDriverException e) {
			reportStep("No Data Available For LearningPlan", "FAIL");
		}
		return this; 
	}
	public LearningPlanPage enterSkillIndex(String data, String data2, String data3) throws InterruptedException {
		typeValue(locateElement("xpath", "LearningPlan.xpath.skillIndex"), data);
		typeValue(locateElement("xpath", "LearningPlan.xpath.skillIndex2"), data2);
		typeValue(locateElement("xpath", "LearningPlan.xpath.skillIndex3"), data3);
		Thread.sleep(2000);
		click(locateElement("xpath", "LearningPlan.xpath.skillIndexGo"));
		return this;
	}
	public LearningPlanPage learningProgressGraph1() {
		scrolltoview("LearningPlan.xpath.download1");
		return this;
	}
	public LearningPlanPage learningProgressGraph2() {
		scrolltoview("LearningPlan.xpath.download2");
		return this;
	}
	public LearningPlanPage filterByStatus() {
		click(locateElement("xpath", "LearningPlan.xpath.filterByStatus"));
		click(locateElement("xpath", "LearningPlan.xpath.enrolled"));
		click(locateElement("xpath", "LearningPlan.xpath.yetToStart"));
		click(locateElement("xpath", "LearningPlan.xpath.inProgress"));
		return this;
	}
	public LearningPlanPage clickPersonalizedPlan() throws InterruptedException {
		Thread.sleep(2000); 
		WebElement personalizedPlan = locateElement("xpath", "LearningPlan.xpath.personalizedPlan");
		//if(personalizedPlan.isSelected()!=true) {
		click(personalizedPlan);      
		//}
		return this;
	} 
	public LearningPlanPage enterDayPlan() throws InterruptedException {
		WebElement eleDay = locateElement("xpath", "PlanPage.xpath.day");
		click(eleDay); 
		click(locateElement("xpath", "PlanPage.xpath.Icon"));
		click(locateElement("xpath", "PlanPage.xpath.dayValue"));
		return this;
	}	
	public LearningPlanPage enterWeekPlan() throws InterruptedException {
		WebElement eleWeek = locateElement("xpath", "PlanPage.xpath.week");
		click(eleWeek); 
		Thread.sleep(2000);
		click(locateElement("xpath", "PlanPage.xpath.Icon"));
		click(locateElement("xpath", "PlanPage.xpath.weekValue"));
		return this;
	}
	public LearningPlanPage enterMonthPlan() throws InterruptedException {
		WebElement eleMonth = locateElement("xpath", "PlanPage.xpath.month");
		click(eleMonth); 
		Thread.sleep(2000);
		click(locateElement("xpath", "PlanPage.xpath.Icon"));
		click(locateElement("xpath", "PlanPage.xpath.monthValue"));
		return this; 
	}
	public LearningPlanPage clickRecommendedPlan() {
		WebElement personalizedPlan = locateElement("xpath", "LearningPlan.xpath.recommendedPlan");
		if(personalizedPlan.isSelected()!=true) {
			click(personalizedPlan);  
		}
		return this;
	}
	public LearningPlanPage clickReplan() throws InterruptedException {
		click(locateElement("xpath", "LearningPlan.xpath.replan"));
		Thread.sleep(2000);   
		if(locateElement("xpath", "LearningPlan.xpath.newPlanToUser").getText().contains("Plan has been updated")) {
			reportStep("New plan has been updated", "PASS");
		} else {
			String text = locateElement("xpath", "LearningPlan.xpath.oldPlanToUser").getText();
			reportStep(text, "PASS");
		}
		return this;
	}
	public LearningPlanPage verifyLearningProgressGraph1() throws InterruptedException{
		Thread.sleep(5000); 
		verifyPartialAttribute(locateElement("xpath","PlanPage.xpath.role1grp"), "id", "rolesSection.graphs.individual.inCount");
		verifyPartialAttribute(locateElement("xpath","PlanPage.xpath.role2grp"), "id", "rolesSection.graphs.individual.inDuration");
		verifyPartialAttribute(locateElement("xpath","PlanPage.xpath.role3grp"), "id", "rolesSection.graphs.vsPeers.inCount");
		return this;
	} 
	public LearningPlanPage verifyLearningProgressGraph2() throws InterruptedException{ 
		verifyPartialAttribute(locateElement("xpath","PlanPage.xpath.skill1grp"), "id", "skillsSection.graphs.individual.inCount");
		verifyPartialAttribute(locateElement("xpath","PlanPage.xpath.skill2grp"), "id", "skillsSection.graphs.individual.inDuration");
		verifyPartialAttribute(locateElement("xpath","PlanPage.xpath.skill3grp"), "id", "skillsSection.graphs.vsPeers.inDuration");
		return this;
	} 

	public LearningPlanPage userDragAndDropTheCourses() throws InterruptedException {
		Thread.sleep(3000);  
		WebElement monthTable = locateElement("xpath", "LearningPlan.xpath.dragNDropTable");
		//to get today's day
		DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
		String lowerCase = dayOfWeek.toString().toLowerCase(); 
		String substring = lowerCase.substring(0, 3);
		String todaysWeek = substring.substring(0, 1).toUpperCase()+substring.substring(1);
		System.out.println(todaysWeek);
		WebElement weekOfTheDay = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//th/span[text()='"+todaysWeek+"']"));

		if(!weekOfTheDay.getText().equals("Sat") && !weekOfTheDay.getText().equals("Sun")) {
			WebElement todayDate = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//td[contains(@class, 'fc-today')]//span"));
			moveToElment(todayDate);
			if(monthTable.findElement(By.xpath("//a[contains(@class, 'day-grid-event fc-h-event fc-event fc-start fc-not-end fc-draggable')]")).getAttribute("class").contains("draggable")) {

				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d");  
				LocalDateTime now = LocalDateTime.now();  
				String format = dtf.format(now);
				int parseInt = Integer.parseInt(format);
				int dropDate = parseInt+4;

				WebElement dragElement = monthTable.findElement(By.xpath("//a[contains(@class, 'day-grid-event fc-h-event fc-event fc-start fc-not-end fc-draggable')]"));
				WebElement dropElement = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//td/span[text()='"+dropDate+"']"));
				dragAndDrop(dragElement, dropElement); 

				if(locateElements("xpath", "NotificationsPage.xpath.confirmdialogMsg").isEmpty()!= true) {
					String text = locateElement("xpath", "NotificationsPage.xpath.confirmdialogMsg").getText();
					reportStep(text, "PASS");
					click(locateElement("xpath", "NotificationsPage.xpath.planGuideYes"));  
				} else {
					reportStep("Alert box not populated to send aproval", "PASS");
				}
				verifyPartialText(locateElement("xpath", "LearningPlan.xpath.approvalSucMsg"), "Your revised plan is sent for approval");
			}
		} else {
			reportStep("User can not drag and drop courses on holidays", "PASS");
		}
		return this;  
	}

	public LearningPlanPage userDragAndDropTheCourseWithStartAndndDateYetToStart() throws InterruptedException {
		Thread.sleep(3000);  
		WebElement monthTable = locateElement("xpath", "LearningPlan.xpath.dragNDropTable");
		//to get today's day
		DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
		String lowerCase = dayOfWeek.toString().toLowerCase(); 
		String substring = lowerCase.substring(0, 3);
		String todaysWeek = substring.substring(0, 1).toUpperCase()+substring.substring(1);
		System.out.println(todaysWeek);
		WebElement weekOfTheDay = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//th/span[text()='"+todaysWeek+"']"));

		if(!weekOfTheDay.getText().equals("Sat") && !weekOfTheDay.getText().equals("Sun")) {
			WebElement todayDate = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//td[contains(@class, 'fc-today')]//span"));
			moveToElment(todayDate);
			if(monthTable.findElement(By.xpath("//a[contains(@class, 'fc-draggable')]")).getAttribute("class").contains("draggable")) {

				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd");  
				LocalDateTime now = LocalDateTime.now();  
				String format = dtf.format(now);
				int parseInt = Integer.parseInt(format);
				int dropDate = parseInt+4;

				WebElement dragElement = monthTable.findElement(By.xpath("//a[contains(@class, 'fc-draggable')]"));
				WebElement dropElement = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//td/span[text()='"+dropDate+"']"));
				dragAndDrop(dragElement, dropElement); 

				if(locateElements("xpath", "LearningPlan.xpath.coursesUnderInProgress").isEmpty()!= true) {
					String text = locateElement("xpath", "LearningPlan.xpath.coursesUnderInProgress").getText();
					reportStep("User not allow to drag the course because "+text, "PASS");
				} else {
					reportStep("Beyond course end date user not allowed to drag the course", "PASS");
				}
			}
		} else {
			reportStep("User can not drag and drop courses on holidays", "PASS");
		}
		return this;  
	}

	public LearningPlanPage userDragAndDropTheCourseWithStartAndndDateInProgress() throws InterruptedException {
		Thread.sleep(3000);  
		WebElement monthTable = locateElement("xpath", "LearningPlan.xpath.dragNDropTable");
		//to get today's day
		DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
		String lowerCase = dayOfWeek.toString().toLowerCase(); 
		String substring = lowerCase.substring(0, 3);
		String todaysWeek = substring.substring(0, 1).toUpperCase()+substring.substring(1);
		System.out.println(todaysWeek);
		WebElement weekOfTheDay = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//th/span[text()='"+todaysWeek+"']"));

		if(!weekOfTheDay.getText().equals("Sat") && !weekOfTheDay.getText().equals("Sun")) {
			WebElement todayDate = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//td[contains(@class, 'fc-today')]//span"));
			moveToElment(todayDate);
			if(monthTable.findElement(By.xpath("//a[contains(@class, 'fc-draggable')]")).getAttribute("class").contains("draggable")) {

				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd");  
				LocalDateTime now = LocalDateTime.now();  
				String format = dtf.format(now);
				int parseInt = Integer.parseInt(format);
				int dropDate = parseInt+4;

				WebElement dragElement = monthTable.findElement(By.xpath("//a[contains(@class, 'fc-draggable')]"));
				WebElement dropElement = monthTable.findElement(By.xpath("(//div[@class='fc-view fc-month-view fc-basic-view'])[4]/table//td/span[text()='"+dropDate+"']"));
				dragAndDrop(dragElement, dropElement); 

				if(locateElements("xpath", "LearningPlan.xpath.beyondCourseEndDate").isEmpty()!= true) {
					String text = locateElement("xpath", "LearningPlan.xpath.beyondCourseEndDate").getText();
					reportStep("User not allow to drag the course because "+text, "PASS");
				} else {
					reportStep("User is not allow to drag the courses if the courses are in-progress status", "PASS");
				}
			}
		} else {
			reportStep("User can not drag and drop courses on holidays", "PASS");
		}
		return this;  
	}
}


////p[text()='Beyond course end date is not allowed']
////p[text()='Not allowed as plan got overloaded compare to Hours Planning']



