package pages.User;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class RecommendationPage extends PreAndPost {

	public RecommendationPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	
	public RecommendationPage verifyFilterTextMessage(String expectedText) {
		String text = getText(locateElement("xpath", "AssessmentPage.xpath.helpText"));
		System.out.println("Help "+text); 
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.helpText"), expectedText);
		return this;
	}
	public RecommendationPage enterCurrentRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.currentRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.currentRole"), data);
		return this;
	}
	public RecommendationPage enterCurrentRole() throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.currentRole"));
		return this;
	}
	public RecommendationPage unSelectDefaultCurrentRole() throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.removeCurrentRole"));
		Thread.sleep(1000);
		return this;
	}
	public RecommendationPage enterAspiringRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"), data);
		return this;
	}
	public RecommendationPage scrollToUp() throws IOException, InterruptedException {
		Thread.sleep(1000); 
		scrolltoview("AssessmentPage.xpath.filterWithRoles");
		reportStep("Element Scrolled to up", "PASS");
		return this;  
	}
	public RecommendationPage enterNextRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.nextRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.nextRole"), data);
		return this;
	}
	public RecommendationPage verifyNextRole() {
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.nextRole"), "Next Role - "+userNextRole);
		return this;
	}
	public RecommendationPage enterAspiringRole() {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		if(locateElements("xpath", "AssessmentPage.xpath.aspiringRole").isEmpty() == true) {
			System.out.println("Aspiring Role is Empty");
		} else {
			click(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"));
		}
		return this; 
	}
	public RecommendationPage clickGo() throws InterruptedException {
		Thread.sleep(5000);  
		waitUntilVisibilityOfWebElement("AssessmentPage.xpath.go"); 
		click(locateElement("xpath", "AssessmentPage.xpath.go"));
		return this;
	} 
	public RecommendationPage clickRecommendationTopFilter() throws InterruptedException {
		Thread.sleep(2000);
		scrolltoview("RecommendationPage.xpath.topSkillLrn"); 
		click(locateElement("xpath", "AssessmentPage.xpath.assessmentNewFilter"));
		return this;
	}
	public RecommendationPage getMentorsDetails() throws InterruptedException {
		Thread.sleep(2000); 
		if(locateElements("xpath", "RecommendationPage.xpath.mentorsDetails").isEmpty()!=true) {
			List<WebElement> allElements = locateElements("xpath", "RecommendationPage.xpath.mentorsDetails");
			for (int i = 0; i < allElements.size(); i++) {
				if(allElements.get(i).getText().contains("Contact")) {
					List<WebElement> mentorNames = locateElements("xpath", "RecommendationPage.xpath.mentorName");	
					List<WebElement> mentoremailIds = locateElements("xpath", "RecommendationPage.xpath.mentoremailId");
					List<WebElement> mentorSkills = locateElements("xpath", "RecommendationPage.xpath.mentorSkill");
					System.out.println(allElements.get(i).getText());  
					scrolltoview("RecommendationPage.xpath.skill"); 
					reportStep("Mentor Name : "+mentorNames.get(i).getText()+" Mentor EmailId : "+mentoremailIds.get(i).getText()+" mentor skill : "+mentorSkills.get(i).getText()+" \t", "PASS");  
				}
			} 
		} else {
			reportStep("No recommended mentors for the user", "PASS");
		}
		return this;
	}
	public RecommendationPage clickContactButton() {
		List<WebElement> allElements = locateElements("xpath", "RecommendationPage.xpath.contactButton");
		for (int i = 0; i < allElements.size(); i++) {
			allElements.get(i).click(); 
		}
		return this;
	}
	public RecommendationPage verifyRequestSentMssage(String data) {
		waitUntilVisibilityOfWebElement("RecommendationPage.xpath.mentorReqMsg");
		verifyPartialText(locateElement("xpath", "RecommendationPage.xpath.mentorReqMsg"), data); 
		return this;
	}

	//public int recommendationCompleted;
	public RecommendationPage verifyRecommendedCoursCount() throws InterruptedException {
		Thread.sleep(2000);  
		double recommended = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalRecommended").getText().replaceAll("\\D", "")); 
		double enrolld = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalEnrolled").getText().replaceAll("\\D", ""));
		int yetToStart = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalYetToStart").getText().replaceAll("\\D", ""));
		int inProgress = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalInProgress").getText().replaceAll("\\D", ""));
		recommendationCompleted = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalCompleted").getText().replaceAll("\\D", ""));
		int notEnrolld = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalNotEnrolled").getText().replaceAll("[ Not Enrolled ]", ""));
		recommended = enrolld+(notEnrolld);
		System.out.println("The count of Recommended courses is equal to enrolled, notEnrolled. Count is: "+recommended);
		reportStep("Enrolled course count = "+enrolld+" NotEnrolled course count = "+notEnrolld+" is equals to Recommended course count ="+recommended, "PASS");
		enrolld = yetToStart+inProgress+recommendationCompleted;
		System.out.println("The count of Enrolled is equal to inProgress,notEnrolled,yetToStart. Count is: "+enrolld); 
		reportStep("Enrolled course count = "+enrolld+" is eqalent to InProcess course count = "+inProgress+" Completed course count = "+recommendationCompleted+" yetToStart course count = "+yetToStart, "PASS");
		return this;
	}
	
	public RecommendationPage verifyRecommendedAndCompletedCoursesCount() throws InterruptedException {
		recommended = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalRecommended").getText().replaceAll("\\D", "")); 
		recommendationCompleted = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalCompleted").getText().replaceAll("\\D", ""));
		System.out.println("The count of Recommended courses is : "+recommended+" and count of completed courses is : "+recommendationCompleted);
		reportStep("The count of Recommended courses is : "+recommended+" and count of completed courses is : "+recommendationCompleted, "PASS");
		return this;
	}
	
	String text, process;
	public RecommendationPage getTopSkilledLearnerDetails() throws InterruptedException, IOException {
		Thread.sleep(3000); 
		List<WebElement> topSkilled = locateElements("xpath", "RecommendationPage.xpath.member");
		if(locateElements("xpath", "RecommendationPage.xpath.member").isEmpty() != true) {
			List<WebElement> processOfTopSkilledLearner = locateElements("xpath", "RecommendationPage.xpath.memberProgress");
			int topSkilledCount = topSkilled.size();
			System.out.println("Top Skilled Learner Count : "+topSkilledCount); 
			for(int i = 0; i<=(topSkilledCount)-1; i++) {
				text = topSkilled.get(i).getText(); 
				System.out.println(text);  
			}			
			for (int j = 0; j <=(processOfTopSkilledLearner.size())-1; j++) {
				process = processOfTopSkilledLearner.get(j).getAttribute("style").replaceAll("\\D", "");
				System.out.println("process is : "+process+" %");   
			}
			scrolltoview("RecommendationPage.xpath.skill"); 
			reportStep("Top Skilled Learners:"+process, "PASS"); 
		} else {
			reportStep("No Peers To Display!!", "PASS");
		}
		return this;
	}
	public static String yourGoalsText;
	public RecommendationPage getYourLearningGoalsList() throws InterruptedException {
		Thread.sleep(2000); 
		if(locateElements("xpath", "RecommendationPage.xpath.yourLearningGoals").isEmpty() != true) {
			List<WebElement> learningGoals = locateElements("xpath", "RecommendationPage.xpath.yourLearningGoals");
			int learningGoalsCount = learningGoals.size();
			System.out.println("Your Learning Goals List count : "+(learningGoalsCount-1)); 
			reportStep("Your Learning Goals Course Cont : "+(learningGoalsCount-1), "PASS");
			for(int i = 1; i<=learningGoalsCount-1; i++) {
				yourGoalsText = learningGoals.get(i).getText(); 
				try {
					if(yourGoalsText.length()==0) {
						click(locateElement("xpath", "RecommendationPage.xpath.yourGoalsIcon1"));
						yourGoalsText = learningGoals.get(i).getText();
						reportStep("Your Learning Goals Course: "+yourGoalsText, "PASS");
					} else{
						reportStep("Your Learning Goals Course: "+yourGoalsText, "PASS");
					}} catch (Exception e) {
						reportStep("Unknown exception occured while clicking in the field :", "FAIL");
					}
			}} else {
				reportStep("No Learning Goals!!", "PASS");
			}
		return this;
	}
	public static String completedCoursesText;
	public RecommendationPage getCompletedCoursesList() throws InterruptedException {
		if(locateElements("xpath", "RecommendationPage.xpath.completedRef").isEmpty() != true) {
			List<WebElement> completedCourses = locateElements("xpath", "RecommendationPage.xpath.completedRef");
			int completedCoursesCount = completedCourses.size();
			System.out.println("Completed Courses List count : "+(completedCoursesCount-1)); 
			reportStep("Completed Courses List count : "+(completedCoursesCount-1), "PASS");
			for(int i = 1; i<=(completedCoursesCount)-1; i++) {
				completedCoursesText = completedCourses.get(i).getText();
				try {
					if(completedCoursesText.length()==0) {
						click(locateElement("xpath", "RecommendationPage.xpath.yourGoalsIcon1"));
						completedCoursesText = completedCourses.get(i).getText();
						reportStep("Completed Courses: "+completedCoursesText, "PASS");
					} else {
						reportStep("Completed Courses: "+completedCoursesText, "PASS");
					}} catch (Exception e) {
						reportStep("Unknown exception occured while clicking in the field :", "FAIL");
					}
			}} else {
				scrolltoview("RecommendationPage.xpath.NoCompletedCourses");
				reportStep("No Completed Courses!!", "PASS");
			}
		return this;
	}
	public static String suggestedCoursesText;
	public RecommendationPage getSuggestedCoursesList() throws InterruptedException {
		if(locateElements("xpath", "RecommendationPage.xpath.suggestedRef").isEmpty() != true) {
			List<WebElement> suggestedCourses = locateElements("xpath", "RecommendationPage.xpath.suggestedRef");
			int suggestedCoursesCount = suggestedCourses.size();
			System.out.println("Suggested Courses List count : "+(suggestedCoursesCount-1)); 
			reportStep("Suggested Courses List count : "+(suggestedCoursesCount-1), "PASS");
			for(int i = 1; i<=(suggestedCoursesCount)-1; i++) {
				suggestedCoursesText = suggestedCourses.get(i).getText();
				try {
					if(suggestedCoursesText.length()==0) { 
						click(locateElement("xpath", "RecommendationPage.xpath.yourGoalsIcon3"));
						suggestedCoursesText = suggestedCourses.get(i).getText();
						reportStep("Suggested Courses: "+suggestedCoursesText, "PASS");
					} else {
						reportStep("Suggested Courses: "+suggestedCoursesText, "PASS");
					}} catch (Exception e) {
						reportStep("Unknown exception occured while clicking in the field :", "FAIL");
					}
			}} else {
				scrolltoview("RecommendationPage.xpath.NoSuggestedCourses");
				reportStep("No Suggested Courses!", "PASS");
			}
		return this;
	}

	public RecommendationPage verifyUserNextRole() {
		verifyPartialText(locateElement("xpath", "AchievementPage.xpath.nextRoleName"), userNextRole); 
		return this; 
	}
}










