package pages.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AchievementPage extends PreAndPost {

	public AchievementPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public AchievementPage verifyAchievementsHeaderSection() {
		verifyExactText(locateElement("xpath", "AchievementPage.xpath.textAchievment"), "Achievements");
		return this;
	}

	/*public AchievementPage getTheUserData() throws InterruptedException {

		try {
			String text = getText(locateElement("xpath", "AchievementPage.xpath.aspireName"));
			System.out.println(text);
			String text2 = getText(locateElement("xpath", "AchievementPage.xpath.aspireEmail"));
			System.out.println(text2);
			reportStep("User Name : "+text+" User Email : "+text2, "PASS");
		} catch (WebDriverException e) {
			reportStep("Problem With User Data", "FAIL");  
		}
		return this;
	}*/
	public AchievementPage getTheUserData() throws InterruptedException {

		try {
			String text = getText(locateElement("xpath", "AchievementPage.xpath.aspireName"));
			System.out.println(text);
			String text2 = getText(locateElement("xpath", "AchievementPage.xpath.aspireEmail"));
			System.out.println(text2);
			reportStep("User First Name & Last Name : "+text+" User Email : "+text2, "PASS");
			System.out.println(); 
			String text3 = getText(locateElement("xpath", "AchievementPage.xpath.AllData"));
			reportStep("User Data : "+text3, "PASS");
		} catch (WebDriverException e) {
			reportStep("Problem With User Data", "FAIL");  
		}
		return this;
	}

	public AchievementPage getUserRoleDetails() {
		String current = getText(locateElement("xpath", "AchievementPage.xpath.curerntAch")).trim().replaceAll(" - CURRENT", "");
		reportStep("User current role : "+current, "PASS");
		String next = getText(locateElement("xpath", "AchievementPage.xpath.nextAch")).trim().replaceAll(" - NEXT", "");
		reportStep("User next role : "+next, "PASS");
		String aspiring = getText(locateElement("xpath", "AchievementPage.xpath.aspiringAch")).trim().replaceAll(" - ASPIRING", "");
		reportStep("User aspiring role : "+aspiring, "PASS");
		return this;
	}

	public AchievementPage getAvailableAspiringRoleForUser() {
		List<String > allAspiringRoles = new ArrayList<String>();
		click(locateElement("xpath", "AchievementPage.xpath.clearAndSle"));  
		String current = locateElement("xpath", "AchievementPage.xpath.curerntAch").getText().trim();
		String next = locateElement("xpath", "AchievementPage.xpath.curerntAch").getText().trim();
		List<WebElement> allElements = locateElements("xpath", "AchievementPage.xpath.allAspiringRoles");
		for (int i = 0; i <allElements.size(); i++) {
			String text = allElements.get(i).getText();
			allAspiringRoles.add(text); 
			if(!current.equals(text) && !next.equals(text)) {
				reportStep("Available aspiring roles for user : "+text, "PASS"); 
			} else {
				reportStep("Something went wrong while fetching user aspiring role", "FAIL"); 
			}
		}
		return this;
	}
    public AchievementPage selectAspiringRoleForUser(String aspiringRole, String successMsg) throws InterruptedException {
    	click(locateElement("xpath", "AchievementPage.xpath.aspiringDrop"));
    	click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+aspiringRole+"']"));
    	Thread.sleep(1000);
    	click(locateElement("xpath", "AchievementPage.xpath.updateNewAspireBtn")); 
    	waitUntilVisibilityOfWebElement("AchievementPage.xpath.aspSucMsg");
    	verifyExactText(locateElement("xpath", "AchievementPage.xpath.aspSucMsg"), successMsg);
    	return this;
    }
	public AchievementPage enterAspiringRole() throws InterruptedException {
		Thread.sleep(2000);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.aspireRole");
		click(element);
		Thread.sleep(2000);
		typeValue(locateElement("xpath", "AchievementPage.xpath.aspireRole"), "Senior Developer - PHP");
		click(locateElement("xpath", "AchievementPage.xpath.updateButton"));
		return this;
	}

	public AchievementPage verificationOfAspiringRole() throws InterruptedException {
		Thread.sleep(2000);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.aspireRole");
		click(element);
		Thread.sleep(1000);
		click(locateElement("xpath", "AchievementPage.xpath.allAspiringRoles"));
		String actualAspiringRole = getAttribute(locateElement("xpath", "AchievementPage.xpath.allAspiringRoles"), "value").trim(); 
		click(locateElement("xpath", "AchievementPage.xpath.updateButton"));
		Thread.sleep(3000);
		String expectedAspiringRole = getText(locateElement("xpath", "AchievementPage.xpath.aspiringAch")).trim().replaceAll(" - aspiring", "");
		if(actualAspiringRole.equals(expectedAspiringRole)) { 
			reportStep("User aspiring role : "+expectedAspiringRole, "PASS");
		} else {
			reportStep("Something went wrong while fetching user aspiring role ", "FAIL");
		}
		return this;
	}

	public AchievementPage monthWiseCourseCompletionDuration() {
		scrolltoview("AchievementPage.xpath.monthlyDuration");
		return this;
	}

	public AchievementPage verifyUserCurrentRole() throws IOException {
		//userCurrentRole = getText(locateElement("xpath", "AchievementPage.xpath.curerntAch")).trim().replaceAll(" - current", "");
		userCurrentRole = readData(3, 1, "DefaultNextRole", "DataSheet");
		System.out.println("User current role : "+userCurrentRole);
		//userNextRole = getText(locateElement("xpath", "AchievementPage.xpath.nextAch")).trim().replaceAll(" - next", "");
		userNextRole = readData(4, 1, "DefaultNextRole", "DataSheet");
		System.out.println("User next role : "+userNextRole);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.curerntAch");
		String text = element.getText();
		String newString = " ";
		String[] split = text.split("/n");
		for (String eachSplit : split) { 
			System.out.println("Each Value "+eachSplit);
		if(userCurrentRole.trim().contains(newString)) {
			reportStep("User current role :"+userCurrentRole, "PASS");
		} else {
			reportStep("Unknown problem occured while fetching user current role ", "FAIL");
		}
		if(userNextRole.contains(newString)) {
			reportStep("User next role : "+userNextRole, "PASS");
		} else {
			reportStep("Unknown problem occured while fetching user next role ", "FAIL");
		}
		}
		String skillName = "";
		String skillPercentage = "";
		List<WebElement> skill = locateElements("xpath", "AchievementPage.xpath.progress-text");
		int skillSize = skill.size();
		for (int i = 0; i < ((skill.size()+1)-(skillSize)); i++) { 
			skillName = skill.get(0).getText().trim(); 
			reportStep("User skill : "+skillName, "PASS"); 
		} 
		List<WebElement> percentage = locateElements("xpath", "AchievementPage.xpath.pro-val");
		int percentageSize = percentage.size();
		for (int i = 0; i < ((percentage.size()+1)-(percentageSize)); i++) { 
			skillPercentage = percentage.get(0).getText().trim().replaceAll("/100", "").concat(" %");   
			reportStep("User skill Percentage : "+skillPercentage, "PASS"); 
		}
		//String[] values = {skillName,skillPercentage}; 
		return this;
	}

	public AchievementPage verifyUserNextRole() {
		String next = getText(locateElement("xpath", "AchievementPage.xpath.nextAch")).trim().replaceAll(" - next", "");
		List<WebElement> allElements = locateElements("xpath", "AchievementPage.xpath.currentSkill");
		for (int i = 0; i < allElements.size(); i++) {
			String nextRole = allElements.get(1).getText(); 
			if(next.equals(nextRole)) {
				reportStep("User next role :"+next, "PASS");
			} else {
				reportStep("Unknown problem occured while fetching user next role ", "FAIL");
			}
		}
		return this;
	}

	public AchievementPage verifyUserAspiringRole() throws InterruptedException {
		Thread.sleep(2000); 
		String aspiring = getText(locateElement("xpath", "AchievementPage.xpath.aspiringAch")).trim().replaceAll(" - aspiring", "");
		List<WebElement> allElements = locateElements("xpath", "AchievementPage.xpath.currentSkill");
		for (int i = 0; i < allElements.size(); i++) {
			String aspiringRole = allElements.get(2).getText(); 
			if(aspiring.equals(aspiringRole)) {
				reportStep("User current role :"+aspiring, "PASS");
			} else {
				reportStep("Unknown problem occured while fetching user aspiring role ", "FAIL");
			}
		}
		return this;
	}

	public AchievementPage aspireTopAchievments() throws InterruptedException {
		List<WebElement> skillBadges = locateElements("xpath", "AchievementPage.xpath.RecentSkillBadges");
		for (WebElement eachSkillBadges : skillBadges) {
			System.out.println(eachSkillBadges.getText().trim()); 
		}
		Thread.sleep(1000);
		//scrolltoview("AchievementPage.xpath.RecentSkillBadges"); 
		reportStep("Aspire Recent Skill Badges:", "PASS");
		return this;  
	}

	public AchievementPage aspireCourseCertificates() throws InterruptedException {
		Thread.sleep(3000);
		List<WebElement> skillBadges = locateElements("xpath", "AchievementPage.xpath.RecentCourseCertificates");
		System.out.println("Recent Course Certificates Count :"+(skillBadges.size()-3));
		Thread.sleep(3000);
		for (int i = 1; i < skillBadges.size()-2; i++) { 
			System.out.println(skillBadges.get(i).getAttribute("value").trim());  
		}
		Thread.sleep(1000);
		scrolltoview("AchievementPage.xpath.RecentCourseCertificates"); 
		reportStep("Aspire Top Achievements:", "PASS");
		return this;
	} 
	/*public AchievementPage selectCategory() {
		click(locateElement("xpath", "AchievementPage.xpath.selectCatgory"));
		return this;
	}*/
	public AchievementPage selectNextRole() {
		click(locateElement("xpath", "AchievementPage.xpath.selectNextRole"));
		return this;
	}

	public AchievementPage clickOnUpdate() {
		click(locateElement("xpath", "AchievementPage.xpath.roleUpdate"));
		return this;
	}

	public AchievementPage getRoleDetails() throws InterruptedException {
		click(locateElement("xpath", "AchievementPage.xpath.selectCatgory"));
		Thread.sleep(2000);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.selectCategoryRole");
		System.out.println(getText(element));
		click(locateElement("xpath", "AchievementPage.xpath.selectCategoryRole"));
		//click(locateElement("xpath", "AchievementPage.xpath.circle"));
		return this;
	}

	public AchievementPage getRoleOverallAnalysis() throws InterruptedException {
		Thread.sleep(3000);
		String text  = "";
		List<WebElement> element = locateElements("xpath", "AchievementPage.xpath.role"); 
		for (int i = 0; i <element.size(); i++) {
			text = element.get(i).getText();
			System.out.println(text);
			if(text.contains("%")) {
				reportStep("Role progress : "+text, "PASS"); 
			} else {
				reportStep("Role progress : "+text+" 100% Completed", "PASS");  
			}
		}
		Thread.sleep(1000); 
		return this;
	}

	public AchievementPage getSkillDetails() throws InterruptedException {
		click(locateElement("xpath", "AchievementPage.xpath.selectCatgory"));
		Thread.sleep(2000);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.selectCategorySkill");
		System.out.println(getText(element));
		click(locateElement("xpath", "AchievementPage.xpath.selectCategorySkill"));
		//click(locateElement("xpath", "AchievementPage.xpath.circle"));
		return this;
	}

	public AchievementPage getSkillOverallAnalysis() throws InterruptedException {
		Thread.sleep(2000);
		String text  = "";
		List<WebElement> element = locateElements("xpath", "AchievementPage.xpath.skill");
		for (int i = 0; i <element.size(); i++) {
			text = element.get(i).getText();
			System.out.println(text);
			if(text.contains("%")) {
				reportStep("Skill progress : "+text, "PASS"); 
			} else {
				reportStep("Skill progress : "+text+" 100% Completed", "PASS");
			}
		}
		Thread.sleep(1000);
		return this;
	}

	public AchievementPage clickSelectCategory() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AchievementPage.xpath.selectCatgory"));
		Thread.sleep(2000);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.selectCategoryCourse");
		System.out.println(getText(element));
		reportStep("New user course count is: "+0, "PASS"); 
		return this;
	}

	public AchievementPage getCourseDetails() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AchievementPage.xpath.selectCatgory"));
		Thread.sleep(2000);
		WebElement element = locateElement("xpath", "AchievementPage.xpath.selectCategoryCourse");
		System.out.println(getText(element));
		click(locateElement("xpath", "AchievementPage.xpath.selectCategoryCourse"));
		//click(locateElement("xpath", "AchievementPage.xpath.circle"));
		return this;
	}

	public AchievementPage getCourseOverallAnalysis() throws InterruptedException {
		Thread.sleep(3000);
		try {
			String text="";
			List<WebElement> element = locateElements("xpath", "AchievementPage.xpath.allCoursesModule");
			overAllCoursesCount = element.size();
			System.out.println(overAllCoursesCount); 
			for (int i = 0; i <element.size(); i++) {
				text = element.get(i).getText();
				System.out.println(text);
				if(text.contains("%")) {
					reportStep("Courses progress : "+text, "PASS");
				} else {
					reportStep("Courses progress : "+text+" 100% Completed", "PASS");
				}
			}
			scrolltoview("AchievementPage.xpath.allCoursesModuleLast");   
			reportStep("Overall courses with suggested courses count at AchievementPage is: "+overAllCoursesCount, "PASS");
		} catch (WebDriverException e) {
			reportStep("Problem at Overall courses", "FAIL");
		}
		Thread.sleep(1000);
		return this;
	}

	public AchievementPage overAllCourseCountValidation() {
		if (rowCount == overAllCoursesCount) {
			reportStep("Imported courses count matched with overall count "+rowCount+" = "+overAllCoursesCount, "PASS");
		} else {
			reportStep("Imported courses count not matched with overall count "+rowCount+" != "+overAllCoursesCount, "PASS");
		}
		return this;
	}

	public AchievementPage overAllReImportedCourseCountValidation() {
		int sum = (overAllCoursesCount-rowCount);
		if (rowCount == overAllCoursesCount) {
			reportStep("(OverAll Courses Count) - (Newly Imported Courses Count) is: "+overAllCoursesCount+" - "+rowCount+" = "+sum, "PASS");
		} else {
			reportStep("(OverAll Courses Count) - (Newly Imported Courses Count) is: "+overAllCoursesCount+" - "+rowCount+" = "+sum, "PASS");
		}
		return this;
	}

	public AchievementPage userSkillAchievementPercentage() {
		String skillPercentage = locateElement("xpath", "AchievementPage.xpath.skillPercentage").getText().replaceAll("\\D", "");
		reportStep("User skill completion percentage : "+skillPercentage, "PASS");
		return this; 
	}

	double userSkillPercentage;
	public AchievementPage userSkillPercentage() {
		double totalAssignedAndRecommended = (assigned+recommended);
		double totalCompleted = (assssmentCompleted+recommendationCompleted);
		userSkillPercentage = ((totalCompleted)/(totalAssignedAndRecommended)*100);
		int roundupPercentage = (int)userSkillPercentage; 
		String replaceAll = locateElement("xpath", "AchievementPage.xpath.skillPercentage").getText().replaceAll("\\D", "");
		String substring = replaceAll.substring(0, 2);
		int skillPercent = Integer.parseInt(substring); 
		if(roundupPercentage == skillPercent) {
			reportStep("User skill completion percentage : "+roundupPercentage, "PASS"); 
		} else {
			reportStep("User skill completion percentage : "+roundupPercentage+" percentage not matched", "FAIL");
		}
		return this; 
	}

	public AchievementPage selectNextRoleAsDefault() {
		click(locateElement("xpath", "AchievementPage.xpath.nextRoleNew"));
		click(locateElement("xpath", "AchievementPage.xpath.clickNextRole"));
		click(locateElement("xpath", "AchievementPage.xpath.nextButton"));
		return this;
	}

	public AchievementPage verifySuccessMessage(String data) {
		verifyExactText(locateElement("xpath", "AchievementPage.xpath.nextUpdateSuc"), data);  
		return this;
	}

	public AchievementPage verifyOverAllAnalysisText() {
		verifyPartialText(locateElement("xpath", "AchievementPage.xpath.newOverAllAnalysis"), "Overall Analysis"); 
		return this;
	}

	public AchievementPage verifyGraphsDrillDownText() {
		verifyPartialText(locateElement("xpath", "AchievementPage.xpath.drillDownGraphs"), "Click on each of the graphs to drill down");
		return this;
	}

	public AchievementPage userTopAchievements() {

		if(locateElements("xpath", "AchievementPage.xpath.topAchBadges").isEmpty()!= true) {
			List<WebElement> allElements = locateElements("xpath", "AchievementPage.xpath.topAchBadges");
			for (int i = 0; i < allElements.size(); i++) {
				String text = allElements.get(i).getText();
				reportStep("User top achievements skill badges : "+text, "PASS");
			}
		}
		else {
			reportStep("User not completed the skill 100 %", "PASS");
		}
		return this;
	}

}




