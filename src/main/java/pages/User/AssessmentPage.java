package pages.User;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AssessmentPage extends PreAndPost {

	public AssessmentPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public AssessmentPage verifyFilterTextMessage(String expectedText) {
		String text = locateElement("xpath", "AssessmentPage.xpath.helpText").getText();
		System.out.println("Help "+text); 
		waitUntilVisibilityOfWebElement("AssessmentPage.xpath.helpText");
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.helpText"), expectedText);
		return this;
	}
	public AssessmentPage enterCurrentRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.currentRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.currentRole"), data);
		return this;
	}
	public AssessmentPage unSelectDefaultCurrentRole() throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.removeCurrentRole"));
		Thread.sleep(1000);
		return this;
	}
	public AssessmentPage enterNextRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.nextRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.nextRole"), data);
		return this;
	}
	public AssessmentPage verifyNextRole() {
		WebElement locateElement = locateElement("xpath", "AssessmentPage.xpath.nextRole");
		System.out.println(locateElement.getText());  
		System.out.println(userNextRole); 
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.nextRole"), "Next Role - "+userNextRole);
		return this;
	} 
	public AssessmentPage enterAspiringRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"), data);
		return this;
	}
	public AssessmentPage enterAspiringRole() {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		if(locateElements("xpath", "AssessmentPage.xpath.aspiringRole").isEmpty() == true) {
			System.out.println("Aspiring Role is Empty");
		} else {
			click(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"));
		}
		return this;
	}
	public AssessmentPage clickGo() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AssessmentPage.xpath.go"));
		return this;
	}
	public AssessmentPage verifyInternalAssessmentUserReview(String expectedText) {
		waitUntilVisibilityOfWebElement("AssessmentPage.xpath.PendingAssessments"); 
		verifyPartialText(locateElement("xpath", "AssessmentPage.xpath.PendingAssessments"), expectedText);
		return this;
	}
	public AssessmentPage verifyCreateAssessment(String expectedText) {
		waitUntilVisibilityOfWebElement("AssessmentPage.xpath.CompletedAssessments");
		verifyPartialText(locateElement("xpath", "AssessmentPage.xpath.CompletedAssessments"), expectedText);
		return this; 
	}
	public AssessmentPage clickInternalAssessmentUserReview() {
		waitUntilVisibilityOfWebElement("AssessmentPage.xpath.clickIntAssUserReview");
		click(locateElement("xpath", "AssessmentPage.xpath.clickIntAssUserReview"));
		return this;
	}
	public AssessmentPage clickCreateInternalAssessment() {
		click(locateElement("xpath", "AssessmentPage.xpath.clickCreateIntAss"));
		return this; 
	}
	public AssessmentPage verifyInternalAssessmentReviewFromTitle(String expectedText) throws InterruptedException {
		waitUntilVisibilityOfWebElement("AssessmentPage.xpath.revForm");
		verifyPartialText(locateElement("xpath", "AssessmentPage.xpath.revForm"), expectedText);
		return this;
	}
	public AssessmentPage verifyAssessmentBuilderTitle(String expectedText) throws InterruptedException {
		Thread.sleep(2000);
		verifyPartialText(locateElement("xpath", "AssessmentPage.xpath.assBuild"), expectedText);
		return this; 
	}

	public AssessmentPage verifyTotalAssessmentCoursCount() throws InterruptedException {
		int enrolld, yetToStart = 0, inProgress = 0;
		try {
			Thread.sleep(3000);
			WebElement totalToBeCompleted = locateElement("xpath", "AssessmentPage.xpath.totalToBeAssigned");
			int toBeCompleted = Integer.parseInt(totalToBeCompleted.getText().replaceAll("\\D", "")); 
			enrolld = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalEnrolled").getText().replaceAll("\\D", ""));
			yetToStart = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalYetToStart").getText().replaceAll("\\D", ""));
			inProgress = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalInProgress").getText().replaceAll("\\D", ""));
			assssmentCompleted = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalCompleted").getText().replaceAll("\\D", ""));
			int notEnrolld = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalNotEnrolled").getText().replaceAll("\\D", ""));
			toBeCompleted = enrolld+notEnrolld;
			System.out.println("The count of toBeCompleted courses is equal to enrolled, notEnrolled. Count is: "+toBeCompleted);
			reportStep("Enrolled course count = "+enrolld+" NotEnrolled course count = "+notEnrolld+" is equals to ToBeComplmted course count ="+toBeCompleted, "PASS");
		}catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		try {
			enrolld = yetToStart+inProgress+assssmentCompleted;
			System.out.println("The count of Enrolled is equal to inProgress,notEnrolled,yetToStart. Count is: "+enrolld); 
			reportStep("Enrolled course count = "+enrolld+" is eqalent to InProcess course count = "+inProgress+" Completed course count = "+assssmentCompleted+" yetToStart course count = "+yetToStart, "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this; 
	}

	public AssessmentPage verifyAssignedAndCompletedCoursesCount() {
		try {
			WebElement totalToBeCompleted = locateElement("xpath", "AssessmentPage.xpath.totalToBeAssigned");
			assigned = Integer.parseInt(totalToBeCompleted.getText().replaceAll("\\D", "")); 
			assssmentCompleted = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalCompleted").getText().replaceAll("\\D", ""));
			System.out.println("The count of assigned courses is : "+assigned+" count of completed courses is :"+assssmentCompleted);
			reportStep("The count of assigned courses is : "+assigned+" count of completed courses is :"+assssmentCompleted, "PASS");
		}catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this; 

	}
	public List<String> pendingAssementList;
	public static String text;
	static int assessmntsCount;
	public AssessmentPage getPendingAssessmentsCount() throws InterruptedException {
		Thread.sleep(1000); 
		if (locateElements("xpath", "AssessmentPage.xpath.pendingAssessmentsNew").isEmpty() != true) {
			//scrolltoview("AssessmentPage.xpath.PendingAssessments");
			List<WebElement> assessmnts = locateElements("xpath", "AssessmentPage.xpath.pendingAssessmentsNew");
			assessmntsCount = assessmnts.size();
			System.out.println("Pending Assessments List count : " + (assessmntsCount-1));
			for (int i = 1; i <= (assessmntsCount - 1); i++) {
				text = assessmnts.get(i).getText();
				try {
					if (text.length() == 0) {
						click(locateElement("xpath", "AssessmentPage.xpath.nextButton1"));
						text = assessmnts.get(i).getText();
						reportStep("Pending Assessments name: " + text, "PASS");
					} else {
						reportStep("Pending Assessments name: " + text, "PASS");
					}
				} catch (Exception e) {
					reportStep("Unknown exception occured while clicking in the field :", "FAIL");
				}
			} 
		} else {
			scrolltoview("AssessmentPage.xpath.NoPendingCourses"); 
			reportStep("No Pending Assessments!!", "PASS");  
		}
		return this;
	}

	int completedAssessmentCount;
	public static String compltedText;
	public AssessmentPage getCompletedAssessmentsCount() throws InterruptedException{
		if (locateElements("xpath", "AssessmentPage.xpath.completedAssessmentsCount").isEmpty() != true) {
			scrolltoview("AssessmentPage.xpath.CompletedAssessments");
			List<WebElement> courseList = locateElements("xpath", "AssessmentPage.xpath.completedAssessmentsCount");
			completedAssessmentCount = courseList.size();
			System.out.println("Completed Assessment List count :" + (completedAssessmentCount - 1));
			for (int i = 1; i <= (completedAssessmentCount - 1); i++) {
				compltedText = courseList.get(i).getText();
				try {
					if (compltedText.length() == 0) {
						click(locateElement("xpath", "AssessmentPage.xpath.nextButton1"));
						compltedText = courseList.get(i).getText();
						reportStep("Completed Assessmnts name: " + compltedText, "PASS");
					} else {
						reportStep("Completed Assessmnts name: " + compltedText, "PASS");
					}
				} catch (Exception e) {
					reportStep("Unknown exception occured while clicking in the field :", "FAIL");
				}
			}
		} else {
			scrolltoview("AssessmentPage.xpath.NoCompletedCourses"); 
			reportStep("No Completed Assessments!!", "PASS");
		} 
		return this;
	}
	static int  suggstionsCount;
	public static String suggestionsText;
	public AssessmentPage getSuggestionsCourseCount() throws InterruptedException{
		if(locateElements("xpath", "AssessmentPage.xpath.suggestionsCourseCount").isEmpty() != true) {
			scrolltoview("AssessmentPage.xpath.suggestions"); 
			List<WebElement> courseList = locateElements("xpath", "AssessmentPage.xpath.suggestionsCourseCount");
			suggstionsCount = courseList.size();
			System.out.println("Suggestions Course List count :"+(suggstionsCount-1));
			for (int i = 1; i <= (suggstionsCount -1); i++) {
				suggestionsText = courseList.get(i).getText();
				try { 
					if (suggestionsText.length() == 0) {
						//click(locateElement("xpath", "AssessmentPage.xpath.nextButton3"));
						suggestionsText = courseList.get(i).getText(); 
						reportStep("Suggestions Course name: " + suggestionsText, "PASS");
					} else {
						reportStep("Suggestions Course name: " + suggestionsText, "PASS");
					}
				} catch (Exception e) {
					reportStep("Unknown exception occured while clicking in the field :", "FAIL");
				}
			}
		} else {
			scrolltoview("AssessmentPage.xpath.NoSuggestedCourses");  
			reportStep("No Suggested Assessments!!", "PASS");
		}
		return this;
	}

	public AssessmentPage getInternalAssessmentCount() {
		List<WebElement> allElements = locateElements("xpath", "AssessmentPage.xpath.pendingAssessmentsNewStart"); 
		if(locateElements("xpath", "AssessmentPage.xpath.startPendingAss").isEmpty() != true) {
			reportStep("Internal assessment count is : "+allElements.size(), "PASS"); 
			click(locateElement("xpath", "AssessmentPage.xpath.startPendingAss"));
		} else {
			reportStep("No Internal assessment available", "PASS"); 
		}
		return this;
	}
	public AssessmentPage assessmentsStatus() throws IOException, InterruptedException {
		Thread.sleep(1000); 
		scrolltoview("AssessmentPage.xpath.download");
		reportStep("Review Progress Current Status", "PASS");
		return this; 
	}
	public AssessmentPage clickAssessmentTopFilter() {
		click(locateElement("xpath", "AssessmentPage.xpath.assessmentNewFilter"));
		return this;
	}
	public AssessmentPage scrollToUp() throws IOException, InterruptedException {
		Thread.sleep(1000); 
		scrolltoview("AssessmentPage.xpath.filterWithRoles");
		reportStep("Element Scrolled to up", "PASS");
		return this; 
	}

	public AssessmentPage clickOnSearchButton() {
		click(locateElement("xpath", "AssessmentPage.xpath.filterButton"));  
		return this;
	}
	public AssessmentPage enterPendingAssessment() {
		if(locateElements("xpath", "AssessmentPage.xpath.pendingArrow").isEmpty() != true) {
			click(locateElement("xpath", "AssessmentPage.xpath.pendingArrow")); 		
			List<WebElement> allElements = locateElements("xpath", "AssessmentPage.xpath.pndingAssessmentPagCount");
			for (WebElement eachElement : allElements) {
				click(eachElement); 
			}
			click(locateElement("xpath", "AssessmentPage.xpath.selectCourseGo"));
		}
		return this;
	}
	public AssessmentPage verifyPendingAssessments() {
		if(locateElements("xpath", "AssessmentPage.xpath.PendingAssessments").isEmpty() != true) {
			verifyExactText(locateElement("xpath", "AssessmentPage.xpath.PendingAssessments"), "Pending Assessments");
		} else {
			reportStep("No Pending Assessments!!", "PASS");
		}
		return this; 
	}
	public AssessmentPage verifyCompletedAssessments() {
		if(locateElements("xpath", "AssessmentPage.xpath.CompletedAssessments").isEmpty() != true) {
			verifyExactText(locateElement("xpath", "AssessmentPage.xpath.CompletedAssessments"), "Completed Assessments");
		} else {
			reportStep("No Completed Assessments!!", "PASS");
		}
		return this;
	}
	public AssessmentPage verifySuggestions() {
		if(locateElements("xpath", "AssessmentPage.xpath.suggestions").isEmpty() != true) {
			verifyExactText(locateElement("xpath", "AssessmentPage.xpath.suggestions"), "Suggestions");
		} else {
			reportStep("No Suggested Assessments!!", "PASS");
		}
		return this; 
	}
	public AssessmentPage verifyReviewProgress() {
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.RevieProgress"), "Review Progress");
		return this;
	}
	//review
	public AssessmentPage startInternalAssessmentsCreatedByAdmin() throws InterruptedException {
		List<WebElement> element = locateElements("xpath", "AssessmentPage.xpath.startPendindAss");
		if (locateElements("xpath", "AssessmentPage.xpath.startPendindAss").isEmpty() != true) {
			element.get(0).click(); 
			Thread.sleep(1000);
			String internalAssessmentName = locateElement("xpath", "AssessmentPage.xpath.intAssName").getText();
			reportStep("Internal Assessment Name "+internalAssessmentName, "PASS");
			String internalAssessmentStatus = locateElement("xpath", "AssessmentPage.xpath.intAss").getText();
			reportStep("Internal Assessment Name and Status"+internalAssessmentStatus, "PASS");
		} else {
			reportStep("No Internal Assessments Available", "PASS");
		}
		return this;
	}
	public AssessmentPage enterOriginalAnswers(String data1, String data2) throws InterruptedException {
		List<WebElement> allElements = locateElements("xpath", "AssessmentPage.xpath.intAss1input");
		for (int i = 0; i < allElements.size(); i++) {
			String attribute = allElements.get(i).getAttribute("value");
			System.out.println("Value : "+attribute); 
			if(allElements.get(i).getAttribute("value").isEmpty()!=true) {
				String attributeValue = allElements.get(i).getAttribute("value"); 
				reportStep("Assessment original answer : "+attributeValue, "PASS");
			} else {
				type(locateElement("xpath", "AssessmentPage.xpath.intAss1input"), data1);
				Thread.sleep(1000);
				type(locateElement("xpath", "AssessmentPage.xpath.intAss2input"), data2);
			}
		}
		return this; 
	}
	public AssessmentPage submitInternalAssessment() {
		click(locateElement("id", "AssessmentPage.id.subBtn")); 
		acceptAlert(); 
		return this;
	}
	
	public AssessmentPage verifyUserNextRole() {
		verifyPartialText(locateElement("xpath", "AchievementPage.xpath.nextRoleName"), userNextRole); 
		return this;
	} 
	
	public AssessmentPage downloadCurrentStatusImage() {
		downloadFile();
		return this;
	}

}




















