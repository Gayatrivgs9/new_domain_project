package pages.User;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import lib.selenium.PreAndPost;

public class ReportsPage extends PreAndPost {

	public ReportsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public ReportsPage verifyReportsPageTitle(String data) {
		verifyPartialText(locateElement("xpath", "ReportsPage.xpath.pageTitle"), data);
		return this;
	}

	public ReportsPage verifyReportsHelpText(String data) {
		verifyPartialText(locateElement("xpath", "ReportsPage.xpath.helpTextOfReports"), data);  
		return this;
	}

	public ReportsPage verifyManagerViewTab(String data) {
		verifyExactText(locateElement("id", "ReportsPage.id.managerTab"), data);		
		return this;
	}  

	public ReportsPage verifyBusinessUnitWithCount() {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon1"));
		List<WebElement> allElements = locateElements("xpath", "ReportsPage.xpath.busin");
		int BusinessUnits = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfBnsUnit").getText().replaceAll("\\D", ""));
		if(BusinessUnits == allElements.size()) {
			reportStep("Number of values in Business Unit drop down menu matched with the count populated in Business Unit card in carousel section", "PASS");
		} else {
			reportStep("Number of values in Business Unit drop down menu dosn't matched with the count populated in Business Unit card in carousel section", "FAIL");
		}
		return this;
	}

	public ReportsPage verifyDepartmentWithCount() {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon2"));
		List<WebElement> allElements = locateElements("xpath", "ReportsPage.xpath.dep");
		int departments = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfDep").getText().replaceAll("\\D", ""));
		if(departments == allElements.size()) {
			reportStep("Number of values in Departments drop down menu matched with the count populated in Departments card in carousel section", "PASS");
		} else {
			reportStep("Number of values in DepartmentsBusiness Unit drop down menu dosn't matched with the count populated in Departments card in carousel section", "FAIL");
		}
		return this;
	}

	public ReportsPage verifyHrViewTab(String data) {
		verifyExactText(locateElement("id", "ReportsPage.id.hrTab"), data);		
		return this;
	}
	public ReportsPage clickManagerView() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "ReportsPage.xpath.managerView"));
		return this;
	}
	public ReportsPage clickLearningPartnerView() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "ReportsPage.xpath.lpView"));
		return this;
	}
	public ReportsPage clickHrView() throws InterruptedException {
		waitUntilVisibilityOfWebElement("ReportsPage.xpath.reportView"); 
		click(locateElement("xpath", "ReportsPage.xpath.reportView"));
		return this;                 
	}
	public ReportsPage enterHRBusinessUnits(String bu1, String bu2) throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon1"));
		verifyExactText(locateElement("xpath", "ReportsPage.xpath.busin"), bu1);
		verifyExactText(locateElement("xpath", "ReportsPage.xpath.businNew"), bu2);
		click(locateElement("xpath", "ReportsPage.xpath.busin"));
		return this; 
	}
	public ReportsPage enterBusinessUnit(String bu1) throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon1"));
		verifyExactText(locateElement("xpath", "ReportsPage.xpath.busin"), bu1);
		//verifyExactText(locateElement("xpath", "ReportsPage.xpath.businNew"), bu2);
		click(locateElement("xpath", "ReportsPage.xpath.busin"));
		return this; 
	}
	public ReportsPage enterManagerBusinessUnits(String bu1, String bu2) throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon1"));
		verifyExactText(locateElement("xpath", "ReportsPage.xpath.busin"), bu1);
		//verifyExactText(locateElement("xpath", "ReportsPage.xpath.businNew"), bu2);
		click(locateElement("xpath", "ReportsPage.xpath.busin"));
		return this; 
	}

	public ReportsPage enterHRDepartments(String dep1, String dep2) throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon2"));
		verifyExactText(locateElement("xpath", "ReportsPage.xpath.dep"), dep1);
		verifyExactText(locateElement("xpath", "ReportsPage.xpath.depNew"), dep2);
		List<WebElement> eles = locateElements("xpath", "ReportsPage.xpath.dep");
		for (int i = 0; i < eles.size(); i++) {
			click(eles.get(i));
		}
		return this;
	}
	
	public ReportsPage enterDepartment() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.bussIcon2"));
		List<WebElement> allElements = locateElements("xpath", "ReportsPage.xpath.dep");
		for (int i = 0; i < allElements.size(); i++) {
			click(allElements.get(i));    
		}
		return this;
	}

	public ReportsPage clickGoButton() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.reportsGo"));  
		return this;
	}

	public ReportsPage verifyUserEmail() {
		try {
			String text = getText(locateElement("xpath", "ReportsPage.xpath.homeEmail"));
			reportStep("Home Email : "+text, "PASS");
		} catch (WebDriverException e) {
			reportStep("text not available", "FAIL");
		}		
		return this;
	}

	public ReportsPage verifyReportsCount() throws InterruptedException {
		int departments, roles = 0, comptencies = 0, skills = 0, courses=0;
		try {
			Thread.sleep(2000);
			WebElement BusinessUnits = locateElement("xpath", "ReportsPage.xpath.countOfBnsUnit");
			int bUnit = Integer.parseInt(BusinessUnits.getText().replaceAll("\\D", "")); 
			departments = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfDep").getText().replaceAll("\\D", ""));
			roles = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfRole").getText().replaceAll("\\D", ""));
			comptencies = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfCompetencies").getText().replaceAll("\\D", ""));
			skills = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfSkill").getText().replaceAll("\\D", ""));
			courses = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.countOfCourses").getText().replaceAll("\\D", ""));
			System.out.println("Business Units Total Count: "+bUnit);
			System.out.println("Departments Total Count: "+departments); 
			reportStep("Business Units Total Count: "+bUnit+" Departments Total Count: "+departments, "PASS");
		}catch (Exception e) { 
			reportStep("WebDriver Exception occured while clicking in the field :", "FAIL");
		}
		try {
			reportStep("Total Roles count = "+roles+" Total SkillGroup count = "+comptencies+" Total Skills Count = "+skills+" Total Courses Count = "+courses, "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL"); 
		}
		return this; 
	}

	public ReportsPage verifyReportsCompetencyCount() throws InterruptedException {
		int departments, roles = 0, skills = 0, courses=0, competency=0;
		try {
			Thread.sleep(2000); 
			WebElement BusinessUnits = locateElement("xpath", "AssessmentPage.xpath.totalToBeCompleted");
			int bUnit = Integer.parseInt(BusinessUnits.getText().replaceAll("\\D", "")); 
			departments = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalEnrolled").getText().replaceAll("\\D", ""));
			roles = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalYetToStart").getText().replaceAll("\\D", ""));
			competency = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalInProgress").getText().replaceAll("\\D", ""));
			skills = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalCompleted").getText().replaceAll("\\D", ""));
			courses = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalNotEnrolled").getText().replaceAll("\\D", ""));
			System.out.println("Business Units Total Count: "+bUnit);
			System.out.println("Departments Total Count: "+departments); 
			reportStep("Business Units Total Count: "+bUnit+" Departments Total Count: "+departments, "PASS");
		}catch (Exception e) { 
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		try {
			reportStep("Total Roles count = "+roles+" Total Competency Count = "+competency+" Total Skills Count = "+skills+" Total Courses Count = "+courses, "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL"); 
		}
		return this; 
	}

	public ReportsPage verifyUserReports() throws InterruptedException {
		int roles = 0, skills = 0, courses=0;
		try {
			Thread.sleep(3000);
			roles = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalToBeCompleted").getText().replaceAll("\\D", ""));
			skills = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalEnrolled").getText().replaceAll("\\D", ""));
			courses = Integer.parseInt(locateElement("xpath", "AssessmentPage.xpath.totalYetToStart").getText().replaceAll("\\D", "")); 
			reportStep("Total Roles count = "+roles+" Total Skills Count = "+skills+" Total Courses Count = "+courses, "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL"); 
		}
		return this; 
	}

	public ReportsPage getRoleCount(String data) {
		try {
			int roleCount = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.roleCount").getText().replaceAll("\\D", ""));
			if(data.equals(""+roleCount)) {
				reportStep("Mapping Roles Count: "+roleCount, "PASS");
				reportStep("Mapping Roles Count : "+roleCount+" matched with expected count "+data, "PASS");
			}
		} catch (NumberFormatException e) {
			reportStep("NumberFormatException Occured", "FAIL");
		}
		return this;
	} 

	public ReportsPage getSkillCount(String data) {
		try {
			int skillCount = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.skillCount").getText().replaceAll("\\D", ""));
			if(data.equals(""+skillCount)) {
				reportStep("Mapping SKills Count: "+skillCount, "PASS");
				reportStep("Mapping Skills Count : "+skillCount+" matched with expected count "+data, "PASS");
			}
		} catch (NumberFormatException e) {
			reportStep("NumberFormatException Occured", "FAIL");
		}
		return this;
	}

	public ReportsPage getCourseCount(String data) {
		try {
			int courseCount = Integer.parseInt(locateElement("xpath", "ReportsPage.xpath.coursesCount").getText().replaceAll("\\D", ""));
			if(data.equals(""+courseCount)) {
				reportStep("Mapping Courses Count: "+courseCount, "PASS");
				reportStep("Mapping Courses Count : "+courseCount+" matched with expected count "+data, "PASS");
			}
		} catch (NumberFormatException e) {
			reportStep("NumberFormatException Occured", "FAIL");
		}
		return this;
	}

	public ReportsPage enterRole(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "ReportsPage.xpath.filterByRole"), data);
		return this;  
	}

	public ReportsPage enterAllTheRoles() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.filterByRole"));
		List<WebElement> allElements = locateElements("xpath", "ReportsPage.xpath.busin");
		for (int i = 0; i < allElements.size(); i++) {
			click(allElements.get(i));    
		}
		return this;
	}

	public ReportsPage enterTypeAsCurrentRole() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.filterByType"));
		click(locateElement("xpath", "ReportsPage.xpath.currentNew"));
		click(locateElement("xpath", "ReportsPage.xpath.allRolesGoBtn"));
		String current = "";
		List<WebElement> allElements = locateElements("xpath", "ReportsPage.xpath.allRolesNew");
		for (int i = 0; i < allElements.size(); i++) {
			current = allElements.get(i).getText();
			reportStep("Current Roles :"+current, "PASS"); 
		}
		Thread.sleep(2000);
		return this;
	}
	public ReportsPage enterTypeAsNextRole() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.filterByType"));
		click(locateElement("xpath", "ReportsPage.xpath.nextNew"));
		String next = "";
		List<WebElement> nextAllElements = locateElements("xpath", "ReportsPage.xpath.allRolesNew");
		for (int i = 0; i < nextAllElements.size(); i++) {
			next = nextAllElements.get(i).getText();
			reportStep("Next Roles :"+next, "PASS"); 
		}
		Thread.sleep(2000);
		return this;
	}
	public ReportsPage enterTypeAsAspiringRole() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.filterByType"));
		click(locateElement("xpath", "ReportsPage.xpath.aspNew"));
		String aspiring = "";
		List<WebElement> aspirAllElements = locateElements("xpath", "ReportsPage.xpath.allRolesNew");
		for (int i = 0; i < aspirAllElements.size(); i++) {
			aspiring = aspirAllElements.get(i).getText();
			reportStep("Next Roles :"+aspiring, "PASS"); 
		}
		Thread.sleep(2000);
		return this;
	}
	
	public ReportsPage enterType(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "ReportsPage.xpath.filterByType"), data);
		click(locateElement("xpath", "ReportsPage.xpath.filterByTypeGo"));  
		return this;
	}
	public ReportsPage verifyAllTheRoleNames(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.detailsPageNew");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Role name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Role name "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Role name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Role name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Role Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheRoleNamesCount(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.detailsPage");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText().trim();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Role count "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Role count "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Role name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Role name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Role Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheRoleNamesPercentge(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.detailsPer");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText().trim();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Role name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Role name "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Role name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Role name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Role Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	
	public ReportsPage verifyAllTheSkillNames(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.skillNames");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Skill name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Skill name "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Skill name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Skill name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Role Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheSkillsNamesCount(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.skillCountNames");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText().trim();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Skill count "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Skill count "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Skill name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Skill name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Role Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheSkillsNamesPercentge(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.skillPercentage");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText().trim();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Skill name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Skill name "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Skill name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Skill name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Role Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheCoursesNames(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.coursesData");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Coure name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Course name "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Course name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Course name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Course Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheCoursesNamesCount(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.coursesCountData");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText().trim();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Course count "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Course count "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Course name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Course name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Course Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage verifyAllTheCourseNamesPercentge(String fileName) throws IOException {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.coursesPer");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText().trim();
			ls.add(text);
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				for (int i = 0; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2); 
					String roleName = cell.getStringCellValue().trim();
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Course name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Course name "+roleName+" not matched", "FAIL");
					}
				}
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2); 
					String roleName = cell.getStringCellValue().trim(); 
					System.out.println(roleName);
					if(roleName.equals(ls.get(i))) {
						reportStep("Course name "+roleName+" Matched successfully", "PASS");
					} else {
						reportStep("Course name "+roleName+" not matched", "FAIL");
					}
				}
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("Course Not uploaded successfully", "FAIL");
		}
		return this;  
	}
	public ReportsPage clickSelectAndContinue(String data) throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "ReportsPage.xpath.roleNewSelectAll"));
		Thread.sleep(1000);
		List<WebElement> allEle = locateElements("xpath", "ReportsPage.xpath.AllCheckBox");
		for (int i = 0; i < allEle.size(); i++) {
			if(allEle.get(i).isSelected() == true) {
				reportStep("Check box selected", "PASS");
			} else {
				reportStep("Check box not selected", "FAIL");	
			}
		}
		String selectedRolesCount = locateElement("xpath", "ReportsPage.xpath.selectedRolesCount").getText().replaceAll("\\D", "");
		if(selectedRolesCount.equals(data)) {
			reportStep("Selected roles count is matched", "PASS");
		} else {
			reportStep("Selected roles count is not matched", "FAIL");
		}
		Thread.sleep(1000);
		click(locateElement("xpath", "ReportsPage.xpath.roleConNext"));
		return this;
	}
	public ReportsPage clickSelectAndContinueOfSkill(String data) throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "ReportsPage.xpath.skillselAll"));
		Thread.sleep(1000);
		List<WebElement> allEle = locateElements("xpath", "ReportsPage.xpath.skillselAllCheckBox");
		for (int i = 0; i < allEle.size(); i++) {
			if(allEle.get(i).isSelected() == true) {
				reportStep("Check box selected", "PASS");
			} else {
				reportStep("Check box not selected", "FAIL");	
			}
		}
		String selectedRolesCount = locateElement("xpath", "ReportsPage.xpath.skillSelCount").getText().replaceAll("\\D", "");
		if(selectedRolesCount.equals(data)) {
			reportStep("Selected roles count is matched", "PASS");
		} else {
			reportStep("Selected roles count is not matched", "FAIL");
		}
		Thread.sleep(1000);
		click(locateElement("xpath", "ReportsPage.xpath.skillCntBtn"));
		return this;
	}
	public ReportsPage clickSelectAndContinueOfCourse(String data) throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "ReportsPage.xpath.coursesSelNewAll"));
		Thread.sleep(1000);
		List<WebElement> allEle = locateElements("xpath", "ReportsPage.xpath.coursesCheckBoxNew");
		for (int i = 0; i < allEle.size(); i++) {
			if(allEle.get(i).isSelected() == true) {
				reportStep("Check box selected", "PASS");
			} else {
				reportStep("Check box not selected", "FAIL");	
			}
		}
		String selectedRolesCount = locateElement("xpath", "ReportsPage.xpath.couSelCount").getText().replaceAll("\\D", "");
		if(selectedRolesCount.equals(data)) {
			reportStep("Selected roles count is matched", "PASS");
		} else {
			reportStep("Selected roles count is not matched", "FAIL");
		}
		return this;
	}
	public ReportsPage getTheRoleBasedUserCount() {
		List<String> ls = new ArrayList<>();
		List<WebElement> allRoles = locateElements("xpath", "ReportsPage.xpath.detailsPageNew");
		for (int i = 0; i < allRoles.size(); i++) {
			String text = allRoles.get(i).getText();
			ls.add(text);
		}
		
		
		ArrayList<String> lst = new ArrayList<>();
		List<WebElement> allEle = locateElements("xpath", "ReportsPage.xpath.detailsPage");
		for (int i = 0; i < allEle.size(); i++) {
			String text = allEle.get(i).getText();
			lst.add(text);
		}
		click(locateElement("xpath", "ReportsPage.xpath.newDetailsView"));
		return this;  
	}

	public ReportsPage selectAllTheRoles() {
		click(locateElement("xpath", "ReportsPage.xpath.rolesSelAll"));
		click(locateElement("xpath", "ReportsPage.xpath.rolesSelCon"));
		return this; 
	}

	public ReportsPage selectAllTheSkills() {
		click(locateElement("xpath", "ReportsPage.xpath.skillsSelAll"));
		click(locateElement("xpath", "ReportsPage.xpath.skillsSelCon"));
		return this; 
	}

	public ReportsPage selectAllTheCourses() {
		click(locateElement("xpath", "ReportsPage.xpath.rolesSelAll"));
		click(locateElement("xpath", "ReportsPage.xpath.rolesSelCon"));
		return this; 
	}

	public ReportsPage viewUsersForRoles() throws InterruptedException {
		try {
			clickNew(locateElement("xpath", "ReportsPage.xpath.roles-1"));
			reportStep("User Details", "PASS");  
		}catch (WebDriverException e) {
			reportStep("Problem in clicking User Details", "FAIL");
		}
		Thread.sleep(1000);
		try {
			clickNew(locateElement("xpath", "ReportsPage.xpath.viewDets"));
			reportStep("User Details", "PASS");
		}  catch (WebDriverException e) {
			reportStep("Problem with User Details", "FAIL");
		}
		Thread.sleep(1000);
		try {
			clickNew(locateElement("xpath", "ReportsPage.xpath.crossButton"));
			reportStep("Cross button clicked", "PASS");
		} catch (WebDriverException e) {
			reportStep("Cross button not clicked", "FAIL");
		}
		Thread.sleep(1000);
		click(locateElement("xpath", "ReportsPage.xpath.roleGoToSelectedRoles"));
		click(locateElement("xpath", "ReportsPage.xpath.roleSelectAll"));
		click(locateElement("xpath", "ReportsPage.xpath.rolesContinueNext"));
		Thread.sleep(1000);
		return this;
	}

	
	public ReportsPage enterTheCategory(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "ReportsPage.xpath.filterByCategory"), data);  
		return this;
	}

	public ReportsPage enterSubCategory(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "ReportsPage.xpath.filterBySubcategory"), data); 
		return this;
	}
	

	public ReportsPage enterSkill(String data) throws InterruptedException {
		typeValue(locateElement("xpath", "ReportsPage.xpath.filterBySkill"), data);
		click(locateElement("xpath", "ReportsPage.xpath.filterBySkillGo")); 
		click(locateElement("xpath", "ReportsPage.xpath.skillsSelAll"));
		click(locateElement("xpath", "ReportsPage.xpath.skillsSelCon"));
		return this; 
	}

	public ReportsPage viewUsersForSkills() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.skillSelectAllBtn"));
		click(locateElement("xpath", "ReportsPage.xpath.skillCntBtn"));  
		return this;
	}

	/*public ReportsPage clickSkillContinue() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.skillsContinueNext"));  
		return this;
	}*/

	public ReportsPage viewUsersForCourses() throws InterruptedException {
		click(locateElement("xpath", "ReportsPage.xpath.viewUsersCourses"));  
		return this;
	}

	public ReportsPage enterCourseType(String data1, String data2) {
		typeValue(locateElement("xpath", "ReportsPage.xpath.coursesEnterCourse"), data1);
		typeValue(locateElement("xpath", "ReportsPage.xpath.coursesEnterCourse2"), data2);
		return this;  
	}

	public ReportsPage clickCourseSelectAll() throws InterruptedException {
		Thread.sleep(2000); 
		//click(locateElement("xpath", "ReportsPage.xpath.coursesGoButton"));  
		click(locateElement("xpath", "ReportsPage.xpath.coursesSelectAllButton"));
		return this;
	}

	public ReportsPage exportAsCsvAllTheRoles() {
		try {
			click(locateElement("xpath", "ReportsPage.xpath.rolesExpFile"));
			reportStep("All the roles exported into csv file", "PASS");  
		} catch (Exception e) { 
			reportStep("Unknown exception occured while exporting as csv", "FAIL");
		} 
		return this;
	}

	public ReportsPage exportAsCsvAllTheSkills() {
		try {
			click(locateElement("xpath", "ReportsPage.xpath.skillsExpFile"));
			reportStep("All the skills exported into csv file", "PASS");  
		} catch (Exception e) { 
			reportStep("Unknown exception occured while exporting as csv", "FAIL");
		} 
		return this;
	}

	public ReportsPage exportAsCsvAllTheCourses() {
		try {
			click(locateElement("xpath", "ReportsPage.xpath.coursesExpFile"));
			reportStep("All the courses exported into csv file", "PASS");  
		} catch (Exception e) { 
			reportStep("Unknown exception occured while exporting as csv", "FAIL");
		} 
		return this; 
	}

	public ReportsPage removeUnusedRolesFiles() {
		if(environment.equals("local")) {
			File fileName = new File("./src/main/java/downloadFiles/Roles_Report.csv");
			if(fileName.delete()){
				System.out.println("Roles_Report.csv File deleted from Project root directory");
			}else {
				System.out.println("File Roles_Report.csv doesn't exist in the project root directory");
			} 
		} else {
			File fileName = new File("classes/downloadFiles/Roles_Report.csv");
			if(fileName.delete()){
				System.out.println("Roles_Report.csv File deleted from Project root directory");
			}else {
				System.out.println("File Roles_Report.csv doesn't exist in the project root directory");
			}	
		}
		return this;
	}

	public ReportsPage removeUnusedSkillsFiles() {
		if(environment.equals("local")) {
			File fileName = new File("./src/main/java/downloadFiles/Skills_Report.csv");
			if(fileName.delete()){
				System.out.println("Skills_Report.csv File deleted from Project root directory");
			}else {
				System.out.println("File Skills_Report.csv doesn't exist in the project root directory");
			} 
		} else {
			File fileName = new File("classes/downloadFiles/Skills_Report.csv");
			if(fileName.delete()){
				System.out.println("Skills_Report.csv File deleted from Project root directory");
			}else {
				System.out.println("File Skills_Report.csv doesn't exist in the project root directory");
			}	
		}
		return this;
	}

	public ReportsPage removeUnusedCoursesFiles() {
		if(environment.equals("local")) {
			File fileName = new File("./src/main/java/downloadFiles/Courses_Report.csv");
			if(fileName.delete()){
				System.out.println("Courses_Report.csv File deleted from Project root directory");
			}else {
				System.out.println("File Courses_Report.csv doesn't exist in the project root directory");
			} 
		} else {
			File fileName = new File("classes/downloadFiles/Courses_Report.csv");
			if(fileName.delete()){
				System.out.println("Courses_Report.csv File deleted from Project root directory");
			}else {
				System.out.println("File Courses_Report.csv doesn't exist in the project root directory");
			}	
		}
		return this;  
	}

	public ReportsPage verifyExportedRolesPercentage(String fileName) {
		try {
			String filePath; 
			if(environment.equals("local")) {
				filePath = System.getProperty("user.dir")+"\\src\\main\\java\\downloadFiles\\Roles_Report.csv";
			} else {
				filePath = "classes//downloadFiles//Roles_Report.csv";
			}
			FileReader file = new FileReader(filePath); 
			CSVReader csvReader = new CSVReaderBuilder(file).withSkipLines(3).build(); 
			List<String[]> allData = csvReader.readAll(); 
			// print Data 
			for (String[] csvRow : allData) { 
				int j =0; String[] c = new String[4];  
				for (String cell : csvRow) { 
					c[j] = cell ; 
					j++;
				}
				reportStep("FiltersApplied-RoleType as : "+c[0]+", Role : "+c[1]+". Number of users count : "+c[2]+", and the percentage : "+c[3]+" %\t", "PASS"); 
				List<String> ls = new ArrayList<>();
				System.out.println();
				try {
					XSSFWorkbook wb = null;
					if(environment.equals("local")) {
						wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\verifications\\"+fileName+".xlsx");
						XSSFSheet sheetAt = wb.getSheetAt(0); 
						rowCount = sheetAt.getLastRowNum(); 
						for (int i = 0; i <=rowCount; i++) { 
							XSSFRow row = sheetAt.getRow(i);
							XSSFCell cell = row.getCell(2); 
							String roleName = cell.getStringCellValue().trim();
							System.out.println(roleName);
							if(roleName.equals(ls.get(i))) {
								reportStep("Skill name "+roleName+" Matched successfully", "PASS");
							} else {
								reportStep("Skill name "+roleName+" not matched", "FAIL");
							}
						}
					} else {
						wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/verifications/"+fileName+".xlsx");
						XSSFSheet sheetAt = wb.getSheetAt(0); 
						rowCount = sheetAt.getLastRowNum();
						for (int i = 1; i <=rowCount; i++) { 
							XSSFRow row = sheetAt.getRow(i);
							XSSFCell cell = row.getCell(2); 
							String roleName = cell.getStringCellValue().trim(); 
							System.out.println(roleName);
							if(roleName.equals(ls.get(i))) {
								reportStep("Skill name "+roleName+" Matched successfully", "PASS");
							} else {
								reportStep("Skill name "+roleName+" not matched", "FAIL");
							}
						}
					}
					wb.close();
				}catch (WebDriverException e) {
					reportStep("Role Not uploaded successfully", "FAIL");
				}

			}
		} catch (IOException e) {
			System.err.println("Unknown exception occured while exporting all the roles into csv file");
		} catch (CsvException e) {
			System.err.println("Unknown exception occured while exporting all the roles into csv file"); 
		}
		return this; 
	}

	public ReportsPage verifyExportedSkillsPercentage() {
		try {
			String filePath; 
			if(environment.equals("local")) {
				filePath = System.getProperty("user.dir")+"\\src\\main\\java\\downloadFiles\\Skills_Report.csv";
			} else {
				filePath = "classes//downloadFiles//Skills_Report.csv";
			}
			FileReader file = new FileReader(filePath); 
			CSVReader csvReader = new CSVReaderBuilder(file).withSkipLines(3).build(); 
			List<String[]> allData = csvReader.readAll(); 

			// print Data 
			for (String[] row : allData) { 
				int i =0; String[] c = new String[5];
				for (String cell : row) {
					c[i] = cell ; 
					i++;
				} 
				reportStep("FiltersApplied-Category as : "+c[0]+", FiltersApplied-SubCategory as: "+c[1]+", skill : "+c[2]+". Nnumber of users : "+c[3]+" and the percentage : "+c[4]+" %\t", "PASS");
				System.out.println();
			}
		} catch (IOException e) {
			System.err.println("Unknown exception occured while exporting all the skills into csv file");
		} catch (CsvException e) {
			System.err.println("Unknown exception occured while exporting all the skills into csv file"); 
		}
		return this; 
	}

	public ReportsPage verifyExportedCoursesPercentage() {
		try {
			String filePath; 
			if(environment.equals("local")) {
				filePath = System.getProperty("user.dir")+"\\src\\main\\java\\downloadFiles\\Courses_Report.csv";
			} else {
				filePath = "classes//downloadFiles//Courses_Report.csv";
			}
			FileReader file = new FileReader(filePath); 
			CSVReader csvReader = new CSVReaderBuilder(file).withSkipLines(3).build(); 
			List<String[]> allData = csvReader.readAll(); 

			// print Data 
			for (String[] row : allData) { 
				int i =0; String[] c = new String[4];
				for (String cell : row) {
					c[i] = cell; 
					i++;
				} 
				reportStep("FiltersApplied-Type as : "+c[0]+" course : "+c[1]+" with number of enrolled users : "+c[2]+" and the percentage : "+c[3]+" %\t", "PASS");
				System.out.println("FiltersApplied-Type as : "+c[0]+" course : "+c[1]+" with number of enrolled users : "+c[2]+" and the percentage : "+c[3]+" %\t");

			}
		} catch (IOException e) {
			System.err.println("Unknown exception occured while exporting all the courses into csv file");
		} catch (CsvException e) {
			System.err.println("Unknown exception occured while exporting all the courses into csv file"); 
		}
		return this; 
	}
	
	public ReportsPage clickViewUserMapping() {
		click(locateElement("xpath", "ReportsPage.xpath.newViewMapBtn"));  
	    //ReportsPage.xpath.userMapExportCsv
		return this;
	}
	public ReportsPage exportUserDataIntoCSVFile() {
		
		return this;
	}
}








