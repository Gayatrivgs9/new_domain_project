package pages.User;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class UserSettingsPage extends PreAndPost{

	public UserSettingsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test   = test;
	}

	public UserSettingsPage enterCourseType(String data) {
		typeValue(locateElement("xpath", "UserSettingsPage.xpath.courseType"), data);   
		return this;
	}

	public UserSettingsPage clickSubmitButton() throws InterruptedException {
		click(locateElement("xpath", "UserSettingsPage.xpath.courseTypeSubmitButton"));
		//Thread.sleep(3000); 
		return this;
	}
	public UserSettingsPage verifySuccessMsg() throws InterruptedException {
		//Thread.sleep(500);
		verifyPartialText(locateElement("xpath", "UserSettingsPage.xpath.successMsg"), "Settings Updated Successfully!");
		return this;
	}
	public UserSettingsPage clickCourseTypeAsYes() {
		WebElement element = locateElement("xpath", "UserSettingsPage.xpath.input1");
		if (element.isSelected()!=true) {
			click(element); 
		}
		return this;
	}
	public UserSettingsPage clickMentorsSubmitButton() {
		click(locateElement("xpath", "UserSettingsPage.xpath.clickSubmit"));
		return this;
	}
	public UserSettingsPage verifyUpdatedMsg() {
		verifyExactText(locateElement("xpath", "UserSettingsPage.xpath.updated"), "updated your preference");
		return this; 
	}

	public UserSettingsPage clickSettings() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "UserSettingsPage.xpath.set")); 
		return this;
	}
	public UserSettingsPage selectSkillRadioButtonsAsYes() throws InterruptedException {
		List<WebElement> allEle = locateElements("xpath", "UserSettingsPage.xpath.yesLabel");
		for (WebElement eachEle : allEle) {
			if(eachEle.isSelected() != true) {
			eachEle.click();
			}
		}
		Thread.sleep(500);
		click(locateElement("xpath", "UserSettingsPage.xpath.sub2"));
		waitUntilVisibilityOfWebElement("UserSettingsPage.xpath.updatedPre"); 
		verifyExactText(locateElement("xpath", "UserSettingsPage.xpath.updatedPre"), "updated your preference");
		return this;
	}

	public UserSettingsPage selectContentType() throws InterruptedException {
		click(locateElement("xpath", "UserSettingsPage.xpath.contentType"));
		Thread.sleep(1000);
		List<WebElement> allEle = locateElements("xpath", "UserSettingsPage.xpath.allOpts");
		for (WebElement each : allEle) {
			each.click();
		}
		click(locateElement("xpath", "UserSettingsPage.xpath.sub1"));
		waitUntilVisibilityOfWebElement("UserSettingsPage.xpath.sucNewMsg");
		verifyExactText(locateElement("xpath", "UserSettingsPage.xpath.sucNewMsg"), "Settings Updated Successfully!");
		return this;
	}


}















