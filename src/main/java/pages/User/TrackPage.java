package pages.User;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;


public class TrackPage extends PreAndPost {

	public TrackPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	public TrackPage waitUpToFoldingCubeInvisible() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.invisibilityOf(locateElement("xpath", "LoginPage.xpath.foldingCube"))); 
		return this; 
	}
	public TrackPage verifyNextRole() {
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.nextRole"), "Next Role - "+userNextRole);
		return this;
	}
	public TrackPage enterAspiringRole(String data) throws InterruptedException {
		click(locateElement("xpath", "AssessmentPage.xpath.filterWithRoles"));
		click(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"));
		verifyExactText(locateElement("xpath", "AssessmentPage.xpath.aspiringRole"), data);
		return this;
	}
	public TrackPage verifySkillsGraphs1() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph1Conut"), "id", "skillsSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph2Duration"), "id", "skillsSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph3Projection"), "id", "skillsSection.graphs.vsIndividual.projection");
		return this;		
	} 
	public TrackPage verifySkillsGraphs2() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph1Conut"), "id", "skillsSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph2Duration"), "id", "skillsSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph4Radar"), "id", "skillsSection.graphs.vsIndividual.radar");
		return this;		
	} 
	public TrackPage verifySkillsGraphs3() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph1Conut"), "id", "skillsSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph2Duration"), "id", "skillsSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph5CountIn"), "id", "skillsSection.graphs.vsPeers.inCount");
		return this;		
	} 
	public TrackPage verifySkillsGraphs4() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph1Conut"), "id", "skillsSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph2Duration"), "id", "skillsSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph6Dur"), "id", "skillsSection.graphs.vsPeers.inDuration");
		return this;		
	}
	public TrackPage verifySkillsGraphs5() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph1Conut"), "id", "skillsSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph2Duration"), "id", "skillsSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.skillGraph7rad"), "id", "skillsSection.graphs.vsPeers.radar");
		return this;		
	}

	public TrackPage verifyGraphs1() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph1Count"), "id", "rolesSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph2Duration"), "id", "rolesSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph3Projection"), "id", "rolesSection.graphs.vsIndividual.projection");
		return this;		
	} 
	public TrackPage verifyGraphs2() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph1Count"), "id", "rolesSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph2Duration"), "id", "rolesSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph4PeersCount"), "id", "rolesSection.graphs.vsPeers.inCount");		
		return this;
	} 
	public TrackPage verifyGraphs3() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph1Count"), "id", "rolesSection.graphs.vsIndividual.inCount");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph2Duration"), "id", "rolesSection.graphs.vsIndividual.inDuration");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.graph5PeersDuration"), "id", "rolesSection.graphs.vsPeers.inDuration");		
		return this;
	}
	public TrackPage verifyManagerRolesGraphs() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.managerView1Graph"), "id", "rolesSectionManager.graphs.timeLine.inCount.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.managerView2Graph"), "id", "rolesSectionManager.graphs.timeLine.inCount.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.managerView3Graph"), "id", "rolesSectionManager.graphs.timeLine.inCount.percent");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.managerView4Graph"), "id", "rolesSectionManager.graphs.timeLine.inDuration.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.managerView5Graph"), "id", "rolesSectionManager.graphs.timeLine.inDuration.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.managerView6Graph"), "id", "rolesSectionManager.graphs.timeLine.inDuration.percent");
		return this;
	} 
	public TrackPage verifyManagerSkillsGraphs1() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser1Graph"), "id", "skillsSectionManager.graphs.timeLine.inCount.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser2Graph"), "id", "skillsSectionManager.graphs.radar.inDuration.percent");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser3Graph"), "id", "skillsSectionManager.graphs.polar.weightage.percent");
		return this;
	} 
	public TrackPage verifyManagerSkillsGraphs2() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser1Graph"), "id", "skillsSectionManager.graphs.timeLine.inCount.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser4Graph"), "id", "skillsSectionManager.graphs.timeLine.inCount.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser5Graph"), "id", "skillsSectionManager.graphs.timeLine.inCount.percent");
		return this;
	}
	public TrackPage verifyManagerSkillsGraphs3() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser6Graph"), "id", "skillsSectionManager.graphs.timeLine.inDuration.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser7Graph"), "id", "skillsSectionManager.graphs.timeLine.inDuration.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngUser8Graph"), "id", "skillsSectionManager.graphs.timeLine.inDuration.percent");
		return this;
	}
	public TrackPage verifyHrRolesGraphs() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUser1Graph"), "id", "rolesSectionHr.graphs.timeLine.inCount.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUser2Graph"), "id", "rolesSectionHr.graphs.timeLine.inCount.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUser3Graph"), "id", "rolesSectionHr.graphs.timeLine.inCount.percent");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUser4Graph"), "id", "rolesSectionHr.graphs.timeLine.inDuration.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUser5Graph"), "id", "rolesSectionHr.graphs.timeLine.inDuration.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUser6Graph"), "id", "rolesSectionHr.graphs.timeLine.inDuration.percent");
		return this;
	} 
	public TrackPage verifyHrSkillsGraphs1() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill1Graph"), "id", "skillsSectionHr.graphs.radar.inDuration.percent");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill2Graph"), "id", "skillsSectionHr.graphs.polar.weightage.percent");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill3Graph"), "id", "skillsSectionHr.graphs.timeLine.inCount.sum");
		return this;
	} 
	public TrackPage verifyHrSkillsGraphs2() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill3Graph"), "id", "skillsSectionHr.graphs.timeLine.inCount.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill4Graph"), "id", "skillsSectionHr.graphs.timeLine.inCount.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill5Graph"), "id", "skillsSectionHr.graphs.timeLine.inCount.percent");
		return this;
	}
	public TrackPage verifyHrSkillsGraphs3() throws InterruptedException{
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill6Graph"), "id", "skillsSectionHr.graphs.timeLine.inDuration.sum");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill7Graph"), "id", "skillsSectionHr.graphs.timeLine.inDuration.avg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserSkill8Graph"), "id", "skillsSectionHr.graphs.timeLine.inDuration.percent");
		return this;
	}
	public TrackPage verifyAggregateHrGraph() {
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserAgg1Graph"), "id", "rolesSectionHr.graphs.showAgg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.hrUserAgg2Graph"), "id", "skillsSectionHr.graphs.showAgg");
		return this;
	}
	public TrackPage verifyAggregateGraph() {
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngViewRole"), "id", "rolesSectionManager.graphs.showAgg");
		verifyPartialAttribute(locateElement("xpath","TrackPage.xpath.mngViewSkill"), "id", "skillsSectionManager.graphs.showAgg");
		return this;
	}
	public TrackPage clickRecommendedLearningType() throws InterruptedException {
		click(locateElement("xpath","TrackPage.xpath.filter"));
		click(locateElement("xpath","TrackPage.xpath.recommended"));
		return this;
	}
	public TrackPage clickSuggestedLearningType() throws InterruptedException {
		click(locateElement("xpath","TrackPage.xpath.filter"));
		click(locateElement("xpath","TrackPage.xpath.suggested"));
		return this;
	}
	public TrackPage clickAspiringRole() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.rolefilter"));
		click(locateElement("xpath", "TrackPage.xpath.aspiringRole"));  
		click(locateElement("xpath", "TrackPage.xpath.currentRole"));  
		click(locateElement("xpath", "TrackPage.xpath.nextRole")); 
		return this;
	}
	public void clickCurrentRole() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.rolefilter"));
	}
	public void clickNextRole() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.rolefilter"));
	}
	public TrackPage selectStartEndDate(String sDate, String eDate) throws Exception {			
		dateSelection("TrackPage.xpath.calendar", sDate, eDate);		
		return this;
	}
	public TrackPage clickCalendar() throws InterruptedException {
		click(locateElement("xpath", "LearningPlan.xpath.calendar"));
		return this;
	}
	public TrackPage selectFromAndToDate() {
		click(locateElement("xpath", "LearningPlan.xpath.fromDate"));
		click(locateElement("xpath", "LearningPlan.xpath.toDate"));
		click(locateElement("xpath", "LearningPlan.xpath.apply"));
		return this;
	} 
	public TrackPage clickClear() throws InterruptedException { 
		Thread.sleep(3000);
		click(locateElement("xpath", "LearningPlan.xpath.clear"));
		return this;
	}
	public TrackPage clickGo() {
		click(locateElement("xpath", "TrackPage.xpath.calendarGoButton"));
		return this;
	}
	public TrackPage clickUserView() throws InterruptedException {
		//waitUpToFoldingCubeInvisible();
		Thread.sleep(2000);
		click(locateElement("xpath", "TrackPage.xpath.userView"));
		return this;
	}
	@SuppressWarnings("unlikely-arg-type")
	public TrackPage verifySuggestionsCount() throws InterruptedException {
		Thread.sleep(2000);
		List<String> ls = Arrays.asList("1","0","2"); 
		if (locateElements("xpath", "TrackPage.xpath.SuggestedCount").isEmpty()!= true) {
			List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.SuggestedCount");
			for (int i = 0; i < allElements.size(); i++) {
				String text = allElements.get(i).getText();
				if(allElements.get(i).equals(ls.get(i))) {
					reportStep("Suggested Roles, Skills count matched : "+text, "PASS");
				}
			} 
		} else {
			reportStep("No records found for suggested roles, skills", "FAIL");
		}
		return this;
	}
	public TrackPage clickFlagIconForSuggestions() {
		waitUntilVisibilityOfWebElement("TrackPage.xpath.SugFlagIcon");
		click(locateElement("xpath", "TrackPage.xpath.SugFlagIcon"));
		click(locateElement("xpath", "TrackPage.xpath.SugAllSugges"));
		return this;
	}
	public TrackPage getSuggestedSkillsData() throws InterruptedException {
		Thread.sleep(2000);
		if (locateElements("xpath", "TrackPage.xpath.sugSkillsNames").isEmpty() != true) {
			List<WebElement> allEle = locateElements("xpath", "TrackPage.xpath.sugSkillsNames");
			for (int i = 0; i < allEle.size()-1; i++) {
				String text = allEle.get(i).getText();
				reportStep("Suggested Skill(s) name "+text, "PASS");
			}
		} else {
			reportStep("There is no Suggested Skills(s) available to enrole", "FAIL");
		}
		return this;
	}
	public TrackPage enroleSuggestedRoles(String data) {
		if (locateElement("xpath", "TrackPage.xpath.sugRoleName") != null) {
			String text = locateElement("xpath", "TrackPage.xpath.sugRoleName").getText();
			reportStep("Suggested Role(s) name(s) "+text, "PASS");
		} else {
			reportStep("There is no Suggested Role(s) available to enrole", "FAIL");
		}
		click(locateElement("xpath", "TrackPage.xpath.newRoleSugBtn"));
		waitUntilVisibilityOfWebElement("TrackPage.xpath.sugRoleEnrolledSuc");
		verifyExactText(locateElement("xpath", "TrackPage.xpath.sugRoleEnrolledSuc"), data);
		return this;
	}
	public TrackPage getTheSuggestedRolesDetails() {
		if (locateElements("xpath", "TrackPage.xpath.SuggestedRoles").isEmpty()!= true) {
			List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.SuggestedRoles");
			for (int i = 0; i < allElements.size(); i++) {
				if(allElements.get(1).getText().contains("Role")) {
					String text = allElements.get(i).getText();
					reportStep("Suggested Role name : "+text, "PASS");
				}
			} 
		} else {
			reportStep("No records found for suggested roles", "FAIL");
		}
		return this; 
	}
	public TrackPage getTheSuggestedCompetenciesDetails() {
		if (locateElements("xpath", "TrackPage.xpath.SuggestedCompetencies").isEmpty()!= true) {
			List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.SuggestedCompetencies");
			for (int i = 0; i < allElements.size(); i++) {
				if(allElements.get(i).getText().contains("Competency")) {
					String text = allElements.get(i).getText();
					reportStep("Suggested Competencies name : "+text, "PASS");
				} 
			}
		} else {
			reportStep("No records found for suggested competencies", "FAIL");
		}
		return this; 
	}
	public TrackPage getTheSuggestedSkillsDetails() {
		if (locateElements("xpath", "TrackPage.xpath.SuggestedSkills").isEmpty()!= true) {
			List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.SuggestedSkills");
			for (int i = 1; i < allElements.size(); i++) {
				if(allElements.get(i).getText().contains("Skill")) {
					String text = allElements.get(i).getText();
					reportStep("Suggested Skills name : "+text, "PASS");
				}
			} 
		} else {
			reportStep("No records found for suggested skills", "FAIL");
		}
		return this; 
	}
	public TrackPage verifyAdvancedMsg(String data, String data2) throws InterruptedException {
		Thread.sleep(2000);
		verifyPartialText(locateElement("xpath", "TrackPage.xpath.trackAdsMsg2"), data);
		verifyPartialText(locateElement("xpath", "TrackPage.xpath.trackAdsMsg1"), data2); 
		return this;
	}
	public TrackPage verifyAtAGlanceSection(String data, String courses) {
		String text = locateElement("xpath", "TrackPage.xpath.atAGlanceTrack").getText();
		verifyTwoStringPartialValues(text, data);
		String text2 = locateElement("xpath", "TrackPage.xpath.atAGlanceTrackCourses").getText();
		verifyTwoStringPartialValues(text2, courses); 
		return this;
	} 
	public TrackPage yourRolesAchievementsDetails() {
		scrolltoview("TrackPage.xpath.RolesAchievements");
		reportStep("View your roles achievement details graphs", "PASS"); 
		return this;
	}
	public TrackPage yourSkillsAchievementsDetails() {
		scrolltoview("TrackPage.xpath.SkillsAchievements"); 
		reportStep("View your skills achievement details graphs", "PASS"); 
		return this;
	}
	public TrackPage enterSkillIndex(String data1) {
		typeValue(locateElement("xpath", "TrackPage.xpath.skillIndex"), data1);
		//typeValue(locateElement("xpath", "TrackPage.xpath.skillIndex"), data2);  
		return this;
	}
	public TrackPage enterSkillIndex(String data, String data2, String data3) {
		typeValue(locateElement("xpath", "TrackPage.xpath.fwc"), data);
		typeValue(locateElement("xpath", "TrackPage.xpath.fws"), data2);
		typeValue(locateElement("xpath", "TrackPage.xpath.fwcs"), data3);
		//click(locateElement("xpath", "LearningPlan.xpath.skillIndexGo"));
		return this; 
	}
	public TrackPage enterSkillIndex2(String data, String data2, String data3) {
		typeValue(locateElement("xpath", "TrackPage.xpath.fwc1"), data);
		typeValue(locateElement("xpath", "TrackPage.xpath.fws1"), data2);
		typeValue(locateElement("xpath", "TrackPage.xpath.fwcs1"), data3);
		//click(locateElement("xpath", "LearningPlan.xpath.skillIndexGo"));
		return this; 
	}
	public TrackPage clickSkillIndexGo() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.skillIndexGo"));
		return this;
	}
	public TrackPage clickManagerView() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "TrackPage.xpath.atTrackManagrView"));
		return this;
	}
	public TrackPage enterSkillIndexManagerView(String data1, String data2) {
		typeValue(locateElement("xpath", "TrackPage.xpath.skillIndexManagerView"), data1);
		typeValue(locateElement("xpath", "TrackPage.xpath.skillIndexManagerView"), data2);  
		return this;
	}
	public TrackPage clickSkillIndexManagerViewGo() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.skillIndexManagerViewGo"));
		return this;
	}
	public TrackPage viewSkillDistributionGraph() {
		scrolltoview("TrackPage.xpath.managerViewDownload");
		reportStep("Skill Distribution Graph", "PASS");
		return this;
	}
	public TrackPage clickHrView() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "TrackPage.xpath.hrView"));
		Thread.sleep(5000);
		return this;
	}
	public TrackPage enterTheRole(String data1, String data2, String data3) {
		typeValue(locateElement("xpath", "TrackPage.xpath.rolefilter"), data1);
		//typeValue(locateElement("xpath", "TrackPage.xpath.rolefilter"), data2);
		//typeValue(locateElement("xpath", "TrackPage.xpath.rolefilter"), data3);
		return this;
	}
	public TrackPage clickResourceStatus() {
		click(locateElement("xpath", "TrackPage.xpath.resourceStatus"));
		return this;
	}
	public TrackPage clickSuccessionView() throws InterruptedException {
		Thread.sleep(1000); 
		click(locateElement("xpath", "TrackPage.xpath.successionView"));
		return this;
	}
	public TrackPage clickHRViewSearch() {
		click(locateElement("xpath", "TrackPage.xpath.hrViewSearch"));
		return this;
	}
	public TrackPage enterSkill(String data1, String data2) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterWithSkills"), data1);
		typeValue(locateElement("xpath", "TrackPage.xpath.filterWithSkills"), data2);
		return this;
	}
	public TrackPage clickSkillIndexGoButton() {
		click(locateElement("xpath", "TrackPage.xpath.resourceGo"));
		return this;
	}
	public TrackPage viewAchievmentsGraph() {
		scrolltoview("TrackPage.xpath.buttonDownload");
		reportStep("Achievements Graph", "PASS");
		return this;
	}
	public TrackPage enterSelectRole(String data) throws InterruptedException {
		Thread.sleep(2000); 
		moveToElment(locateElement("xpath", "TrackPage.xpath.filterNewRole"));
		//jsClick(locateElement("xpath", "TrackPage.xpath.filterNewRole"));
		Thread.sleep(2000); 
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		return this;
	}
	public TrackPage enterLevel(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterLevel"), data);
		return this;
	}
	public TrackPage enterSelectUser(String data) throws InterruptedException {
		Thread.sleep(2000);
		moveToElment(locateElement("xpath", "TrackPage.xpath.filterWithNewUser"));
		//jsClick(locateElement("xpath", "TrackPage.xpath.filterWithNewUser"));
		Thread.sleep(2000); 
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		return this;
	}
	public TrackPage enterRange(String data) {
		typeValue(locateElement("xpath", "TrackPage.xpath.filterRange"), data);
		return this;
	}
	public TrackPage enterSkill(String data) throws InterruptedException {
		Thread.sleep(2000);
		moveToElment(locateElement("xpath", "TrackPage.xpath.filterskillNew"));
		//jsClick(locateElement("xpath", "TrackPage.xpath.filterskillNew"));
		Thread.sleep(2000);  
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		return this;
	}
	public TrackPage clickMatchButton() {
		click(locateElement("xpath", "TrackPage.xpath.filterMatchButton"));
		return this;
	}
	public TrackPage verifyNumberOfRecordsMatch() {
		String numberOfMatches = locateElement("xpath", "TrackPage.xpath.successionViewMatch").getText();
		int countOfMatches = Integer.parseInt(numberOfMatches.replaceAll("\\D", "")); 
		reportStep("Records found for succession : "+countOfMatches, "PASS");
		return this;
	}
	static String[] c = null;  
	public TrackPage getUserDetails() throws InterruptedException {
		Thread.sleep(2000);  
		String allText = ""; 
		List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.newUserDetails");
		for (int i = 0; i < allElements.size(); i++) {
			allText = allElements.get(i).getText();
			/*int j =0; c = new String[12];
	         c[j] = allText;*/ 
		}
		reportStep(allText, "PASS");
		//reportStep(c[0]+" : "+c[1]+"  "+c[2]+" : "+c[3]+"  "+c[4]+" : "+c[5]+"  "+c[6]+" : "+c[7]+"  "+c[8]+" : "+c[9]+"  "+c[10]+" : "+c[11]+" \t", "PASS");
		return this;
	}
	public TrackPage getUserMatches() {
		List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.newUserMatches");
		for (int i = 0; i < allElements.size(); i++) {
			reportStep("User details : "+allElements.get(i).getText(), "PASS");
		} 
		return this;
	}
	public TrackPage enterSearchBy(String data) throws InterruptedException {
		Thread.sleep(2000);
		moveToElment(locateElement("xpath", "TrackPage.xpath.searchBy"));
		//jsClick(locateElement("xpath", "TrackPage.xpath.searchBy"));
		Thread.sleep(1000);
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		Thread.sleep(2000);
		return this;
	} 
	public TrackPage enterSearchRole(String data) throws InterruptedException {
		Thread.sleep(2000); 
		moveToElment(locateElement("xpath", "TrackPage.xpath.hrRangeUpdated3"));
		//jsClick(locateElement("xpath", "TrackPage.xpath.hrRangeUpdated3"));
		Thread.sleep(1000);
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		Thread.sleep(2000);
		return this;
	}
	public TrackPage enterSearchRange(String data) throws InterruptedException {
		Thread.sleep(2000); 
		moveToElment(locateElement("xpath", "TrackPage.xpath.hrRangeNew"));
		//jsClick(locateElement("xpath", "TrackPage.xpath.hrRangeNew"));
		Thread.sleep(1000);
		click(locateElementWithoutProp("xpath", "//div[@role='option']/span[text()='"+data+"']"));
		Thread.sleep(2000);
		return this;
	}

	public TrackPage clickSelectSearchButton() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.selectSearchButton"));
		return this;
	}
	public TrackPage clickAIMLUserGraph() throws InterruptedException {
		click(locateElement("xpath", "TrackPage.xpath.aimlClick"));
		//Thread.sleep(3000); 
		return this; 
	}
	public TrackPage getAllTheRolsData() {
		List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.aimlClick");
		for (int i = 0; i < allElements.size(); i++) {
			System.out.println(allElements.size()); 
		}
		return this;
	}

	public TrackPage captureAIMLGraph() throws InterruptedException {
		Thread.sleep(3000); 
		try {
			scrolltoview("TrackPage.xpath.aimlImage");
			reportStep("AIML graph view", "PASS");
		} catch (Exception e) {
			reportStep("Problem with AIML graph view", "FAIL");
		} 
		return this;
	}

	public TrackPage searchUserDetails() throws InterruptedException {
		Thread.sleep(2000); 
		List<WebElement> allElements = locateElements("xpath", "TrackPage.xpath.searchUserDetails");
		for (int i = 0; i < allElements.size(); i++) {
			String text = allElements.get(i).getText();
			reportStep("HR Search User Details"+text, "PASS"); 
		}
		return this;
	}

	public TrackPage captureBubbleGRaph() {

		return this;
	}


















}


