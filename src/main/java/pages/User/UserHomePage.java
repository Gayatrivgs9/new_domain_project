package pages.User;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class UserHomePage extends PreAndPost {

	public UserHomePage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	public UserHomePage waitUpToFoldingCubeInvisible() {
		if(locateElement("xpath", "LoginPage.xpath.foldingCube")!= null) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfAllElements((locateElement("xpath", "LoginPage.xpath.foldingCube"))));
		} else {
            System.out.println("FoldingCube is invisible");  
		}
		return this;  
	}
	
	public AssessmentPage clickAssessment() throws InterruptedException {
		Thread.sleep(2000);
		waitUntilVisibilityOfWebElement("UserHomePage.xpath.assessment"); 
		click(locateElement("xpath", "UserHomePage.xpath.assessment"));
		return new AssessmentPage(driver,test);		
	}
	
	public RecommendationPage clickRecommendation() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "UserHomePage.xpath.recommendation"));
		return new RecommendationPage(driver,test);		
	}
	public RecommendationPage clickRecommendationField() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "UserHomePage.xpath.recommendation"));
		return new RecommendationPage(driver,test);		
	}
    
	public LearningPlanPage clickLearningPlan() throws InterruptedException {
		waitUntilVisibilityOfWebElement("UserHomePage.xpath.learningPlan"); 
		click(locateElement("xpath", "UserHomePage.xpath.learningPlan"));
		return new LearningPlanPage(driver,test);		
	}

	public TrackPage clickTrack() throws InterruptedException {
		Thread.sleep(500);
		waitUntilVisibilityOfWebElement("UserHomePage.xpath.track");
		click(locateElement("xpath", "UserHomePage.xpath.track"));
		return new TrackPage(driver,test);		
	}

	public AchievementPage clickAchievement() throws InterruptedException {
		waitUntilVisibilityOfWebElement("UserHomePage.xpath.achievement");
		click(locateElement("xpath", "UserHomePage.xpath.achievement"));  
		return new AchievementPage(driver,test);		
	}
	public ReportsPage clickReports() throws InterruptedException {
		waitUntilVisibilityOfWebElement("UserHomePage.xpath.userReports"); 
		click(locateElement("xpath", "UserHomePage.xpath.userReports")); 
		return new ReportsPage(driver,test);		
	}
	
	public UserHomePage acceptTheAlertBox() throws InterruptedException {
		Thread.sleep(2000);
		if(locateElements("xpath", "NotificationsPage.xpath.planGuideYes").isEmpty()!= true) {
			click(locateElement("xpath", "NotificationsPage.xpath.planGuideYes")); 
		} else {
			reportStep("Alert box already accepted", "PASS");
			click(locateElement("xpath", "UserHomePage.xpath.learningPlan"));
		}
		return this; 
	}
	public UserSettingsPage clickUserImage() {
		waitUntilVisibilityOfWebElement("UserPage.xpath.profiledropdown");
		click(locateElement("xpath", "UserPage.xpath.profiledropdown"));
		return new UserSettingsPage(driver,test);
	}
}



