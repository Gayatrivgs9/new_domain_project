package pages.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class GenerlSettingsPage extends PreAndPost {

	public GenerlSettingsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public GenerlSettingsPage clickLocal() {	
		WebElement eleLocal = locateElement("name", "GeneralPage.name.local");
		if (eleLocal.isSelected()!=true) {
			click(eleLocal); 
		}
		return this;
	}	
	public GenerlSettingsPage clickOAuth() {	
		WebElement eleOAuth = locateElement("name", "GeneralPage.name.oAuth");
		if (eleOAuth.isSelected()!=true) {
			click(eleOAuth); 
		}
		return this;
	}
	public GenerlSettingsPage clickThirdParty() {	
		WebElement eleThirdParty = locateElement("name", "GeneralPage.name.thirdParty");
		if (eleThirdParty.isSelected()!=true) {
			click(eleThirdParty); 
		}
		return this;
	}
	public GenerlSettingsPage clickIsLive() {	
		WebElement eleIsLive = locateElement("name", "GeneralPage.name.isLive");
		if (eleIsLive.isSelected()!=true) {
			click(eleIsLive); 
		}
		return this;
	}
	public GenerlSettingsPage enterDefaultCourseDuration(String data) {
		type(locateElement("name", "GeneralPage.name.defaultCourseDuration"), data);
		return this;
	}
	public GenerlSettingsPage clickEnableCourseWeightage() {
		WebElement element = locateElement("name", "GeneralPage.name.courseWeightage");
		if(element.isSelected()!=true) {
			click(element);
		}
		return this;
	}
	public GenerlSettingsPage clickUpdateButton() {
		click(locateElement("xpath", "GeneralPage.xpath.updateButton"));
		return this;
	}
	public GenerlSettingsPage clickCancelButton() {
		click(locateElement("xpath", "GeneralPage.xpath.cancelButton"));
		return this;
	}
	public GenerlSettingsPage verifySuccessMsg() {
		verifyPartialText(locateElement("xpath",  "GeneralPage.xpath.successMsg"), "Your settings are updated");
		return this;
	}

























}
