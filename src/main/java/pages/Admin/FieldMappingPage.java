package pages.Admin;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class FieldMappingPage extends PreAndPost {

	public FieldMappingPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public FieldMappingPage selectTheTableAsSchema(String data, String table) {
		if(table.equals("user")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.table"));
			click(locateElement("xpath", "FieldMappingPage.xpath.tableUser"));
		}else if(table.equals("role")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.table"));
			click(locateElement("xpath", "FieldMappingPage.xpath.tableRole"));
		}else if(table.equals("skill")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.table"));
			click(locateElement("xpath", "FieldMappingPage.xpath.tableSkill"));
		}else if(table.equals("course")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.table"));
			click(locateElement("xpath", "FieldMappingPage.xpath.tableCourse"));
		}else if(table.equals("courseTransaction")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.table"));
			click(locateElement("xpath", "FieldMappingPage.xpath.tableCourseTransaction"));
		}
		return this;
	}
	public FieldMappingPage selectTheTableAsRole() {
		click(locateElement("xpath", "FieldMappingPage.xpath.table"));
		click(locateElement("xpath", "FieldMappingPage.xpath.tableRole"));
		return this;
	}
	public FieldMappingPage selectTheTableAsSkill() {
		click(locateElement("xpath", "FieldMappingPage.xpath.table"));
		click(locateElement("xpath", "FieldMappingPage.xpath.tableSkill"));
		return this;
	}
	public FieldMappingPage selectTheTableAsCourse() {
		click(locateElement("xpath", "FieldMappingPage.xpath.table"));
		click(locateElement("xpath", "FieldMappingPage.xpath.tableCourse"));
		return this;
	}
	public FieldMappingPage selectTheSource() {
		click(locateElement("xpath", "FieldMappingPage.xpath.source"));
		click(locateElement("xpath", "FieldMappingPage.xpath.sourceLivewire"));
		System.out.println("Source entered");
		return this;
	}
	public FieldMappingPage clickUploadNewFile() {
		click(locateElement("xpath", "FieldMappingPage.xpath.uploadNewFile"));
		return this; 
	}
	public FieldMappingPage enterUserUploadFilePath(String data) throws InterruptedException {
		if(data.equals("user")) {
			Thread.sleep(2000); 
			WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
			if(environment.equals("local")) {
				type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\users_nd04.csv");
			} else {
				type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/users_nd04.csv");
			}
		} else if(data.equals("role")) {
			Thread.sleep(2000); 
			WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
			if(environment.equals("local")) {
				type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\roles_nd04.csv");
			} else {
				type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/roles_nd04.csv");
			}
		} else if(data.equals("skill")) {
			Thread.sleep(2000); 
			WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
			if(environment.equals("local")) {
				type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\skills_nd04.csv");
			} else {
				type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/skills_nd04.csv");
			}
		} else if(data.equals("course")) {
			Thread.sleep(2000); 
			WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
			if(environment.equals("local")) {
				type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\course_nd04.csv");
			} else {
				type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/Course1(2).csv");
			}
		} else if (data.equals("courseTransaction")) {
			Thread.sleep(2000); 
			WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
			if(environment.equals("local")) {
				type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\transactions_nd04.csv");
			} else {
				type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/transactions_nd04.csv");
			}
		}

		return this;
	}
	public FieldMappingPage enterRoleUploadFilePath() throws InterruptedException {
		Thread.sleep(2000); 
		WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
		if(environment.equals("local")) {
			type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\roles_nd04.csv");
		} else {
			type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/roles_nd04.csv");
		}

		return this;
	}

	public FieldMappingPage enterSkillUploadFilePath() throws InterruptedException {
		Thread.sleep(2000); 
		WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
		if(environment.equals("local")) {
			type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\skills_nd04.csv");
		} else {
			type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/skills_nd04.csv");
		}

		return this;
	}

	public FieldMappingPage enterCourseUploadFilePath() throws InterruptedException {
		Thread.sleep(2000); 
		WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
		if(environment.equals("local")) {
			type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\courses_nd04.csv");
		} else {
			type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/Course1(2).csv");
		}

		return this;
	}
	public FieldMappingPage enterCourseTransactionFilePath() throws InterruptedException {
		Thread.sleep(2000); 
		WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
		if(environment.equals("local")) {
			type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\transactions_nd04.csv");
		} else {
			type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/transactions_nd04.csv");
		}

		return this;
	}
	
	public FieldMappingPage clickUploadButton() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUploadButton"));   
		return this;
	}

	public FieldMappingPage clickGeneratePassword() {
		click(locateElement("xpath", "FieldMappingPage.xpath.generatePassword"));
		return this;
	}
	
	public FieldMappingPage userFieldMapping(String data) throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		if(data.equals("user")) {
			/*List<String> lst = new ArrayList<String>(Arrays.asList("username","lastname","id","email","password","parentid","designation","hrId"));
			for (int i = 0; i < lst.size(); i++) {
				click(locateElement("xpath", "FieldMappingPage.xpath.firtsNameField")); 
				typeValue(locateElement("xpath", "FieldMappingPage.xpath.firtsNameField"), lst.get(i)); 
			}*/
			click(locateElement("xpath", "FieldMappingPage.xpath.firtsNameField")); 
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.firtsNameField"), "username");
			click(locateElement("xpath", "FieldMappingPage.xpath.lastNameField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.lastNameField"), "lastname");
			click(locateElement("xpath", "FieldMappingPage.xpath.foreignDbUserIdField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.foreignDbUserIdField"), "id");
			click(locateElement("xpath", "FieldMappingPage.xpath.emailField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.emailField"), "email");
			click(locateElement("xpath", "FieldMappingPage.xpath.passwordField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.passwordField"), "password");
			click(locateElement("xpath", "FieldMappingPage.xpath.parentIdField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.parentIdField"), "parentid");
			click(locateElement("xpath", "FieldMappingPage.xpath.designationField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.designationField"), "designation");
			click(locateElement("xpath", "FieldMappingPage.xpath.hrIdValue"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.hrIdValue"), "parentid");
		} else if(data.equals("role")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.nameField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.nameField"), "jobrole");
			click(locateElement("xpath", "FieldMappingPage.xpath.businessUnitField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.businessUnitField"), "businessunit");
			click(locateElement("xpath", "FieldMappingPage.xpath.departmentFiled"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.departmentFiled"), "department");
		}else if(data.equals("skill")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.nameField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.nameField"), "skill");
			click(locateElement("xpath", "FieldMappingPage.xpath.categoryField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.categoryField"), "category");
			click(locateElement("xpath", "FieldMappingPage.xpath.subCategoryField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.subCategoryField"), "subcategory");
		}else if(data.equals("course")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.foreignField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.foreignField"), "courseid");
			click(locateElement("xpath", "FieldMappingPage.xpath.titleField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.titleField"), "title");
			click(locateElement("xpath", "FieldMappingPage.xpath.descriptionField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.descriptionField"), "description");
			click(locateElement("xpath", "FieldMappingPage.xpath.startField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.startField"), "start");
			click(locateElement("xpath", "FieldMappingPage.xpath.endField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.endField"), "end");
			click(locateElement("xpath", "FieldMappingPage.xpath.durationInHoursField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.durationInHoursField"), "durationInHours");
			click(locateElement("xpath", "FieldMappingPage.xpath.typeField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.typeField"), "type");
			click(locateElement("xpath", "FieldMappingPage.xpath.contentTypeField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.contentTypeField"), "contenttype");
			click(locateElement("xpath", "FieldMappingPage.xpath.imageField"));
		}else if(data.equals("courseTransaction")) {
			click(locateElement("xpath", "FieldMappingPage.xpath.queryParamField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.queryParamField"), "email");
			click(locateElement("xpath", "FieldMappingPage.xpath.idField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.idField"), "course_id");
			click(locateElement("xpath", "FieldMappingPage.xpath.statusField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.statusField"), "status");
			click(locateElement("xpath", "FieldMappingPage.xpath.assessmentResultField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.assessmentResultField"), "result");
			click(locateElement("xpath", "FieldMappingPage.xpath.durationCompletedInHrsField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.durationCompletedInHrsField"), "durationCompletedInHrs");
			click(locateElement("xpath", "FieldMappingPage.xpath.enrolledDateField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.enrolledDateField"), "enrolledDate");
			click(locateElement("xpath", "FieldMappingPage.xpath.completedDateField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.completedDateField"), "completedDate");
			click(locateElement("xpath", "FieldMappingPage.xpath.startField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.startField"), "start");
			click(locateElement("xpath", "FieldMappingPage.xpath.endField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.endField"), "end");
			click(locateElement("xpath", "FieldMappingPage.xpath.typeField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.typeField"), "type");
			click(locateElement("xpath", "FieldMappingPage.xpath.durationInHoursField"));
			typeValue(locateElement("xpath", "FieldMappingPage.xpath.durationInHoursField"), "durationInHours");
		}
		click(locateElement("xpath", "BaseliningData.xpath.map1")); 
		//click(locateElement("xpath", "FieldMappingPage.xpath.table1Value"));
		//click(locateElement("xpath", "FieldMappingPage.xpath.map1table1Value"));
		click(locateElement("xpath", "FieldMappingPage.xpath.header1Value"));
		click(locateElement("xpath", "FieldMappingPage.xpath.map1table2Value"));
		click(locateElement("xpath", "FieldMappingPage.xpath.saveNewChanges1"));  
		Thread.sleep(2000);
		click(locateElement("xpath", "BaseliningData.xpath.map2")); 
		WebElement element = locateElement("id", "FieldMappingPage.id.nextSystemTable");
		if(element.isSelected()!= true) {
			click(element); 
		}
		click(locateElement("xpath", "FieldMappingPage.xpath.table2Value")); 
		click(locateElement("xpath", "FieldMappingPage.xpath.map2table1Value"));
		click(locateElement("xpath", "FieldMappingPage.xpath.header2Value"));
		click(locateElement("xpath", "FieldMappingPage.xpath.map2table2Value"));
		click(locateElement("xpath", "FieldMappingPage.xpath.saveNewChanges2"));
		return this;
	}
	public FieldMappingPage clickSubmitButton() {
		click(locateElement("xpath", "FieldMappingPage.xpath.submit"));
		return this;
	}
	
	//
	
}


























