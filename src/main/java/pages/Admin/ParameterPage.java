package pages.Admin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ParameterPage extends PreAndPost{

	public  ParameterPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	public ParameterPage getAllTheSkillAttributes() {
		List<WebElement> allElements = locateElements("xpath", "ParameterPage.xpath.SelectSchemaAttributesSkill");
		for (WebElement eachElement : allElements) {
			eachElement.click(); 
		}
		return this;
	}
	public ParameterPage getAllTheRoleAttributes() {
		List<WebElement> allElements = locateElements("xpath", "ParameterPage.xpath.SelectSchemaAttributesRole");
		for (WebElement eachElement : allElements) {
			eachElement.click(); 
		}
		return this;
	}
	public ParameterPage getAllTheCourseAttributes() {
		List<WebElement> allElements = locateElements("xpath", "ParameterPage.xpath.SelectSchemaAttributesCourse");
		for (WebElement eachElement : allElements) {
			eachElement.click(); 
		}
		return this;
	}
	public ParameterPage clickUpdate() throws InterruptedException {
		Thread.sleep(2000);  
		click(locateElement("xpath", "ParameterPage.xpath.update"));
		return this;
	}
	public ParameterPage clickCancel() {
		click(locateElement("xpath", "ParameterPage.xpath.cancel"));
		return this;
	}
	
	

}



























