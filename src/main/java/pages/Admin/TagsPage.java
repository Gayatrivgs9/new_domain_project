package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class TagsPage extends PreAndPost {

	public TagsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
		
	public TagsPage enterSourceSystem(String data) {		
		type(locateElement("xpath", "TagsPage.xpath.sourceSystem"), data);
		return this;
	}	
	public TagsPage enterSelectSchema(String data) {
		type(locateElement("xpath", "TagsPage.xpath.selectSchema"), data); 		
		return this;
	}
	public TagsPage enterSelectKeys(String data) {
		type(locateElement("xpath", "TagsPage.xpath.selectKeys"), data);
		return this;
	}
	public TagsPage clickUpdateButton() {
		click(locateElement("xpath", "TagsPage.xpath.updateButton"));
		return this;
	}
	public TagsPage clickDownload() {
		click(locateElement("xpath", "TagsPage.xpath.download"));
		return this;
	}
	public TagsPage enterDomain(String data) {
		type(locateElement("xpath", "TagsPage.xpath.userdomain"), data);
		return this;
	}
	public TagsPage enterSchemaSelection(String data) {
		type(locateElement("xpath", "TagsPage.xpath.schema"), data);
		return this;
	}
	public TagsPage uploadNewFile(String data) {
		type(locateElement("xpath", "TagsPage.xpath.uploadFile"), data);
		return this;
	}
	public TagsPage clickUploadButton() {
		click(locateElement("xpath", "TagsPage.xpath.uploadButton"));
		return this;
	}
	public TagsPage enterManualTriggerDomain() {
		//type(locateElement("xpath", "TagsPage.xpath.manualTriggerDomain"), data);
		click(locateElement("xpath", "TagsPage.xpath.manualTriggerRun"));   
		return this;
	}
	public TagsPage enterAIMLBasedDomain() throws InterruptedException {
		Thread.sleep(2000); 
		//type(locateElement("xpath", "TagsPage.xpath.aimlBasedTriggerDomain"), data);
		click(locateElement("xpath", "TagsPage.xpath.aimlBasedTriggerRun"));   
		return this;
	}
	
			
	
	

	
	

}
