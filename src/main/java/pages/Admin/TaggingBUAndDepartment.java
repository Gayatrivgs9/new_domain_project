package pages.Admin;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class TaggingBUAndDepartment extends PreAndPost{


	public  TaggingBUAndDepartment(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public TaggingBUAndDepartment clickLearningPartnerUserList() throws InterruptedException {
		Thread.sleep(2000);
		moveToElment(locateElement("xpath", "TaggingBUPage.xpath.userList"));
		return this;
	}
	public TaggingBUAndDepartment enterLearningPartnerName(String data) {
		click(locateElement("xpath", "TaggingBUPage.xpath.selectUser"));
		return this;
	}
	public TaggingBUAndDepartment clickLinkBU() {
		click(locateElement("xpath", "TaggingBUPage.xpath.linkBU"));
		return this;
	}
	public TaggingBUAndDepartment verifyTaggingBUDepData( ) throws IOException, InterruptedException {
		Thread.sleep(3000); 

		try {
			List<WebElement> allEles = locateElements("xpath", "TaggingBUPage.xpath.headerData");
			for (int i = 0; i < allEles.size(); i++) {
				reportStep("Tagged BU Details : "+allEles.get(i).getText(), "PASS");
			}
			verifyPartialText(allEles.get(1), readData(3, 1, "Tagging BUAndDep", "DataSheet"));
		} catch (Exception e) {
			reportStep("Unknown problem occured", "FAIL");
		}
		return this; 
	}
	static String businessUnit;
	public TaggingBUAndDepartment enterBusinessUnit(String data) throws InterruptedException {
		Thread.sleep(5000);
		click(locateElement("xpath", "TaggingBUPage.xpath.clickBU"));
		businessUnit = getText(locateElement("xpath", "TaggingBUPage.xpath.enterBU"));
		click(locateElement("xpath", "TaggingBUPage.xpath.enterBU")); 
		return this;
	}
	static String dep1;
	static String dep2;
	public TaggingBUAndDepartment enterDepartment() {
		click(locateElement("xpath", "TaggingBUPage.xpath.clickDep"));
		click(locateElement("xpath", "TaggingBUPage.xpath.enterDep"));
		dep1 = locateElement("xpath", "TaggingBUPage.xpath.DepTableData1").getText().replaceAll("\\d", "").replace("�", "").trim();
		dep2 = locateElement("xpath", "TaggingBUPage.xpath.DepTableData2").getText().replaceAll("\\d", "").replace("�", "").trim();
		
		return this;
	}
	public TaggingBUAndDepartment clickBUAndDepAddButton() {
		click(locateElement("xpath", "TaggingBUPage.xpath.BuDepAdd"));
		return this;
	}
	public TaggingBUAndDepartment clickBUAndDepSaveButton() {
		click(locateElement("xpath", "TaggingBUPage.xpath.BuDepSaveBtn"));
		return this;
	}
	public TaggingBUAndDepartment verifyBUAndDepartment() throws InterruptedException {
		Thread.sleep(2000);
		verifyPartialText(locateElement("xpath", "TaggingBUPage.xpath.BuDepTableData"), businessUnit);
		verifyPartialText(locateElement("xpath", "TaggingBUPage.xpath.BuDepTableData"), dep1);
		verifyPartialText(locateElement("xpath", "TaggingBUPage.xpath.BuDepTableData"), dep2);
		return this; 
	}
}



















