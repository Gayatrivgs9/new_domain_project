package pages.Admin;


import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class BaseliningPage extends PreAndPost {

	public BaseliningPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	private WebElement eleSchemaDropDown;
	private WebElement eleSourceDropDown;
	private WebElement eleAttribute;
	private WebElement eleType;
	private WebElement eleValidation;
	private WebElement eleCritical;
	private WebElement elePrimaryKey;
	private WebElement eleAdd;
	private WebElement eleSubmit;
	private WebElement schemadd;

	public BaseliningPage clickBaselinigTag() throws InterruptedException {	
		Thread.sleep(2000);
		click(locateElement("xpath", "BaseliningPage.xpath.baselining"));
		System.out.println("BaseliningPage");
		return this;
	}
	public AttributeMapping clickAttributeMapping() {		
		click(locateElement("xpath", "BaseliningPage.xpath.attributeMapping")); 
		return new AttributeMapping(driver, test);
	}	
	public FieldMappingPage clickFieldMapping() {		
		click(locateElement("xpath", "BaseliningPage.xpath.fieldMapping")); 
		System.out.println("FieldMapping");
		return new FieldMappingPage(driver,test); 
	}
	public BaseliningPage clickSystemMapping() {		
		click(locateElement("xpath", "BaseliningPage.xpath.syatemMapping")); 
		return this;
	}
	public BaseliningDataPage clickBaseliningData() throws InterruptedException {		
		click(locateElement("xpath", "AdminHomePage.xpath.baseliningData"));
		Thread.sleep(2000);
		return new BaseliningDataPage(driver, test);
	}
	public BaseliningPage enterSchemaName(String data) {
		WebElement eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema"); 
		selectDropDownUsingValue(eleSchemaDropDown, data); 
		return this; 
	}
	public BaseliningPage verifySourceName() { 
		WebElement eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource"); 
		String[] newList = new String[4]; 
		List<WebElement> allOptions = new Select(eleSchemaDropDown).getOptions();
		int x =1;
		for (x = 1; x < allOptions.size(); x++) {
			String text = allOptions.get(x).getText().trim();
			newList[x] = text; 
			verifyTwoStringValues(newList[x], systemName[x]); 
		}
		String[] array1= systemName; String[] array2=newList;  
		String[] res; 
		if(array1.length>array2.length){
			res=new String[array2.length];
		}else{
			res=new String[array1.length];
		}
		int k=0;
		for(int i=0;i<array1.length;i++)
		{
			for(int j=0;j<array2.length;j++)
			{
				if(array1[i].equals((array2[j]))) 
				{
					res[k]=array1[i];
					k++;
					break;
				}
			}
		}
		for(int l=0;l<res.length-1;l++){
			System.out.print(res[l]);
			System.out.println();
			selectDropDownUsingVisibleText(eleSchemaDropDown, res[l]);  
		}
		return this;
	} 

	public BaseliningPage addUserAttributes(String schema, String source) throws InterruptedException {
		eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
		eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
		eleAttribute = locateElement("BaseliningPage.id.attribute");
		eleType = locateElement("xpath","BaseliningPage.xpath.selectSchemaType");
		eleValidation = locateElement("BaseliningPage.id.validation");

		selectDropDownUsingValue(eleSchemaDropDown, schema);
		Thread.sleep(1000);  
		selectDropDownUsingValue(eleSourceDropDown, source);

		//add email as primary 
		type(locateElement("BaseliningPage.id.attribute"), "email"); 
		selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
		type(locateElement("BaseliningPage.id.validation"), "no"); 
		click(locateElement("xpath", "BaseliningPage.xpath.inputPrimaryKey"));
		click(locateElement("xpath", "BaseliningPage.xpath.add"));  
		//add attributes
		List<String> ls = new ArrayList<>(Arrays.asList("fname","lname","age","location","password","designation","businessUnit","department","employeeID","skillgroup","manager","hr","actionflag","active_record"));
		for (int i = 0; i < ls.size(); i++) {
			typeValueWithTab(locateElement("BaseliningPage.id.attribute"), ls.get(i)); 
			selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
			typeValueWithTab(locateElement("BaseliningPage.id.validation"), "no"); 
			click(locateElement("xpath", "BaseliningPage.xpath.add"));
		}
		//create a new attribute
		type(locateElement("BaseliningPage.id.attribute"), "new_attr"); 
		selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
		click(locateElement("xpath", "BaseliningPage.xpath.add")); 

		return this; 
	}
	public BaseliningPage addRoleAttributes(String schema, String source) throws InterruptedException {
		eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
		eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
		eleAttribute = locateElement("BaseliningPage.id.attribute");
		eleType = locateElement("xpath","BaseliningPage.xpath.selectSchemaType");
		eleValidation = locateElement("BaseliningPage.id.validation");

		//add attributes
		selectDropDownUsingValue(eleSchemaDropDown, schema);
		Thread.sleep(1000);  
		selectDropDownUsingValue(eleSourceDropDown, source);
		//add jobRole as primary
		type(locateElement("BaseliningPage.id.attribute"), "name");
		selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
		type(locateElement("BaseliningPage.id.validation"), "no"); 
		click(locateElement("xpath", "BaseliningPage.xpath.inputPrimaryKey"));
		click(locateElement("xpath", "BaseliningPage.xpath.add"));  
		//add attributes
		List<String> ls = new ArrayList<>(Arrays.asList("nextrole","businessunit","department","active_record"));
		for (int i = 0; i < ls.size(); i++) {
			typeValueWithTab(locateElement("BaseliningPage.id.attribute"), ls.get(i)); 
			selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
			typeValueWithTab(locateElement("BaseliningPage.id.validation"), "no"); 
			click(locateElement("xpath", "BaseliningPage.xpath.add"));
		}
		return this; 
	}

	public BaseliningPage addSkillAttributes(String schema, String source) throws InterruptedException {
		eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
		eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
		eleAttribute = locateElement("BaseliningPage.id.attribute");
		eleType = locateElement("xpath","BaseliningPage.xpath.selectSchemaType");
		eleValidation = locateElement("BaseliningPage.id.validation");
		//add attributes
		selectDropDownUsingValue(eleSchemaDropDown, schema);
		Thread.sleep(1000);  
		selectDropDownUsingValue(eleSourceDropDown, source);
		//add skill as primary
		type(locateElement("BaseliningPage.id.attribute"), "name");
		selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
		type(locateElement("BaseliningPage.id.validation"), "no"); 
		click(locateElement("xpath", "BaseliningPage.xpath.inputPrimaryKey"));
		click(locateElement("xpath", "BaseliningPage.xpath.add"));

		List<String> ls = new ArrayList<>(Arrays.asList("role","skillgroup","category","subcategory","actionflag","active_record"));
		for (int i = 0; i < ls.size(); i++) {
			type(locateElement("BaseliningPage.id.attribute"), ls.get(i));
			selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
			type(locateElement("BaseliningPage.id.validation"), "no"); 
			click(locateElement("xpath", "BaseliningPage.xpath.add"));
		}
		return this; 
	}
	public BaseliningPage addCourseAttributes(String schema, String source) throws InterruptedException {
		eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
		eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
		eleAttribute = locateElement("BaseliningPage.id.attribute");
		eleType = locateElement("xpath","BaseliningPage.xpath.selectSchemaType");
		eleValidation = locateElement("BaseliningPage.id.validation");
		//add attributes
		selectDropDownUsingValue(eleSchemaDropDown, schema);
		Thread.sleep(1000);  
		selectDropDownUsingValue(eleSourceDropDown, source);
		//add course as primary
		type(locateElement("BaseliningPage.id.attribute"), "course_id"); 
		selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
		type(locateElement("BaseliningPage.id.validation"), "no");
		click(locateElement("xpath", "BaseliningPage.xpath.inputPrimaryKey"));
		click(locateElement("xpath", "BaseliningPage.xpath.add"));
		List<String> ls = new ArrayList<>(Arrays.asList("skill","skillgroup","title","description","start","end","duration","type","contenttype","actionflag","active_record"));
		for (int i = 0; i < ls.size(); i++) {
			type(locateElement("BaseliningPage.id.attribute"), ls.get(i)); 
			selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
			type(locateElement("BaseliningPage.id.validation"), "no");
			click(locateElement("xpath", "BaseliningPage.xpath.add"));
		}
		return this; 
	}

	public BaseliningPage addDepartmentskillAttributes(String schema, String source) throws InterruptedException {
		eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
		eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
		eleAttribute = locateElement("BaseliningPage.id.attribute");
		eleType = locateElement("xpath","BaseliningPage.xpath.selectSchemaType");
		eleValidation = locateElement("BaseliningPage.id.validation");
		//add attributes
		selectDropDownUsingValue(eleSchemaDropDown, schema);
		Thread.sleep(1000);  
		selectDropDownUsingValue(eleSourceDropDown, source);
		//add course as primary
		type(locateElement("BaseliningPage.id.attribute"), "businessUnit"); 
		selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
		type(locateElement("BaseliningPage.id.validation"), "no");
		click(locateElement("xpath", "BaseliningPage.xpath.inputPrimaryKey"));
		click(locateElement("xpath", "BaseliningPage.xpath.add"));
		List<String> ls = new ArrayList<>(Arrays.asList("department","skill","actionflag"));
		for (int i = 0; i < ls.size(); i++) {
			type(locateElement("BaseliningPage.id.attribute"), ls.get(i)); 
			selectDropDownUsingValue(locateElement("xpath","BaseliningPage.xpath.selectSchemaType"), "String");
			type(locateElement("BaseliningPage.id.validation"), "no");
			click(locateElement("xpath", "BaseliningPage.xpath.add"));
		}
		return this; 
	}


	// To add all the schemas in one go
	public BaseliningPage addAttribute(String schema, String source, String attribute, String type) throws IOException, InterruptedException{

		eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
		eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
		eleAttribute = locateElement("BaseliningPage.id.attribute");
		eleType = locateElement("xpath","BaseliningPage.xpath.selectSchemaType");
		eleValidation = locateElement("BaseliningPage.id.validation");
		selectDropDownUsingValue(eleSchemaDropDown, schema);
		Thread.sleep(1000); 
		selectDropDownUsingValue(eleSourceDropDown, source);
		type(eleAttribute, attribute);
		selectDropDownUsingValue(eleType, type); 
		//type(eleValidation, validation);
		/*eleCritical = locateElement("BaseliningPage.id.critical");
		if(eleCritical.isSelected() == false)  {
			click(eleCritical);
		}*/
		elePrimaryKey = locateElement("BaseliningPage.id.primary-key");
		if(elePrimaryKey.isSelected() == false){
			click(elePrimaryKey); 
		}
		eleAdd = locateElement("xpath", "BaseliningPage.xpath.add");
		click(eleAdd);
		Thread.sleep(2000);
		List<WebElement> columns = locateElements("xpath", "BaseliningPage.xpath.table");
		List<String> attributeList = new ArrayList<>();
		for (int i =0; i <5; i++) {
			String trim = columns.get(i).getText().trim();
			attributeList.add(trim);
		}
		List<String> list = new ArrayList<>();
		list.add(schema);
		list.add(source);
		list.add(attribute);
		list.add(type);
		//list.add(validation);
		list.add("true");
		list.add("true");
		if (list.equals(attributeList)) { 
			System.out.println("Attributes matched in schema grid");
		} else {
			System.out.println("Attributes not matched in schema grid");
		}
		return this;
	}

	public BaseliningPage clickSubmitButton() { 
		eleSubmit = locateElement("xpath", "BaseliningPage.xpath.submit");
		click(eleSubmit);
		return this;
	}
	public BaseliningPage verifyBaselineSuccessMsg() {
		WebElement eleSuccessMsg = locateElement("xpath", "BaseliningPage.xpath.successMsg");
		verifyPartialText(eleSuccessMsg, "Baselined Successfully");
		return this;
	}

	public static boolean baselineCreated;
	public BaseliningPage addSchemas(String schemaName, String RN, int CN) throws IOException, InterruptedException, EncryptedDocumentException, InvalidFormatException{

		if(!schemaName.equalsIgnoreCase("NA")){
			int LastrowNo = getRowNo("Schema", "DataSheet");
			boolean added = false;
			String text = " Select Source ";
			for(int i=1; i<=LastrowNo; i++){
				String schema = readData(0, i, "Schema", "DataSheet");
				String source = readData(1, i, "Schema", "DataSheet");
				String attribute = readData(2, i, "Schema", "DataSheet");
				String schemaType = readData(3, i, "Schema", "DataSheet");
				//String validation = readData(4, i, "Schema", "DataSheet");
				String critical = readData(4, i, "Schema", "DataSheet");
				String primaryKey = readData(5, i, "Schema", "DataSheet");
				eleSchemaDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
				eleSourceDropDown = locateElement("xpath", "BaseliningPage.xpath.selectSource");
				eleAttribute = locateElement("BaseliningPage.id.attribute");
				eleType = locateElement("xpath", "BaseliningPage.xpath.selectSchemaType");
				if(schema.equalsIgnoreCase(schemaName)){
					Thread.sleep(5000);
					selectDropDownUsingValue(eleSchemaDropDown, schema);
					if(!eleSourceDropDown.getText().trim().equals(text.trim())) {
						String trim = eleSourceDropDown.getText().trim();
						System.out.println(trim); 
						Thread.sleep(5000);	
						selectDropDownUsingValue(eleSourceDropDown, source);
					}
					type(eleAttribute, attribute);
					selectDropDownUsingValue(eleType, schemaType); 
					//type(eleValidation, validation);
					if(critical.equalsIgnoreCase("Yes")){
						eleCritical = locateElement("BaseliningPage.id.critical");
						click(eleCritical);
					}
					if(primaryKey.equalsIgnoreCase("Yes")){
						elePrimaryKey = locateElement("BaseliningPage.id.primary-key");
						click(elePrimaryKey);
					}
					Thread.sleep(1000);
					eleAdd = locateElement("xpath", "BaseliningPage.xpath.add");
					click(eleAdd);
					Thread.sleep(1000);
					added = true;
				}
			}
			/*if(added==true){
				eleSubmit = locateElement("xpath", "BaseliningPage.xpath.submit");
				click(eleSubmit);
				int rowNum = Integer.parseInt(RN); 
				try {
					String alertText = eleSuccess.getText();
					reportStep(alertText+ " for "+schemaName, "PASS");				
					writeData(CN, rowNum, "PASS", "BaselineCreation", "Green");
					baselineCreated = true;
				} catch (NoSuchElementException e) {
					try {
						String alertText = eleError.getText();
						reportStep(alertText+ " for "+schemaName, "FAIL");
						writeData(CN, rowNum, "FAIL", "BaselineCreation", "Red");
						baselineCreated = false;
					} catch (NoSuchElementException e2) {
						writeData(CN, rowNum, "FAIL", "BaselineCreation", "Red");
						reportStep("Baseline Creation Failed for "+schemaName, "FAIL");
						baselineCreated = false;
					}				
				}
			}*/
		}
		return this;
	}

	public String browser = "chrome";
	public BaseliningPage testUserUploadFiles(String uploadURL, String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException{
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
					cdriver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
					cdriver = new FirefoxDriver();
				}
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: "+browser+" launched successfully with URL: "+uploadURL, "PASS");
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				cdriver = new FirefoxDriver(firefoxOptions);          
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: launched successfully", "PASS");
			}} catch (WebDriverException e) {			
				cdriverReportStep("The browser: "+browser+" could not be launched", "FAIL");
			}	  
		try {
			cdriver.findElementByLinkText("Upload").click();
			cdriver.findElementByXPath("//label[text()='Domain']/following-sibling::input").sendKeys(domain);
			WebElement select = cdriver.findElementByXPath("//label[text()='Schema: ']/following::select");
			new Select(select).selectByValue("Skill"); 
			new Select(select).selectByValue(schema); 
			cdriver.findElementByXPath("//label[text()='System: ']/following-sibling::input").sendKeys(system);
			cdriver.findElementByXPath("//label[text()='Email: ']/following-sibling::input").sendKeys(email); 
			cdriver.findElementByName("api").sendKeys(api);
			cdriverReportStep("Data entered successfully", "PASS");
		} catch(WebDriverException e){
			cdriverReportStep("Data not entered", "FAIL");  
		} 
		try {	
			Thread.sleep(2000); 
			if(environment.equals("local")) { 
				XSSFWorkbook wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0);  
				int rowCount = sheetAt.getLastRowNum();
				//int columnCount = sheetAt.getRow(0).getLastCellNum();
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) {
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0);
					emailId = cell.getStringCellValue();
					System.out.println(emailId); 
					list.add(emailId); 
				}
				cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".csv"); 
				cdriverReportStep("Imported Users Count is: "+rowCount, "PASS");
			} else {
				WebElement uploadCsvFile = cdriver.findElementByXPath("//input[@type='file']");
				String filePath = System.getProperty("user.dir");
				System.out.println(filePath+"/classes/data/csvfiles/"+filename+".csv"); 
				uploadCsvFile.sendKeys(filePath+"/classes/data/csvfiles/"+filename+".csv");	
			}
			Thread.sleep(2000);
			cdriver.findElementByXPath("//input[@value='Upload']").click();
			/*Thread.sleep(2000);
			System.out.println("Alert text :"+cdriver.switchTo().alert().getText());
			cdriver.switchTo().alert().accept();*/
			Thread.sleep(10000);
			String status = "";
			String text = cdriver.findElementByXPath("//td[contains(text(), 'Acceptance')]").getText();
			System.out.println(text); 
			List<WebElement> allElements = cdriver.findElementsByXPath("//td[contains(text(), 'Acceptance')]/following::td[contains(text(), 'true')]");
			for (int i = 0; i < allElements.size(); i++) {
				status = allElements.get(i).getText();
			}
			System.out.println("Acceptance status: "+status);
			cdriverReportStep("api view part", "PASS");
		} catch(WebDriverException e){
			cdriverReportStep("api view part", "FAIL");  
		}
		/*//schemadd = locateElement("xpath", "BaseliningPage.xpath.selectSchema");
			WebElement testSchema = locateElement("xpath", "BaseliningPage.xpath.testPalSchema");
			selectDropDownUsingValue(testSchema, schema[num]);
			type(locateElement("xpath", "BaseliningPage.xpath.testPalSystem"), "");
			type(locateElement("xpath", "BaseliningPage.xpath.testPalEmail"), "");
			cdriver.findElementById("txtFileUpload").click();  

			uploadfile(filePath+fileName[num]);
			Thread.sleep(2000);

			if(num==0){
				selectDropDownUsingValue(schemadd,schema[num+1]);
				selectDropDownUsingValue(schemadd,schema[num+2]);
				selectDropDownUsingValue(schemadd,schema[num]);
			}

			cdriver.findElementByName("api").sendKeys("http://test1pal.sifylivewire.com:3000/admins/dataupload");
			cdriver.findElementByXPath("//input[@value='Upload']").click();
		 */	
		Thread.sleep(2000);
		cdriver.quit();
		Thread.sleep(2000);
		clickBaseliningData()
		.enterLimit()
		.verifyUserName();//.verifyUserUploadData();			
		//	}
		return this;
	}

	public BaseliningPage testRoleUploadFiles(String uploadURL, String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException{
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
					cdriver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
					cdriver = new FirefoxDriver();
				}
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: launched successfully", "PASS");
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				cdriver = new FirefoxDriver(firefoxOptions);          
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: launched successfully", "PASS");
			}} catch (WebDriverException e) {			
				cdriverReportStep("The browser: "+browser+" could not be launched", "FAIL");
			}
		try {
			cdriver.findElementByLinkText("Upload").click();
			cdriver.findElementByXPath("//label[text()='Domain']/following-sibling::input").sendKeys(domain);
			WebElement select = cdriver.findElementByXPath("//label[text()='Schema: ']/following::select");
			new Select(select).selectByValue(schema);
			cdriver.findElementByXPath("//label[text()='System: ']/following-sibling::input").sendKeys(system);
			cdriver.findElementByXPath("//label[text()='Email: ']/following-sibling::input").sendKeys(email); 
			cdriver.findElementByName("api").sendKeys(api);
			cdriverReportStep("Data entered successfully", "PASS");
		} catch(WebDriverException e){
			cdriverReportStep("Data not entered", "FAIL");  
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".csv");
				Thread.sleep(2000);
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0);  
				rowCount = sheetAt.getLastRowNum();
				//int columnCount = sheetAt.getRow(0).getLastCellNum();
				System.out.println(rowCount); 
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(1); 
					String skillTitles = cell.getStringCellValue().trim(); 
					System.out.println(skillTitles); 
					list.add(skillTitles);
				}
				cdriverReportStep("Imported Roles Count is: "+rowCount, "PASS");
			} else {
				String filePath = System.getProperty("user.dir");
				cdriver.findElementById("txtFileUpload").sendKeys(filePath+"/classes/data/csvfiles/"+filename+".csv");
			}
			wb.close(); 
			cdriver.findElementByXPath("//input[@value='Upload']").click();
			/*Thread.sleep(2000);
			System.out.println("Alert text : "+cdriver.switchTo().alert().getText());
			cdriver.switchTo().alert().accept();*/
			Thread.sleep(2000);
			String status = "";
			String text = cdriver.findElementByXPath("//td[contains(text(), 'Acceptance')]").getText();
			List<WebElement> allElements = cdriver.findElementsByXPath("//td[contains(text(), 'Acceptance')]/following::td[contains(text(), 'true')]");
			for (int i = 0; i < allElements.size(); i++) {
				status = allElements.get(i).getText();
			}			System.out.println(text+" status: "+status);
			cdriverReportStep("api view part", "PASS");
		} catch(WebDriverException e){
			cdriverReportStep("api view part", "FAIL");  
		}
		cdriver.quit();
		Thread.sleep(3000);
		clickBaseliningData()
		.enterLimit()
		.clickRole()
		.verifyRoleName();//.clickRoleHeadRow();//.verifyRoleUploadData();			
		return this;
	}

	public BaseliningPage testSkillUploadFiles(String uploadURL, String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException{
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
					cdriver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
					cdriver = new FirefoxDriver();
				}
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: "+browser+" launched successfully", "PASS");
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				cdriver = new FirefoxDriver(firefoxOptions);          
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: launched successfully", "PASS");
			}} catch (WebDriverException e) {			
				cdriverReportStep("The browser: "+browser+" could not be launched", "FAIL");
			}
		try {
			cdriver.findElementByLinkText("Upload").click();
			cdriver.findElementByXPath("//label[text()='Domain']/following-sibling::input").sendKeys(domain);
			WebElement select = cdriver.findElementByXPath("//label[text()='Schema: ']/following::select");
			new Select(select).selectByValue(schema);
			cdriver.findElementByXPath("//label[text()='System: ']/following-sibling::input").sendKeys(system);
			cdriver.findElementByXPath("//label[text()='Email: ']/following-sibling::input").sendKeys(email); 
			cdriver.findElementByName("api").sendKeys(api);
			cdriver.findElementById("groupEnabled").click(); 
			cdriverReportStep("Data entered successfully", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("Data not entered", "FAIL");
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".csv"); 
				Thread.sleep(2000);
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0);  
				rowCount = sheetAt.getLastRowNum();
				//int columnCount = sheetAt.getRow(0).getLastCellNum();
				System.out.println(rowCount); 
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0);
					String skillTitles = cell.getStringCellValue().trim(); 
					System.out.println(skillTitles); 
					list.add(skillTitles);
				}
				cdriverReportStep("Imported Skills Count is: "+rowCount, "PASS");
			} else {
				String filePath = System.getProperty("user.dir");
				cdriver.findElementById("txtFileUpload").sendKeys(filePath+"/classes/data/csvfiles/"+filename+".csv");
			}
			wb.close(); 
			cdriver.findElementByXPath("//input[@value='Upload']").click();
			/*Thread.sleep(2000);
			System.out.println("Alert text : "+cdriver.switchTo().alert().getText());
			cdriver.switchTo().alert().accept();*/
			Thread.sleep(2000);
			String status = "";
			String text = cdriver.findElementByXPath("//td[contains(text(), 'Acceptance')]").getText();
			List<WebElement> allElements = cdriver.findElementsByXPath("//td[contains(text(), 'Acceptance')]/following::td[contains(text(), 'true')]");
			for (int i = 0; i < allElements.size(); i++) {
				status = allElements.get(i).getText();
			}
			System.out.println(text+" status: "+status);
			cdriverReportStep("api view part", "PASS"); 
		} catch (WebDriverException e) {
			cdriverReportStep("api view part", "FAIL");
		}
		cdriver.quit();
		Thread.sleep(2000);
		clickBaseliningData()
		.enterLimit()
		.clickSkill() 
		.verifySkillName();//.clickSkillHeadRow();//.verifySkillUploadData();			
		return this;
	}

	public BaseliningPage testCourseUploadFiles(String uploadURL,String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException{
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
					cdriver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
					cdriver = new FirefoxDriver();
				}
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: "+browser+" launched successfully", "PASS");
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				cdriver = new FirefoxDriver(firefoxOptions);          
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: launched successfully", "PASS");
			}} catch (WebDriverException e) {			
				cdriverReportStep("The browser firefox: "+browser+" could not be launched", "FAIL");
			}
		try {
			cdriver.findElementByLinkText("Upload").click();
			cdriver.findElementByXPath("//label[text()='Domain']/following-sibling::input").sendKeys(domain);
			WebElement select = cdriver.findElementByXPath("//label[text()='Schema: ']/following::select");
			new Select(select).selectByValue(schema); 
			cdriver.findElementByXPath("//label[text()='System: ']/following-sibling::input").sendKeys(system);
			cdriver.findElementByXPath("//label[text()='Email: ']/following-sibling::input").sendKeys(email); 
			cdriver.findElementByName("api").sendKeys(api);
			//cdriver.findElementById("groupEnabled").click();
			cdriverReportStep("Data entered successfully", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("Data not entered", "FAIL");
		}
		cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\Q3FeaturesSet3\\"+filename+".csv");
		try {
			/*	XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\Q3FeaturesSet3\\"+filename+".csv"); 
				System.out.println(System.getProperty("user.dir")); 
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\Q3FeaturesSet3\\"+filename+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0);  
				rowCount = sheetAt.getLastRowNum();
				//int columnCount = sheetAt.getRow(0).getLastCellNum();
				System.out.println(rowCount); 
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(2);
					String courseTitles = cell.getStringCellValue().trim(); 
					System.out.println(courseTitles); 
					list.add(courseTitles);
				}
				cdriverReportStep("Imported Courses Count is: "+rowCount, "PASS");
			} else {
				String filePath = System.getProperty("user.dir");
				cdriver.findElementById("txtFileUpload").sendKeys(filePath+"/classes/data/csvfiles/Q3FeaturesSet3/"+filename+".csv");
			}
			wb.close();*/ 
			cdriver.findElementByXPath("//input[@value='Upload']").click();
			Thread.sleep(4000);
			String status = "";
			String text = cdriver.findElementByXPath("//td[contains(text(), 'processed')]").getText();
			List<WebElement> allElements = cdriver.findElementsByXPath("//td[contains(text(), 'processed')]/following::td[contains(text(), 'true')]");
			for (int i = 0; i < allElements.size(); i++) {
				status = allElements.get(i).getText();
			}
			System.out.println(text+" status: "+status);
			cdriverReportStep("api view part", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("api view part", "FAIL");
		}
		cdriver.quit();
		Thread.sleep(2000);
		/*Thread.sleep(2000);
		clickBaseliningData()
		.enterLimit()
		.clickCourse()
		.verifyCourseName();*///.clickCourseHeadRow();//.verifyCourseUploadData();			
		return this;
	}

	public BaseliningPage testLivewireCourseUploadFiles(String domain,String schema,String system,String email,String api, String filename) throws IOException, AWTException, InterruptedException, EncryptedDocumentException, InvalidFormatException{
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
					cdriver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
					cdriver = new FirefoxDriver();
				}
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				cdriver.get("http://demopal.sifylivewire.com:4200/upload");
				cdriverReportStep("The browser: "+browser+" launched successfully", "PASS");
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				cdriver = new FirefoxDriver(firefoxOptions);          
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				cdriver.get("http://demopal.sifylivewire.com:4200/upload");
				cdriverReportStep("The browser: launched successfully", "PASS");
			}} catch (WebDriverException e) {			
				cdriverReportStep("The browser: "+browser+" could not be launched", "FAIL");
			}
		try {
			cdriver.findElementByLinkText("Upload").click();
			cdriver.findElementByXPath("//label[text()='Domain']/following-sibling::input").sendKeys(domain);
			WebElement select = cdriver.findElementByXPath("//label[text()='Schema: ']/following::select");
			new Select(select).selectByValue(schema); 
			cdriver.findElementByXPath("//label[text()='System: ']/following-sibling::input").sendKeys(system);
			cdriver.findElementByXPath("//label[text()='Email: ']/following-sibling::input").sendKeys(email); 
			cdriver.findElementByName("api").sendKeys(api);
			cdriver.findElementById("groupEnabled").click();
			cdriverReportStep("Data entered successfully", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("Data not entered", "FAIL");
		}
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				writeDataToLiveire(0, 1, courseTitle, "CourseId", "course_livedata", "Green");
				cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".csv"); 
				System.out.println(System.getProperty("user.dir")); 
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+filename+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0);  
				rowCount = sheetAt.getLastRowNum();
				//int columnCount = sheetAt.getRow(0).getLastCellNum();
				System.out.println(rowCount); 
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0);
					String courseTitles = cell.getStringCellValue().trim(); 
					System.out.println(courseTitles); 
					list.add(courseTitles);
				}
				cdriverReportStep("Imported Courses Count is: "+rowCount, "PASS");
			} else {
				String filePath = System.getProperty("user.dir");
				cdriver.findElementById("txtFileUpload").sendKeys(filePath+"/classes/data/csvfiles/"+filename+".csv");
			}
			wb.close(); 
			cdriver.findElementByXPath("//input[@value='Upload']").click();
			Thread.sleep(2000);
			/*String status = "";
			String text = cdriver.findElementByXPath("//td[contains(text(), 'Acceptance')]").getText();
			List<WebElement> allElements = cdriver.findElementsByXPath("//td[contains(text(), 'Acceptance')]/following::td[contains(text(), 'true')]");
			for (int i = 0; i < allElements.size(); i++) {
				status = allElements.get(i).getText();
			}
			System.out.println(text+" status: "+status);*/
			cdriverReportStep("api view part", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("api view part", "FAIL");
		}
		cdriver.quit();
		Thread.sleep(2000);
		return this;
	}
	public BaseliningPage verifyCouseName() throws InterruptedException {
		Thread.sleep(2000); 
		clickBaseliningData()
		.enterLimit()
		.clickCourse()
		.verifyCourseName();//.clickCourseHeadRow();//.verifyCourseUploadData();
		return this;
	}

	public BaseliningPage testCourseTransactionUploadFiles(String uploadURL,String domain,String schema,String system,String email,String api,String fileName) throws IOException, AWTException, InterruptedException{
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
					cdriver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
					cdriver = new FirefoxDriver();
				}
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: "+browser+" launched successfully", "PASS");
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				cdriver = new FirefoxDriver(firefoxOptions);          
				cdriver.manage().window().maximize();
				cdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				cdriver.get(uploadURL);
				cdriverReportStep("The browser: launched successfully", "PASS");
			}} catch (WebDriverException e) {			
				cdriverReportStep("The browser: "+browser+" could not be launched", "FAIL");
			}
		try {
			cdriver.findElementByLinkText("Upload").click();
			cdriver.findElementByXPath("//label[text()='Domain']/following-sibling::input").sendKeys(domain);
			WebElement select = cdriver.findElementByXPath("//label[text()='Schema: ']/following::select");
			new Select(select).selectByValue(schema);  
			cdriver.findElementByXPath("//label[text()='System: ']/following-sibling::input").sendKeys(system);
			cdriver.findElementByXPath("//label[text()='Email: ']/following-sibling::input").sendKeys(email); 
			cdriver.findElementByName("api").sendKeys(api);
			cdriver.findElementById("groupEnabled").click();
			cdriverReportStep("Data entered successfully", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("Data not entered", "FAIL");
		}
		try {
			if(environment.equals("local")) {
				cdriver.findElementById("txtFileUpload").sendKeys(System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\"+fileName+".csv"); 
				System.out.println(System.getProperty("user.dir"));
				cdriverReportStep("Imported Courses Count is: "+rowCount, "PASS");
			} else {
				String filePath = System.getProperty("user.dir");
				cdriver.findElementById("txtFileUpload").sendKeys(filePath+"/classes/data/csvfiles/"+fileName+".csv");
			}
			cdriver.findElementByXPath("//input[@value='Upload']").click();
			WebDriverWait wait = new WebDriverWait(cdriver, 30);
			wait.until(ExpectedConditions.visibilityOf(cdriver.findElementByXPath("//td[contains(text(), 'processed')]"))); 
			String status = "";
			String text = cdriver.findElementByXPath("//td[contains(text(), 'processed')]").getText();
			List<WebElement> allElements = cdriver.findElementsByXPath("//td[contains(text(), 'processed')]/following::td[contains(text(), 'true')]");
			for (int i = 0; i < allElements.size(); i++) {
				status = allElements.get(i).getText();
			}
			System.out.println(text+" status: "+status);
			cdriverReportStep("api view part", "PASS");
		} catch (WebDriverException e) {
			cdriverReportStep("api view part", "FAIL");
		}
		cdriver.quit();
		Thread.sleep(2000);			
		return this;
	}

	public BaseliningPage clickDataUploads() {
		click(locateElement("xpath", "BaseliningPage.xpath.uploadData"));
		return this;
	}
	public BaseliningPage clickMasterUpload() {
		click(locateElement("xpath", "BaseliningPage.xpath.masUpload")); 
		return this;
	}
	public BaseliningPage enterLimit() throws InterruptedException {
		type(locateElement("xpath", "BaseliningData.xpath.enterLimitData"), "100"); 
		click(locateElement("xpath", "BaseliningData.xpath.limitSubmitButton"));
		Thread.sleep(1000); 
		return this;
	}
	public BaseliningPage clickCourseTransactionMapping() {
		click(locateElement("xpath", "BaseliningPage.xpath.courseTransMapping"));
		return this;
	} 
	public BaseliningPage clickUploadMasterSheets() throws InterruptedException {
		click(locateElement("xpath", "BaseliningPage.xpath.masterDataButton"));
		Thread.sleep(2000); 
		return this;
	}
	public BaseliningPage clickCourseTransactionUploadButton() {
		click(locateElement("xpath", "BaseliningPage.xpath.courseTransButton"));
		return this;
	}
	public BaseliningPage uploadMasterDataFile(String fileName, String successMsg) throws InterruptedException {
		if(environment.equals("local")) {
			type(locateElement("xpath", "BaseliningPage.xpath.masterDataFile"), System.getProperty("user.dir")+"\\src\\main\\java\\data\\masterUpload\\"+fileName+".csv");
			reportStep("Imported Courses Count is: "+rowCount, "PASS");
		} else {
			String filePath = System.getProperty("user.dir");
			type(locateElement("xpath", "BaseliningPage.xpath.masterDataFile"), filePath+"/classes/data/masterUpload/"+fileName+".csv");
		}
		Thread.sleep(1000);
		click(locateElement("xpath", "BaseliningPage.xpath.masterUpload"));
		waitUntilVisibilityOfWebElement("BaseliningPage.xpath.masterSucMsg"); 
		verifyExactText(locateElement("xpath", "BaseliningPage.xpath.masterSucMsg"), successMsg);
		waitUntilVisibilityOfWebElement("BaseliningPage.xpath.progressComp");
		WebElement rowData = locateElement("xpath", "BaseliningPage.xpath.UploadRowData");
		if(rowData.getText().contains("100%")) {
			reportStep("Master data uploaded successfully", "PASS");
		} else {
			reportStep("Master data uploaded failed", "FAIL");
		}
		if(locateElementWithoutProp("xpath", "//table/tbody/tr/td[2]").getText().equals("Completed")) {
			reportStep("Master data uploaded successfully", "PASS");
		} else {
			getText(locateElement("xpath", "BaseliningPage.xpath.failedStatus")); 
			reportStep("Master data uploaded failed", "FAIL"); 
		}
		return this;
	}

	public BaseliningPage downloadSampleTransaction() {
		click(locateElement("xpath", "BaseliningPage.xpath.sampleTrans"));
		return this;
	}

	public BaseliningPage selectSourceSystem() {
		click(locateElement("xpath", "FieldMappingPage.xpath.source"));
		click(locateElement("xpath", "FieldMappingPage.xpath.sourceLivewire"));
		System.out.println("Source entered");
		return this;
	}

	public BaseliningPage clickUploadNewFile() {
		click(locateElement("xpath", "FieldMappingPage.xpath.uploadNewFile"));
		return this; 
	}

	public BaseliningPage uploadCourseTransactionFile(String fileName, String msg) throws InterruptedException {
		WebElement eleChooseFile = locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUpload");
		if(environment.equals("local")) {
			type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\masterUpload\\"+fileName+".csv");
		} else {
			type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/masterUpload/"+fileName+".csv");
		}
		click(locateElement("xpath", "FieldMappingPage.xpath.foreignFieldsUploadButton"));
		verifyExactText(locateElement("xpath", "BaseliningPage.xpath.courseTransSuccessmsg"), msg);
		return this;
	}

	public BaseliningPage clickGeneratePassword() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "FieldMappingPage.xpath.generatePassword"));
		return this; 
	}
	public BaseliningPage courseTransactionFieldMapping() throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		click(locateElement("xpath", "FieldMappingPage.xpath.queryParamField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.queryParamField"), "email");
		click(locateElement("xpath", "FieldMappingPage.xpath.idField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.idField"), "course_id");
		click(locateElement("xpath", "FieldMappingPage.xpath.statusField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.statusField"), "course_status");
		click(locateElement("xpath", "FieldMappingPage.xpath.assessmentResultField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.assessmentResultField"), "course_result");
		click(locateElement("xpath", "FieldMappingPage.xpath.durationCompletedInHrsField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.durationCompletedInHrsField"), "course_duration_completed_in_hours");
		click(locateElement("xpath", "FieldMappingPage.xpath.enrolledDateField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.enrolledDateField"), "course_enrolled_date");
		click(locateElement("xpath", "FieldMappingPage.xpath.completedDateField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.completedDateField"), "course_completed_date");
		click(locateElement("xpath", "FieldMappingPage.xpath.startField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.startField"), "course_start");
		click(locateElement("xpath", "FieldMappingPage.xpath.endField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.endField"), "course_end");
		click(locateElement("xpath", "FieldMappingPage.xpath.typeField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.typeField"), "course_type");
		click(locateElement("xpath", "FieldMappingPage.xpath.durationInHoursField"));
		typeValue(locateElement("xpath", "FieldMappingPage.xpath.durationInHoursField"), "course_duration_in_hours");

		return this;
	}
	public BaseliningPage clickCoursTransSubmitButton() throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "FieldMappingPage.xpath.submit"));
		return this;
	}

}
