package pages.Admin;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class BaseliningDataPage extends PreAndPost {

	public BaseliningDataPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	public BaseliningDataPage enterLimit() throws InterruptedException {
		type(locateElement("xpath", "BaseliningData.xpath.enterLimitData"), "1000"); 
		click(locateElement("xpath", "BaseliningData.xpath.limitSubmitButton"));
		Thread.sleep(1000); 
		return this;
	}
	public BaseliningDataPage verifyUserName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.userTitleSearchBox");
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			String trim2 = list.get(i).trim();
			System.out.println(trim2); 
			type(element, list.get(i).trim()); 
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[2]//td[text()=' "+list.get(i)+" ']");
			String trim = elements2.getText().trim();
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i).trim()); 
		}
		return this;
	}
	public BaseliningDataPage verifyRoleName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.roleTitleSearchBox"); 
		Thread.sleep(1000);
		for (int i = 0; i < list.size(); i++) { 
			Thread.sleep(1000);
			type(element, list.get(i).trim()); 
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[4]//td[text()=' "+list.get(i)+" ']");
			String trim = elements2.getText().trim(); 
			System.out.println(trim); 
			String trim2 = list.get(i).trim();
			verifyPartialText(elements2, trim2);
		}
		return this;
	}
	public BaseliningDataPage verifySkillName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.skillTitleSearchBox"); 
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			type(element, list.get(i).trim()); 
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[6]//td[text()=' "+list.get(i)+" ']");
			String trim = elements2.getText().trim(); 
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i).trim());
		}
		return this;
	}
	public BaseliningDataPage verifySkillGroupName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.skillGroupGlobalSearch"); 
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			type(element, list.get(i).trim()); 
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[2]//td[text()=' "+list.get(i)+" ']");
			String trim = elements2.getText().trim(); 
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i).trim());
		}
		return this;
	}
	static String courseName;
	public BaseliningDataPage verifyCourseName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.skillGroupGlobalSearch"); 
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			type(element, list.get(i).trim());
			courseName = list.get(i);
			Thread.sleep(1000);
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[2]//td[text()=' "+courseName+" ']"); 
			String trim = elements2.getText().trim(); 
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i)); 
		}
		return this;
	}
	public BaseliningDataPage verifyBusinessUnitName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.skillGroupGlobalSearch"); 
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			type(element, list.get(i).trim()); 
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[2]//td[text()=' "+list.get(i)+" ']");
			String trim = elements2.getText().trim(); 
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i).trim());
		}
		return this;
	}
	public BaseliningDataPage verifyDepartmentName() throws InterruptedException { 
		WebElement element = locateElement("xpath", "BaseliningData.xpath.skillGroupGlobalSearch"); 
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			type(element, list.get(i).trim()); 
			WebElement elements2 = locateElementWithoutProp("xpath", "(//table)[2]//td[text()=' "+list.get(i)+" ']");
			String trim = elements2.getText().trim(); 
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i).trim());
		}
		return this;
	}
	public BaseliningDataPage clickUser() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.user"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickRole() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.Role"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickSkill() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.Skill"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickSkillGroup() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.SkillGroup"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickCourse() throws InterruptedException {
		Thread.sleep(10000);
		click(locateElement("xpath", "BaseliningData.xpath.Course")); 
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickBusinessUnit() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.BU"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickDepartment() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.Dep"));
		Thread.sleep(2000);
		return this;
	}

	public BaseliningDataPage clickUserHeadRow() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.firstName"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickRoleHeadRow() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.RoleHeadRow"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickSkillHeadRow() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.SkillHeadRow"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage clickCourseHeadRow() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.CourseHeadRow"));
		Thread.sleep(2000);
		return this;
	}
	public BaseliningDataPage verifyUserUploadData() {
		String eleText = locateElement("xpath", "BaseliningData.xpath.tableRow").getText();
		System.out.println(eleText); 
		verifyPartialText(locateElement("xpath", "BaseliningData.xpath.tableRow"), "gayatri ");
		return this;
	}
	public BaseliningDataPage verifyRoleUploadData() {
		String eleText = locateElement("xpath", "BaseliningData.xpath.tableRow").getText();
		System.out.println(eleText); 
		verifyPartialText(locateElement("xpath", "BaseliningData.xpath.tableRow"), "gayatri ");
		return this;
	}

	public BaseliningDataPage verifySkillUploadData() {
		String eleText = locateElement("xpath", "BaseliningData.xpath.tableRow").getText();
		System.out.println(eleText); 
		verifyPartialText(locateElement("xpath", "BaseliningData.xpath.tableRow"), "gayatri ");
		return this;
	}

	public BaseliningDataPage verifyCourseUploadData() {
		String eleText = locateElement("xpath", "BaseliningData.xpath.tableRow").getText();
		System.out.println(eleText); 
		verifyPartialText(locateElement("xpath", "BaseliningData.xpath.tableRow"), "gayatri ");
		return this;
	}
	public BaseliningDataPage enterLimitAndSubmit(String data){
		type(locateElement("xpath", "BaseliningData.xpath.limit"), data);
		click(locateElement("xpath", "BaseliningData.xpath.submitButton"));
		return this;
	}

	public BaseliningDataPage clickMultiSelect() throws InterruptedException {
		click(locateElement("xpath", "BaseliningData.xpath.skillMultiSelect"));  
		WebElement element = locateElement("xpath", "BaseliningData.xpath.skillMulSelCheckBox");
		if(element.isSelected()==true) {
			click(element); 
			Thread.sleep(1000);
			click(locateElement("xpath", "BaseliningData.xpath.skillNameChkBox"));  
		}
		WebElement elementValue = locateElement("xpath", "BaseliningData.xpath.skillTitleSearchBox"); 
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) { 
			type(elementValue, list.get(i).trim()); 
			WebElement elements2 = locateElement("xpath", "BaseliningData.xpath.skillNameValidation");
			String trim = elements2.getText().trim();
			System.out.println(trim); 
			verifyPartialText(elements2, list.get(i).trim());
		}
		return this;
	}


	public BaseliningDataPage verifySchemaMasterData(String fileName) throws IOException {
		try {
			XSSFWorkbook wb = null;
			if(environment.equals("local")) {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"\\src\\main\\java\\data\\masterDataVerification\\"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum(); 
				System.out.println(rowCount); 
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String userName = cell.getStringCellValue().trim(); 
					System.out.println(userName); 
					list.add(userName);
				}
				reportStep("Imported Users Count is: "+rowCount, "PASS");
			} else {
				wb = new XSSFWorkbook(System.getProperty("user.dir")+"/classes/data/masterDataVerification/"+fileName+".xlsx");
				XSSFSheet sheetAt = wb.getSheetAt(0); 
				rowCount = sheetAt.getLastRowNum();
				System.out.println(rowCount); 
				list = new ArrayList<>(); 
				for (int i = 1; i <=rowCount; i++) { 
					XSSFRow row = sheetAt.getRow(i);
					XSSFCell cell = row.getCell(0); 
					String userName = cell.getStringCellValue().trim(); 
					System.out.println(userName); 
					list.add(userName);
				}
				reportStep("Imported Users Count is: "+rowCount, "PASS");
			}
			wb.close();
		}catch (WebDriverException e) {
			reportStep("User Not uploaded successfully", "FAIL");
		}
		return this;
	}
}



















