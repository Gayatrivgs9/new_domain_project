package pages.Admin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class PlanPage extends PreAndPost {

	public PlanPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public PlanPage enterDayPlan() throws InterruptedException {
		WebElement eleDay = locateElement("xpath", "PlanPage.xpath.modefiedDayValue");
		click(eleDay); 
		click(locateElement("xpath", "PlanPage.xpath.Icon"));
		click(locateElement("xpath", "PlanPage.xpath.dayValue"));
		return this;
	}	
	public PlanPage enterWeekPlan() throws InterruptedException {
		WebElement eleWeek = locateElement("xpath", "PlanPage.xpath.week");
		click(eleWeek); 
		Thread.sleep(1000);
		click(locateElement("xpath", "PlanPage.xpath.Icon"));
		return this;
	}
	public PlanPage enterMonthPlan() throws InterruptedException {
		WebElement eleMonth = locateElement("xpath", "PlanPage.xpath.month");
		click(eleMonth); 
		Thread.sleep(2000);
		click(locateElement("xpath", "PlanPage.xpath.Icon"));
		return this;
	}
	public PlanPage clickIncludeAssessments() {
		WebElement assessmentPlan = locateElement("xpath", "PlanPage.xpath.assessmentPlanYes");
		click(assessmentPlan);
		return this;
	}
	public PlanPage enterDateRange(String data) {
		type(locateElement("xpath", "PlanPage.xpath.defaultDataRange"), data);
		return this;
	}
	public PlanPage enterHelpText(String data) {
		type(locateElement("xpath", "PlanPage.xpath.helpText"), data);
		return this;
	}		
	public PlanPage clickRolesSectionGraphs() {
		WebElement eleRolesSection = locateElement("xpath", "PlanPage.xpath.graphRolesSectionYes");
		click(eleRolesSection);
		if(locateElement("xpath", "PlanPage.xpath.roleSectionIndNumOfCourses").isSelected()!=true) 
			click(locateElement("xpath", "PlanPage.xpath.roleSectionIndNumOfCourses"));
		if(locateElement("xpath", "PlanPage.xpath.roleSectionIndCoursesDuration").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.roleSectionIndCoursesDuration"));
		if(locateElement("xpath", "PlanPage.xpath.roleSectionPeersNumOfCourses").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.roleSectionPeersNumOfCourses"));
		/*if(locateElement("xpath", "PlanPage.xpath.roleSectionPeersCoursesDuration").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.roleSectionPeersCoursesDuration"));*/
		return this;
	}
	public PlanPage clickRoleSectionGraphs() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllRoles();
		WebElement ele1 = locateElement("xpath", "PlanPage.xpath.roleSectionIndNumOfCourses");
		WebElement ele2 = locateElement("xpath", "PlanPage.xpath.roleSectionIndCoursesDuration");
		WebElement ele3 = locateElement("xpath", "PlanPage.xpath.roleSectionPeersNumOfCourses");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public PlanPage clickSkillSectionGraphs() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllSkills();
		WebElement ele1 = locateElement("xpath", "PlanPage.xpath.skillSectionIndNumOfCourses");
		WebElement ele2 = locateElement("xpath", "PlanPage.xpath.skillSectionIndCoursesDuration");
		WebElement ele3 = locateElement("xpath", "PlanPage.xpath.skillSectionPeersCoursesDuration");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public PlanPage unselctAllRoles() throws InterruptedException {
		WebElement eleRolesSection = locateElement("xpath", "PlanPage.xpath.graphRolesSectionYes");
		if (eleRolesSection.isSelected()!=true) {
			click(eleRolesSection);
		}
		List<WebElement> elements = locateElements("xpath", "PlanPage.xpath.roles");
		for (WebElement eachElement : elements) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		}
		WebElement element = locateElement("xpath", "PlanPage.xpath.rolesPeers");
		if (element.isSelected()==true) {
			click(element); 
		} 
		WebElement element2 = locateElement("xpath", "PlanPage.xpath.2rolesPeers");
		if (element2.isSelected()==true) {
			click(element2); 
		}
		return this; 
	}
	public PlanPage unselctAllSkills() {
		WebElement eleRolesSection = locateElement("xpath", "PlanPage.xpath.displayskillsSectionGraphs");
		if (eleRolesSection.isSelected()!=true) {
			click(eleRolesSection);
		}
		List<WebElement> elements = locateElements("xpath", "PlanPage.xpath.skills");
		for (WebElement eachElement : elements) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		}
		WebElement element = locateElement("xpath", "PlanPage.xpath.skillsPeers");
		if (element.isSelected()==true) {
			click(element); 
		} 
		WebElement element2 = locateElement("xpath", "PlanPage.xpath.2skillsPeers");
		if (element2.isSelected()==true) {
			click(element2); 
		} 
		return this; 
	}
	public PlanPage clickSkillsSectionGraphs() {
		WebElement eleSkillsSection = locateElement("xpath", "PlanPage.xpath.displayskillsSectionGraphs");
		click(eleSkillsSection);
		if(locateElement("xpath", "PlanPage.xpath.skillSectionIndNumOfCourses").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.skillSectionIndNumOfCourses"));
		if(locateElement("xpath", "PlanPage.xpath.skillSectionIndCoursesDuration").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.skillSectionIndCoursesDuration"));
		if(locateElement("xpath", "PlanPage.xpath.skillSectionPeersNumOfCourses").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.skillSectionPeersNumOfCourses"));
		/*if(locateElement("xpath", "PlanPage.xpath.skillSectionPeersCoursesDuration").isSelected()!=true)
			click(locateElement("xpath", "PlanPage.xpath.skillSectionPeersCoursesDuration"));*/
		return this;
	}
	public PlanPage clickUpdateButton() {
		click(locateElement("xpath", "PlanPage.xpath.updateButton"));
		return this;
	}





}
