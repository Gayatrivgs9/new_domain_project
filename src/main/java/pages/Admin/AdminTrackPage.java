package pages.Admin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AdminTrackPage extends PreAndPost {

	public AdminTrackPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}

	public AdminTrackPage enterDefaultDateRange(String data) {		
		type(locateElement("xpath", "TrackPage.xpath.defaultDateRange"), data);
		return this;
	}	
	public AdminTrackPage clickRoleeSectionVsOwnNotifications() throws Exception {
		click(locateElement("xpath", "TrackPage.xpath.vsOwn")); 
		type(locateElement("xpath","TrackPage.xpath.textarea1"),  "You are lagging behind against your goal to complete role based learning and development!");
		type(locateElement("xpath","TrackPage.xpath.textarea2"),  "You are lagging behind in some courses against your own plan, please focus on those!");
		type(locateElement("xpath","TrackPage.xpath.textareaOnPaln1"),  "You are on target towards your role based learning and development");
		type(locateElement("xpath","TrackPage.xpath.textareaOnPaln2"),  "You are on track in some course which is good, keep it up!");
		type(locateElement("xpath","TrackPage.xpath.textareaAHead1"),  "You are going ahead of schedule to accomplish your role based learning and development!");
		type(locateElement("xpath","TrackPage.xpath.textareaAHead2"),  "You have completed some courses ahead of time, that is great, keep it up!");
		return this;
	}
	public AdminTrackPage clickRoleSectionVsPeersNotifications() {
		click(locateElement("xpath", "TrackPage.xpath.vsPeers"));
		type(locateElement("xpath","TrackPage.xpath.lagging1"),  "Slow progress.");
		type(locateElement("xpath","TrackPage.xpath.lagging2"),  "Your progress is slower than your peers in some cases!");
		type(locateElement("xpath","TrackPage.xpath.equal1"),  "Good progress.");
		type(locateElement("xpath","TrackPage.xpath.equal2"),  "In some cases you are progressing as your peers are doing!");
		type(locateElement("xpath","TrackPage.xpath.aHead1"),  "Excellent progress.");
		type(locateElement("xpath","TrackPage.xpath.aHead2"),  "You are doing great and progressing ahead of your peers ! Great job!");
		return this;
	}
	public AdminTrackPage clickSkillSectionVsOwnNotifications() {
		click(locateElement("xpath", "TrackPage.xpath.skillSectionVsOwn"));
		type(locateElement("xpath","TrackPage.xpath.skillLagging1"),  "You are lagging behind against your goal to complete some skill development!");
		type(locateElement("xpath","TrackPage.xpath.skillLagging2"),  "You are lagging behind in some skills against your own plan, please focus on those!");
		type(locateElement("xpath","TrackPage.xpath.skillOnPaln1"),  "You are on target towards your skill development plan");
		type(locateElement("xpath","TrackPage.xpath.skillOnPaln2"),  "You are on track in some skills which is good, keep it up!");
		type(locateElement("xpath","TrackPage.xpath.skillAHead1"),  "You are going ahead of schedule to accomplish your skill development plan.");
		type(locateElement("xpath","TrackPage.xpath.skillAHead2"),  "You have completed some skills  ahead of time, that is great, keep it up!");
		return this;
	}
	public AdminTrackPage clickSkillSectionVsPeersNotifications() {
		click(locateElement("xpath", "TrackPage.xpath.skillSectionVsPeers"));
		type(locateElement("xpath","TrackPage.xpath.skillPeersLagging1"),  "Slow progress.");
		type(locateElement("xpath","TrackPage.xpath.skillPeersLagging2"),  "Your progress is slower than your peers in some cases!");
		type(locateElement("xpath","TrackPage.xpath.skillPeersEqual1"),  "Good progress.");
		type(locateElement("xpath","TrackPage.xpath.skillPeersEqual2"),  "In some cases you are progressing as your peers are doing!");
		type(locateElement("xpath","TrackPage.xpath.skillPeersAHead1"),  "Excellent progress.");
		type(locateElement("xpath","TrackPage.xpath.skillPeersAHead2"),  "You are doing great and progressing ahead of your peers ! Great job!");
		return this;
	}
	public AdminTrackPage clickRolesSectionPredictiveAnalysis() {
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionPredictiveAnalysis"));
		type(locateElement("xpath", "TrackPage.xpath.roleAdvance"), "You are expected to achieve your goals to perform your role in advance");
		type(locateElement("xpath", "TrackPage.xpath.roleInTime"), "You are expected to achieve your goals to perform your role  in time");
		type(locateElement("xpath", "TrackPage.xpath.roleOverDue"), "You are expected to achieve your goals to perform your role later than planned.");
		type(locateElement("xpath", "TrackPage.xpath.advance1"), "3");
		type(locateElement("xpath", "TrackPage.xpath.inTime1"), "1");
		type(locateElement("xpath", "TrackPage.xpath.overDue1"), "0");
		type(locateElement("xpath", "TrackPage.xpath.advance2"), "3");
		type(locateElement("xpath", "TrackPage.xpath.inTime2"), "0");
		type(locateElement("xpath", "TrackPage.xpath.overDue2"), "-1");
		type(locateElement("xpath", "TrackPage.xpath.advance3"), "5");
		type(locateElement("xpath", "TrackPage.xpath.inTime3"), "0");
		type(locateElement("xpath", "TrackPage.xpath.overDue3"), "-2");
		return this;
	}
	public AdminTrackPage clickSkillsSectionPredictiveAnalysis() {
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionPredictiveAnalysis"));
		type(locateElement("xpath", "TrackPage.xpath.skillAdvance"), "You are expected to complete your skills in advance");
		type(locateElement("xpath", "TrackPage.xpath.skillInTime"), "You are expected to complete your skills  in time");
		type(locateElement("xpath", "TrackPage.xpath.skillOverDue"), "You are expected to complete your skills later than planned.");
		type(locateElement("xpath", "TrackPage.xpath.skillAdvance1"), "10");
		type(locateElement("xpath", "TrackPage.xpath.skillInTime1"), "5");
		type(locateElement("xpath", "TrackPage.xpath.skillOverDue1"), "3");
		type(locateElement("xpath", "TrackPage.xpath.skillAdvance2"), "10");
		type(locateElement("xpath", "TrackPage.xpath.skillInTime2"), "5");
		type(locateElement("xpath", "TrackPage.xpath.skillOverDue2"), "3");
		type(locateElement("xpath", "TrackPage.xpath.skillAdvance3"), "10");
		type(locateElement("xpath", "TrackPage.xpath.skillInTime3"), "5");
		type(locateElement("xpath", "TrackPage.xpath.skillOverDue3"), "3");
		return this;
	}
	public AdminTrackPage clickSkillsSectionGraphs() {
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionNumberOfCourses"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionCourseDuration"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionProjection"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionPercentage"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionPeersNumberOfCourses"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionPeersCourseDuration"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionPeersPercentage"));
		return this;
	}
	public AdminTrackPage clickManagerRolesSectionGraphs() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllManagerSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.1graphRoleMng");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.2graphRoleMng");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.3graphRoleMng");
		WebElement ele4 = locateElement("xpath", "TrackPage.xpath.4graphRoleMng");
		WebElement ele5 = locateElement("xpath", "TrackPage.xpath.5graphRoleMng");
		WebElement ele6 = locateElement("xpath", "TrackPage.xpath.6graphRoleMng");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		if(ele4.isSelected()!=true)
			click(ele4);
		if(ele5.isSelected()!=true)
			click(ele5);
		if(ele6.isSelected()!=true)
			click(ele6);
		return this;
	}
	
	public AdminTrackPage clickSkillsSectionGraphs1() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.skillsSectionCourseDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.skillsSectionProjection");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}  
	public AdminTrackPage clickSkillsSectionGraphs2() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.skillsSectionCourseDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.skillsSectionPercentage");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsSectionGraphs3() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.skillsSectionCourseDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.skillsSectionPeersNumberOfCourses");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsSectionGraphs4() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.skillsSectionCourseDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.skillsSectionPeersCourseDuration");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsSectionGraphs5() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.skillsSectionCourseDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.skillsSectionPeersPercentage");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsManagerSectionGraphs1() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllManagerSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.mng1graph");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.mng2graph");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.mng3graph");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsManagerSectionGraphs2() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllManagerSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.mng3graph");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.mng4graph");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.mng5graph");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsManagerSectionGraphs3() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllManagerSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.mng6graph");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.mng7graph");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.mng8graph");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsHrSectionGraphs1() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllHrSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.hrRole2Polar");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.hrRole3Radar");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph1");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsHrSectionGraphs2() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllHrSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph1");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph2");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph3");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickSkillsHrSectionGraphs3() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllHrSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph4");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph5");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.hrSkillsGraph6");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage unselctAllRoles() {
		List<WebElement> elements = locateElements("xpath", "TrackPage.xpath.inputAll");
		for (WebElement eachElement : elements) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		}
		return this; 
	}
	public AdminTrackPage unselctAllSkills() {
		List<WebElement> elements = locateElements("xpath", "TrackPage.xpath.skillAll");
		for (WebElement eachElement : elements) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		}
		return this;
	}
	public AdminTrackPage unselctAllManagerRolesOptions() {
		WebElement yes = locateElement("xpath", "TrackPage.xpath.rolesSectionGraphs");
		if(yes.isSelected()!=true) {
			click(yes); 
		}
		List<WebElement> elements = locateElements("xpath", "TrackPage.xpath.allOptionsOfManagerRoles");
		for (WebElement eachElement : elements) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		}  
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.rolesSectionAggregateGraphs1");
		if(ele1.isSelected()==true) {
			click(ele1);
		}
		return this;
	}
	public AdminTrackPage unselctAllManagerSkills() {
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionGraphs");
		if(ele1.isSelected()!=true) {
			click(ele1);
		}
		WebElement ele = locateElement("xpath", "TrackPage.xpath.allOptionsOfManagerGraph1");
		if(ele.isSelected()==true) {
			click(ele);
		}
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.allOptionsOfManagerGraph2");
		if(ele2.isSelected()==true) {
			click(ele2);
		} 
		List<WebElement> elements2 = locateElements("xpath", "TrackPage.xpath.allOptionsOfManagerGraph3");
		for (WebElement eachElement : elements2) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		} 
		WebElement yes2 = locateElement("xpath", "TrackPage.xpath.rolesSectionAggregateGraphs2");
		if(yes2.isSelected()!=true) {
			click(yes2); 
		}
		return this;
	} 
	public AdminTrackPage unselctAllHrRolesOptions() {
		WebElement yes = locateElement("xpath", "TrackPage.xpath.rolesSectionHrViewGraphs");
		if(yes.isSelected()!=true) {
			click(yes); 
		}
		List<WebElement> elements = locateElements("xpath", "TrackPage.xpath.hrRole1");
		for (WebElement eachElement : elements) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		}  
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.rolesSectionHrViewAggregateGraphs");
		if(ele1.isSelected()==true) {
			click(ele1);
		}
		return this;
	}
	public AdminTrackPage unselctAllHrSkills() {
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.skillsSectionHrViewGraphs");
		if(ele1.isSelected()!=true) {
			click(ele1);
		}
		WebElement ele = locateElement("xpath", "TrackPage.xpath.hrRole2Polar");
		if(ele.isSelected()==true) {
			click(ele);
		}
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.hrRole3Radar");
		if(ele2.isSelected()==true) {
			click(ele2);
		} 
		List<WebElement> elements2 = locateElements("xpath", "TrackPage.xpath.hrRole4all");
		for (WebElement eachElement : elements2) {
			if (eachElement.isSelected()==true) {
				click(eachElement); 
			}
		} 
		WebElement yes2 = locateElement("xpath", "TrackPage.xpath.rolesSectionHrViewAggregateGraphs");
		if(yes2.isSelected()!=true) {
			click(yes2); 
		}
		return this;
	}
	public AdminTrackPage clickRolesSectionGraphs1() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllRoles();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.rolesSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.rolesSectionCoursesDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.rolesSectionProjection");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}
	public AdminTrackPage clickRolesSectionGraphs2() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllRoles();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.rolesSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.rolesSectionCoursesDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.rolesSectionPeersNumberOfCourses");
		if(ele1.isSelected()!=true)
			click(ele1);  
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3);
		return this;
	}
	public AdminTrackPage clickRolesSectionGraphs3() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllRoles();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.rolesSectionNumberOfCourses");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.rolesSectionCoursesDuration");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.rolesSectionPeersCoursesDuration");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		return this;
	}

	public AdminTrackPage clickManagerView() {
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionGraphs"));
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionAggregateGraphs1"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionGraphs"));
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionAggregateGraphs1"));
		return this;
	}
	public AdminTrackPage clickHrView() {
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionHrViewGraphs"));
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionHrViewAggregateGraphs"));
		click(locateElement("xpath", "TrackPage.xpath.skillsSectionHrViewGraphs"));
		click(locateElement("xpath", "TrackPage.xpath.rolesSectionHrViewAggregateGraphs"));
		return this;
	}
	public AdminTrackPage selectTimeRange() {
		click(locateElement("xpath", "TrackPage.xpath.aggregateGraphToday"));
		click(locateElement("xpath", "TrackPage.xpath.aggregateGraph3Months"));
		click(locateElement("xpath", "TrackPage.xpath.aggregateGraph6Months"));
		click(locateElement("xpath", "TrackPage.xpath.aggregateGraph1Year"));
		click(locateElement("xpath", "TrackPage.xpath.aggregateGraph2Years"));
		click(locateElement("xpath", "TrackPage.xpath.aggregateGraph3Years"));
		return this;
	}
	

	public AdminTrackPage clickHrRolesSectionGraphs() throws InterruptedException {
		Thread.sleep(2000);
		unselctAllHrSkills();
		WebElement ele1 = locateElement("xpath", "TrackPage.xpath.hrRoleGraph1");
		WebElement ele2 = locateElement("xpath", "TrackPage.xpath.hrRoleGraph2");
		WebElement ele3 = locateElement("xpath", "TrackPage.xpath.hrRoleGraph3");
		WebElement ele4 = locateElement("xpath", "TrackPage.xpath.hrRoleGraph4");
		WebElement ele5 = locateElement("xpath", "TrackPage.xpath.hrRoleGraph5");
		WebElement ele6 = locateElement("xpath", "TrackPage.xpath.hrRoleGraph6");
		if(ele1.isSelected()!=true)
			click(ele1);
		if(ele2.isSelected()!=true)
			click(ele2);
		if(ele3.isSelected()!=true)
			click(ele3); 
		if(ele4.isSelected()!=true)
			click(ele4);
		if(ele5.isSelected()!=true)
			click(ele5);
		if(ele6.isSelected()!=true)
			click(ele6);
		return this;
	}

	public AdminTrackPage selectNumberRange(String num1, String num2) {
		for (int i = 0; i < 9; i++) {
			click(locateElement("xpath", "TrackPage.xpath.aggGraphAdd1"));   
		}
		return this;
	}
	public AdminTrackPage clickUpdateButton() {
		click(locateElement("xpath", "TrackPage.xpath.updateButton"));
		return this;
	}

















}
