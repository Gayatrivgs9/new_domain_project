package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;
import com.aventstack.extentreports.ExtentTest;
import lib.selenium.PreAndPost;

public class ThemePage extends PreAndPost{

	public ThemePage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public ThemePage chooseLogoImage() {
		if(environment.equals("local")) {
			type(locateElement("xpath", "ThemePage.xpath.newLogoUpload"), "D:\\WorkSpace\\PAL\\PAL\\src\\main\\java\\data\\inputFile\\logo.png");
		} else {
			String filePath = System.getProperty("user.dir");
			type(locateElement("xpath", "ThemePage.xpath.newLogoUpload"), filePath+"/classes/data/csvfiles/inputFile/logo.png");
		}
		return this;
	}
	public ThemePage uploadLogoImage() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "ThemePage.xpath.logoUpload"));
		return this;
	}
	public ThemePage chooseBackgroundImage() {
		if(environment.equals("local")) {
		type(locateElement("xpath", "ThemePage.xpath.newImgUpload"), "D:\\WorkSpace\\PAL\\PAL\\src\\main\\java\\data\\inputFile\\Sify.png");
		} else {
			String filePath = System.getProperty("user.dir");
			type(locateElement("xpath", "ThemePage.xpath.newImgUpload"), filePath+"/classes/data/csvfiles/inputFile/logo.png");
		}
		return this;
	}
	public ThemePage uploadBackgroundImage() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "ThemePage.xpath.backGroundUpload"));
		return this;
	}
	public ThemePage enterFontSize() {
		type(locateElement("xpath", "ThemePage.xpath.fontSizeAdmin"), "32");
		return this;
	}
	public ThemePage clickBold() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "ThemePage.xpath.bold"));
		return this;
	}
	public ThemePage clickItalic() {
		click(locateElement("xpath", "ThemePage.xpath.italic"));
		return this;
	}
	public ThemePage clickCapitalize() {
		click(locateElement("xpath", "ThemePage.xpath.capitalize"));
		return this;
	}
	public ThemePage clickUppercase() {
		click(locateElement("xpath", "ThemePage.xpath.uppercase"));
		return this;
	}
	public ThemePage clickLowercase() {
		click(locateElement("xpath","ThemePage.xpath.lowercase"));
		return this;
	}
	public ThemePage clickInherit() {
		click(locateElement("xpath", "ThemePage.xpath.inherit"));
		return this;
	}
	public ThemePage clickResetDefalt() {
		click(locateElement("xpath", "ThemePage.xpath.resetDefault"));
		return this;
	}
	public ThemePage clickUpdate() {
		click(locateElement("xpath", "ThemePage.xpath.updateDefault"));
		return this;
	}
	
	

}



