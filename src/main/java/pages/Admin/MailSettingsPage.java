package pages.Admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class MailSettingsPage extends PreAndPost{

	public  MailSettingsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public MailSettingsPage clickAddNewButton() {
		click(locateElement("xpath", "MailSettingsPage.xpath.addBtn"));  
		return this;
	}
	public MailSettingsPage enterModuleName() throws InterruptedException {
		List<String> lst = new ArrayList<String>(Arrays.asList("Plan Approval","Course Percent","Plan Request","Reset Password","History Analyse"));
		for (int i = 0; i < lst.size(); i++) {
			click(locateElement("xpath", "MailSettingsPage.xpath.addBtn"));
			type(locateElement("xpath", "MailSettingsPage.xpath.moduleName"), lst.get(i));
			click(locateElement("xpath", "MailSettingsPage.xpath.saveAddBtn"));
			Thread.sleep(1000); 
		}
		return this;  
	}
	public MailSettingsPage clickMailSettingsPlanApprovalEdit(String data) {
		if(data.equals("planApproval")) {
			click(locateElement("xpath", "MailSettingsPage.xpath.plan"));
		} else if(data.equals("coursePercent")) {
			click(locateElement("xpath", "MailSettingsPage.xpath.course"));
		}else if(data.equals("planRequest")) {
			click(locateElement("xpath", "MailSettingsPage.xpath.planReq"));
		}else if(data.equals("historyAnalyse")) {
			click(locateElement("xpath", "MailSettingsPage.xpath.historyAnalyse"));
		}else if(data.equals("reset-password")) {
			click(locateElement("xpath", "MailSettingsPage.xpath.resetPassword"));
		}
		return this;
	}
	public MailSettingsPage clickMailSettingsCoursePercentEdit() {
		click(locateElement("xpath", "MailSettingsPage.xpath.course"));
		return this;
	}
	public MailSettingsPage clickMailSettingsPlanRequestEdit() {
		click(locateElement("xpath", "MailSettingsPage.xpath.planReq"));
		return this;
	}
	public MailSettingsPage clickMailSettingsHistoryAnalyseEdit() {
		click(locateElement("xpath", "MailSettingsPage.xpath.historyAnalyse"));
		return this;
	}
	public MailSettingsPage clickMailSettingsResetPasswordEdit() {
		click(locateElement("xpath", "MailSettingsPage.xpath.resetPassword"));
		return this;
	}
	public MailSettingsPage clickSendMailAsYes() {
		WebElement sendMail = locateElement("xpath", "MailSettingsPage.xpath.sendMailYes");
		if(sendMail.isSelected()!=true) {
			click(sendMail);
		}
		return this;
	}
	public MailSettingsPage enterCCType() {                                                              
		click(locateElement("xpath", "MailSettingsPage.xpath.icon2"));                                
		click(locateElement("xpath", "MailSettingsPage.xpath.selectedUser"));
		click(locateElement("xpath", "MailSettingsPage.xpath.acl"));
		click(locateElement("xpath", "MailSettingsPage.xpath.parentRelation"));
		return this;
	}
	public MailSettingsPage enterMailSubject(String data) {
		typeValue(locateElement("xpath", "MailSettingsPage.xpath.mailSubject"), data);
		return this;
	}
	public MailSettingsPage enterScheduleEveryDay(String data) {
		typeValue(locateElement("xpath", "MailSettingsPage.xpath.scheduleDay"), data);
		return this;
	}
	public MailSettingsPage enterScheduleInMinutes(String data) {
		typeValue(locateElement("xpath", "MailSettingsPage.xpath.scheduleMin"), data);
		return this;
	}
	public MailSettingsPage enterScheduleInMin(String data) {
		type(locateElement("xpath", "MailSettingsPage.xpath.scheduleMin"), data);
		return this;
	}
	public MailSettingsPage enterTemplateName(String data) {
		typeValue(locateElement("xpath", "MailSettingsPage.xpath.template"), data);
		return this;
	}
	public MailSettingsPage enterMailCCNames(String data) throws InterruptedException {
		type(locateElement("xpath", "MailSettingsPage.xpath.mailCC"), data);
		Thread.sleep(1000);
		WebElement icon = locateElement("xpath", "MailSettingsPage.xpath.iconMail");
		//click(icon);
		return this;
	}
	public MailSettingsPage enterEmailFromId(String data) {
		type(locateElement("xpath", "MailSettingsPage.xpath.mailFrommail"), data);
		return this;
	}
	public MailSettingsPage clickSchedule() throws InterruptedException {
		Thread.sleep(1000); 
		WebElement schedule = locateElement("xpath", "MailSettingsPage.xpath.schedule");
		if(schedule.isSelected() != true) {
			click(schedule);              
		}
		Thread.sleep(1000);         
		return this;
	}
	public MailSettingsPage clickSave() {
		click(locateElement("xpath", "MailSettingsPage.xpath.save"));
		return this;
	}
	public MailSettingsPage verifyInfoMessage(String data) {
		WebElement info = locateElement("xpath", "MailSettingsPage.xpath.infoMsg");
		System.out.println("Info Msg: "+info.getText());
		verifyPartialText(info, data);
		return this;
	}
	public MailSettingsPage verifySuccessMessage(String data) throws InterruptedException {
		Thread.sleep(1000); 
		WebElement success = locateElement("xpath", "MailSettingsPage.xpath.successMsg");
		System.out.println("Success Msg: "+success.getText());
		verifyPartialText(success, data);
		return this;
	}






















}
