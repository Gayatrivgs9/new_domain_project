package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ThresholdPage extends PreAndPost{

	public  ThresholdPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
		
	public ThresholdPage enterUserThresholdValue(String data) {
		type(locateElement("xpath", "ThresholdPage.xpath.user"), data);
		return this;
	}
	public ThresholdPage enterRoleThresholdValue(String data) {
		type(locateElement("xpath", "ThresholdPage.xpath.role"), data);
		return this;
	}
	public ThresholdPage enterSkillThresholdValue(String data) {
		type(locateElement("xpath", "ThresholdPage.xpath.skill"), data);
		return this;
	}
	public ThresholdPage enterCourseThresholdValue(String value) {
		type(locateElement("xpath", "ThresholdPage.xpath.course"), value);
		return this;
	}
	public ThresholdPage clickSuggestionThresholdSumbitButton() {
		click(locateElement("xpath", "ThresholdPage.xpath.suggestionThresholdSumbitButton"));
		return this;
	}
	public ThresholdPage enterThresholdFitValue(String value) {
		type(locateElement("xpath", "ThresholdPage.xpath.fit"), value);
		return this;
	}
	public ThresholdPage enterThresholdPotentialValue(String value) {
		type(locateElement("xpath", "ThresholdPage.xpath.potential"), value);
		return this;
	}
	public ThresholdPage clickThresholdSubmit() {
		click(locateElement("xpath", "ThresholdPage.xpath.thresholdSubmit"));
		return this;
	}
    public ThresholdPage verifySuccessMsg() {
    	waitUntilVisibilityOfWebElement("ReportSettingsPage.xpath.verifySuccessMsg");
    	verifyExactText(locateElement("xpath", "ReportSettingsPage.xpath.verifySuccessMsg"), "Your settings are updated");
        return this;
    }
	

}



























