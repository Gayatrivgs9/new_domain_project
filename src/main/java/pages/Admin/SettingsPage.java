package pages.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class SettingsPage extends PreAndPost{

	public SettingsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	
	public GenerlSettingsPage clickGeneral() {
		click(locateElement("link", "SettingsPage.link.General"));
		return new GenerlSettingsPage(driver, test);
	} 
	public ThemePage clickTheme() {
		click(locateElement("link", "SettingsPage.link.Theme"));
		return new ThemePage(driver, test);
	}
	public PlanPage clickPlan() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("link", "SettingsPage.link.Plan"));
		return new PlanPage(driver, test);
	}
	public AdminTrackPage clickTrack() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("link", "SettingsPage.link.Track"));
		return new AdminTrackPage(driver, test);
	}
	public AdminRecommendationPage clickRecommendation() {
		click(locateElement("link", "SettingsPage.link.Recommendation"));
		return new AdminRecommendationPage(driver, test);
	}
	public ThresholdPage clickThreshold() {
		click(locateElement("link", "SettingsPage.link.Threshold"));
		return new ThresholdPage(driver, test);
	}
	public BatchProcessPage clickBatchProcess() throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("link", "SettingsPage.link.Batch"));
		return new BatchProcessPage(driver, test);
	}
	public CourseWeightageConfigurationPage clickCourseWeightageConfiguration() {
		click(locateElement("link", "SettingsPage.link.CourseWeightageConfiguration"));
		return new CourseWeightageConfigurationPage(driver, test);
	}
	public TagsPage clickTags() {
		click(locateElement("link", "SettingsPage.link.Tags"));
		return new TagsPage(driver, test);
	}
	public UrlPage clickURL() {
		click(locateElement("link", "SettingsPage.link.URL"));
		return new UrlPage(driver, test); 
	}
	public ParameterPage clickParameter() {
		click(locateElement("link", "SettingsPage.link.Parameter"));
		return new ParameterPage(driver, test); 
	}
	public ReportSettingsPage clickReports() {
		click(locateElement("link", "SettingsPage.link.ReportsSettings"));
		return new ReportSettingsPage(driver, test); 
	}
	
	public SettingsPage clickLocal() {
		WebElement eleLocal = locateElement("name", "GeneralPage.name.local");
		if(!eleLocal.isSelected()) 
		click(eleLocal);
 		return this;
	}
	public SettingsPage clickOAuth() {
		WebElement eleOAuth = locateElement("name", "GeneralPage.name.oAuth");
		if(!eleOAuth.isSelected()) 
		click(eleOAuth); 
 		return this;
	}
	public SettingsPage clickThirdParty() {
		WebElement eleThirdParty = locateElement("name", "GeneralPage.name.thirdParty");
		if(!eleThirdParty.isSelected()) 
		click(eleThirdParty);
 		return this;
	}
	public SettingsPage clickIsLive() {
		WebElement eleIsLive = locateElement("name", "GeneralPage.name.isLive");
		if(!eleIsLive.isSelected()) 
		click(eleIsLive); 
 		return this;
	}
	public SettingsPage enterDefaultCourseDuration(String data) {
		WebElement eleIsLive = locateElement("name", "GeneralPage.name.defaultCourseDuration");
		type(eleIsLive, data); 
 		return this;
	}
	public SettingsPage clickUpdateButton() {
		click(locateElement("xpath", "GeneralPage.xpath.updateButton"));
		return this;
	}
	public SettingsPage clickCancelButton() {
		click(locateElement("xpath", "GeneralPage.xpath.cancelButton"));
		return this; 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
