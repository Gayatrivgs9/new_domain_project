package pages.Admin;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AttributeMapping extends PreAndPost {

	public AttributeMapping(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	@FindBy(how=How.XPATH,using="//button[text()='Save']") private WebElement eleSave1;	
	@FindBy(how=How.XPATH,using="(//button[text()='Save'])[2]") private WebElement eleSave2;
	@FindBy(how=How.XPATH,using="//button[text()=' Map New schema']") private WebElement eleMapNewSchema;

	private WebElement eleUser;
	private WebElement eleRole;
	//private WebElement eleCompetency;
	private WebElement eleSkill;
	//private WebElement eleProgram;
	private WebElement eleCourse;

	public AttributeMapping clickCompetency() {
		WebElement element = locateElement("name", "AttributeMapping.name.competency");
		if(element.isSelected()!=true) {
			click(element);
		}
		return this; 
	}
	public AttributeMapping clickCompetencyNo() {
		WebElement element = locateElement("name", "AttributeMapping.name.competency");
		if(element.isSelected()==true) {
			click(element);
		}
		return this; 
	}
	public void selectSchemaConfiguration() {
		eleUser = locateElement("name", "AttributeMapping.name.user");
		eleRole = locateElement("name", "AttributeMapping.name.role");
		if (eleUser.isSelected()==false || eleRole.isSelected()==false || eleSkill.isSelected()==false || eleCourse.isSelected()==false) {
			click(eleUser);
			click(eleRole);
			click(eleSkill);
			click(eleCourse); 
		}
	}
	public AttributeMapping selectMultipleSchemaEnableLevels() {
		click(locateElement("xpath","AttributeMapping.xpath.inputSchemaConfig"));
		click(locateElement("xpath","AttributeMapping.xpath.userSchemaConfig"));
		click(locateElement("xpath","AttributeMapping.xpath.roleSchemaConfig"));
		click(locateElement("xpath","AttributeMapping.xpath.skillSchemaConfig"));
		click(locateElement("xpath","AttributeMapping.xpath.courseSchemaConfig"));
		return this;
	}
	public AttributeMapping clickSchemaConfigSaveButton() {
		click(locateElement("xpath","AttributeMapping.xpath.schemaConfigSaveButton"));
		return this;
	}
	public AttributeMapping clickMapNewSchemaSaveButton() throws InterruptedException {
		click(locateElement("xpath","AttributeMapping.xpath.newSchmaSaveButton"));
		Thread.sleep(2000); 
		return this;
	}
	public AttributeMapping verifySuccssMsg() {
		verifyExactText(locateElement("xpath", "AttributeMapping.xpath.successMsg"), "Mappings Saved successfully"); 
		return this;
	}
	
	public AttributeMapping uploadFileForSchemaMaping() throws InterruptedException {
		Thread.sleep(2000); 
		WebElement eleChooseFile = locateElement("xpath", "AttributeMapping.xpath.uploadFileChoosebutton");
		if(environment.equals("local")) {
			type(eleChooseFile, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\AttributesMappings.xlsx");
		} else {
			type(eleChooseFile, System.getProperty("user.dir")+"/classes/data/csvfiles/AttributesMappings.xlsx");
		}
		click(locateElement("xpath","AttributeMapping.xpath.uploadButton"));
		return this; 
	}
	public AttributeMapping selectSchemas(String SchemaName1, String SchemaName2, String SchemaName4, String SchemaName6) {
		eleUser = locateElement("name", "AttributeMapping.name.user");
		eleRole = locateElement("name", "AttributeMapping.name.role");
		eleSkill = locateElement("name", "AttributeMapping.name.skill");
		eleCourse = locateElement("name", "AttributeMapping.name.course");
		if(eleUser.isSelected()!=true) {
			click(eleUser); 
		}
		if(eleRole.isSelected()!=true) {
			click(eleRole); 
		}
		if(eleSkill.isSelected()!=true) {
			click(eleSkill); 
		}
		if(eleCourse.isSelected()!=true) {
			click(eleCourse);  
		}
		/*if(SchemaName1.equalsIgnoreCase("Yes")){if(verifySelectedBoolean(eleUser)==false){click(eleUser);}}
		if(SchemaName1.equalsIgnoreCase("No")){if(verifySelectedBoolean(eleUser)==true){click(eleUser);}}
		if(SchemaName2.equalsIgnoreCase("Yes")){if(verifySelectedBoolean(eleRole)==false){click(eleRole);}}
		if(SchemaName2.equalsIgnoreCase("No")){if(verifySelectedBoolean(eleRole)==true){click(eleRole);}}
		//if(SchemaName3.equalsIgnoreCase("Yes")){if(verifySelectedBoolean(eleCompetency)==false){click(eleCompetency);}}
		//if(SchemaName3.equalsIgnoreCase("No")){if(verifySelectedBoolean(eleCompetency)==true){click(eleCompetency);}}
		if(SchemaName4.equalsIgnoreCase("Yes")){if(verifySelectedBoolean(eleSkill)==false){click(eleSkill);}}
		if(SchemaName4.equalsIgnoreCase("No")){if(verifySelectedBoolean(eleSkill)==true){click(eleSkill);}}
		//if(SchemaName5.equalsIgnoreCase("Yes")){if(verifySelectedBoolean(eleProgram)==false){click(eleProgram);}}
		//if(SchemaName5.equalsIgnoreCase("No")){if(verifySelectedBoolean(eleProgram)==true){click(eleProgram);}}
		if(SchemaName6.equalsIgnoreCase("Yes")){if(verifySelectedBoolean(eleCourse)==false){click(eleCourse);}}
		if(SchemaName6.equalsIgnoreCase("No")){if(verifySelectedBoolean(eleCourse)==true){click(eleCourse);}}*/
		return this;
	}

	public AttributeMapping schemaMapping() throws InterruptedException {
		typeValue(locateElement("xpath", "AttributeMapping.xpath.schema1dd1"), "user"); 
		typeValue(locateElement("xpath", "AttributeMapping.xpath.schema1dd2"), "role");
		Thread.sleep(1000);
		selectDropDownUsingVisibleText(locateElement("xpath", "AttributeMapping.xpath.schema1dd3"), "jobroles");
		selectDropDownUsingVisibleText(locateElement("xpath", "AttributeMapping.xpath.schema1dd4"), "name");
		Thread.sleep(2000);
		typeValue(locateElement("xpath", "AttributeMapping.xpath.schema2dd1"), "role");
		typeValue(locateElement("xpath", "AttributeMapping.xpath.schema2dd2"), "skill");
		Thread.sleep(1000);
		selectDropDownUsingVisibleText(locateElement("xpath", "AttributeMapping.xpath.schema2dd3"), "skill");
		selectDropDownUsingVisibleText(locateElement("xpath", "AttributeMapping.xpath.schema2dd4"), "name");
		Thread.sleep(2000);
		typeValue(locateElement("xpath", "AttributeMapping.xpath.schema3dd1"), "skill");
		typeValue(locateElement("xpath", "AttributeMapping.xpath.schema3dd2"), "course");
		Thread.sleep(1000);
		selectDropDownUsingVisibleText(locateElement("xpath", "AttributeMapping.xpath.schema3dd3"), "name");
		selectDropDownUsingVisibleText(locateElement("xpath", "AttributeMapping.xpath.schema3dd4"), "skill");
		return this;
	}
	public AttributeMapping attributeMap() throws IOException, InterruptedException {
		int LastrowNo = getRowNo("Attribute", "DataSheet");	
		String prevSchemaNo = "";
		int SchemaCount = 1;
		for(int i=1; i<=LastrowNo; i++){			
			String SchemaNo = readData(0, i, "Attribute", "DataSheet");
			String SchemaL = readData(1, i, "Attribute", "DataSheet");
			String SchemaR = readData(2, i, "Attribute", "DataSheet");
			String AttL = readData(3, i, "Attribute", "DataSheet");
			String AttR = readData(4, i, "Attribute", "DataSheet");
			if((i>1)&&(!prevSchemaNo.equals(SchemaNo))){
				click(eleMapNewSchema);
			}
			//Select schema LHS
			click(locateElement("xpath", "//span[text()='Schema - "+SchemaNo+"']/../..//input[@role='combobox']"));
			Thread.sleep(1000);
			List<WebElement> ddList1 = driver.findElements(By.xpath("//div[starts-with(@class, 'ng-option')]/span"));
			for(WebElement dd : ddList1){
				String temp = getText(dd);
				if(temp.equalsIgnoreCase(SchemaL)){
					dd.click();
					break;
				}
			}
			//Select schema RHS
			click(locateElement("xpath", "(//span[text()='Schema - "+SchemaNo+"']/../..//input[@role='combobox'])[2]"));
			Thread.sleep(1000);
			List<WebElement> ddList2 = driver.findElements(By.xpath("//div[starts-with(@class, 'ng-option')]/span"));
			for(WebElement dd : ddList2){
				String temp = getText(dd);
				if(temp.equalsIgnoreCase(SchemaR)){
					dd.click();
					break;
				}
			}			
			// Select Attributes
			if(prevSchemaNo.equals(SchemaNo)){
				click(locateElement("xpath", "//span[text()='Schema - "+SchemaNo+"']/../..//button[text()=' Add']"));
				SchemaCount++;
			}
			else{
				SchemaCount = 1;
			}
			int LC = (SchemaCount*2)-1; 
			int RC = SchemaCount*2;
			WebElement AttLeft = locateElement("xpath", "(//span[text()='Schema - "+SchemaNo+"']/../..//select)["+LC+"]");
			WebElement AttRight = locateElement("xpath", "(//span[text()='Schema - "+SchemaNo+"']/../..//select)["+RC+"]");
			selectDropDownUsingValue(AttLeft, AttL);
			selectDropDownUsingValue(AttRight, AttR);
			prevSchemaNo = SchemaNo;
		}
		click(eleSave2);
		return this;
	}

	public AttributeMapping clickSave2() {
		click(eleSave2);
		return this;
	}

	public AttributeMapping clickMapNewSchema() {
		click(eleMapNewSchema);
		return this;
	}








}
