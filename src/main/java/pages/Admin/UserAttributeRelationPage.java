package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class UserAttributeRelationPage extends PreAndPost{

	
	public  UserAttributeRelationPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public UserAttributeRelationPage clickCourses() {
		click(locateElement("xpath", "UserAttributeRelationPage.xpath.courses"));
		return this;
	}
	public UserAttributeRelationPage clickSuggestionData() {
		click(locateElement("xpath", "UserAttributeRelationPage.xpath.suggestionData"));
		return this;
	}
	public UserAttributeRelationPage clickRecommendations() {
		click(locateElement("xpath", "UserAttributeRelationPage.xpath.recommendations"));
		return this;
	}
	public UserAttributeRelationPage clickDataTypeMatch() {
		click(locateElement("xpath", "UserAttributeRelationPage.xpath.datatypematch"));
		return this;
	}
	public UserAttributeRelationPage clickSuggestedCourses() {
		click(locateElement("xpath", "UserAttributeRelationPage.xpath.suggestedCourses"));
		return this;
	}
	public UserAttributeRelationPage clickSave() {
		click(locateElement("xpath", "UserAttributeRelationPage.xpath.saveButton"));
		return this;
	}
}
