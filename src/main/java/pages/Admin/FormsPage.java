package pages.Admin;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class FormsPage extends PreAndPost{

	public  FormsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public FormsPage clickBuildNewAssignment() {
		click(locateElement("xpath", "FormsPage.xpath.buildNewAssignment"));
		return this;
	}
	public FormsPage clickModifyAssignment() throws InterruptedException {
		Thread.sleep(2000);
		moveToElment(locateElement("xpath", "FormsPage.xpath.modifyAssignment")); 
		//click(locateElement("xpath", "FormsPage.xpath.modifyAssignment"));
		return this;
	}
	public FormsPage enterTypeOfAssignment(String data) {
		type(locateElement("xpath", "FormsPage.xpath.type"), data);
		return this;
	}
	public FormsPage enterApprovalType(String data) {
		typeValue(locateElement("xpath", "FormsPage.xpath.ApprovalType"), data);
		return this;
	}
	static String courseName;
	public FormsPage enterTypeOfName(String data) throws IOException, EncryptedDocumentException, InvalidFormatException {
		courseName = data;  
		type(locateElement("xpath", "FormsPage.xpath.name"), courseName);
		writeData(0, 1, courseName, "AdminFormsRndNum", "DataSheet", "Green"); 
		return this;
	}	
	public FormsPage enterMapWith(String data) {
		type(locateElement("xpath", "FormsPage.xpath.mapWith"), data);
		return this;
	}
	public FormsPage enterDurationInMin(String data) {
		type(locateElement("xpath", "FormsPage.xpath.duration"), data);
		return this;
	}
	public FormsPage enterMappingKey(String data) {
		type(locateElement("xpath", "FormsPage.xpath.mappingKey"), data);
		return this;
	}
	public static String mapingValue;
	public FormsPage enterMappingValue(String data) throws InterruptedException {
		Thread.sleep(1000);
		click(locateElement("xpath", "FormsPage.xpath.mapVAlueArrow")); 
		/*mapingValue = locateElement("xpath", "FormsPage.xpath.mapVAlue11").getText();
		System.out.println(mapingValue);*/
		click(locateElement("xpath", "FormsPage.xpath.mapSGSelf"));
		click(locateElement("xpath", "FormsPage.xpath.mapSG1"));
		click(locateElement("xpath", "FormsPage.xpath.mapSG2"));
		click(locateElement("xpath", "FormsPage.xpath.mapSG3"));
		click(locateElement("xpath", "FormsPage.xpath.mapSG4"));
		return this;
	}
	public FormsPage enterPassPercentage(String data) {
		type(locateElement("xpath", "FormsPage.xpath.passPer"), data);
		return this;
	}
	public FormsPage enterFormId(String data) {
		type(locateElement("xpath", "FormsPage.xpath.formId"), data);
		return this;
	}
	public static int number0fQuestions; 
	public FormsPage enterNoOfQuestions(String qnsCount) {
		number0fQuestions = Integer.parseInt(qnsCount);
		type(locateElement("xpath", "FormsPage.xpath.noOfQuestions"), qnsCount);
		return this;
	}
	public FormsPage enterNewTag(String data) {
		type(locateElement("xpath", "FormsPage.xpath.enterANewTag"), data);
		System.out.println(data);
		return this;
	}
	/*public FormsPage dradAndDropTheTextField(String qns1, String qns2) {
		int questions = Integer.parseInt(number0fQuestions);
		for (int i = 0; i < questions; i++) {
			WebElement source = locateElement("FormsPage.id.textField");
			WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit");
			dragAndDrop(source, target);
			type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), qns1); 
			click(locateElement("xpath", "FormsPage.xpath.saveButton"));
		}
		return this;
	}*/
	/*public AssignmentPage dradAndDropTextField() throws InterruptedException {
		WebElement source = locateElement("FormsPage.id.textField");
		WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit");
		dragAndDrop(source, target);
		Thread.sleep(2000);
		//click(locateElement("xpath", "FormsPage.xpath.textFieldSubmit"));
		return new AssignmentPage(driver, test); 
	} 
	public AssignmentPage dradAndDropNumberField() {
		WebElement source = locateElement("FormsPage.id.number");
		WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit");
		dragAndDrop(source, target);
		return new AssignmentPage(driver, test);
	}*/
	public FormsPage dradAndDropTextArea() throws InterruptedException, IOException {
		for(int i=1; i<=number0fQuestions; i++) {                   
			WebElement source = locateElement("xpath","FormsPage.id.textArea");
			WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit");
			if(i==1){
				dragAndDrop(source, target);
			} else {
				WebElement target2 = driver.findElementByXPath("//label[contains(text(), 'Question :')]");
				dragAndDrop(source, target2);
			} 
			if(i==1) {
				String question1 = readData(15, 1, "AdminForms", "DataSheet");
				System.out.println(question1); 
				type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), readData(15, 1, "AdminForms", "DataSheet"));
				click(locateElement("xpath", "FormsPage.xpath.saveButton"));
			}else {
				String question1 = readData(16, 1, "AdminForms", "DataSheet");
				System.out.println(question1); 
				type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), readData(16, 1, "AdminForms", "DataSheet"));
				click(locateElement("xpath", "FormsPage.xpath.saveButton"));
			}
		}            
		return new FormsPage(driver, test);            
	}

	public FormsPage reviewDradAndDropTextArea() throws InterruptedException, IOException {
		for(int i=1; i<=number0fQuestions; i++) {                   
			WebElement source = locateElement("xpath","FormsPage.id.ReviewtextArea");
			WebElement target = locateElement("xpath", "FormsPage.id.ReviewtextAreaSubmit");
			if(i==1){
				dragAndDrop(source, target);
			} else {
				WebElement target2 = driver.findElementByXPath("//label[contains(text(),'review :')]");
				dragAndDrop(source, target2);
			} 
			if(i==1) {
				type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), readData(17, 1, "AdminForms", "DataSheet"));
				click(locateElement("xpath", "FormsPage.xpath.saveButton"));
			}else {
				type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), readData(18, 1, "AdminForms", "DataSheet"));
				click(locateElement("xpath", "FormsPage.xpath.saveButton"));
			}
		}            
		return new FormsPage(driver, test);            
	}
	/*public AssignmentPage dradAndDropTextArea() throws InterruptedException {
		Thread.sleep(1000); 
		WebElement source = locateElement("xpath", "FormsPage.id.textArea");
		WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit");
		dragAndDrop(source, target);
		return new AssignmentPage(driver, test);
	}  */
	/*public AssignmentPage reviewDradAndDropTextArea() throws InterruptedException {
		Thread.sleep(1000); 
		WebElement source = locateElement("FormsPage.id.ReviewtextArea");
		WebElement target = locateElement("xpath", "FormsPage.id.ReviewtextAreaSubmit");
		dragAndDrop(source, target);
		return new AssignmentPage(driver, test);
	}  
	public AssignmentPage dradAndDropTextArea2() throws InterruptedException {
		Thread.sleep(1000);  
		WebElement source = locateElement("xpath", "FormsPage.id.textArea");
		WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit22");
		clickAndHold(source, target);
		return new AssignmentPage(driver, test);
	}
	public AssignmentPage reviewDradAndDropTextArea2() throws InterruptedException {
		Thread.sleep(1000);  
		WebElement source = locateElement("FormsPage.id.ReviewtextArea2");
		WebElement target = locateElement("xpath", "FormsPage.id.ReviewtextAreaSubmit22");
		clickAndHold(source, target);
		return new AssignmentPage(driver, test);
	}*/
	public AssignmentPage dradAndDropRadioField() {
		WebElement source = locateElement("FormsPage.id.radio");
		WebElement target = locateElement("xpath", "FormsPage.xpath.assessmentSubmit");
		dragAndDrop(source, target);
		return new AssignmentPage(driver, test);
	}
	public FormsPage clickAssessmentSubmit() {
		click(locateElement("xpath", "FormsPage.xpath.assessmentSubmitButton"));
		return this;
	}
	public FormsPage clickSaveForms() throws InterruptedException {
		Thread.sleep(4000); 
		click(locateElement("xpath", "FormsPage.xpath.newSaveButton"));  
		return this;
	}	
	public FormsPage enterSelectForm() throws InterruptedException, IOException {
		/*Thread.sleep(1000); 
		click(locateElement("xpayh", "FormsPage.xpath.modAssFormArr"));
		Thread.sleep(1000);
		click(locateElement("xpath", "FormsPage.xpath.modAssFormNme"));   */
		typeValue(locateElement("xpath", "FormsPage.xpath.selctForm"), readData(0, 1, "AdminFormsRndNum", "DataSheet"));  
		return this; 
	}
	public FormsPage enterOriginalAnswers(String data1, String data2) throws InterruptedException {
		List<WebElement> allElements = locateElements("xpath", "FormsPage.xpath.selctFormLabel1");
		for (int i = 0; i < allElements.size(); i++) {
			String attribute = allElements.get(i).getAttribute("value");
			System.out.println("Value : "+attribute); 
			if(allElements.get(i).getAttribute("value").isEmpty()!=true) {
				String attributeValue = allElements.get(i).getAttribute("value"); 
				reportStep("Assessment original answer : "+attributeValue, "PASS");
			} else {
				type(locateElement("xpath", "FormsPage.xpath.selctFormLabel1"), data1);
				Thread.sleep(1000);
				type(locateElement("xpath", "FormsPage.xpath.selctFormLabel2"), data2);
			}
		}
		return this; 
	}
	public FormsPage clickSubmit() {
		click(locateElement("xpath", "FormsPage.xpath.AssSubBtn"));
		return this;
	}

     public FormsPage verifySuccessMsg() {
    	 verifyExactText(locateElement("xpath", "FormsPage.xpath.uploadSuc"), "Updated successfully");
    	 return this;
     }

}



























