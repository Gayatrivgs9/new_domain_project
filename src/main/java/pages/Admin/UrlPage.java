package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class UrlPage extends PreAndPost{

	public  UrlPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	public UrlPage enterSystemName(String data) {
		type(locateElement("xpath", "UrlPage.xpath.systemName"), data);
		return this;
	}
	public UrlPage enterLaunch(String data) {
		type(locateElement("xpath", "UrlPage.xpath.launchButton"), data);
		return this;
	}
	public UrlPage enterCompleted(String data) {
		type(locateElement("xpath", "UrlPage.xpath.completedButton"), data);
		return this;
	}
	public UrlPage enterBaseUrl(String data) {
		type(locateElement("name", "UrlPage.name.baseUrl"), data);
		return this;
	}
	public UrlPage enterSelectSchema(String data) {
		typeValue(locateElement("xpath", "UrlPage.xpath.selectSchema"), data);
		return this;
	}
	public UrlPage enterMappingKey(String data) {
		typeValue(locateElement("xpath", "UrlPage.xpath.mappingKey"), data);
		return this;
	}
    public UrlPage enterMappingWith(String data) {
		typeValue(locateElement("xpath", "UrlPage.xpath.mappingWith"), data);
		return this;
	}
	public UrlPage clickAddParam() {
		click(locateElement("xpath", "UrlPage.xpath.addParam"));
		return this;
	}
	public UrlPage enterUserame(String data) {
		type(locateElement("xpath", "UrlPage.xpath.username"), data);
		return this;
	}
	public UrlPage enterPassword(String data) {
		type(locateElement("xpath", "UrlPage.xpath.password"), data);
		return this;
	}
	public UrlPage clickAdd() {
		click(locateElement("xpath", "UrlPage.xpath.add"));
		return this;
	}



}



























