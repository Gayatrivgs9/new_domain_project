package pages.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ReportSettingsPage extends PreAndPost{

	public  ReportSettingsPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public ReportSettingsPage clickBusinessUnits() {
		WebElement businessUnits = locateElement("xpath", "ReportSettingsPage.xpath.businessUnits");
		if(businessUnits.isSelected()!=true) {
			click(businessUnits);
		}
		return this;
	}
	public ReportSettingsPage clickDepartments() {
		WebElement departments = locateElement("xpath", "ReportSettingsPage.xpath.departments");
		if(departments.isSelected()!=true) {
			click(departments);
		}
		return this;
	}
	public ReportSettingsPage clickBusinessUnitsNo() {
		WebElement businessUnits = locateElement("xpath", "ReportsIntegration.xpath.no1");
		if(businessUnits.isSelected()!=true) {
			click(businessUnits);
		}
		return this;  
	}	
	public ReportSettingsPage clickDepartmentUnitsNo() {		
		WebElement departments = locateElement("xpath", "ReportsIntegration.xpath.no2");
		if(departments.isSelected()!=true) {
			click(departments);
		}
		return this;  
	}
	public ReportSettingsPage clickUpdateButton() {
		click(locateElement("xpath", "ReportSettingsPage.xpath.button"));
		return this;
	}
    public ReportSettingsPage verifySuccessMsg() {
		verifyExactText(locateElement("xpath", "ReportSettingsPage.xpath.verifySuccessMsg"), "Your settings are updated");
    	return this;
	}
    
    public ReportSettingsPage verifyValue() {
    	verifyDisplayed(locateElement("xpath", "LearningPlan.xpath.skillIndexGo")); 
    	return this;
    }
    
    


}











