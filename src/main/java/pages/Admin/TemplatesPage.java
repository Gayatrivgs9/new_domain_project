package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class TemplatesPage extends PreAndPost{

	
	public  TemplatesPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	 
	public TemplatesPage clickCreate() {
		click(locateElement("xpath", "TemplatesPage.xpath.create"));
		return this;
	}
	public TemplatesPage enterTemplateName(String data) throws InterruptedException {
		type(locateElement("xpath", "TemplatesPage.xpath.templateName"), data);
		Thread.sleep(1000);
		return this;
	}
	public TemplatesPage enterPlaceHoldersRole(String data) {
		typeValue(locateElement("xpath", "TemplatesPage.xpath.Placeholders"), data);
		return this;
	}
	public TemplatesPage enterTemplateData(String data) {
		switchToFrame(0);
		type(locateElement("xpath", "TemplatesPage.xpath.textArea"), data);
		defaultContent();
		return this;
	}
	public TemplatesPage clickUpdate() {
		click(locateElement("xpath", "TemplatesPage.xpath.bodyCreateButton"));
		return this;
	}
	public TemplatesPage clickCancel() {
		click(locateElement("xpath", "TemplatesPage.xpath.bodyCancelButton"));
		return this;
	}
}
