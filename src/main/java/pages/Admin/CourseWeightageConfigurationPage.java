package pages.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CourseWeightageConfigurationPage extends PreAndPost{

	public  CourseWeightageConfigurationPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public CourseWeightageConfigurationPage downloadCourseAsCSV() {
		click(locateElement("xpath", "CourseWeightageConfigurationPage.xpath.downloadCSV"));
		return this;
	}
	public CourseWeightageConfigurationPage chooseFilePath(String data) {
		WebElement fileUpload = locateElement("xpath", "CourseWeightageConfigurationPage.xpath.file");
		if(environment.equals("local")) {
		   type(fileUpload, System.getProperty("user.dir")+"\\src\\main\\java\\data\\csvfiles\\course_nd04.csv"); 
		} else {
			type(fileUpload, "/classes/data/csvfiles/course_nd04.csv");
		}
		return this;
	}
	
	public CourseWeightageConfigurationPage clickSave() {
		scrolltoview("CourseWeightageConfigurationPage.xpath.lastSaveButton");
		reportStep("Clicked the save button", "PASS");  
		return this; 
	}
	
	

}



























