package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AdminHomePage extends PreAndPost {

	public AdminHomePage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	
	public SettingsPage clickSettings() throws InterruptedException {	
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.settings"));
		return new SettingsPage(driver, test);
	}
	public TaggingBUAndDepartment clickTaggingBUAndDepartment() throws InterruptedException {	
		Thread.sleep(2000);
		click(locateElement("xpath", "TaggingBUPage.xpath.bu"));
		return new TaggingBUAndDepartment(driver, test); 
	}
	public BaseliningPage clickBaselining() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.baselining"));
		return new BaseliningPage(driver, test);
	}
	public BaseliningDataPage clickBaseliningData() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.baseliningData"));
		return new BaseliningDataPage(driver, test);
	}
	public AdminReportPage clickReport() throws InterruptedException {	
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.report"));
		return new AdminReportPage(driver, test);
	}
	public MailSettingsPage clickMailSettings() throws InterruptedException {	
		Thread.sleep(2000); 
		click(locateElement("xpath", "AdminHomePage.xpath.mailSettings"));
		return new MailSettingsPage(driver, test);
	}
	public TemplatesPage clickTemplates() throws InterruptedException {	
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.templates"));
		return new TemplatesPage(driver, test);
	}
	public AclPage clickAcl() throws InterruptedException {	
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.acl"));
		return new AclPage(driver, test);
	}
	public UserAttributeRelationPage clickUserAttributeRelation() throws InterruptedException {		
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.userAttributeRelation"));
		return new UserAttributeRelationPage(driver, test);
	}
	public FormsPage clickForms() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.forms"));
		return new FormsPage(driver, test);
	}
	public LinkagePage clickLinkage() throws InterruptedException {
		Thread.sleep(2000);
		click(locateElement("xpath", "AdminHomePage.xpath.linkage"));
		return new LinkagePage(driver, test); 
	}
	



}
