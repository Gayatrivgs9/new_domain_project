package pages.Admin;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AclPage extends PreAndPost{

	public  AclPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public AclPage clickRoles() throws InterruptedException {
		Thread.sleep(2000); 
		moveToElment(locateElement("xpath", "AclPage.xpath.roles"));
		return this;
	}
	public AclPage clickUserOptionEditButton() throws InterruptedException {
		click(locateElement("xpath", "AclPage.xpath.userViewEditBtn"));
		return this;  
	}
	public AclPage clickUserData() {
		click(locateElement("xpath", "AclPage.xpath.userPersonalData"));
		click(locateElement("xpath", "AclPage.xpath.userProfile"));
		return this;
	}
	public AclPage clickUserDataNo() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.userPersonalData");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.userProfile");
		if(ele1.isSelected()&&ele2.isSelected()==true) {
			click(ele1);
			click(ele2); 
		}
		return this;
	}
	public AclPage clickUsers() throws InterruptedException {
		Thread.sleep(2000);
		WebElement eleUser = locateElement("xpath", "AclPage.xpath.users");
		click(eleUser);
		return this;
	}
	public AclPage enterShowRecordsValue() {
		type(locateElement("xpath", "AclPage.xpath.showRecords"), "100");
		return this;
	}
	public AclPage verifyUserEmailId(String data) {
		WebElement element = locateElement("xpath", "AclPage.xpath.filterWithEmailNew");
		type(element, data);
		String emailId = element.getAttribute("value");
		String actualEmailId = driver.findElementByXPath("(//table)[2]/tbody/tr/td[text()='"+emailId+"']").getText();
		if(emailId.equals(actualEmailId)) {
			driver.findElementByXPath("(//table)[2]/tbody/tr/td[text()='"+emailId+"']/preceding::input[1]").click();
		} else {
			reportStep("Email id value is not matched", "FAIL"); 
		}
		return this;
	}
	public AclPage verifyEmailId(String emailIdValue) {
		List<WebElement> allEements = locateElements("xpath", "AclPage.xpath.allEmailIds");
		for (WebElement each : allEements) {
			String trim = each.getText().trim();
			if(emailIdValue.equals(trim)) {
				driver.findElementByXPath("(//table)[2]/tbody/tr/td[text()='"+emailIdValue+"']/preceding::input[1]").click();
				moveToElment(locateElement("xpath", "AclPage.xpath.addRemoveRoles"));   
				break;
			}
		}
		click(locateElement("xpath", "AclPage.xpath.userPermission"));
		click(locateElement("xpath", "AclPage.xpath.userPermSave"));  
		return this;  
	}
	public AclPage clickAddRole() throws InterruptedException {
		Thread.sleep(2000); 
		click(locateElement("xpath", "AclPage.xpath.addRole"));
		return this;
	}
	public AclPage clickCloneRole() {
		click(locateElement("xpath", "AclPage.xpath.cloneRole"));
		return this;
	}
	public AclPage enterAddRoleName(String data) {
		type(locateElement("xpath", "AclPage.xpath.addRoleName"), data);
		return this;
	}
	public AclPage selectStatusActive() {
		WebElement eleSlectActive = locateElement("xpath", "AclPage.xpath.statusActive");
		if (eleSlectActive.isSelected()!=true) {
			click(eleSlectActive);
		}
		return this;
	}
	public AclPage selectStatusInActive() {
		WebElement eleSelectInActive = locateElement("xpath", "AclPage.xpath.statusInActive");
		if (eleSelectInActive.isSelected()!=true) {
			click(eleSelectInActive);
		}
		return this;
	}
	public AclPage selectRoleType(String value) {
		typeValue(locateElement("xpath", "AclPage.xpath.selectRoleType"), value);
		return this;
	}
	public AclPage clickSave() {
		click(locateElement("xpath","AclPage.xpath.save"));
		return this;
	}
	public AclPage clickUserOptions() { 
		try {
			List<WebElement> eleUsers = locateElements("xpath", "AclPage.xpath.roleUsers");
			for (WebElement users : eleUsers) {
				if(users.isSelected()!=true) {
					clickNew(users);
				}
			}
			reportStep("ACL Permissions for user options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickUnSelectSomeUserOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.getManagerViewData");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.getHRViewData");
		WebElement ele3 = locateElement("xpath", "AclPage.xpath.getsCoursesEnrolledGroupedByUsers");
		WebElement ele4 = locateElement("xpath", "AclPage.xpath.getsCoursesForHR");
		if(ele1.isSelected() == true) {
			click(ele1);
		}
		if(ele2.isSelected() == true) {
			click(ele2);
		}
		if(ele3.isSelected() == true) {
			click(ele3);
		}
		if(ele4.isSelected() == true) {
			click(ele4);
		}
		return this;
	}
	public AclPage clickUserOptionsNo() {
		try {
			WebElement eleAchievmenet = locateElement("xpath", "AclPage.xpath.userOptionsasNo");
			click(eleAchievmenet);
			if (eleAchievmenet.isSelected()==true) {
				click(eleAchievmenet);
			}
			/*List<WebElement> eleAchievmenet = locateElements("xpath", "AclPage.xpath.achOptNo");
			for (WebElement achievement : eleAchievmenet) {		
				if(achievement.isSelected()==true) {
					clickNew(achievement);
				}
			}*/
			reportStep("ACL Permissions for User options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickAchievementOptions() {
		try {
			List<WebElement> eleAchievmenet = locateElements("xpath", "AclPage.xpath.achievement");
			for (WebElement achievement : eleAchievmenet) {		
				if(achievement.isSelected()!=true) {
					clickNew(achievement);
				}
			}
			reportStep("ACL Permissions for Achievement options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickUnSelectSomeAchievementOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.recentSkillBatchSection");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.recentCourseCertificates");
		if(ele1.isSelected() == true) {
			click(ele1);
		}
		if(ele2.isSelected() == true) {
			click(ele2);
		}
		return this;
	}
	public AclPage clickAchievementOptionsNo() {
		try {
			WebElement eleAchievmenet = locateElement("xpath", "AclPage.xpath.achOptNo");
			click(eleAchievmenet);
			if (eleAchievmenet.isSelected()==true) {
				click(eleAchievmenet);
			}
			/*List<WebElement> eleAchievmenet = locateElements("xpath", "AclPage.xpath.achOptNo");
			for (WebElement achievement : eleAchievmenet) {		
				if(achievement.isSelected()==true) {
					clickNew(achievement);
				}
			}*/
			reportStep("ACL Permissions for Achievement options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickAssessmentOptions() {
		try {
			List<WebElement> eleAssessment = locateElements("xpath", "AclPage.xpath.assessment");
			for (WebElement assessment : eleAssessment) {
				if(assessment.isSelected()!=true) {
					clickNew(assessment);
				}
			}
			reportStep("ACL Permissions for assessment options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickUnSelectSomeAssessmentOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.assessmentMagViewTab");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.assessmentHRViewTab");
		if(ele1.isSelected() == true) {
			click(ele1);
		}
		if(ele2.isSelected() == true) {
			click(ele2);
		}
		return this;
	}
	public AclPage clickAssessmentOptionsNo() {
		try {
			WebElement ele = locateElement("xpath", "AclPage.xpath.assOptNo");
			click(ele);
			if (ele.isSelected()==true) {
				click(ele);	
			}
			/*List<WebElement> eleAssessment = locateElements("xpath", "AclPage.xpath.assessment");
			for (WebElement assessment : eleAssessment) {
				if(assessment.isSelected()==true) {
					clickNew(assessment);
				}
			}*/
			reportStep("ACL Permissions for assessment options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickNotificationOptions() {
		try {
			List<WebElement> eleNotification = locateElements("xpath", "AclPage.xpath.notification");
			for (WebElement notification : eleNotification) {
				if(notification.isSelected()!=true) {
					clickNew(notification);
				}
			}
			reportStep("ACL Permissions for Notification options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickNotificationOptionsNo() {
		try {
			WebElement ele = locateElement("xpath", "AclPage.xpath.notificationsPageOpTnO");
			click(ele);
			if (ele.isSelected()==true) {
				click(ele);
			}
			/*List<WebElement> eleNotification = locateElements("xpath", "AclPage.xpath.notification");
			for (WebElement notification : eleNotification) {
				if(notification.isSelected()==true) {
					clickNew(notification);
				}
			}*/
			reportStep("ACL Permissions for Notification options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickPlanOptions() {
		try {
			List<WebElement> elePlan = locateElements("xpath", "AclPage.xpath.plan");
			for (WebElement plan : elePlan) {
				if(plan.isSelected()!=true) {
					clickNew(plan);
				}
			}
			reportStep("ACL Permissions for Plan options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickUnSelectSomePlanOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.competencyCategoryFilter");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.competencySubcategoryFilter");
		if(ele1.isSelected() == true) {
			click(ele1);
		}
		if(ele2.isSelected() == true) {
			click(ele2);
		}
		return this;
	}
	public AclPage clickPlanOptionsNo() {
		try {
			WebElement ele = locateElement("xpath", "AclPage.xpath.userPlanOpt");
			click(ele);
			if (ele.isSelected()==true) {
				click(ele);
			}
			/*List<WebElement> elePlan = locateElements("xpath", "AclPage.xpath.plan");
			for (WebElement plan : elePlan) {
				if(plan.isSelected()==true) {
					clickNew(plan);
				}
			}*/
			reportStep("ACL Permissions for Plan options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickRecommendationOptions() {
		try {
			List<WebElement> eleRecommendation = locateElements("xpath", "AclPage.xpath.recommendation");
			for (WebElement recommendation : eleRecommendation) {
				if(recommendation.isSelected()!=true) {
					clickNew(recommendation);
				}
			}
			reportStep("ACL Permissions for Recommendation options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickRecommendationOptionsNo() {
		try {
			WebElement ele = locateElement("xpath", "AclPage.xpath.recommentationOPTnO");
			click(ele);
			if (ele.isSelected()==true) {
				click(ele);
			}
			/*List<WebElement> eleRecommendation = locateElements("xpath", "AclPage.xpath.recommendation");
			for (WebElement recommendation : eleRecommendation) {
				if(recommendation.isSelected()==true) {
					clickNew(recommendation);
				}
			}*/
			reportStep("ACL Permissions for Recommendation options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickTrackOptions() {
		try {
			List<WebElement> eleTrack = locateElements("xpath", "AclPage.xpath.track");
			for (WebElement track : eleTrack) {
				if(track.isSelected()!=true) {
					clickNew(track);
				}
			}
			reportStep("ACL Permissions for Track options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	} 
	public AclPage clickTrackOptionsNo() {
		try {
			WebElement ele = locateElement("xpath", "AclPage.xpath.trackNoOptions");
			click(ele);
			if (ele.isSelected()==true) {
				click(ele);
			}
			/*List<WebElement> eleTrack = locateElements("xpath", "AclPage.xpath.track");
			for (WebElement track : eleTrack) {
				if(track.isSelected()==true) {
					clickNew(track);
				}
			}*/
			reportStep("ACL Permissions for Track options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	} 
	public AclPage clickUserMenuOptions() {
		try {
			WebElement eleUserMenu = locateElement("xpath", "AclPage.xpath.userMenu");
			if(eleUserMenu.isSelected()!=true) {
				clickNew(eleUserMenu);
			}
			reportStep("ACL Permissions for User menu options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickUserMenuOptionsNo() {
		try {
			WebElement ele = locateElement("xpath", "AclPage.xpath.userMenuOptionsasNo");
			click(ele);
			if (ele.isSelected()==true) {
				click(ele);
			}
			/*WebElement eleUserMenu = locateElement("xpath", "AclPage.xpath.userMenu");
			if(eleUserMenu.isSelected()==true) {
				clickNew(eleUserMenu);
			}*/
			reportStep("ACL Permissions for User menu options", "PASS");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking in the field :", "FAIL");
		}
		return this;
	}
	public AclPage clickSlectAllUsers() {
		click(locateElement("xpath", "AclPage.xpath.selectAllUsers"));
		return this;
	}
	public AclPage clickAddRemoveRoles() {
		click(locateElement("xpath", "AclPage.xpath.addRemoveRoles"));
		return this;
	}
	public AclPage clickUserRoles() {
		List<WebElement> allElement = locateElements("xpath", "AclPage.xpath.userPermissionNew");
		for (int i = 0; i < allElement.size(); i++) {
			if(allElement.get(i).isSelected()!=true) {
				click(allElement.get(i));  
			} 
		}
		return this;
	}
	public AclPage verifyClickUserRoles() {
		List<WebElement> allElement = locateElements("xpath", "AclPage.xpath.userPermissionNew");
		for (int i = 0; i < allElement.size(); i++) {
			if(allElement.get(i).isSelected()==true) {
				click(allElement.get(i));  
			} else {
				System.out.println("Checkbox is already not checked");
			}
		}
		return this;
	}
	public AclPage clickManagerUsersOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.getManagerViewData");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.getsMastersData");
		WebElement ele3 = locateElement("xpath", "AclPage.xpath.getsCoursesEnrolledGroupedByUsers");
		WebElement ele4 = locateElement("xpath", "AclPage.xpath.getsMasterData");
		WebElement ele5 = locateElement("xpath", "AclPage.xpath.getsMastersDataCount");
		WebElement ele6 = locateElement("xpath", "AclPage.xpath.getsRolesMasterData");
		WebElement ele7 = locateElement("xpath", "AclPage.xpath.getsSkillsMasterData");
		WebElement ele8 = locateElement("xpath", "AclPage.xpath.getsUsersMasterData");
		WebElement ele9 = locateElement("xpath", "AclPage.xpath.getsCompetencyMasterData");
		if(ele1.isSelected() != true) {
			click(ele1); 
		}
		if(ele2.isSelected() != true) {
			click(ele2); 
		}
		if(ele3.isSelected() != true) {
			click(ele3); 
		}
		if(ele4.isSelected() != true) {
			click(ele4); 
		}
		if(ele5.isSelected() != true) {
			click(ele5); 
		}
		if(ele6.isSelected() != true) {
			click(ele6); 
		}
		if(ele7.isSelected() != true) {
			click(ele7); 
		}
		if(ele8.isSelected() != true) {
			click(ele8); 
		}
		if(ele9.isSelected() != true) {
			click(ele9); 
		}
		return this;
	}
	public AclPage clickManagerAssessmentOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.assessmentMagViewTab");
		if(ele1.isSelected() != true) {
			click(ele1); 
		}
		return this;
	}
	public AclPage clickManagerPlanOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.PlanHeader");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.SkillIndex");
		if(ele1.isSelected() != true) {
			click(ele1); 
		}
		if(ele2.isSelected() != true) {
			click(ele2); 
		}
		return this;
	}
	public AclPage clickManagerTrackOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.trackSkillIndex");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.trackManagerViewTab");
		WebElement ele3 = locateElement("xpath", "AclPage.xpath.trackSkillCatgFilter");
		WebElement ele4 = locateElement("xpath", "AclPage.xpath.trackSkillSubcatgFilter");
		WebElement ele5 = locateElement("xpath", "AclPage.xpath.trackHRSkillCatgFilter");
		WebElement ele6 = locateElement("xpath", "AclPage.xpath.trackHRSkillSubcatgFilter");
		
		if(ele1.isSelected() != true) 
			click(ele1); 
		if(ele2.isSelected() != true) 
			click(ele2); 
		if(ele3.isSelected() != true) 
			click(ele3); 
		if(ele4.isSelected() != true) 
			click(ele4); 
		if(ele5.isSelected() != true) 
			click(ele5); 
		if(ele6.isSelected() != true) 
			click(ele6); 
		return this;
	}
	public AclPage clickManagerUserMenuOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.UserMenuReport");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.userMenunoHrTab");
		if(ele1.isSelected() != true) {
			click(ele1); 
		}
		if(ele2.isSelected() == true) {
			click(ele2); 
		}
		return this;
	}
	public AclPage clickHRAssessmentOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.userMenuHRTab");
		if(ele1.isSelected() != true) {
			click(ele1); 
		}
		return this;
	}
	public AclPage clickHRUserMenuOptions() {
		WebElement ele1 = locateElement("xpath", "AclPage.xpath.UserMenuReport");
		WebElement ele2 = locateElement("xpath", "AclPage.xpath.userMenuReportManTab");
		if(ele1.isSelected() != true) {
			click(ele1); 
		}
		if(ele2.isSelected() == true) {
			click(ele2); 
		}
		return this;
	}
}









