package pages.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AdminRecommendationPage extends PreAndPost{

	public  AdminRecommendationPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public AdminRecommendationPage enterHowManyTopPeersAppear(String data) {
		WebElement elePeers = locateElement("xpath", "RecommendationPage.xpath.TopPeers");
		type(elePeers, data);
		return this;
	}
	public AdminRecommendationPage enterBasicThresholdToSort(String data) {
		WebElement eleThreshold = locateElement("xpath", "RecommendationPage.xpath.thresholdToSort");
		type(eleThreshold, data);
		return this;
	}
	public AdminRecommendationPage enterBasicThresholdForMentor(String data) {
		type(locateElement("xpath", "RecommendationPage.xpath.mentorThreshold"), data);
		return this;
	}
	public AdminRecommendationPage clickSkill() {
		WebElement eleSkill = locateElement("xpath", "RecommendationPage.xpath.mentorBasedOnSkill");
		if(eleSkill.isSelected()!=true)
			click(eleSkill);
		return this;
	}
	public AdminRecommendationPage clickCompetency() {
		WebElement eleCompetency = locateElement("xpath", "RecommendationPage.xpath.competency");
		if(eleCompetency.isSelected()!=true)
			click(eleCompetency); 
		return this;
	}
	public AdminRecommendationPage clickCourse() {
		WebElement eleCourse = locateElement("xpath", "RecommendationPage.xpath.course");
		if(eleCourse.isSelected()!=true)
			click(eleCourse);
		return this;
	}
	public AdminRecommendationPage clickSubmitButton() {
		click(locateElement("xpath", "RecommendationPage.xpath.submitButton"));
		return this;
	}
	public AdminRecommendationPage enterCourseOption1(String value) {
		type(locateElement("xpath", "RecommendationPage.xpath.courseOption1"), value);
		return this;
	}
	public AdminRecommendationPage enterCourseOption2(String value) {
		type(locateElement("xpath", "RecommendationPage.xpath.courseOption2"), value);
		return this;
	}
	public AdminRecommendationPage clickAdd() {
		click(locateElement("xpath", "RecommendationPage.xpath.add"));
		return this;
	}
	public AdminRecommendationPage clickCourseOptionSubmit() {
		click(locateElement("xpath", "RecommendationPage.xpath.courseOptionsSubmitButton"));
		return this;
	}
	public AdminRecommendationPage enterCourseOptions() throws InterruptedException {
		typeValue(locateElement("xpath", "RecommendationPage.xpath.courseOpt1"), "Advanced JAVA");
		typeValue(locateElement("xpath", "RecommendationPage.xpath.courseOpt2"), "Advanced JAVA part2");
		typeValue(locateElement("xpath", "RecommendationPage.xpath.courseOpt3"), "J2EE");
		typeValue(locateElement("xpath", "RecommendationPage.xpath.courseOpt4"), "J2EE - 2.0");
		return this;
	}

}



























