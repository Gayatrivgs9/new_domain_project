package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AssignmentPage extends PreAndPost {

	public AssignmentPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
		PageFactory.initElements(driver,this);
	}
	
	public AssignmentPage enterLabel(String data) {		
		type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), data);
		return this;
	}
	public AssignmentPage enterReviewLabel(String data) {		
		type(locateElement("xpath", "FormsPage.xpath.labelAsInput"), data);
		return this;
	}
	public AssignmentPage enterPlaceholder(String data) {
		typeValue(locateElement("xpath", ""), data);
		return this;
	}
	public AssignmentPage enterDescription(String data) throws Exception {
		typeValue(locateElement("xpath", ""), data);	
		return this;
	}
	public AssignmentPage enterErrorLabel(String data) {
		typeValue(locateElement("xpath", ""), data);
		return this;
	}
	public FormsPage clickSaveButton() {
		click(locateElement("xpath", "FormsPage.xpath.saveButton"));  
		return new FormsPage(driver, test);
	}
	public AssignmentPage clickCancel() {
		click(locateElement("xpath", "LogsPage.xpath.filterWithMessage"));
		return this;
	}
	public AssignmentPage clickRemove() {
		click(locateElement("xpath", "LogsPage.xpath.filter"));
		return this;
	}
	
	
	
	
	

}
