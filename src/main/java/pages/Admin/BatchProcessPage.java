package pages.Admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class BatchProcessPage extends PreAndPost{

	public  BatchProcessPage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
			
	public BatchProcessPage enterBatchProcessDomain(String data) {
		type(locateElement("name", "BatchProcessPage.name.domain"), data);
		return this;
	}
	public BatchProcessPage clickRunTransaction() {
		click(locateElement("xpath", "BatchProcessPage.xpath.runTransaction"));
		return this;
	}
	public BatchProcessPage selectSchemaSlection() throws InterruptedException {
		WebElement eleSchema = locateElement("xpath", "BatchProcessPage.xpath.schemaSection");
		click(eleSchema);
		Thread.sleep(1000);  
		click(locateElement("xpath", "BatchProcessPage.xpath.user"));
		click(locateElement("xpath", "BatchProcessPage.xpath.role"));
		click(locateElement("xpath", "BatchProcessPage.xpath.skill"));
		click(locateElement("xpath", "BatchProcessPage.xpath.course"));
		return this;
	}
	public BatchProcessPage clickRunBatch() {
		click(locateElement("xpath", "BatchProcessPage.xpath.runBatch"));
		return this;
	}
	public BatchProcessPage waitForInvisibilityOfSpin() throws InterruptedException {
		waitUntilInvisibilityOfWebElement("BatchProcessPage.xpath.spin1");
		//invisibilityOfWebElement("BatchProcessPage.xpath.spin2");
		return this;
	}
	public BatchProcessPage clickClear() {
		click(locateElement("xpath", "BatchProcessPage.xpath.clear"));
		return this;
	}
	public BatchProcessPage verifyBatchProcessCompletedMsg() throws InterruptedException {
		Thread.sleep(2000); 
		verifyPartialText(locateElement("xpath", "BatchProcessPage.xpath.BatchProcessWaitTime"), "Your batch process completed");
		return this;
	}

	

}



























