package pages.Admin;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class LinkagePage extends PreAndPost{

	public  LinkagePage(RemoteWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public LinkagePage enterSelectedOptions() throws InterruptedException {
		click(locateElement("xpath", "LinkagePage.xpath.selectOption"));
		click(locateElement("xpath", "LinkagePage.xpath.enterValue"));
		return this;
	}
	public LinkagePage clickOnSubmitButton() {
		click(locateElement("xpath", "LinkagePage.xpath.submitButton"));
		return this;  
	}
	public LinkagePage clickOnClearButton() {
		click(locateElement("xpath", "LinkagePage.xpath.clearButton"));
		return this;
	}
	

}


