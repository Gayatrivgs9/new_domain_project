package demo;

import java.util.Scanner;

public class ReverseAnArray {


	static int reverseNum() {
		int[] a = {1,2,3,4,5};
		int i;
		for ( i = a.length-1; i > 0; i--) {
			System.out.print(a[i]+" ");
			//System.out.println(); 
		}
		return a[i]; 
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int i = scan.nextInt();      
		double d = scan.nextDouble(); 
		scan.nextLine();
		String s = scan.nextLine(); 
		scan.close(); 
		System.out.println("String: " + s);
		System.out.println("Double: " + d);
		System.out.println("Int: " + i);
		//System.out.println(reverseNum());  
		//int[] a = new int[6];
		/*int a[] = {1,2,3,4,5};
		for (int i = a.length-1; i >=0; i--) {
			 System.out.println(a[i]);  
		}*/

	}
}
