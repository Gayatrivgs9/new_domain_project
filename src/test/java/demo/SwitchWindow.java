package demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class SwitchWindow {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		/*driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		System.out.println(driver.getSessionId());
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//a[text()='Contact Us']").click();
		System.out.println(driver.getSessionId());
		System.out.println(driver.getTitle());

		Set<String> allWindows = driver.getWindowHandles(); 
		System.out.println(driver.getSessionId());
		List<String> list = new ArrayList<String>();
		list.addAll(allWindows);
		driver.switchTo().window(list.get(1));
		System.out.println("Second window session id "+driver.getSessionId());
		System.out.println("Second window title of the page " +driver.getTitle());
		driver.close(); */

		/*driver.get("https://www.crystalcruises.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println(driver.getSessionId());
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//strong[text()='View']").click();
		System.out.println(driver.getSessionId());
		System.out.println(driver.getTitle());

		Set<String> allWindows = driver.getWindowHandles(); 
		System.out.println(driver.getSessionId());
		List<String> list = new ArrayList<String>();
		list.addAll(allWindows);
		driver.switchTo().window(list.get(1));
		//System.out.println("Second window session id "+driver.getSessionId());
		System.out.println("Second window title of the page " +driver.getTitle());
		System.out.println("Second window session id "+driver.getSessionId());
		driver.close(); */


		driver.get("http://sifynet.sify.net/sifynet_v1/index.php");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println(driver.getSessionId());
		System.out.println(driver.getTitle());
		String parentWinHandle = driver.getWindowHandle();
		System.out.println("Parent window handle: " + parentWinHandle);
		driver.findElementByXPath("//a[text()=' Attendance']").click();  
		Thread.sleep(10000);

		Set<String> allWindows = driver.getWindowHandles(); 
		List<String> list = new ArrayList<String>();
		list.addAll(allWindows);
		driver.switchTo().window(list.get(1));
	
		System.out.println("Second window title of the page " +driver.getTitle());
		System.out.println("Second window session id "+driver.getSessionId());
		System.out.println("Second window URL "+driver.getCurrentUrl());
	//	Thread.sleep(5000); 
		driver.close();


		
		
		
		
	}

}










