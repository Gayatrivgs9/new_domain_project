package demo;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC038_CompletePendingAssessmentAndGetApproval extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		String string = "TC038_CompletePendingAssessmentAndGetApproval";
		testCaseName = string; 
		testCaseStatus = "PASS"; 
		testDescription = "Complete Pending Assessment And Get Approval";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserLogin";
	}

	@Test(dataProvider = "fetchData")	
	public void completePendingAssessmentAndGetApproval(String urlData, String uName, String pwd) throws InterruptedException, IOException {		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible()
		.clickAssessment()
		//.enterCurrentRole() 
		.clickGo() 
		.verifyTotalAssessmentCoursCount();
		//.startPendingAssessments();
		
	}
 }





