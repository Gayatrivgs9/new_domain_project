package demo;

import java.util.Scanner;

public class StaticClass {

	static int a;
	public static void main(String[] args) {


		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		sc.close();
		str = "Gayatri";
		str = "java";
		System.out.println("Hello World!");
		if(str=="Gayatri" || str =="java") {
			System.out.println(str); 
		}
		StaticClass obj1 = new StaticClass();
		StaticClass obj2 = new StaticClass();
		obj1.a=5;
		System.out.println(obj1.a);
		obj2.a=7;
		System.out.println(obj2.a); 
		System.out.println(obj1.a);
		System.out.println(a); 

	}
}
