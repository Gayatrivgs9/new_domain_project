package demo;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	@Test
	public void learnReports() throws IOException {
		System.out.println(System.getProperty("user.dir"));  
		//generate html
		ExtentHtmlReporter html = new ExtentHtmlReporter(System.getProperty("user.dir")+"\\reports\\result.html"); 
		html.setAppendExisting(true); 
		//attach the file 
		ExtentReports extent = new ExtentReports(); 
		extent.attachReporter(html);
	
		ExtentTest test = extent.createTest("TC001_LoginToPAL", "Login into PAL application"); 
		test.assignAuthor("Gayatri");
		test.assignCategory("Smoke"); 
		
		test.pass("Username entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("D:\\WorkSpace\\PAL\\PAL_NewDomain\\images\\131191409.jpg").build()); 
		test.pass("Password entered successfully"); 
		test.fail("Login clicked successfully");
		
		//do flush
		extent.flush(); 
		
	}
}
