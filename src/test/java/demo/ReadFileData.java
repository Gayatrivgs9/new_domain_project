package demo;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import lib.selenium.WebDriverServiceImpl;

public class ReadFileData extends WebDriverServiceImpl{
	static long randomNumber;
	@Test
	public void test() throws IOException, EncryptedDocumentException, InvalidFormatException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("D:\\WorkSpace\\PAL\\PAL_NewDomain\\src\\main\\java\\data\\DataSheet.xlsx");
		XSSFSheet sheet = wbook.getSheet("LivewireData");
		String stringCellValue = sheet.getRow(1).getCell(0).getStringCellValue();
		System.out.println(stringCellValue);
		randomNumber = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		System.out.println(randomNumber); 
		writeData(0, 1, "JAVA COURSE_"+randomNumber, "course", "", "Green");  
		
	}
}
