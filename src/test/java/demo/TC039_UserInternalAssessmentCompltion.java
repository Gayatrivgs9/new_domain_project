package demo;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.SuperAdmin.LoginPage;
import pages.User.UserHomePage;

public class TC039_UserInternalAssessmentCompltion extends PreAndPost{	

	@BeforeTest
	public void setValues() {
		testCaseName = "TC039_UserInternalAssessmentCompltion";
		testDescription = "User internal assessment compltion review with manager verification check";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
		dataSheetName = "DataSheet";
		sheetName = "UserViewTrack";
	}

	@Test(dataProvider = "fetchData")	
	public void userInternalAssessmentCompltion(String urlData, String uName, String pwd) throws Exception {		
		
		new LoginPage(driver,test)		
		.startUserLogin(urlData)
		.enterUserName(uName)
		.enterPassword(pwd)
		.clickLoginButton();
		new UserHomePage(driver, test)
		.waitUpToFoldingCubeInvisible() 
		.clickAssessment()
		//.enterCurrentRole()
		.clickGo() 
		.verifyTotalAssessmentCoursCount()
		
		.getPendingAssessmentsCount()
		.getCompletedAssessmentsCount()
		.getSuggestionsCourseCount() 
		.assessmentsStatus();

		
	}
}





















