package demo;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateTestNG {

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Befor Suite");
	}
	@BeforeTest
	public void beforeTest() {
		System.out.println("Befor Test");
	}
	@BeforeClass
	public void beforeClass1() {
		System.out.println("Befor Class -- 1");
	}
	@BeforeClass
	public void beforeClass2() {
		System.out.println("Befor Class -- 2");
	}
	@BeforeMethod
	public void beforeMethod1() {
		System.out.println("Befor Method -- 1");
	}
	@Test
	public void test1() {
		System.out.println("Test -- 1");
	}
	@Test
	public void test2() {
		System.out.println("Test -- 2");
	}
	
	@AfterMethod
	public void afterMethod1() {
		System.out.println("After Method -- 1");
	}
	@AfterClass
	public void afterClass1() {
		System.out.println("After Class -- 1");
	}
	@AfterClass
	public void afterClass2() {
		System.out.println("After Class -- 2");
	}
	@AfterTest
	public void afterTest() {
		System.out.println("After Test");
	}
	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite");
	}
	
	
	
	
	
	
	
	
}
