package excelCode;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class LoginITestSify extends ReadData{

	public RemoteWebDriver driver ;
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("I am Before Suite");
	}
	@BeforeTest
	public void beforeTest() {
		System.out.println("I am Before Test");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("I am Before Class");
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://regqc.sifyitest.com/improvementemp/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	@DataProvider(name = "fetchData")
	public Object[][] getData() throws IOException {
		Object[][] data = ReadData.readData("DataSheet", "LoginData");  
		return data; 
	}
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("I am Before Method");
		/*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://regqc.sifyitest.com/improvementemp/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);*/
	}
	
	@Test(dataProvider = "fetchData")
	public void loginITestSify(String username, String password, String testStatus) throws IOException {
		driver.findElementById("txtregno").sendKeys(username);
		driver.findElementById("txtpass").sendKeys(password);
		driver.findElementById("Submit").click();
		WebElement element = driver.findElementByLinkText("Logout");
		if(element.getText().equals("Logout")) {
			writeData(2, 1, "PASS", "LoginData", "DataSheet"); 
		} else {
			writeData(2, 1, "FAIL", "LoginData", "DataSheet"); 
		}
	}
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("I am After Method");
		//driver.close();
	}
	@AfterClass
	public void afterClass() {
		System.out.println("I am After Class");
		driver.close();
	}
	@AfterTest
	public void afterTest() {
		System.out.println("I am After Test");
	}
	@AfterSuite
	public void afterSuite() {
		System.out.println("I am After Suite");
	}
	
	
	
	
	
	
	/*public static void loginSifyITest() throws IOException {
		for (int i = 1; i <=2; i++) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://regqc.sifyitest.com/improvementemp/");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementById("txtregno").sendKeys(readData(i, 0, "LoginData","DataSheet"));
			driver.findElementById("txtpass").sendKeys(readData(i, 1, "LoginData","DataSheet"));
			driver.findElementById("Submit").click();
			WebElement element = driver.findElementByLinkText("Logout");
			if(element.getText().equals("Logout")) {
				writeData(2, i, "PASS", "LoginData", "DataSheet"); 
			} else {
				writeData(2, i, "FAIL", "LoginData", "DataSheet");
			}
			driver.close(); 
		}
	}
	public static void main(String[] args) throws IOException {
		loginSifyITest(); 
	}*/

}
