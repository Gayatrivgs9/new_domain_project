package excelCode;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginSifyITest extends ReadData{

	public static void loginSifyITest() throws IOException {
		for (int i = 1; i <=2; i++) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://regqc.sifyitest.com/improvementemp/");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementById("txtregno").sendKeys(readData(i, 0, "LoginData","DataSheet"));
			driver.findElementById("txtpass").sendKeys(readData(i, 1, "LoginData","DataSheet"));
			driver.findElementById("Submit").click();
			WebElement element = driver.findElementByLinkText("Logout");
			if(element.getText().equals("Logout")) {
				writeData(2, i, "PASS", "LoginData", "DataSheet"); 
			} else {
				writeData(2, i, "FAIL", "LoginData", "DataSheet");
			}
			driver.close(); 
		}
	}
	public static void main(String[] args) throws IOException {
		loginSifyITest(); 
	}

}
