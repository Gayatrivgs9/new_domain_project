package excelCode;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestClass {

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("I am Before Suite");
	}
	@BeforeTest
	public void beforeTest() {
		System.out.println("I am Before Test");
		//throw new RuntimeException();
	}
	@BeforeTest(dependsOnMethods="beforeTest")
	public void beforeTest2() {
		System.out.println("I am Before Test2");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("I am Before Class");
	}
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("I am Before Method");
	}
	@Test(groups = "smoke", timeOut = 3000)
	public void test1() {
		System.out.println("I am Test1");
	}
	@Test(dependsOnMethods = {"test1"}, dependsOnGroups = {"smoke"}, alwaysRun = true)
	public void test2() {
		System.out.println("I am Test2");
		throw new RuntimeException();
	}
	@Test(dependsOnMethods = {"test1","test2"})
	public void test3() {
		System.out.println("I am Test3");
	}
	@AfterMethod
	public void afterMethod() {
		System.out.println("I am After Method");
	}
	@AfterClass
	public void afterClass() {
		System.out.println("I am After Class");
	}
	@AfterTest
	public void afterTest() {
		System.out.println("I am After Test");
	}
	@AfterSuite
	public void afterSuite() {
		System.out.println("I am After Suite");
	}
}








