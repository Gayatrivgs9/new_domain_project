package excelCode;

import java.io.File;
import java.io.IOException;

public class CreateFile {


	public static void main(String[] args) {

		File file = new File(System.getProperty("user.dir")+"\\textFile\\demoFile.txt");
		System.out.println(file);
		//Create the file
		try { 
			if (file.createNewFile()) 
			{
				System.out.println("File is created!");
			} else {
				System.out.println("File already exists.");
			}
		} catch (IOException e) {
			e.printStackTrace(); 
		}    
	}
}
