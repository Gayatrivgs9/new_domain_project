package excelCode;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadData {

	//Basic read and write data into excel file
	public static Object[][] readData(String excelFileName, String sheetName) throws IOException {
		String fileName = System.getProperty("user.dir")+"\\data\\"+excelFileName+".xlsx";
		Workbook wbook = null;
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wbook = new XSSFWorkbook(fileInputStream);  
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wbook);
		} else if (fileExtensionName.equals(".xls")) {
			wbook = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wbook);
		}
		Sheet sheet = wbook.getSheet(sheetName); 
		int rowCount = sheet.getLastRowNum();
		int columnCount = sheet.getRow(0).getLastCellNum();

		System.out.println("Row Count "+rowCount+" and Column Count "+columnCount); 
        Object[][] data = new Object[rowCount][columnCount];
		
        for (int i = 1; i <=rowCount; i++) {
			Row row = sheet.getRow(i); 
			for (int j = 0; j < columnCount; j++) {
				Cell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
				data[i-1][j] = stringCellValue;
			}
		}
		wbook.close(); 
		return data;
	}

	public static void writeData(String[] dataToWrite) throws IOException {

		String fileName = System.getProperty("user.dir")+"\\data\\DataSheet.xlsx";
		Workbook wbook = null;
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wbook = new XSSFWorkbook(fileInputStream);  
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wbook);
		} else if (fileExtensionName.equals(".xls")) {
			wbook = new HSSFWorkbook(fileInputStream); 
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wbook);
		}
		FileInputStream fis = new FileInputStream(fileName);
		XSSFWorkbook wBook = new XSSFWorkbook(fis); 
		XSSFSheet sheet = wBook.getSheet("WriteUserData");

		int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
		int columnCount = sheet.getRow(0).getLastCellNum();
		Row newRow = sheet.createRow(rowCount+1);

		for(int j = 0; j < columnCount; j++){
			Cell cell = newRow.createCell(j);
			cell.setCellValue(dataToWrite[j]);
		}
		fis.close();
		FileOutputStream fos = new FileOutputStream(fileName);
		wBook.write(fos);
		fos.close();
		wBook.close();
	}

	//Advanced read and write data into excel file
	public static String readData(int rows, int column, String sheetname, String excelFileName) throws IOException {
		String data = null;
		Workbook wb = null;

		String fileName = System.getProperty("user.dir")+"\\data\\"+excelFileName+".xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb = new XSSFWorkbook(fileInputStream);  
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		} else if (fileExtensionName.equals(".xls")) {
			wb = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		}
		Sheet sheet = wb.getSheet(sheetname);
		Row row = sheet.getRow(rows);
		Cell cell = row.getCell(column);
		data = cell.getStringCellValue();
		return data;
	}

	public static void writeData(int coloum, int rows, String data, String sheetname,String excelFileName) throws IOException {
		Workbook wbook = null;
		Sheet sheet;
		Row row;
		Cell cell;
		String fileName = System.getProperty("user.dir")+"\\data\\"+excelFileName+".xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wbook = new XSSFWorkbook(fileInputStream);  
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wbook);
		} else if (fileExtensionName.equals(".xls")) {
			wbook = new HSSFWorkbook(fileInputStream); 
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wbook);
		}

		sheet = wbook.getSheet(sheetname);
		row = sheet.getRow(rows);
		cell = row.getCell(coloum);
		cell.setCellValue(data);
		FileOutputStream fos = new FileOutputStream(fileName);
		wbook.write(fos);  
		fos.close();
	}


	public static void main(String[] args) throws IOException {
		//readData(); 
		String[] writeValue = {"1","GopalaKrishnan","Manager"}; 
		writeData(writeValue);
	}

}














