Feature: Login into all the PAL modules 

Scenario: Login Super-Admin 
	Given Load super-admin url 
	And Enter superadmin username 
	And Enter superadmin password 
	Then Click login button 
	
	
Scenario: Login Admin 
	Given Load admin url
	And Enter admin username 
	And Enter admin password 
	Then Click login button 
	
	
Scenario: Login User 
	Given Load user url 
	And Enter user username 
	And Enter user password 
	Then Click login button 