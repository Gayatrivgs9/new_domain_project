package simpleprograms;

public class LearnConstructors 
    {

	int eid;
	String ename;
	double esal;
	public static void main(String[] args)
	{
		LearnConstructors lc=new LearnConstructors(501,"subbu",501.252);
		lc.disp();
	}

	void disp()
	{
		System.out.println("eid is "+ eid);
		System.out.println("ename is "+ ename);
		System.out.println("esal is "+ esal);
	}
	
	LearnConstructors(int eid,String ename,double esal)
	{
		this.eid=eid;
		this.ename=ename;
		this.esal=esal;
	}
}
