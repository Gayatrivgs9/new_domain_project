package simpleprograms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FindDuplicatesInArrayWay2 {

	public static void main(String[] args) 

	{
		int arr[]= {2,3,5,6,7,6,2};
		HashMap<Integer, Integer> arrmap=new HashMap<>();
		for(int i=0;i<arr.length;i++)
		{
			if (arrmap.containsKey(arr[i]))
			{
				arrmap.put(arr[i],arrmap.get(arr[i])+1);
			}
			else
			{
				arrmap.put(arr[i],1);	
			}
		}
		Set<Map.Entry<Integer, Integer>> entry=arrmap.entrySet();
		for(Map.Entry<Integer, Integer>  ent: entry)
		{
			if(ent.getValue()>1)
			{
				System.out.println(ent.getKey());
			}

		}

	}

}
