package simpleprograms;

public class LearnSuperKeywordChild  extends LearnSuperKeywordParent
{
	 public LearnSuperKeywordChild ()
	 {
	     
		 System.out.println("this is my m1 from child class ");
	 }
	
	 public LearnSuperKeywordChild (int a)
	 {
	     super(5);
		 System.out.println("this is my child class parameterized constructor ");
	 }
	
}
