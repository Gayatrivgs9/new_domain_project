package simpleprograms;

public class LearnControlStatements {

	public static void main(String[] args)
	
	{
	    int a=200;
	    
	    if(a>100)
	    {
	    	System.out.println("a is greater than 100");
	    }
	    else
	    {
	    	System.out.println("a is not greater than 100");
	    }
				
	}

}
