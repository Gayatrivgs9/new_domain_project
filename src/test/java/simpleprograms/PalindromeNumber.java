package simpleprograms;

import java.util.Scanner;

public class PalindromeNumber {

	public static void main(String[] args)

	{
		int n,r,sum=0;
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number");
		n=sc.nextInt();
		int  t=n;
	    
		while(n>0)
		{
			r=n%10;
			n=n/10;
			sum=sum*10+r;
		}
		if(t==sum)
		{
			System.out.println("palindrome");
		}
		else
		{
			System.out.println("not a palindrome");
		}

	}

}
