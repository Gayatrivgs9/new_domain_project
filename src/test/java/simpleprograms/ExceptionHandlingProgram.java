package simpleprograms;

public class ExceptionHandlingProgram {

	public static void main(String[] args)

	{
		int a = 20, b = 0, c = 0;

		try {
			c = a / b;
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println(c);
		try {
			c = a / b;

		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println(c);
		c = a + b;
		System.out.println(c);
	}

}
