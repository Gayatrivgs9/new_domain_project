package simpleprograms;

import java.util.HashMap;
import java.util.Map;

public class FindNumberOfOccurancesOfCharacterInString {
	static int value = 0;

	public static void main(String[] args)

	
	{
		String ss = "SUBRAHMANYAMPOLAGANI";
		Map<Character, Integer> map = new HashMap<>();

		for (Character ch : ss.toCharArray()) {
			if (map.containsKey(ch)) {
				int value = map.get(ch);
				map.put(ch, value + 1);
			} else {
				map.put(ch, 1);
			}
		}
		System.out.println(map);
	}
}
