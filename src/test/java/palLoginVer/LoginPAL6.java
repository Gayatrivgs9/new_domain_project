package palLoginVer;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excelCode.ReadData;

public class LoginPAL6 extends ReadData{

	static int i = 1;
	@Test(dataProvider = "fetchData")
	public static void loginPAL1(String uname, String pwd, String status) throws IOException, InterruptedException {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://sify.sifyadaptivelearning.com/user/local-login");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementById("email").sendKeys(uname);
			driver.findElementById("password").sendKeys(pwd);
			driver.findElementByXPath("//button[text()='Sign In']").click();
			if(driver.findElementsByXPath("//p[text()='Email does Not exists']").isEmpty() != true) {
				writeData(2, i++, "PASS", "PALLOGIN6", "PAL6"); 
			} else {
				writeData(2, i++, "FAIL", "PALLOGIN6", "PAL6");
			}
			driver.close(); 
	}
	@DataProvider(name = "fetchData")
	public Object[][] getData() throws IOException {
		Object[][] data = ReadData.readData("PAL6", "PALLOGIN6");  
		return data; 
	}
}















