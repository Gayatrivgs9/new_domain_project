package drv;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Flipkart {

	public static WebDriver driver;

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//button[text()='✕']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Search for products, brands and more']")).sendKeys("Asus");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		HashMap<String, Integer> map=new HashMap<>();
		List<WebElement> mobilePrices = driver.findElements(By.xpath("//div[@class='_1vC4OE _2rQ-NK']"));
		for (int i = 0; i < mobilePrices.size(); i++) {
			String expectedPrice = mobilePrices.get(i).getText().replace("₹", "").replace(",", "");
			int expectedPriceInt = Integer.parseInt(expectedPrice);
			if(expectedPriceInt >= 14999) {
				List<WebElement> mobileNames = driver.findElements(By.xpath("//div[@class='_3wU53n']"));
				map.put(mobileNames.get(i).getText(), expectedPriceInt);
			}
		}
		for (Entry<String, Integer> each : map.entrySet()) {
			System.out.println("mobile Name :"+each.getKey()+"  "+"mobile Price "+each.getValue());
		}


	}
}