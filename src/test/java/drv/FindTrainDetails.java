package drv;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindTrainDetails {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.makemytrip.com/railways/chennai-coimbatore-train-tickets.html?cmp=SEM|D|Rail|G|DSA|Rail_DSA_Desktop|New|ETA|Regular|326693927934&s_kwcid=AL!1631!3!326693927934!b!!g!!&ef_id=XHDrzQAAALM06kow:20200220083832:s");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement element = driver.findElementByXPath("(//li[@class='trainList'])[1]");
		String trainDetails = element.getText();
		System.out.println(trainDetails); 
		
		List<WebElement> allElments = driver.findElementsByClassName("trainNameNum"); 
		for (int i = 0; i < allElments.size(); i++) {
			if(allElments.get(i).getText().contains("22")) {
				int trainNum = Integer.parseInt(driver.findElementsByClassName("trainNumber").get(i).getText().replaceAll("\\D", ""));
			    System.out.println("Train Number: "+trainNum);
			    String trainName = driver.findElementsByClassName("trainName").get(i).getText();
			    System.out.println("Train Name: "+trainName);
			}
		}
		
		
		
		
		
		//driver.close();
		
		/*driver.get("https://newdomain01.sifylivewire.com/admin/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("admin01@newdomain08.com",Keys.TAB,"123456",Keys.TAB);*/
	}
}
