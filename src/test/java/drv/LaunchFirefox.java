package drv;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LaunchFirefox {

	public static void main(String[] args) {
		RemoteWebDriver driver = new FirefoxDriver(); 
        driver.get("https://www.google.com/");
        System.out.println(driver.getCurrentUrl()); 
	}

}
