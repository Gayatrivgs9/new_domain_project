package var;

public class SumOfDigits {

	public static void main(String[] args) {
		
		int num = 35965; int sum =0; int rem;
		String txt = Integer.toString(num);
		for(int i =0; i<=txt.length(); i++) {
			rem = num%10;
			sum = sum+rem;			
			num = num/10;			
		}
		System.out.println("Sum of digits: "+sum);
	}
}
