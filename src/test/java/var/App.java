package var;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {

	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", ".\\src\\main\\java\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.momondo.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("AiHd-origin-airport-display-multi-container").click();
		driver.findElementById("AiHd-origin-airport-display-multi-container").sendKeys("Chennai", Keys.TAB);
		driver.findElementById("AiHd-destination-input-wrapper").click();
		driver.findElementById("AiHd-destination-input-wrapper").sendKeys("Canada", Keys.TAB);
		driver.findElementById("AiHd-submit").click();
	}
}
