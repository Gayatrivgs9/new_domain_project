package var;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class PrintMatrix {

	public static void main(String[] args) throws IOException {

		//Print Matrix

		int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}


		System.out.println("*****************************");


		/*BufferedReader BR=new BufferedReader(new InputStreamReader (System.in));
		int Number[][]=new int[3][3];
		int i,j;
		String m;
		System.out.println("Enter Elements for Matrix 3x3 :");
		for(i=0;i<=2;i++)
		{
			for(j=0;j<=2;j++)
			{
				m=BR.readLine();
				Number[i][j]=Integer.parseInt(m);
			}
		}
		System.out.println("Elements in Matrix are : ");
		System.out.println("");
		for(i=0;i<=2;i++)
		{
			for(j=0;j<=2;j++)
			{
				System.out.print(Number[i][j]+"\t");
			}
			System.out.println();
		}*/

		System.out.println("*****************************");


		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int n = sc.nextInt();
		int num[][] = new int[4][4];
		int i,j=0;
		for (i = 0; i < num.length; i++) {
			for (j = 0; j < num.length; j++) {
				num[i][j] = n++;
			}
		}
		System.out.println("3*3 Matrix elements are:");
		for (i = 0; i < num.length; i++) {
			for (j = 0; j < num.length; j++) {
				System.out.print(num[i][j]+"\t");
			}
			System.out.println(); 
		}

	}
}







