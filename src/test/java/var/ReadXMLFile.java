package var;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class ReadXMLFile {
	
	
//	static String key ;
	public static void readPropFileWithMap() throws IOException {
		
		File file = new File("D:\\WorkSpace\\PAL\\PAL_NewDomain\\CallFile.properties");
		FileInputStream stm = new FileInputStream(file);
		
		Properties prop = new Properties();
		prop.load(stm);
		
		HashMap<String, String> mymap= new HashMap<String, String>((Map) prop);
		for (Entry<String, String> each : mymap.entrySet()) {
			System.out.println(each.getKey()+"  =   "+each.getValue()); 
		}
	}

	public static void readXMLFileWithMap() throws FileNotFoundException, IOException {
		Properties pop = new Properties();
		pop.load(new FileInputStream(new File("D:\\WorkSpace\\PAL\\PAL_NewDomain\\Login.xml"))); 
		
		HashMap<String, String> mymap= new HashMap<String, String>((Map) pop);
		for (Entry<String, String> each : mymap.entrySet()) {
			System.out.println(each.getKey()+"  =   "+each.getValue()); 
		}
	} 
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		//readPropFileWithMap(); 
		readXMLFileWithMap();
		
	}
}























