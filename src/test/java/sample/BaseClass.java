package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import lib.selenium.WebDriverServiceImpl;

public class BaseClass extends WebDriverServiceImpl {


	@Test
	public void test() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver_2.46.exe");
		ChromeOptions op = new ChromeOptions();
		op.setExperimentalOption("w3c", true); 
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://google.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("testuser@sifycorp.com");
		driver.findElementById("password").sendKeys("pal123");
		driver.findElementByXPath("//button[text()='Sign In']").click();
		driver.findElementByXPath("//span[text()='Assessment']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[text()='Filter with Roles']/following::input").click();
		//driver.findElementByXPath("//span[text()='Aspiring Role - Practice Lead - Instructional Design']").click();
		WebElement reg = driver.findElementByXPath("//button[@ngbtooltip ='download']");
		int y = reg.getLocation().getY();
		System.out.println("Location of y :"+ y); 
		//driver.executeScript("scroll(0,"+y+")");
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("scroll(0,"+y+");");
		System.out.println("y value :"+y);
		//reportStep("element scrolled", "PASS");
		/*Actions builder = new Actions(driver);
		WebElement source = driver.findElementById("builder-textfield");
		WebElement target = driver.findElementByXPath("//div[contains(@id, 'builder-element')]");
		builder.dragAndDrop(source, target).perform();
		System.out.println("success");*/
		/*new Select(driver.findElementByXPath("//label[text()='Schema: ']/following::select")).selectByValue("User");
		Thread.sleep(10000);*/
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.elementToBeSelected(driver.findElementByXPath("//label[text()='Schema: ']/following::select[2]")));
		/*new Select(driver.findElementByXPath("//label[text()='Schema: ']/following::select[2]")).selectByVisibleText(" Ramco ");
		driver.findElementById("attribute").sendKeys("username");
		new Select(driver.findElementByXPath("//label[text()='Schema: ']/following::select[3]")).selectByValue("String");*/
		/*driver.findElementById("validation").sendKeys("YES");
		driver.findElementById("critical").click();
		driver.findElementById("primary-key").click();
		driver.findElementByXPath("//button[text()='Add']").click();
		Thread.sleep(2000);
		List<WebElement> columns = driver.findElementsByXPath("//table/tbody/tr/td");
		List<String> att = new ArrayList<>();
		for (int i =0; i <5; i++) {
			String trim = columns.get(i).getText().trim();
			att.add(trim);
		}

		System.out.println(att);
		List<String> lst = new ArrayList<>();
		lst.add("username");
		lst.add("String");
		lst.add("YES");
		lst.add("true");
		lst.add("true");

		System.out.println(lst); 

		if (lst.equals(att)) {
			System.out.println("list success");
		} else {
			System.out.println("list not success");
		}*/
	}

}



















