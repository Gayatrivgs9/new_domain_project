package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ZoomCar {

	@Test
	public void login() throws InterruptedException{

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
		//entering data
		driver.findElementByLinkText("Start your wonderful journey").click();
		String areaName = driver.findElementByXPath("//div[text() = 'Chennai']").getText();
		System.out.println("Pick up area name :"+areaName);
		driver.findElementByXPath("//input[@placeholder = 'Tell us your Starting point in Chennai']").click();
		driver.findElementByXPath("//div[text() = 'Locate me']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//button[text() = 'Next']").click();
		WebElement selectedDate = driver.findElementByXPath("//div[@class = 'day full']");
		String selected = selectedDate.getText().replaceAll("\\D", "");
		selectedDate.click();
		driver.findElementByClassName("proceed").click();
		WebElement tomorrowDate = driver.findElementByXPath("//div[@class = 'day picked full']");
		String tomorrow = tomorrowDate.getText().replaceAll("\\D", "");
		if(tomorrow.equals(selected)){
			tomorrowDate.click();
		}else{
			System.out.println("select correct date");
		}
		driver.findElementByXPath("//button[text() = 'Done']").click();
		Thread.sleep(3000);
		//select car
		driver.findElementByClassName("item active-input");




	}
}
