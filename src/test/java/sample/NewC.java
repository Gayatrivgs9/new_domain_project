package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class NewC {

	@Test
	public void test() throws InterruptedException {

		/*int i = 8;
		if(i%2 == 0) {
			System.out.println("even");
		}else {
			System.out.println("odd");
		}*/

		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://newdomain07.sifylivewire.com/admin/login");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementById("email").sendKeys("admin01@newdomain07.com");
			driver.findElementById("password").sendKeys("123456");
			driver.findElementByXPath("//button[text()='Sign In']").click();
			Thread.sleep(2000);
			driver.findElementByXPath("//span[text()='Baselining']").click();
			Thread.sleep(2000);
			new Select(driver.findElementByClassName("form-control")).selectByVisibleText(" Programmer "); 
			new Select(driver.findElementByXPath("(//select[@class='form-control'])[2]")).selectByVisibleText(" livewire ");
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage()); 
			System.err.println("Option is not available");
			System.out.println(e.getMessage()); 
		} catch (WebDriverException e) {
			System.err.println("Webdriver exception occured");
		}
	}
}




















