package sample;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class NumOfChar {

	
	public static void main(String[] args) {
		String txt = "Gayatri";
		Map<Character, Integer> map = new TreeMap<>();
		char[] ch = txt.toCharArray();
		for (char c : ch) {
			if(map.containsKey(c)) {
				map.put(c, map.get(c)+1); 
			} else {
				map.put(c, 1);
			}
		}
		for (Entry<Character, Integer> eachValue : map.entrySet()) {
			System.out.println(eachValue.getKey()+" --> "+eachValue.getValue()); 
		}
	}
}
