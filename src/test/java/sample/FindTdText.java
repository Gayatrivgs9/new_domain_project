package sample;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class FindTdText {

	@Test
	public void test() throws InterruptedException {

		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://erail.in/");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementById("txtStationFrom").clear();
			driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
			driver.findElementById("txtStationTo").clear();
			driver.findElementById("txtStationTo").sendKeys("BZA", Keys.TAB);
			Thread.sleep(2000);
			driver.findElementById("chkSelectDateOnly").click();
			Thread.sleep(3000);
			List<String> trainNameList = new LinkedList<String>(); 
			List<WebElement> trainNames = driver.findElementsByXPath("//table[contains(@class, 'DataTableHeader')]//tr//td/a[text()='Train Name']/following::table[@class='DataTable TrainList TrainListHeader']//tr/td[2]");
			for (int i = 0; i < trainNames.size(); i++) {
				String text = trainNames.get(i).getText();
				trainNameList.add(text);   
			}
			if(trainNameList.contains("RAJDHANI EXP")) { 
				String trainNum = driver.findElementByXPath("//table[contains(@class, 'DataTableHeader')]//tr//td/a[text()='Train Name']/following::table[@class='DataTable TrainList TrainListHeader']//tr/td/a[text()='RAJDHANI EXP']/preceding::td[1]").getText();
				System.out.println("Train number: "+trainNum); 
				String trainFrom = driver.findElementByXPath("//table[contains(@class, 'DataTableHeader')]//tr//td/a[text()='Train Name']/following::table[@class='DataTable TrainList TrainListHeader']//tr/td/a[text()='RAJDHANI EXP']/following::td[1]").getText();
				System.out.println("Train From: "+trainFrom); 
			}
			System.out.println(trainNameList); 
			
			
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage()); 
			System.err.println("Option is not available");
			System.out.println(e.getMessage()); 
		} catch (WebDriverException e) {
			System.err.println("Webdriver exception occured");
		}
	}
}




















