package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class RunClass {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver_2.46.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demopal.sifylivewire.com/super-admin/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("superadmin@test.com");
		driver.findElementById("password").sendKeys("123456");
		driver.findElementByXPath("//button[text()='Sign In']").click();
		driver.findElementByXPath("//label[text()='Select Domain*']/following::input").sendKeys("abcde");
		driver.findElementByXPath("//label[text()='Select Table*']/following::input").sendKeys("skill");
		driver.findElementByXPath("//label[text()='Select Sources*']/following::input").sendKeys("livewire");
		driver.findElementByXPath("//h3[text()='Map System for domains ']/following::button[text()='Register']").click();
		System.out.println("success");

	}

}
