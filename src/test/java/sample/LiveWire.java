package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class LiveWire {

	@Test
	public void test() throws InterruptedException {
		boolean checked = false;
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver_2.46.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://lwawstest1.sifylivewire.com/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByXPath("//a[text()=' Login']").click();  
		driver.findElementById("email").sendKeys("superadmin@mail.com");
		driver.findElementById("password").sendKeys("password");
		driver.findElementByXPath("//button[text()=' Login']").click();
		Thread.sleep(3000); 
		WebElement ele1 = driver.findElementByXPath("//a[text()='Administration ']");
		WebElement ele2 = driver.findElementByXPath("//a[contains(text(),'Site Configuration')]");
		Actions builder = new Actions(driver);
		builder.moveToElement(ele1).pause(1000).click(ele2).perform();
		Thread.sleep(1000); 
		driver.findElementByXPath("//a[contains(text(),'Page Settings')]").click();
		driver.findElementByXPath("//a[contains(text(),'Course Page')]").click(); 
		Thread.sleep(2000);
		JavascriptExecutor je = (JavascriptExecutor) driver;
		WebElement element = driver.findElementByXPath("//label[text()='Enrolled courses in Course page']/following::label");
		je.executeScript("arguments[0].click();", element);
		 WebElement checkbox = driver.findElementByXPath("//label[text()='Enrolled courses in Course page']/following::label");
		    if (checked != checkbox.isSelected())
		    {
		    	System.out.println("verifid true");
		        //checkbox.click();
		    } else {
		    	System.out.println("not verified");
		    }
	
	}
}





























