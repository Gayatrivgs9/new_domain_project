package sample;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class FileDownloadAndSave {

	//public static String rootPath;
	//public static String downloadPath = rootPath+"/src/main/java/downloadFiles";
	//public static String  fileDownloadPath;
	
	@Test  
	public void testDownload() throws Exception {
		System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
		String fileDownloadPath = "D:\\WorkSpace\\PAL\\PAL_NewDomain\\src\\main\\java\\downloadFiles\\"; 

		Map<String, Object> prefsMap = new HashMap<String, Object>();
		prefsMap.put("profile.default_content_settings.popups", 0);
		prefsMap.put("download.default_directory", fileDownloadPath);
		
		ChromeOptions option = new ChromeOptions();
		option.setExperimentalOption("prefs", prefsMap);
		option.addArguments("--test-type");
		option.addArguments("--disable-extensions");
		
		ChromeDriver driver = new ChromeDriver(option);
		driver.manage().window().maximize();
		driver.get("https://newdomain08.sifylivewire.com/user/login");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("gayatri.kolusu@sifycorp.com");
		driver.findElementById("password").sendKeys("pal123");
		driver.findElementByXPath("//button[text()='Sign In']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//span[text()='Reports']").click();
		Thread.sleep(2000);
		driver.findElementByLinkText("HR view").click();
		Thread.sleep(2000);
		WebElement element = driver.findElementByXPath("//span[@class='ng-arrow-wrapper']");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		List<WebElement> allEle1 = driver.findElementsByXPath("//div[@role='option']");
		for (int i = 0; i < allEle1.size(); i++) {
			allEle1.get(i).click();
		}
		Thread.sleep(2000);
		WebElement element2 = driver.findElementByXPath("(//span[@class='ng-arrow-wrapper'])[2]");
		JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		executor2.executeScript("arguments[0].click();", element2);
		List<WebElement> allEle2 = driver.findElementsByXPath("//div[@role='option']");
		for (int i = 0; i < allEle2.size(); i++) {
			allEle2.get(i).click();
		}
		driver.findElementByXPath("//button[text()='Go!']").click();
		driver.findElementByXPath("//button[text()='Select All']").click();
		
		
		driver.findElementByXPath("//button[text()='Export as csv']").click();
		System.out.println("File downloaded successfully"); 
	}
	/*public void downloadFile() {
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>(); 
		chromePrefs.put("profile.default_content_settings.popups", 2);
		chromePrefs.put("download.default_directory", downloadPath);
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>(); 
		options.setExperimentalOption("prefs", chromePrefs);
		options.addArguments("--test-type");
		options.addArguments("--disable-extensions");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	}*/
}

















