package learnTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Login07_New {

	RemoteWebDriver driver;	
	@Test
	@Parameters({"browser","url","uname","pwd"})
	public void login07_new(String browser, String url, String uname, String pwd) {
		if(browser.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
			driver = new ChromeDriver(); 
		}else {
			System.setProperty("webdriver.gecko.driver", "./src/main/java/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys(uname);
		driver.findElementById("password").sendKeys(pwd);
		driver.findElementByXPath("//button[text()='Sign In']").click();
	}
}
