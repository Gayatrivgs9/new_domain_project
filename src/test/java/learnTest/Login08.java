package learnTest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import lib.selenium.WebDriverServiceImpl;

public class Login08 extends WebDriverServiceImpl{

	@BeforeTest
	public void setValues() {
		testCaseName = "Login08";
		testDescription = "Manager View Track Functionality";
		testCaseStatus = "PASS";
		nodes = "User";
		authors = "Gayatri";
		category = "Smoke";
	}

	@Parameters({"browser","url","uname","pwd"})
	@Test
	public void login07(String browser, String url, String uname, String pwd) {
		startApp(browser, url);
		type(locateElement("LoginPage.id.username"), uname);
		type(locateElement("LoginPage.id.password"), pwd);
	}
}
