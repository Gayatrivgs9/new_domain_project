package week1;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class InsertString {

	public static void main(String[] args) {
		/*//M1:
		StringBuffer buffer = new StringBuffer("abcde");
		buffer.insert(2, '*');
		System.out.println(buffer); 
		//M2:
		String text = "abcde";
		String replaceAll = text.replaceAll("abcde", "ab*cde");
		System.out.println(replaceAll);*/

		String s ="Technologies"; 
		char[] ch = s.toCharArray();
		Map<Character, Integer> map = new LinkedHashMap<>();
		for (int i = 0; i < ch.length; i++) {
			if(map.containsKey(ch[i])) {
				map.put(ch[i], map.get(ch[i])+1);
			} else {
				map.put(ch[i], 1);
			}
		}
		System.out.println(map);
		System.out.println(); 
		for (Entry<Character, Integer> c : map.entrySet()) {
			 if(c.getValue()==2) {
				 System.out.println(c.getKey()); 
			 }
		}
		
		

	}

}













