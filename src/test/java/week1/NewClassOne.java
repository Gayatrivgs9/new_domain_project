package week1;

import org.openqa.selenium.chrome.ChromeDriver;

public class NewClassOne implements InterfaceOne{

	@Override
	public void doSum() {
		System.out.println("do sum");
		
	}

	@Override
	public void doSub() {
		System.out.println("do sub");
		
	}
	
	public void doMul() {
		System.out.println("do mul");
	}
	
	public void doDev() {
		System.out.println("do dev");
	}
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/main/java/drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://newdomain07.sifylivewire.com/user/login");
	}

}
