package week1;

import java.util.Arrays;

public class PrintArrayMaxMin {

	public static void main(String[] args) {

		int arr[] = {123, 45, 678, 56, 345, 90, 13, 2233};
		int temp =0; 
		// Printing of descending order
		for (int i = 0; i < arr.length; i++) {
			for (int j = i+1; j < arr.length; j++) {

				if(arr[i] < arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		// Printing of ascending order
		for (int i = 0; i < arr.length; i++) {
			for (int j = i+1; j < arr.length; j++) {

				if(arr[i] > arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
			System.out.print(arr[i]+" ");
		}
		System.out.println();

		// Printing of max,min order

		for (int i = 0; i < arr.length; i++) {
			for (int j = i+1; j < arr.length; j++) {
				if(i%2 == 0) {
					if(arr[i] < arr[j]) {
						temp = arr[i];
						arr[i] = arr[j];
						arr[j] = temp;
					}
				} else if(i%2 != 0) {
					if(arr[i] > arr[j]) {
						temp = arr[i];
						arr[i] = arr[j];
						arr[j] = temp;
					}
				}
			}
			System.out.print(arr[i]+" ");
		}

		// Printing of max,min order
		int[] array1 = {5, 10, 4, 3, 1, 8, 9, 6, 7, 2};	

		Arrays.sort(array1);

		int count = 1;
		int i=array1.length-1;
		int j=0;
		do
		{	

			System.out.print(array1[i]);
			System.out.print(", ");
			System.out.print(array1[j]);
			System.out.print(", ");
			i=i-1;
			j=j+1;
			count++;

		}while(count<=(array1.length)/2);


	}


}












