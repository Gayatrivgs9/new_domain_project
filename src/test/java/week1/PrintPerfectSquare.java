package week1;

import java.util.Scanner;

public class PrintPerfectSquare {

	public static void main(String[] args) {

		System.out.println("Enter a number");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int sum =0, count =0;
		for (int i = 0; i < num/2; i++) {
			sum = i*i;
			if(sum == num) {
				count++;
			} 
		}
		if(count == 1) {
			System.out.println("it's a sqre");
		} else {
			System.out.println("not sqare");
		} 
		System.out.println("Sum value : "+sum);
	}
}
