package week1;

public class FindSeries {

	public static void main(String[] args) {
		
		int num = 7;
		
		int a = 1,b = 0,c = 0;
		/* 
		 * 1+0 = 1
		 * 1+1+2
		 * 2+2=4
		 * 4+3=7 */
		for (int i = 0; i < num; i++) {
			c = a+b;
			a=c;
			b++;
			System.out.print(c+" ");
		}

	}

}
