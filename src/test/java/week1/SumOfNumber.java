package week1;

import java.util.Scanner;

public class SumOfNumber {

	public static void main(String args[])
    {
       /* int m, n, sum = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the number:");
        m = s.nextInt();
        while(m > 0)
        {
            n = m % 10;
            sum = sum + n;
            m = m / 10;
        }
        System.out.println("Sum of Digits:"+sum);*/
		
        sumOfNum();
    }
	
	public static void sumOfNum() { 
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int sum = 0, rem =0;
		System.out.println("Enter new number");
		
		for (int i = 0; i < num; i++) {
			rem = num % 10;
			sum = sum + rem;
			num = num / 10;
		}
		System.out.println("Sum of numbers: "+sum); 
	}
}
