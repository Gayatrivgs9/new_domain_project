package week1;

import java.util.Scanner;

public class PrintInReverseOrder {

	public static void main(String[] args) {
		
		System.out.println("Enter integer");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		
		System.out.println("Enter decimal number");
		//Scanner dec = new Scanner(System.in);
		double doubleNum = sc.nextDouble();
		
		System.out.println("Enter String");
		//Scanner text = new Scanner(System.in);
		sc.nextLine();
		String nextLine = sc.nextLine(); 
		
		System.out.println(nextLine);
		System.out.println(doubleNum);
		System.out.println(num);
		sc.close();
		
	}
}
