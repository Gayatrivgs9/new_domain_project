package week1;

public class PrintDiagonal {

	public static void main(String[] args) throws InterruptedException {

		/*ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(ops);
		driver.manage().window().maximize();
		driver.get("https://www.redbus.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000);
		WebElement source = driver.findElement(By.id("src"));
		source.sendKeys("chennai");
		Thread.sleep(2000);
		source.sendKeys(Keys.ENTER); 
		driver.findElement(By.id("dest")).sendKeys("Sathyamangalam",Keys.ENTER);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//label[@for='onward_cal']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//table)[2]//td[@class='current day']")).click();
		driver.findElement(By.id("search_btn")).click();*/
        
		//print char in between string 
		/*String str = "abcde";
		
		StringBuffer buffer = new StringBuffer(str);
		buffer.insert(3, '*');
		System.out.println(buffer);*/
		
		
		
		
		// print diamond 
		int n =5, m = 4;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) 
				System.out.print(" ");
			
			for (int j = 0; j <= i; j++) 
				System.out.print("* ");
			
			System.out.println();
			m--;
		}
		
		m = 0;
		for (int i = n; i >0; i--) {
			for (int j = 0; j < m; j++) 
				System.out.print(" ");
			
			for (int j = 0; j < i; j++)
				System.out.print("* ");
			
			System.out.println();
			m++; 
		}
		

		System.out.println("****************************");

		
		//  print cross pattern with char
		  
		  String data = "Gayatr";
		int size = data.length();

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if(i==j || (i+j == size-1)) {
					System.out.print(data.charAt(j));
				}else {
					System.out.print(" "); 
				}
			}
			System.out.println(); 
		}

		System.out.println("****************************");
		/*
		 * Print the below pattern 
		 *   
	    #  *000*000*
	    #  0*00*00*0
	    #  00*0*0*00
	    #  000***000	*/
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 9; j++) {
				if(i==j || (i+j == 8) || j==4) {
					System.out.print("*");
				}else {
					System.out.print("0");
				}
			}
			System.out.println();
			//System.out.println("*");
		}

		System.out.println("****************************");
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if(i==0 || j==0 || i ==3 || j==3) {
					System.out.print("* ");		
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}



}
