package pra;

public class LearnStaticNFinal {
	
	//final int k =10;
	static int i =10;
	int j = 10;
	public void getData() {
		int k = 10;
		i++;
		j++;
		k++;
		System.out.println("Static value: "+i+" Con value: "+j+" Inside value: "+k);
	}
	
	public static void main(String[] args) {
		LearnStaticNFinal stc1 = new LearnStaticNFinal();
		LearnStaticNFinal stc2 = new LearnStaticNFinal();
		LearnStaticNFinal stc3 = new LearnStaticNFinal();
		
		stc1.getData();
		stc2.getData();
		stc3.getData();
	}
}
