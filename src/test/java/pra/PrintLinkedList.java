package pra;

import java.util.LinkedList;

public class PrintLinkedList {

	
	public static void main(String[] args) {
		
		LinkedList<Integer> link = new LinkedList<>(); 
		link.add(7);
		link.add(9);
		link.add(5); 
		link.add(6);
		link.add(1);
		link.add(4); 
		
		link.push(3);
		
		System.out.println(link); 
	}
}
