package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import lib.selenium.WebDriverServiceImpl;

public class LoginClass extends WebDriverServiceImpl{

	@Given("Load super-admin url")
	public void launchSuperAdminURL() {
		startApp("chrome", "newdomain08.sa.url");
	}
	@Given("Load admin url")
	public void launchAdminURL() {
		startApp("chrome", "newdomain08.admin.url");
	}
	@Given("Load user url")
	public void launchUserURL() {
		startApp("chrome", "newdomain08.user.url1");
	}
	
	@And("Enter superadmin username")
	public void enterSuperAdminUsername() {
		type(locateElement("id", "LoginPage.id.username"), "superadmin4@pal.com");
	}
	@And("Enter superadmin password")
	public void enterSuperAdminPassword() {
		type(locateElement("id", "LoginPage.id.password"), "superadmin");
	}
	@And("Enter admin username")
	public void enterAdminUsername() {
		type(locateElement("id", "LoginPage.id.username"), "admin01@newdomain08.com");
	}
	@And("Enter admin password")
	public void enterAdminPassword() {
		type(locateElement("id", "LoginPage.id.password1"), "123456");
	}
	@And("Enter user username")
	public void enterUserUsername() {
		type(locateElement("id", "LoginPage.id.username"), "eee1@sifycorp.com");
	}
	@And("Enter user password")
	public void enterUserPassword() {
		type(locateElement("id", "LoginPage.id.password"), "123456");
	}
	@Then("Click login button")
	public void clickLogin() {
		click(locateElement("xpath", "LoginPage.xpath.signin"));
	}
}















